<?php

Route::get('/', 'DashboardAdminController@index')->name('admin.dashboard');
Route::get('/profile', 'AdminController@profile')->name('admin.profile');
Route::put('updatePassword', 'AdminController@updatePassword')->name('admins.updatePassword');
Route::put('updatePhoto', 'AdminController@updatePhoto')->name('admins.updatePhoto');

Route::resource('analytic', 'GoogleAnalyticsController')->only(['index', 'store']);

Route::resource('admins', 'AdminController');
Route::group(['prefix'=>'admins'], function()
{
    Route::put('{id}/trash', 'AdminController@trash')->name('admins.trash');
    Route::put('{id}/restore', 'AdminController@restore')->name('admins.restore');
});

Route::resource('certificate', 'CertificateController', ['except' => ['show','destroy']]);


Route::resource('page', 'PageController', ['except' => ['show','destroy']]);
Route::group(['prefix'=>'page'], function()
{
    Route::put('{id}/updateSingle', 'PageController@updateSingle')->name('page.updateSingle');

});


Route::resource('tenders', 'TenderController', ['except' => ['show', 'destroy']]);
Route::group(['prefix'=>'tenders'], function()
{
    Route::put('{id}/updateSingle', 'TenderController@updateSingle')->name('tenders.updateSingle');
});
Route::resource('tenderTranslation', 'TenderTranslationController')->only(['store', 'update', 'destroy'])->names(['destroy' => 'tenders.destroy']);

Route::resource('vacancies', 'VacancyController', ['except' => ['show', 'destroy']]);
Route::group(['prefix'=>'vacancies'], function()
{
    Route::put('{id}/updateSingle', 'VacancyController@updateSingle')->name('vacancies.updateSingle');
});
Route::resource('vacancyTranslation', 'VacancyTranslationController')->only(['store', 'update', 'destroy'])->names(['destroy' => 'vacancies.destroy']);


Route::resource('pageTranslation', 'PageTranslationController')->only(['store', 'update', 'destroy'])->names(['destroy' => 'page.destroy']);
Route::group(['prefix'=>'pageTranslation'], function()
{
    Route::get('order', 'PageTranslationController@order')->name('pageTranslation.order');
    Route::post('postOrder', 'PageTranslationController@postOrder')->name('post.pageTranslation.order');
    Route::put('{id}/trash', 'PageTranslationController@trash')->name('page.trash');
    Route::put('{id}/restore', 'PageTranslationController@restore')->name('page.restore');
});


Route::resource('projects', 'ProjectController')->except(['show', 'destroy']);

Route::resource('products', 'ProductController')->except(['show', 'destroy']);
Route::group(['prefix'=>'products'], function()
{
    Route::put('{id}/updateSingle', 'ProductController@updateSingle')->name('products.updateSingle');
});

Route::resource('productTranslation', 'ProductTranslationController')->only(['store', 'update', 'destroy'])->names(['destroy' => 'products.destroy']);
Route::group(['prefix'=>'products'], function()
{
    Route::put('{id}/trash', 'ProductTranslationController@trash')->name('products.trash');
    Route::put('{id}/restore', 'ProductTranslationController@restore')->name('products.restore');
});

Route::resource('enterprises', 'EnterpriseController')->except(['show', 'destroy']);
Route::group(['prefix'=>'enterprises'], function()
{
    Route::put('{id}/trash', 'EnterpriseTranslationController@trash')->name('enterprises.trash');
    Route::put('{id}/restore', 'EnterpriseTranslationController@restore')->name('enterprises.restore');
    Route::put('{id}/updateSingle', 'EnterpriseController@updateSingle')->name('enterprises.updateSingle');
});
Route::resource('enterpriseTranslation', 'EnterpriseTranslationController')->only(['store', 'update', 'destroy'])->names(['destroy' => 'enterprises.destroy']);
Route::group(['prefix'=>'enterpriseTranslation'], function()
{
    Route::get('order', 'EnterpriseTranslationController@order')->name('enterpriseTranslation.order');
    Route::post('postOrder', 'EnterpriseTranslationController@postOrder')->name('post.enterpriseTranslation.order');
});



Route::resource('articles', 'ArticleController')->except(['show', 'destroy']);
Route::group(['prefix'=>'articles'], function()
{
    Route::put('{id}/updateSingle', 'ArticleController@updateSingle')->name('article.updateSingle');
});

Route::resource('articleTranslation', 'ArticleTranslationController')->only(['store', 'update', 'destroy'])->names(['destroy' => 'articles.destroy']);
Route::group(['prefix'=>'articles'], function()
{
    Route::put('{id}/trash', 'ArticleTranslationController@trash')->name('articles.trash');
    Route::put('{id}/restore', 'ArticleTranslationController@restore')->name('articles.restore');
});

Route::resource('faq', 'FaqController')->except(['show']);

Route::resource('chartTitle', 'ChartTitleController')->except(['show']);
Route::get('chartTitle/order', 'ChartTitleController@order')->name('chartTitle.order');
Route::post('chartTitle/postOrder', 'ChartTitleController@postOrder')->name('post.chartTitle.order');


Route::resource('chartTitleLineX', 'ChartTitleLineXController')->except(['show']);

Route::resource('mahlic_chart', 'MahlicChartController')->except(['show']);

Route::resource('chartStatistic', 'ChartStatisticController')->except(['show']);

Route::resource('branch_maps', 'BranchMapController')->except(['show']);

Route::resource('productBlocks', 'ProductBlockController')->except(['show']);

Route::resource('enterpriseBlocks', 'EnterpriseBlockController')->except(['show']);

Route::resource('region', 'BranchRegionController')->except(['show']);
Route::resource('network', 'BranchNetworkController')->except(['show']);
Route::resource('branch', 'BranchController')->except(['show']);


Route::resource('kivLogo', 'KivLogoController')->except(['show']);
Route::resource('kivVideo', 'KivVideoController')->except(['show']);

Route::resource('slider', 'SliderController', ['except' => ['show']]);
Route::group(['prefix'=>'slider'], function()
{
    Route::put('{id}/trash', 'SliderController@trash')->name('slider.trash');
    Route::put('{id}/restore', 'SliderController@restore')->name('slider.restore');
});

//partners routes
Route::resource('partners', 'PartnerController');
Route::resource('{model}/{id}/gallery','GalleryController')->only(['index', 'store']);
Route::delete('gallery/{id}', 'GalleryController@destroy')->name('gallery.destroy');


Route::resource('subscribers', 'SubscriberController')->except(['edit']);
Route::resource('config', 'ConfigController', ['only' => ['index', 'edit', 'update']]);
Route::resource('dictionary', 'DictionaryController',['only' => ['index', 'edit', 'update', 'create', 'store']]);
Route::resource('city', 'CityController',['only' => ['index', 'edit', 'update', 'create', 'store']]);
Route::resource('sitemap', 'SitemapController')->only(['index', 'store']);


