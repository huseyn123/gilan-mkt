<?php

return [
    'status' => ['Gizli', 'Aktiv'],
    'application_status' => ['Təsdiq et','Təsdiqlənib'],
    'alert' => ['danger', 'success'],
    "slider-type" => [1 => "Slider", "Kompaniya"],
    "article-status" => ["Qaralama", "Dərc olunub"],
    "building_status" => ["Köhnə tikili", "Yeni tikili"],
    "room_count" => [1=>1,2,3,4,5],
    "label" => ["danger", "success"],
    "menu-target" => [1 => "Self", 0 => "Blank"],

    "menu-visibility" => [
        0 => "Hidden",
        1 => "Header and Footer",
        2 => "Header",
        3 => "Footer"
    ],


    "menu-visibility-boolean" => [0 => "Gizli", "Aktiv"],
    "menu-visibility-label" => [0 => "danger", "success", "warning", "warning", "info"],
    "role" => [1 => "Administrator", "Editor"],
    "blog_page" => [0 => "No", "Yes"],

    "social-network" => ['facebook','instagram','youtube'],
    "social-network-class" => ['facebook' =>'facebook','instagram' => 'instagram','youtube' => 'youtube-play'],



    "subscriber-label" => ["0" => "warning", "1" => "success"],
    "subscriber-status" => ["0" => "pending", "1" => "active"],
    "yes-no" => [0 => "minus", "plus"],
    'prefix-number' => [50 => 50, 51 => 51, 55 => 55, 70 => 70, 77 => 77],
    'slug_replacement' => ['az' => 'azerbaijani', 'en' => 'azerbaijani', 'ru' => 'russian'],
    'lang' => ['az' => 'AZ', 'en' => 'EN', 'ru' => 'RU'],



    "model" => ['articles' => 'App\Models\Article','page' => 'App\Models\Page', 'pageTranslation' => 'App\Models\PageTranslation','products' => 'App\Models\Product', 'productTranslation' => 'App\Models\ProductTranslation', 'articleTranslation' => 'App\Models\ArticleTranslation', 'tenderTranslation' => 'App\Models\TenderTranslation','enterprises' => 'App\Models\Enterprise', 'enterpriseTranslation' => 'App\Models\EnterpriseTranslation','chartTitle' =>'App\Models\ChartTitle'],

    "template" => [
        1 =>'Dropdown',
        2 =>'Blok',
        3 =>'Xəbərlər səhifəsi',
        4 =>'Əlaqə',
//      5 Certificate
        6 =>'Şirkət Haqqında',
        7 =>'İstehsalat Məhsulları',
        8 =>'İnnovasiyalar',
        9 =>'İrad və təkliflər',
        10 =>'İdxal-ixrac',
        11 =>'Tenderlər Səhifəsi',
        12 =>'Müəssisələr Səhifəsi',
        13 =>'Şirkət Rəhbərinin müraciəti',
        14 =>'Missiya və dəyərlərimiz',
        16 =>'Topdan satış',
        17 =>'Filiallar səhifəsi',
        18 =>'Faq',
        19 =>'Sertifikatlar',
        20 =>'Kiv üçün',
        21 =>'Məhsullar Səhifəsi',
        22 =>'Texnikalar səhifəsi',
        23 =>'Texnikalar Kateqoriyasi',
        24 =>'Texnikalar',
    ],


    'enterprise_template' =>[
        1 => 'Şirvan Yağ-Piy Zavodu',
        2 => 'Şirvan Yağ-Piy Filialı',
        3 => 'Gəncə İplik Fabriki',
        4 => 'Bakı İplik Fabriki',
    ],

    'product_template' =>[
        1 => 'JMIX(Pambıq çiyidinin çecəsi)',
        2 => 'Çiyid',
        3 => 'Dənli bitkilər',
        4 => 'Mahlıc',
        5 => 'İplik'
    ],

    'chart_template' =>[
        1 => 'JMIX(Pambıq çiyidinin çecəsi)',
        2 => 'Çiyid',
        3 => 'Dənli bitkilər',
        10 => 'İdxal-ixrac',
    ],

    "mahlic_chart" =>[
        1 => 'Uzunluq',
        2 => 'Rəng (HVİ)',
        3 => 'Ştapel',
        4 => 'Micronaire',
        5 => 'Mahlıcın növü'
    ],

    'product_cower_size' =>[
        1 =>['width' => 715,'height'=>557],
        2 =>['width' => 210,'height'=>183],
        3 =>['width' => 700,'height'=>308],
        4 =>['width' => 710,'height'=>552],
        5 =>['width' => 700,'height'=>467],
    ],

    'product_thumb_cower_size' =>[
        1 =>['width' => 315,'height'=>246],
        2 =>['width' => 110,'height'=>83],
        3 =>['width' => 455,'height'=>143],
        4 =>['width' => 310,'height'=>241],
        5 =>['width' => 300,'height'=>200],
    ],

    'filter-type' => ['Bütün', 'Aktiv', 'Silinən'],
    'custom-apartment-type' => ['Bütün', 'Aktiv', 'Silinən'],

    'kiv_types' => [
        'images' =>'Şirkətdən görüntülər',
        'logos' =>'Şirkətin loqosu',
        'videos' =>'Reklam çarxları'
    ],

    'kiv_type_database' => [
        'logos' =>1,
        'videos' =>2
    ],

    'kiv_class' => [
        'images' => 'company_image',
        'logos' => 'company_logo',
        'videos' => 'adv_video_page',
    ],

    'gallery-side' => [1 => 'left', 'right', 'center'],
    'gallery_size' =>[
        20 =>['width' => '263','height'=>'145'],
    ],

     'page_size'  =>[
         2 =>['width' => '720','height'=>'430'],
         5 =>['width' => '296','height'=>'420'],
         13 =>['width' => '562','height'=>'634'],
         22 =>['width' => '460','height'=>'330'],
         24 =>['width' => '290','height'=>'210']
     ],
    'thumb_page_size'  =>[
        2 =>['width' => '200','height'=>'120'],
        5 =>['width' => '296','height'=>'420'],
        13 =>['width' => '162','height'=>'234'],
        22 =>['width' => '260','height'=>'130'],
        24 =>['width' => '290','height'=>'210']
    ],

    'page_size2'  =>[
        7 =>['width' => '270','height'=>'230'],
        8 =>['width' => '716','height'=>'402'],
    ],
    'thumb_page_size2'  =>[
        7 =>['width' => '270','height'=>'230'],
        8 =>['width' => '316','height'=>'178'],
    ],

    'crud'  =>[
        'articles-list' =>['width' => '720','height'=>'525'],
    ],


    "branch_location" => [
        1 => 'Samux',
        2 => 'Şəmkir',
        3 => 'Tovuz',
        4 => 'Ağstafa',
        5 => 'Qazax',
        6 => 'Gədəbəy',
        7 => 'Daşkəsən',
        8 => 'Ağdam',
        9 => 'Bakı',
        10 => 'Xırdalan',
        11 => 'Qobustan',
        12 => 'Şamaxı',
        13 => 'İsmayıllı',
        14 => 'Hacıqabul',
        15 => 'Səlyan',
        16 => 'Neftçala',
        17 => 'Sabirabad',
        18 => 'Quba',
        19 => 'Qusar',
        20 => 'Xızı',
        21 => 'Biləsuvar',
        22 => 'İmişli',
        23 => 'Saatlı',
        24 => 'Kürdəmir',
        25 => 'Qəbələ',
        26 => 'Şəki',
        27 => 'Qax',
        28 => 'Zaqatala',
        29 => 'Balakən',
        30 => 'Göyçay',
        31 => 'Zərdab',
        32 => 'Beyləqan',
        33 => 'Füzuli',
        34 => 'Şuşa',
        35 => 'Xocavənd',
        36 => 'Qubadlı',
        37 => 'Zəngilan',
        38 => 'Ordubad',
        39 => 'Babək',
        40 => 'Culfa',
        41 => 'Şahbuz',
        42 => 'Şərur',
        43 => 'Sədərək',
        44 => 'Naxçıvan',
        45 => 'Cəbrayıl',
        46 => 'Xankəndi',
        47 => 'Laçın',
        48 => 'Ağcabədi',
        49 => 'Bərdə',
        50 => 'Tərtər',
        51 => 'Goranboy',
        52 => 'Ucar',
        53 => 'Oğuz',
        54 => 'Ağsu',
        55 => 'Cəlilabad',
        56 => 'Masallı',
        57 => 'Lerik',
        58 => 'Astara',
        59 => 'Yardımlı',
        60 => 'Lənkəran',
        61 => 'Siyəzən',
        62 => 'Şabran',
        63 => 'Xaçmaz',
        64 => 'Sumqayıt',
        65 => 'Göygöl',
        66 => 'Yevlax',
        67 => 'Ağdaş',
        68 => 'Mingəçevir',
        69 => 'Gəncə',
        70 => 'Şirvan',
        71 => 'Kəlbəcər',
    ]
    

];
