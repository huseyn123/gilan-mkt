<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchMapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_maps', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('location')->unsigned();
            $table->text('region_az');
            $table->text('region_en')->nullable();
            $table->text('region_ru')->nullable();
            $table->text('branch_title_az');
            $table->text('branch_title_en')->nullable();
            $table->text('branch_title_ru')->nullable();
            $table->integer('farmers_count');
            $table->string('sown_area',40);
            $table->date('published_at')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_maps');
    }
}
