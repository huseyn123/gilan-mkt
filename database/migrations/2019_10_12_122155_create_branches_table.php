<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->text('phone')->nullable();
            $table->unsignedInteger('region_id');
            $table->unsignedInteger('network_id');
            $table->text('location_az');
            $table->text('location_en')->nullable();
            $table->text('location_ru')->nullable();
            $table->timestamps();

            $table->foreign('region_id')->references('id')->on('branch_elements')->onDelete('cascade');
            $table->foreign('network_id')->references('id')->on('branch_elements')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
