<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterpriseTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('enterprise_id');
            $table->unsignedInteger('page_id')->nullable();
            $table->unsignedInteger('order')->default(1);
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('summary', 1000)->nullable();
            $table->text('content')->nullable();
            $table->text('table')->nullable();
            $table->string('lang', 2);
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['enterprise_id', 'lang']);

            $table->foreign('enterprise_id')->references('id')->on('enterprises')->onDelete('cascade');
            $table->foreign('page_id')->references('id')->on('page_translations')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_translations');
    }
}
