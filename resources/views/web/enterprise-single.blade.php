@extends ('layouts.web', ['page_heading' => $enterprise->name,'other_page'=>$enterprise])

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs',['data' => $enterprise])
                    <!-- BreadCrumbs End -->

                    <div class="row pt10">
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <!-- SideMenu Begin -->
                            <nav class="sidemenu">
                                <ul class="list-unstyled">
                                    @foreach($enterprise_menu as $navbar)
                                        <li @if($navbar->tid == $enterprise->tid) class="active" @endif><a href="{{route('showPage',[$navbar->page_slug,$navbar->slug])}}" title="{{$navbar->name}}">{{$navbar->name}}</a></li>
                                    @endforeach
                                </ul>
                            </nav>
                            <!-- SideMenu End -->
                            <!-- PDFDownload -->

                            @if($enterprise->lang_media)
                                <a href="{{$enterprise->lang_media->getFullUrl()}}" download class="pdf_download mt30">{{$dictionary['enterprise_catalog_download']}}</a>
                            @endif
                        </div>

                        <div class="col-md-9 col-sm-8 col-xs-12">

                            @include('web.enterprise.enterprise'.$enterprise->template_id)

                            @if($enterprise->getMedia('gallery')->count() > 0)
                                <!-- NewsGallery Begin -->
                                    <div id="news_gallery" class="owl-carousel @if($enterprise->template_id == 3) mt0 @endif">

                                    @if($enterprise->youtube_link && $enterprise->getFirstMedia('youtube_cover'))

                                        <figure>
                                            <a href="{{$enterprise->youtube_link}}" class="zoom-video">
                                                <i class="icon-video"></i>
                                                <img src="{{asset($enterprise->getFirstMedia('youtube_cover')->getUrl('blade'))}}"  alt="{{$enterprise->name}}" title="{{$enterprise->image_title}}">
                                            </a>
                                        </figure>

                                    @endif

                                    @foreach($enterprise->getMedia('gallery') as $gallery)
                                        <figure>
                                            <a href="{{asset($gallery->getUrl('blade'))}}" class="zoom-photo">
                                                <img src="{{asset($gallery->getUrl('thumb'))}}"  alt="{{$enterprise->name}}" title="{{$enterprise->image_title}}">
                                            </a>
                                        </figure>
                                    @endforeach

                                </div>
                                <!-- NewsGallery End -->
                            @endif

                            @if($enterprise->location)
                                <div class="contact_wrapper">
                                    <div class="row">
                                        <div class="col-md-7 pr0 contact_eq">
                                            <div class="map">
                                                <div id="map"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-5 pl0 contact_eq">
                                            <!-- Form Begin -->
                                            @include('web.form.enterprise')
                                        </div>
                                    </div>
                                </div>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->


@endsection

@push('scripts')
    <script>
        $('.orange_content_block ul').addClass('list-unstyled list');
        $('.enterprise_about_block_blue ul').addClass('list-unstyled list');
        $('.table_style tr td ul').addClass('list-unstyled list');
        $('.table_style tr td ul.list-unstyled').parent('td').addClass('vt');
        $('.ent_only ul.list-unstyled li').addClass('only');
        $('.et4 ul').addClass('enterprise_about_boxs list-unstyled');
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?language=az&amp;key={{$config['google_api_key']}}" type="text/javascript"></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', init);
        function init() {
            var mapOptions = {
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
                scaleControl: false,
                zoomControl: false,
                center: new google.maps.LatLng({{$enterprise->location}}),
                styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"color":"#c8c8c8"},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"on"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#f7f7f7"},{"visibility":"on"}]}]
            };

            var mapElement = document.getElementById('map');
            var map = new google.maps.Map(mapElement, mapOptions);

            var marker = new google.maps.Marker({
                map: map,
                icon:'images/icon/map-marker.svg'
            });
        }
    </script>
@endpush