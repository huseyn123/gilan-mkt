@extends ('layouts.web', ['page_heading' => $article->name,'other_page'=>$article])

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs',['data' => $article])
                    <!-- BreadCrumbs End -->

                    <div class="row pt15">
                        <div class="col-md-9 col-md-push-3 col-xs-12">
                            <!-- NewsDetail Begin -->
                            <div class="news_detail">
                                <h1 class="title">{{ $article->seo_title ? $article->seo_title : $article->name }}</h1>
                                <span class="date">{{ blogDate($article->published_at) }}</span>
                                <div class="text">
                                    {!! $article->content !!}
                                </div>
                            </div>
                            <!-- NewsDetail End -->
                            <!-- NewsGallery Begin -->
                            <div id="news_gallery" class="owl-carousel xs-mb-20">

                                @foreach($article->getMedia('gallery') as $gallery)

                                    <figure>
                                        <a href="{{asset($gallery->getUrl('blade'))}}" class="zoom-photo">
                                            <img src="{{asset($gallery->getUrl('thumb'))}}" alt="{{$article->name}}" title="{{$article->image_title}}" >
                                        </a>
                                    </figure>

                                @endforeach


                            </div>
                            <!-- NewsGallery End -->
                        </div>
                        @if($similarArticles->count())
                            <div class="col-md-3 col-md-pull-9 col-xs-12">
                                <!-- OtherNews Begin -->
                                <div class="other_news">
                                    @include('web.elements.similarArticle')
                                </div>
                                <!-- OtherNews End -->
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->

@endsection

