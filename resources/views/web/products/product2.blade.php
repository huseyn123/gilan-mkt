<div class="row pt10">
    <div class="col-xs-12">
        <!-- GreyContentBlock Begin -->
        <div class="grey_content_block mb30">
            <div class="row">

                <div class="col-md-3 col-sm-4 col-xs-12">
                    @if($product->getFirstMedia('cover'))
                        @if(substr($product->getFirstMedia('cover')->getFullUrl(), -3) == 'svg')
                            <figure>
                                <img  src="{{asset($product->getFirstMedia('cover')->getFullUrl())}}"  alt="{{$product->name}}" title="{{$product->image_title}}"/>
                            </figure>
                        @else
                            <figure>
                                <img  src="{{asset($product->getFirstMedia('cover')->getUrl('blade'))}}"  alt="{{$product->name}}" title="{{$product->image_title}}"/>
                            </figure>
                        @endif
                    @endif
                </div>

                <div class="col-md-9 col-sm-8 col-xs-12">
                    <div class="title">
                        <img src="{{asset('images/delete/icon-ciyid.svg')}}" >
                        <h1>{{$product->seo_title ? $product->seo_title:  $product->name}}</h1>
                    </div>
                    <div class="text">
                        {!! $product->content !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- GreyContentBlock End -->
    </div>
    <div class="col-sm-6 col-xs-12">
        <!-- Table Begin -->
        <div class="table_style xs-mb-20 ptb20 l1-4">
            <table>
                <thead>
                <tr>
                    <td colspan="2" class="text-center">{!! $dictionary['product2_technical_parameters'] !!}</td>
                </tr>
                </thead>
                <tbody>

                    @foreach($blocks as $block)
                        <tr>
                            <td @if($loop->first) class="text-center" @endif>{{$block->title}}</td>
                            <td @if($loop->first) class="text-center" @endif>{{$block->summary}}</td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
        <!-- Table End -->
    </div>
    <div class="col-sm-6 col-xs-12">
        <!-- Form Begin -->
        <div class="form pt30">
            @include('web.form.products')
        </div>
        <!-- Form End -->
    </div>
</div>
<div class="row mt30">
    <div class="col-sm-6 col-xs-12  xs-mb-20">
        <div class="chart_wrapper">
            <h2 class="title_s">{{$dictionary['product2_chart_title']}}</h2>
            <div id='table1'> </div>
        </div>
    </div>
    <div class="col-sm-6 col-xs-12">
        <!-- HeadTitle Begin -->
        <div class="head_title small mb20">
            <h2 class="ns">{{$dictionary['other_products']}}</h2>
        </div>
        <!-- HeadTitle End -->
        <!-- OtherProducts Begin -->
        <div class="other_products">
            <div class="row">

                @foreach($similarProducts as $sm)
                    <div class="col-xs-6 col-mob-12">
                        <div class="box clr_green" @if($sm->color_code) style="background:{{$sm->color_code}} !important;" @endif>
                            <a href="{{route('showPage',[$sm->page_slug,$sm->slug])}}" title="{{$sm->name}}">
                                @if($sm->getFirstMedia())
                                    <figure>
                                        @if(substr($sm->getFirstMedia()->getFullUrl(), -3) == 'svg')
                                            <img src="{{asset($sm->getFirstMedia()->getFullUrl())}}" alt="{{$sm->name}}">
                                        @else
                                            <img src="{{asset($sm->getFirstMedia()->getUrl('thumb'))}}" alt="{{$sm->name}}">
                                        @endif
                                    </figure>
                                @endif
                                <h2 @if($sm->text_color_code) style="color:{{$sm->text_color_code}} !important;" @endif>{{$sm->name}}</h2>
                            </a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
        <!-- OtherProducts End -->
    </div>
</div>

@push('scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script>
        google.charts.load('current',{'packages':['corechart']});
        google.charts.setOnLoadCallback(drawStuff);
        chart = [];
        chart_title = ['years'];

        @foreach($chart_titles as $chart_title)
            chart_title.push('{{$chart_title->title}}');
        @endforeach

        chart_title.push({ role: 'annotation' });

        chart.push(chart_title);

        @foreach($years as $year)
            new_array = [];
            new_array.push('{{$year->year}}');
            @foreach($chart_titles as $chart_title)
                    val =0;
                    @if($year->statistics->count())
                        @foreach($year->statistics as $statistic)
                            @if($statistic->year && $statistic->value && $statistic->category_id == $chart_title->id)
                                val = {{$statistic->value}};
                            @endif
                        @endforeach
                    @endif

                    new_array.push(val);

            @endforeach

             new_array.push('');
            chart.push(new_array);
        @endforeach


        function drawStuff(){
            var data = google.visualization.arrayToDataTable(chart);

            var options = {
                height: 415,
                chartArea: {
                    height: "85%",
                    top: 30,
                    bottom: 80,
                    left: 55,
                    right: 0,
                    backgroundColor: "#F6F6F6",
                },
                legend: { position: 'bottom', maxLines: 2, textStyle: {color: '#283847', fontSize: 12, fontWeight: 500}},
                colors: {!! $chart_titles->pluck('color') !!},
                vAxis: {
                    title: 'Ton',
                    titleTextStyle: {color: '#F5780D', fontSize: 12, bold: true, italic: false},
                    ticks: [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
                    format:'#,###k'
                }

            };

            var table = new google.visualization.ColumnChart(document.getElementById('table1'));

            table.draw(data, options);
        }
    </script>
@endpush