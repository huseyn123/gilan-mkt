<!-- ProductInfo Begin -->
<div class="product_info">
    <div class="row">
        <div class="col-md-4 col-sm-5 col-xs-12 xs-mb-20">

            @if($product->getFirstMedia('cover'))
                @if(substr($product->getFirstMedia('cover')->getFullUrl(), -3) == 'svg')
                    <figure>
                        <img  src="{{asset($product->getFirstMedia('cover')->getFullUrl())}}"  alt="{{$product->name}}" title="{{$product->image_title}}"/>
                    </figure>
                @else
                    <figure>
                        <img  src="{{asset($product->getFirstMedia('cover')->getUrl('blade'))}}"  alt="{{$product->name}}" title="{{$product->image_title}}"/>
                    </figure>
                @endif
            @endif

        </div>
        <div class="col-md-8 col-sm-7 col-xs-12">
            <div class="body">
                <h1 class="title">
                    <img src="{{asset('images/delete/icon-ciyid.svg')}}">
                    {{$product->seo_title ? $product->seo_title:  $product->name}}<span>{{$dictionary['product1_name_comp'] ?? ''}}</span>
                </h1>
                <div class="text">
                    {!! $product->content !!}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ProductInfo End -->
<div class="row mt30">
    <div class="col-sm-6 col-xs-12 xs-mb-20">
        <!-- Table Begin -->
        <div class="table_style">
            <table>
                <thead>
                <tr>
                    <td class="w2">{{$dictionary['product1_indicators'] ?? 'Göstəriçilər (indicators):	'}}</td>
                    <td class="w2">{{$dictionary['product1_test_result'] ?? 'Sınaq nəticəsi ( The result of test)	'}}</td>
                </tr>
                </thead>
                <tbody>

                @foreach($blocks as $block)
                    <tr>
                        <td>{{$block->title}}</td>
                        <td>{{$block->summary}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        <!-- Table End -->
    </div>
    <div class="col-sm-6 col-xs-12">
        <!-- Form Begin -->
        <div class="form pt50 pb50">
            @include('web.form.products')
        </div>
        <!-- Form End -->
    </div>
</div>
<div class="row mt30">
    <div class="col-sm-6 col-xs-12 xs-mb-20">
        <div class="chart_wrapper">
            <h2  class="title_s">{{$dictionary['product1_chart_title'] ?? 'Pambıq Cecəsi istehsalı üzrə göstəricilər	'}}</h2>
            <div id='table1'></div>
        </div>
    </div>
    <div class="col-sm-6 col-xs-12">
        <!-- HeadTitle Begin -->
        <div class="head_title small mb20">
            <h2 class="ns">{{$dictionary['other_products'] ?? 'Digər Məhsullar	'}}</h2>
        </div>
        <!-- HeadTitle End -->
        <!-- OtherProducts Begin -->
        <div class="other_products">
            <div class="row">

                @foreach($similarProducts as $sm)

                    <div class="col-xs-6 col-mob-12">
                    <div class="box clr_green" @if($sm->color_code) style="background:{{$sm->color_code}} !important;" @endif>
                        <a href="{{route('showPage',[$sm->page_slug,$sm->slug])}}" title="{{$sm->name}}">
                            @if($sm->getFirstMedia())
                                <figure>
                                    @if(substr($sm->getFirstMedia()->getFullUrl(), -3) == 'svg')
                                        <img src="{{asset($sm->getFirstMedia()->getFullUrl())}}" alt="{{$sm->name}}">
                                    @else
                                        <img src="{{asset($sm->getFirstMedia()->getUrl('thumb'))}}" alt="{{$sm->name}}">
                                    @endif
                                </figure>
                            @endif
                            <h2 @if($sm->text_color_code) style="color:{{$sm->text_color_code}} !important;" @endif>{{$sm->name}}</h2>
                        </a>
                    </div>
                </div>

                @endforeach

            </div>
        </div>
        <!-- OtherProducts End -->
    </div>
</div>
@push('scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script>
        google.charts.load('current',{'packages':['corechart']});
        google.charts.setOnLoadCallback(drawStuff);

        chart = [];
        chart_title = ['years', '',  { role: 'annotation' } ];
        chart.push(chart_title);

        @foreach($years as $year)

            @if($year->value)
                new_array = ['{!!$year->year!!}',{!!$year->value!!},''];
                chart.push(new_array);
            @endif

        @endforeach

            console.log(chart);
        function drawStuff(){
            var data = google.visualization.arrayToDataTable(chart);

            var options = {
                height: 415,
                chartArea: {
                    height: "100%",
                    top: 30,
                    bottom: 30,
                    left: 55,
                    right: 0,
                    backgroundColor: "#F6F6F6",
                },
                legend: { position: 'none', maxLines: 2, textStyle: {color: '#283847', fontSize: 12, fontWeight: 500}},
                colors: ['#458FD2'],
                vAxis: {
                    title: 'Ton',
                    titleTextStyle: {color: '#F5780D', fontSize: 12, bold: true, italic: false},
                    ticks: [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
                    format:'#,###k',
                }

            };

            var table = new google.visualization.ColumnChart(document.getElementById('table1'));

            table.draw(data, options);
        }
    </script>
@endpush