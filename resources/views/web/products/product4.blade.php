<!-- GreyContentBlock Begin -->
<div class="grey_content_block bg-white">
    <div class="title">
        <img src="{{asset('images/delete/icon-ciyid.svg')}}" >
        <h1>{{$product->seo_title ? $product->seo_title:  $product->name}}</h1>
    </div>
    <div class="text">
        {!! $product->content !!}
    </div>
</div>
<!-- GreyContentBlock End -->
<!-- OrangeContentBlock Begin -->
<div class="orange_content_block photo_block mt30">
    <div class="row">
        <div class="col-md-4 col-sm-5">
            @if($product->getFirstMedia('cover'))
                @if(substr($product->getFirstMedia('cover')->getFullUrl(), -3) == 'svg')
                    <figure>
                        <img  src="{{asset($product->getFirstMedia('cover')->getFullUrl())}}"  alt="{{$product->name}}" title="{{$product->image_title}}"/>
                    </figure>
                @else
                    <figure>
                        <img  src="{{asset($product->getFirstMedia('cover')->getUrl('blade'))}}"   alt="{{$product->name}}" title="{{$product->image_title}}"/>
                    </figure>
                @endif
            @endif
        </div>
        <div class="col-md-8 col-sm-7">
            <div class="title">
                <h2>{{$dictionary['mahlic_technial_parametrs']}}</h2>
            </div>
            <div class="text">
                {!! $product->technical_indicators !!}
            </div>
        </div>
    </div>
</div>
<!-- OrangeContentBlock End -->



<!-- HeadTitle Begin -->
<div class="head_title small mt30">
    <h2 class="ns">{{$dictionary['mahlic_chart1_title']}}</h2>
</div>
<!-- HeadTitle End -->

<div class="row">
    <div class="col-sm-3 col-xs-6">
        <div class="chart_wrapper p20">
            <h2 class="title">{{$dictionary['mahlic_circle_chart1_title']}}</h2>
            <div id="table1"></div>
        </div>
    </div>
    <div class="col-sm-3 col-xs-6">
        <div class="chart_wrapper p20">
            <h2 class="title">{{$dictionary['mahlic_circle_chart2_title']}}</h2>
            <div id="table2"></div>
        </div>
    </div>
    <div class="col-sm-3 col-xs-6">
        <div class="chart_wrapper p20">
            <h2 class="title">{{$dictionary['mahlic_circle_chart3_title']}}</h2>
            <div id="table3"></div>
        </div>
    </div>
    <div class="col-sm-3 col-xs-6">
        <div class="chart_wrapper p20">
            <h2 class="title">{{$dictionary['mahlic_circle_chart4_title']}}</h2>
            <div id="table4"></div>
        </div>
    </div>
</div>

<div class="row mt30">
    <div class="col-sm-6 col-xs-12 xs-mb-20">
        <div class="chart_wrapper">
            <h2 class="title_s">{!! $dictionary['mahlic_chart2_title'] !!}</h2>
            <div id="table5"></div>
        </div>
    </div>
    <div class="col-sm-6 col-xs-12">
        <!-- Form Begin -->
        <div class="form pt30">
            @include('web.form.products')
        </div>
        <!-- Form End -->
    </div>
</div>


<div class="row mt30">
    <div class="col-sm-6 col-xs-12 xs-mb-20">
        <!-- Table Begin -->
        <div class="table_style ptb20">
            <table>
                <tbody>
                <tr>
                    <td class="w2">{{$dictionary['product_name']}}</td>
                    <td class="w2">Mahlıc</td>
                </tr>

                @foreach($blocks as $block)
                    <tr>
                        <td>{{$block->title}}</td>
                        <td>{{$block->summary}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        <!-- Table End -->
    </div>
    <div class="col-sm-6 col-xs-12">
        <!-- HeadTitle Begin -->
        <div class="head_title small mb20">
            <h2 class="ns">{{$dictionary['other_products']}}</h2>
        </div>
        <!-- HeadTitle End -->
        <!-- OtherProducts Begin -->
        <div id="other_products" class="owl-carousel">
            @foreach($similarProducts as $sm)

                <div class="other_products">
                <div class="box clr_green" @if($sm->color_code) style="background:{{$sm->color_code}} !important;" @endif>
                    <a href="{{route('showPage',[$sm->page_slug,$sm->slug])}}" title="{{$sm->name}}">
                        @if($sm->getFirstMedia())
                            <figure>
                                @if(substr($sm->getFirstMedia()->getFullUrl(), -3) == 'svg')
                                    <img src="{{asset($sm->getFirstMedia()->getFullUrl())}}" alt="{{$sm->name}}">
                                @else
                                    <img src="{{asset($sm->getFirstMedia()->getUrl('thumb'))}}" alt="{{$sm->name}}">
                                @endif
                            </figure>
                        @endif
                        <h2 @if($sm->text_color_code) style="color:{{$sm->text_color_code}} !important;" @endif>{{$sm->name}}</h2>
                    </a>
                </div>
            </div>

            @endforeach
        </div>
        <!-- OtherProducts End -->
    </div>
</div>

@push('scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawTable1Chart);
        google.charts.setOnLoadCallback(drawTable2Chart);
        google.charts.setOnLoadCallback(drawTable3Chart);
        google.charts.setOnLoadCallback(drawTable4Chart);
        google.charts.setOnLoadCallback(drawTable5Chart);

        @foreach(config('config.mahlic_chart') as $key => $value)
                @if(!$loop->last)
                    function drawTable{{$key}}Chart() {

                        colors = [];
                        chart = [];
                        @foreach($mahlic_charts as $m_chart)
                            @if($m_chart->chart_type === $key && $m_chart->color)
                                colors.push('{{$m_chart->color}}');
                                chart.push(['{{$m_chart->title}}", {{$m_chart->value}}%',  {{$m_chart->value}}]);
                            @endif
                        @endforeach

                        // Create the data table for Sarah's pizza.
                        var data = new google.visualization.DataTable();
                        data.addColumn('string', 'Topping');
                        data.addColumn('number', 'Slices');
                        data.addRows(chart);

                        // Set options for Sarah's pie chart.
                        var options = {
                            height: 250,
                            legend: { position: 'bottom', maxLines: 2, textStyle: {color: '#283847', fontSize: 12, fontWeight: 500}},
                            colors: colors
                        };

                        // Instantiate and draw the chart for Sarah's pizza.
                        var chart = new google.visualization.PieChart(document.getElementById('table{{$key}}'));
                        chart.draw(data, options);
                    }
                @endif



        @endforeach





        function drawTable5Chart() {
            chart = [['Növ', '', { role: 'style' }]];

            @foreach($mahlic_charts as $m_chart)
                @if($m_chart->chart_type === 5 && $m_chart->color)
                    chart.push(['{{$m_chart->title}}',{{$m_chart->value}},'{{$m_chart->color}}']);
                @endif
            @endforeach

                        console.log(chart);
            var data = google.visualization.arrayToDataTable(chart);

            var options = {
                height: 400,
                chartArea: {
                    backgroundColor: "#F6F6F6",
                },
                bars: "horizontal",
                legend: { position: 'none', maxLines: 3 },
                bar: { groupWidth: '15%' },
                vAxis: {
                    title: 'Nov',
                    titleTextStyle: {color: '#F5780D', fontSize: 12, bold: true, italic: false},
                    ticks: [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
                    format: '#\'%\''
                }

            };

            var chart = new google.charts.Bar(document.getElementById('table5'));

            chart.draw(data, google.charts.Bar.convertOptions(options));
        }
    </script>
@endpush

