<div class="row pt10">
    <div class="col-sm-6 col-sm-push-6 col-xs-12 mb30 eq">
        <!-- IplikPhoto -->
        @if($product->getFirstMedia('cover'))
            @if(substr($product->getFirstMedia('cover')->getFullUrl(), -3) == 'svg')
                <figure class="iplik_photo">
                    <img  src="{{asset($product->getFirstMedia('cover')->getFullUrl())}}"   alt="{{$product->name}}" title="{{$product->image_title}}"/>
                </figure>
            @else
                <figure class="iplik_photo">
                    <img  src="{{asset($product->getFirstMedia('cover')->getUrl('blade'))}}"   alt="{{$product->name}}" title="{{$product->image_title}}"/>
                </figure>
            @endif
        @endif

    </div>
    <div class="col-sm-6 col-sm-pull-6 col-xs-12 mb30 eq">
        <!-- OrangeContentBlock Begin -->
        <div class="orange_content_block">
            <div class="title">
                <img src="{{asset('images/delete/icon-iplik.svg')}}">
                <h1>  {{$product->seo_title ? $product->seo_title:  $product->name}}</h1>
            </div>
            <div class="text">
                {!! $product->content !!}
            </div>
        </div>
        <!-- OrangeContentBlock End -->
    </div>


    <div class="col-sm-6 col-xs-12">
        <!-- Table Begin -->
        <div class="table_style xs-mb-20 ptb20 l1-4">
            <table>
                <thead>
                <tr>
                    <td colspan="2" class="text-center">{!! $dictionary['product5_technical_parameters'] !!}</td>
                </tr>
                </thead>
                <tbody>

                    @foreach($blocks as $block)
                        <tr>
                            <td class="@if($loop->first)  w2 @endif ln-37">{{$block->title}}</td>
                            <td  class=@if($loop->first)"w2 @endif ln-37" >{{$block->summary}}</td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
        <!-- Table End -->
    </div>
    <div class="col-sm-6 col-xs-12">
        <!-- Form Begin -->
        <div class="form pt30">
            @include('web.form.products')
        </div>
        <!-- Form End -->
    </div>


    <div class="row mt30">
        <div class="col-xs-12">
            <!-- HeadTitle Begin -->
            <div class="head_title small mb20">
                <h2 class="ns mt-30">{{$dictionary['other_products']}}</h2>
            </div>
            <!-- HeadTitle End -->
            <!-- OtherProducts Begin -->
            <div class="other_products">
                <div class="row">

                    @foreach($similarProducts as $sm)

                        <div class="col-sm-3 col-xs-6 col-mob-12">
                            <div class="box clr_green" @if($sm->color_code) style="background:{{$sm->color_code}} !important;" @endif>
                                <a href="{{route('showPage',[$sm->page_slug,$sm->slug])}}" title="{{$sm->name}}">
                                    @if($sm->getFirstMedia())
                                        <figure>
                                            @if(substr($sm->getFirstMedia()->getFullUrl(), -3) == 'svg')
                                                <img src="{{asset($sm->getFirstMedia()->getFullUrl())}}" alt="{{$sm->name}}">
                                            @else
                                                <img src="{{asset($sm->getFirstMedia()->getUrl('thumb'))}}" alt="{{$sm->name}}">
                                            @endif
                                        </figure>
                                    @endif
                                    <h2 @if($sm->text_color_code) style="color:{{$sm->text_color_code}} !important;" @endif>{{$sm->name}}</h2>
                                </a>
                            </div>
                        </div>

                    @endforeach

                </div>
            </div>
            <!-- OtherProducts End -->
        </div>
    </div>



</div>
