@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs')
                    <!-- BreadCrumbs End -->

                    <!-- Faq Begin -->
                    <div class="faq">
                        @foreach($faqs as $faq)

                            <div class="item">
                                <div class="head">
                                    <i></i>
                                    <h2>{{$faq->question}}</h2>
                                </div>
                                <div class="body">
                                    <div class="body_inner">
                                        <p>{{$faq->answer}}</p>
                                    </div>
                                </div>
                            </div>

                        @endforeach

                    </div>

                    <!-- FAQ End -->

                    <!-- Pagination Begin -->
                        {!! $faqs->appends(request()->input())->links('vendor.pagination.news') !!}
                    <!-- Pagination End -->

                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->

@endsection