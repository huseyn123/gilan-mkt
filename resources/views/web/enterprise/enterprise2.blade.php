<div class="orange_content_block">
    <div class="title">
        <img src="{{asset('images/delete/icon-factory.svg')}}" alt="{{$enterprise->name}}" title="{{$enterprise->image_title}}"  >
        <h1>{{$enterprise->seo_title ? $enterprise->seo_title : $enterprise->name}}</h1>
    </div>
</div>
<!-- OrangeContentBlock End -->
<!-- EnterpriseAboutBlockBlue Begin -->
<div class="enterprise_about_block_blue">
    {!! $enterprise->content !!}
</div>
<!-- EnterpriseAboutBlockBlue End -->

@if($enterprise->table)

    <!-- HeadTitle Begin -->
    <div class="head_title small mt30">
        <h2 class="ns">{{$dictionary['enterprise2_techinal_parametrs']}}</h2>
    </div>
    <!-- HeadTitle End -->
    <!-- Table Begin -->
    <div class="table_style">
        {!! $enterprise->table !!}
    </div>
    <!-- Table End -->

@endif