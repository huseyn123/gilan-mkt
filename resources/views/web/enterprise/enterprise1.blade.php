<!-- OrangeContentBlock Begin -->
<div class="orange_content_block ">
    <div class="title">
        <img src="{{asset('images/delete/icon-factory.svg')}}" alt="{{$enterprise->name}}" title="{{$enterprise->image_title}}" >
        <h1>{{$enterprise->seo_title ? $enterprise->seo_title : $enterprise->name}}</h1>
    </div>
    {!! $enterprise->content !!}
</div>
<!-- OrangeContentBlock End -->