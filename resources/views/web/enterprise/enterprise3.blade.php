<!-- OrangeContentBlock Begin -->
<div class="orange_content_block ent_only">
    <div class="title">
        <img src="{{asset('images/delete/icon-factory.svg')}}" alt="{{$enterprise->name}}" title="{{$enterprise->image_title}}" >
        <h1>{{$enterprise->seo_title ? $enterprise->seo_title : $enterprise->name}}</h1>
    </div>
    {!! $enterprise->content !!}
</div>
<!-- OrangeContentBlock End -->

@if($blocks->count())
    <!-- EnterpriseAboutBlocks Begin -->
    <div class="enterprise_about_blocks">
        <div class="row">

            @foreach($blocks as $block)

                <div class="@if($loop->first || $loop->last)col-md-7 @else col-md-5 @endif col-sm-6 col-xs-12">
                    <div class="block">
                        {!! $block->content !!}
                    </div>
                </div>

            @endforeach


        </div>
    </div>
    <!-- EnterpriseAboutBlocks End -->
@endif
