<div class="orange_content_block ent_only">
    <div class="title">
        <img src="{{asset('images/delete/icon-factory.svg')}}" alt="{{$enterprise->name}}" title="{{$enterprise->image_title}}" >
        <h1>{{$enterprise->seo_title ? $enterprise->seo_title : $enterprise->name}}</h1>
    </div>
    {!! $enterprise->content !!}
</div>
<!-- OrangeContentBlock End -->

<div class="et4">
        <!-- EnterpriseAboutBoxs Begin -->
    @if($blocks->count())
        @foreach($blocks as $block)
            {!! $block->content !!}
        @endforeach
    @endif
    <!-- EnterpriseAboutBoxs End -->
</div>
