@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs')
                    <!-- BreadCrumbs End -->

                    <!-- HeadTitle Begin -->
                    <div class="head_title">
                        <h1>{{$page->seo_title ? $page->seo_title:  $page->name}}</h1>
                    </div>
                    <!-- HeadTitle End -->


                    <!-- Certificates Begin -->
                    <div class="certificates">
                        <div class="row">
                            @foreach($certificates as $certificate)

                                <div class="col-sm-3 col-xs-6">
                                <div class="box">
                                    <figure>
                                        <a href="@if(substr($certificate->getFirstMedia()->getFullUrl(), -3) == 'svg') {{asset($certificate->getFirstMedia()->getFullUrl())}} @else{{ asset($certificate->getFirstMedia()->getUrl('blade')) }}@endif"  class="zoom-photo">
                                            <img  alt="{{$certificate->name}}" title="{{$certificate->image_title}}" src="@if(substr($certificate->getFirstMedia()->getFullUrl(), -3) == 'svg') {{asset($certificate->getFirstMedia()->getFullUrl())}} @else {{ asset($certificate->getFirstMedia()->getUrl('thumb')) }}@endif"/>
                                        </a>
                                    </figure>
                                    <h2>{{ $certificate->name }}</h2>
                                    @if($certificate->no)<span >No: {{ $certificate->no }}</span>@endif
                                </div>
                            </div>

                            @endforeach

                        </div>
                    </div>
                    <!-- Certificates End -->

                    <!-- Pagination Begin -->
                        {!! $certificates->appends(request()->input())->links('vendor.pagination.news') !!}
                    <!-- Pagination End -->
                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->


@endsection