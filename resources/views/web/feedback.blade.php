@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    @include('web.elements.breadcrumbs')

    <div class="feedback container">

        <div class="row">
            <div class="col-md-6">
                <div class="form">
                    @include('web.form.form_succes')
                    <h3 class="title">{{$page->name}}</h3>
                    @include('web.form.feedback')
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="title">{{ $dictionary['dear_customer'] ?? 'Hörmətli müştəri,'}}</div>
                    {!! $page->content !!}
                </div>
            </div>
        </div>

    </div>

@endsection
