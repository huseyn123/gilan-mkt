@extends ('layouts.user-cabinet')

@section ('section')

    <div class="user_info">
        <div class="item clearfix">
            <span class="lbl">Ad:</span>
            <span class="value">{{ $user->name }}</span>
        </div>
        <div class="item clearfix">
            <span class="lbl">E-poçt:</span>
            <span class="value">{{ $user->email }}</span>
        </div>
        <div class="item clearfix">
            <span class="lbl">Əlaqə nömrəsi:</span>
            <span class="value">{{ $user->phone }}</span>
        </div>
    </div>


@endsection
