@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs')
                    <!-- BreadCrumbs End -->

                    <!-- ProductList Begin -->
                    <div class="product_list pt5">
                        <!-- HeadTitle Begin -->
                        <div class="head_title">
                            <h1>{{$page->seo_title ? $page->seo_title:  $page->name}}</h1>
                        </div>
                        <!-- HeadTitle End -->
                        <div class="row">

                            @include('web.elements.product_crud')

                        </div>
                    </div>
                    <!-- ProductList End -->
                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->
@endsection
