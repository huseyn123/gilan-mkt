@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs')
                    <!-- BreadCrumbs End -->

                    <!-- Contact Begin -->
                    <div class="contact">
                        <h1 class="title">{{$dictionary['contact_information']}}</h1>
                        <div class="contact_info">
                            <div class="box">
                                <span class="lbl">{{$dictionary['location']}}</span>
                                <span class="val">{{$dictionary['address']}}</span>
                            </div>
                            <span class="line"></span>
                            <div class="box">
                                <span class="lbl">{{$dictionary['telephone_number']}}</span>
                                <span class="val">{{$config['contact_phone']}}</span>
                                <span class="val">{{$config['contact_phone2']}}</span>
                            </div>
                            <span class="line"></span>
                            <div class="box">
                                <span class="lbl">{{$dictionary['contact_email']}}</span>
                                <span class="val">{{$config['contact_email']}}</span>
                            </div>
                        </div>
                        <div class="contact_wrapper">
                            <div class="row">
                                <div class="col-md-8 pr0 contact_eq">
                                    <div class="map">
                                        <div id="map"></div>
                                    </div>
                                </div>
                                <div class="col-md-4 pl0 contact_eq">

                                    @include('web.form.contact')

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Contact End -->
                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->

@endsection

@push('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?language=az&amp;key=AIzaSyDOAQMCj2njfFtYCk5nKudFmHMMK6xAD70" type="text/javascript"></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', init);
  function init() {
            var markericon = {
                url: '{{ asset('images/icon/map-marker.png') }}',
                //state your size parameters in terms of pixels
                origin: new google.maps.Point(0,0)
            }

            var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng({{ $config['location'] }})
            };

            var mapElement = document.getElementById('map');
            var map = new google.maps.Map(mapElement, mapOptions);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng({{ $config['location'] }}),
                map: map,
                icon: markericon
            });

            var myoverlay = new google.maps.OverlayView();
            myoverlay.draw = function () {
                this.getPanes().markerLayer.id='markerLayer';
            };
            myoverlay.setMap(map);
        }
    </script>
@endpush