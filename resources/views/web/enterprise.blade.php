@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs')
                    <!-- BreadCrumbs End -->

                    <!-- BreadCrumbs End -->
                    <!-- Enterprises Begin -->
                    <div class="enterprises mt5">
                        <!-- HeadTitle Begin -->
                        <div class="head_title">
                            <h1>{{$page->seo_title ? $page->seo_title:  $page->name}}</h1>
                        </div>
                        <!-- HeadTitle End -->
                        <div class="row">
                            @include('web.elements.enterprise_crud')
                        </div>
                    </div>
                    <!-- Enterprises End -->

                    <!-- Pagination Begin -->
                        {!! $enterprises->appends(request()->input())->links('vendor.pagination.news') !!}
                    <!-- Pagination End -->
                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->

@endsection