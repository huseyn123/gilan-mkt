@extends ('layouts.web', ['page_heading' => null,'index_page' =>true] )

@section ('content')


    @include('web.elements.slider')

    @include('web.elements.video')


    @include('web.elements.statistic')


    <!-- MainWrapper Begin -->
    <section class="main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Banner Begin -->
                    <div class="banner">
                        <img src="{{asset('storage/files/index/banner.png')}}" >
                        <div class="inner">
                            <span>{{$dictionary['cotton_harvesting_title1']}}</span>
                            <span>{{$dictionary['cotton_harvesting_title2']}}</span>
                        </div>
                    </div>
                    <!-- Banner End -->

                    <!-- MainProducts Begin -->
                        @include('web.elements.index_products')
                    <!-- MainProducts End -->


                    @if($enterprises->count())
                        <!-- Enterprises Begin -->
                        <div class="enterprises mt45">
                            <!-- HeadTitle Begin -->
                            <div class="head_title">
                                <h2 class="ns">{{$dictionary['enterprises']}}</h2>
                                <a href="{{route('showPage',[$enterprises->first()->page_slug])}}" class="all_read">{{$dictionary['show_all']}}</a>
                            </div>

                            <!-- HeadTitle End -->
                            <div class="row">
                                @include('web.elements.enterprise_crud')
                            </div>
                        </div>
                        <!-- Enterprises End -->
                    @endif

                    @if($articles->count())
                        <!-- NewsList Begin -->
                        <div class="news_list mt15">
                            <!-- HeadTitle Begin -->
                            <div class="head_title">
                                <h2 class="ns">{{$dictionary['news'] ?? 'Xəbərlər'}}</h2>
                                <a href="{{route('showPage',[$articles->first()->page_slug])}}"  class="all_read">{{$dictionary['show_all']}}</a>
                            </div>
                            <!-- HeadTitle End -->
                            <div class="row">
                                @include('web.elements.news-grid')
                            </div>
                        </div>
                        <!-- NewsList End -->
                     @endif


                </div>
            </div>
        </div>
    </section>
    <!-- MainWrapper End -->



@endsection

