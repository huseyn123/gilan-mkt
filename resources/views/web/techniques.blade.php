@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')


    <!-- PageWrapper Begin -->
    <section class="page_wrapper pt0">
        <!-- Technique Begin -->
        <div class="technique">
            <div class="container">
                <div class="row">

                    <div class="col-xs-12">
                        <!-- BreadCrumbs Begin -->
                            @include('web.elements.breadcrumbs')
                        <!-- BreadCrumbs End -->

                        <!-- TechniqueHead Begin -->
                        <div class="technique_head">
                            <div class="row">

                                <div class="col-sm-6 col-sm-push-6 col-xs-12">
                                    @if($page->getFirstMedia())
                                        <figure>
                                            <img src="{{asset($page->getFirstMedia()->getUrl('blade'))}}">
                                        </figure>
                                    @endif
                                </div>

                                <div class="col-sm-6 col-sm-pull-6 col-xs-12">
                                    <h1 class="title"><i></i>{{$page->seo_title ? $page->seo_title:  $page->name}}</h1>

                                    <div class="text">
                                       {!! $page->content !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- TechniqueHead End -->
                    </div>
                </div>
            </div>
        </div>
        <div class="container pt40">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12 xs-mb-20">
                    <!-- SideMenu Begin -->
                    <nav class="sidemenu">
                        <ul class="list-unstyled">

                            @foreach($categories as $category)
                                <li @if($selectedcat == $category->slug) class="active" @endif><a href="{{route('showPage',[$page->slug])}}?category={{$category->slug}}" title="">{{$category->name}}</a></li>
                            @endforeach

                        </ul>
                    </nav>
                    <!-- SideMenu End -->
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <!-- TechniqueList Begin -->
                    <div class="techique_list">
                        <div class="row">
                            @foreach($techniques as $technique)

                                <div class="col-md-4 col-sm-6 col-xs-6 col-mob-12">
                                    <div class="box">
                                        <a>
                                            @if($technique->getFirstMedia())
                                                <figure>
                                                    @if(substr($technique->getFirstMedia()->getFullUrl(), -3) == 'svg')
                                                        <img src="{{$technique->getFirstMedia()->getFullUrl()}}" alt="{{$technique->name}}"  alt="{{$technique->name}}" title="{{$technique->image_title}}">
                                                    @else
                                                        <img src="{{$technique->getFirstMedia()->getUrl('blade')}}" alt="{{$technique->name}}"  alt="{{$technique->name}}" title="{{$technique->image_title}}">
                                                    @endif
                                                </figure>
                                            @endif
                                            <div class="body">
                                                <h2>{{$technique->name}}</h2>
                                                <span>{{$technique->summary}}</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                            @endforeach
                        </div>
                    </div>
                    <!-- TechniqueList End -->
                </div>
            </div>
        </div>
        <!-- Technique End -->
    </section>
    <!-- PageWrapper End -->

@endsection
