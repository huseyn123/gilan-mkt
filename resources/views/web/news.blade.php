@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs')
                    <!-- BreadCrumbs End -->

                    <!-- HeadTitle Begin -->
                    <div class="head_title">
                        <h1>{{$page->seo_title ? $page->seo_title:  $page->name}}</h1>
                    </div>
                    <!-- HeadTitle End -->

                    @if($years->count())

                        <!-- Years Begin -->
                        <div class="years">

                            @foreach($years as $year)
                                <a   @if($selectedYear == $year) class="active" @endif href="{{ route('showPage',[$page->slug]).'?year='.$year }}" >{{ $year }}</a>
                            @endforeach

                        </div>

                    @endif

                <!-- Years End -->
                    <!-- NewsList Begin -->
                    <div class="news_list">
                        <div class="row">

                            @include('web.elements.news-grid')

                        </div>
                    </div>
                    <!-- NewsList End -->

                    <!-- Pagination Begin -->
                        {!! $articles->appends(request()->input())->links('vendor.pagination.news') !!}
                    <!-- Pagination End -->

                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->

@endsection
