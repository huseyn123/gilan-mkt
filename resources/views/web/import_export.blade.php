@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- BreadCrumbs Begin -->
                @include('web.elements.breadcrumbs')
                <!-- BreadCrumbs End -->
                    <!-- GreyTextBlock Begin -->
                    <div class="grey_text_block mt10">
                        <div class="text_block">
                            <h2 class="title">{{$page->name}}</h2>
                            <div class="text">
                                {!! $page->content !!}
                            </div>
                        </div>
                    </div>
                    <!-- GreyTextBlock End -->
                    <div class="chart_wrapper mt30">
    					<h1 class="title">{{$page->seo_title ?  $page->seo_title : $dictionary['import_export_chart_title']}}</h1>
    					<div id='table1'></div>
    				</div>

                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->

@endsection

@push('scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script>
        google.charts.load('current',{'packages':['corechart']});
        google.charts.setOnLoadCallback(drawStuff);

        chart = [];
        chart_title = [];

        chart_title.push('Cotton fiber');

        @foreach($chart_titles as $chart_title)
            chart_title.push('{{$chart_title->title}}');
        @endforeach

        chart_title.push({ role: 'annotation' });

        chart.push(chart_title);

        @foreach($line_x as $lx)
            new_array = [];
            new_array.push('{{$lx->title}}');
            @foreach($chart_titles as $chart_title)
                val =0;
                @if($lx->statistics_line_x->count())
                    @foreach($lx->statistics_line_x as $statistic)
                        @if($statistic->percent && $statistic->category_id == $chart_title->id)
                            val = {{$statistic->percent}};
                        @endif
                    @endforeach
                @endif

                new_array.push(val);

                @endforeach

                new_array.push('');
                chart.push(new_array);
            @endforeach

        function drawStuff(){
            var data = google.visualization.arrayToDataTable(chart);

            var interval = 10;
            var dataRange = 100;

            var vTicks = [];
            for (var i = interval; i < dataRange + interval; i = i + interval) {
                vTicks.push(i);
            }


            var options = {
                height: 355,
                chartArea: {
                    width: '95%',
                    height: "100%",
                    top: 30,
                    bottom: 80,
                    left: 48,
                    right: 0,
                    backgroundColor: "#F6F6F6",
                },
                vAxis: {
                    ticks: vTicks,
                    format: '#\'%\''

                },

                legend: { position: 'bottom', maxLines: 2, textStyle: {color: '#283847', fontSize: 12, fontWeight: 500}},
                colors: {!! $chart_titles->pluck('color') !!},
            };

            var table = new google.visualization.ColumnChart(document.getElementById('table1'));

            table.draw(data, options);
        }
    </script>
@endpush