@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')


    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs')
                    <!-- BreadCrumbs End -->

                    <div class="head_title">
                        <h1>{{$page->seo_title ? $page->seo_title:  $page->name}}</h1>
                    </div>

                    <!-- GreyTextBlock Begin -->
                    <div class="grey_text_block">
                        <div class="row">
                            <div class="col-lg-3 col-sm-4 col-xs-12 xs-mb-20">
                                @include('web.elements.cover')
                            </div>
                            <div class="col-lg-9 col-sm-8 col-xs-12">
                                <div class="text_block">
                                    <div class="text">
                                        {!! $page->content !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- GreyTextBlock End -->
                    <!-- Sxem Begin -->
                    <div class="sxem">
                        <div class="row">

                            @if($page->lang_media)
                                <div class="col-xs-12">
                                    <div class="inner">
                                        <div class="responsive_layout">
                                            <img src="{{$page->lang_media->getFullUrl()}}">
                                        </div>
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>
                    <!-- Sxem End -->
                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->



@endsection
