@extends ('layouts.web', [
'page_heading' => Cache::get('dictionary_'.app()->getLocale())['search_result_title'],
'menuWithoutSlug' => true,
'menu' => \App\Logic\Menu::all(),
'config' => \App\Logic\WebCache::config()->pluck('value', 'key'),
'dictionary' => Cache::get('dictionary_'.app()->getLocale()),
'social' => Cache::get('social'),
'lang' => app()->getLocale(),
])

@section ('content')




    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- BreadCrumbs Begin -->
                    <div class="breadcrumbs">
                        <div class="inner">
                            <a href="#" title="">Ana səhifə</a>
                            <i class="fav4 fa-angle-right"></i>
                            <span>Axtarışın nəticəsi</span>
                        </div>
                    </div>
                    <!-- BreadCrumbs End -->
                    <!-- PageSearch Begin -->
                    <div class="page_search">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
                                <form action="">
                                    <button type="submit"><i class="icofont-search-1"></i></button>
                                    <input type="text" name="" placeholder="Axtar">
                                </form>
                            </div>
                        </div>
                        <span class="search_word">"Gilan MKT"</span>
                        <span class="result_count">10 nəticə tapıldı</span>
                    </div>
                    <!-- PageSearch End -->
                    <!-- SearchResults Begin -->
                    <div class="search_results">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="box">
                                    <h2><a href="#" title="">Gilan MKT pambıqçılıq üzrə rekord məhsuldarlıq əldə edib</a></h2>
                                    <p>Kənd təsərrüfatı insan fəaliyyətinin ən başlıca və qədim sahəsidir. Dünyada elə bir ölkə yoxdur ki, orada kənd təsərrüfatı və ona yaxın olan meşə təsərrüfatı, ovçuluq, balıqçılıqla məşğul olmasınlar.</p>
                                    <div class="bottom">
                                        <a href="#" title="">Ardını oxu<i class="fav4 fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="box">
                                    <h2><a href="#" title="">Gilan MKT pambıqçılıq üzrə rekord məhsuldarlıq əldə edib</a></h2>
                                    <p>Kənd təsərrüfatı insan fəaliyyətinin ən başlıca və qədim sahəsidir. Dünyada elə bir ölkə yoxdur ki, orada kənd təsərrüfatı və ona yaxın olan meşə təsərrüfatı, ovçuluq, balıqçılıqla məşğul olmasınlar.</p>
                                    <div class="bottom">
                                        <a href="#" title="">Ardını oxu<i class="fav4 fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="box">
                                    <h2><a href="#" title="">Gilan MKT pambıqçılıq üzrə rekord məhsuldarlıq əldə edib</a></h2>
                                    <p>Kənd təsərrüfatı insan fəaliyyətinin ən başlıca və qədim sahəsidir. Dünyada elə bir ölkə yoxdur ki, orada kənd təsərrüfatı və ona yaxın olan meşə təsərrüfatı, ovçuluq, balıqçılıqla məşğul olmasınlar.</p>
                                    <div class="bottom">
                                        <a href="#" title="">Ardını oxu<i class="fav4 fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="box">
                                    <h2><a href="#" title="">Gilan MKT pambıqçılıq üzrə rekord məhsuldarlıq əldə edib</a></h2>
                                    <p>Kənd təsərrüfatı insan fəaliyyətinin ən başlıca və qədim sahəsidir. Dünyada elə bir ölkə yoxdur ki, orada kənd təsərrüfatı və ona yaxın olan meşə təsərrüfatı, ovçuluq, balıqçılıqla məşğul olmasınlar.</p>
                                    <div class="bottom">
                                        <a href="#" title="">Ardını oxu<i class="fav4 fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="box">
                                    <h2><a href="#" title="">Gilan MKT pambıqçılıq üzrə rekord məhsuldarlıq əldə edib</a></h2>
                                    <p>Kənd təsərrüfatı insan fəaliyyətinin ən başlıca və qədim sahəsidir. Dünyada elə bir ölkə yoxdur ki, orada kənd təsərrüfatı və ona yaxın olan meşə təsərrüfatı, ovçuluq, balıqçılıqla məşğul olmasınlar.</p>
                                    <div class="bottom">
                                        <a href="#" title="">Ardını oxu<i class="fav4 fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="box">
                                    <h2><a href="#" title="">Gilan MKT pambıqçılıq üzrə rekord məhsuldarlıq əldə edib</a></h2>
                                    <p>Kənd təsərrüfatı insan fəaliyyətinin ən başlıca və qədim sahəsidir. Dünyada elə bir ölkə yoxdur ki, orada kənd təsərrüfatı və ona yaxın olan meşə təsərrüfatı, ovçuluq, balıqçılıqla məşğul olmasınlar.</p>
                                    <div class="bottom">
                                        <a href="#" title="">Ardını oxu<i class="fav4 fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="box">
                                    <h2><a href="#" title="">Gilan MKT pambıqçılıq üzrə rekord məhsuldarlıq əldə edib</a></h2>
                                    <p>Kənd təsərrüfatı insan fəaliyyətinin ən başlıca və qədim sahəsidir. Dünyada elə bir ölkə yoxdur ki, orada kənd təsərrüfatı və ona yaxın olan meşə təsərrüfatı, ovçuluq, balıqçılıqla məşğul olmasınlar.</p>
                                    <div class="bottom">
                                        <a href="#" title="">Ardını oxu<i class="fav4 fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="box">
                                    <h2><a href="#" title="">Gilan MKT pambıqçılıq üzrə rekord məhsuldarlıq əldə edib</a></h2>
                                    <p>Kənd təsərrüfatı insan fəaliyyətinin ən başlıca və qədim sahəsidir. Dünyada elə bir ölkə yoxdur ki, orada kənd təsərrüfatı və ona yaxın olan meşə təsərrüfatı, ovçuluq, balıqçılıqla məşğul olmasınlar.</p>
                                    <div class="bottom">
                                        <a href="#" title="">Ardını oxu<i class="fav4 fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="box">
                                    <h2><a href="#" title="">Gilan MKT pambıqçılıq üzrə rekord məhsuldarlıq əldə edib</a></h2>
                                    <p>Kənd təsərrüfatı insan fəaliyyətinin ən başlıca və qədim sahəsidir. Dünyada elə bir ölkə yoxdur ki, orada kənd təsərrüfatı və ona yaxın olan meşə təsərrüfatı, ovçuluq, balıqçılıqla məşğul olmasınlar.</p>
                                    <div class="bottom">
                                        <a href="#" title="">Ardını oxu<i class="fav4 fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="box">
                                    <h2><a href="#" title="">Gilan MKT pambıqçılıq üzrə rekord məhsuldarlıq əldə edib</a></h2>
                                    <p>Kənd təsərrüfatı insan fəaliyyətinin ən başlıca və qədim sahəsidir. Dünyada elə bir ölkə yoxdur ki, orada kənd təsərrüfatı və ona yaxın olan meşə təsərrüfatı, ovçuluq, balıqçılıqla məşğul olmasınlar.</p>
                                    <div class="bottom">
                                        <a href="#" title="">Ardını oxu<i class="fav4 fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- SearchResults End -->
                    <!-- Pagination Begin -->
                    <nav class="pag">
                        <ul class="list-unstyled">
                            <li class="disabled prev"><i class="fav4 fa-angle-left"></i></li>
                            <li><a href="#" title="">1</a></li>
                            <li class="active">2</li>
                            <li><a href="#" title="">3</a></li>
                            <li class="dots">...</li>
                            <li class="next"><a href="#" title=""><i class="fav4 fa-angle-right"></i></a></li>
                        </ul>
                    </nav>
                    <!-- Pagination End -->
                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->

@endsection