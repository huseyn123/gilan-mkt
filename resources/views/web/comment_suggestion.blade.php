@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs')
                    <!-- BreadCrumbs End -->

                    <div class="row pt10">
                        <div class="col-md-6 col-md-push-6 col-xs-12">
                            <!-- HSBlock Begin -->
                            <div class="hs_block">
                                <h4>{{$dictionary['dear_customer'] ?? ''}}</h4>
                                <div class="text">
                                    {!! $page->content !!}
                                </div>
                            </div>
                            <!-- HSBlock End -->
                        </div>
                        <div class="col-md-6 col-md-pull-6 col-xs-12">

                            <!-- Form Begin -->
                            <div class="form pt30">

                                <!-- SuccessForm Begin -->
                                    <h1>{{$page->seo_title ? $page->seo_title : $page->name}}</h1>
                                    @include('web.form.comment_suggestion')
                                <!-- SuccessForm End -->

                            </div>
                            <!-- Form End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->



@endsection
