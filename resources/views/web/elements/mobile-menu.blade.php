<!-- MobileNav Begin -->
<div class="mobile_nav">
    <span class="close"></span>
    <div class="lang_panel">

        @include('web.elements.lang',['type' => 'mobile'])

    </div>

    <div class="body">

        @include('web.elements.menu', ['parent' => null,'sub' => true,'lang' => $lang, 'hidden' => [0,3],'type' => 'header'])

        <!-- Social -->
        <nav class="social">
            @include('web.elements.social')
        </nav>
    </div>
</div>
<!-- MobileNav Begin -->