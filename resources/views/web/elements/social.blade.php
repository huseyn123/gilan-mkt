@foreach(config('config.social-network') as $key => $item)

    @if(isset($config[$item]) && trim($config[$item]) != '')
        <a href="{{ $config[$item] }}"><i class="fa fa-{{$item}}"></i></a>

    @endif

@endforeach
