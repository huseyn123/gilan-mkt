@if($config['video_link'])
    <!-- PromotionVideo Begin -->
    <section class="promotion_video">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="inner veq">
                        <h3>{{$dictionary['index_video_title']}}</h3>
                        <p>{{$dictionary['index_video_description']}}</p>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8">
                    <figure class="veq">
                        <a href="{{$config['video_link']}}" title="" class="zoom-video"><img src="{{asset('storage/files/index/promotionideoimg.png')}}" ></a>
                    </figure>
                </div>
            </div>
        </div>
    </section>
    <!-- PromotionVideo End -->
@endif
