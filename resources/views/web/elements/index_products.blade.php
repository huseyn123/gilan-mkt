@if($products->count())
    <div class="main_products">
        <!-- HeadTitle Begin -->
        <div class="head_title">
            <h2 class="ns">{{$dictionary['products']}}</h2>
            <a href="{{route('showPage',[$products->first()->page_slug])}}" title="" class="all_read">{{$dictionary['show_all']}}</a>
        </div>
        <!-- HeadTitle End -->
        <!-- ProductsCarousel Begin -->
        <div id="productscarousel" class="owl-carousel">

            @foreach($products as $product)

                <div class="item">
                    <div class="product_box clr_green" @if($product->color_code) style="background:{{$product->color_code}} !important;" @endif>
                        <div class="caption">
                            <h2 @if($product->text_color_code) style="color:{{$product->text_color_code}} !important;" @endif>{{$product->name}}</h2>
                            <p>{{$product->summary}}</p>
                            <a href="{{route('showPage',[$product->page_slug,$product->slug])}}" title="{{$product->name}}" class="btn_more" style="color:{{$product->text_color_code}} !important;" ><span>{{$dictionary['more']}}</span></a>
                        </div>

                        @if($product->getFirstMedia())
                            <figure>
                                @if(substr($product->getFirstMedia()->getFullUrl(), -3) == 'svg')
                                    <img src="{{asset($product->getFirstMedia()->getFullUrl())}}" alt="{{$product->name}}">
                                @else
                                    <img src="{{asset($product->getFirstMedia()->getUrl('blade'))}}" alt="{{$product->name}}">
                                @endif
                            </figure>
                        @endif

                    </div>
                </div>

            @endforeach

        </div>
        <!-- ProductsCarousel End -->
    </div>
@endif
