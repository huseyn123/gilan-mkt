<div class="breadcrumbs">
    <div class="inner">
        {!! Breadcrumbs::render('breadcrumb', $page, $dictionary['home_page'] ?? $config['name'], isset($data) ? $data : false) !!}
    </div>
</div>
