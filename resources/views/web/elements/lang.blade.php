@if(isset($type) && $type =='mobile')

    @foreach(config("app.locales") as $key => $locale)
        <a href="{{ url($relatedPages[$key] ?? $key) }}" title="">{{$key}}</a>
        @if(!$loop->last)  <span></span> @endif
    @endforeach

@else
    <span>{{strtoupper($lang)}}<i class="fav4 fa-angle-down" aria-hidden="true"></i></span>
    <div class="lang_list">

        @foreach(config("app.locales") as $key => $locale)
            @if($key != $lang)
                <a  href="{{ url($relatedPages[$key] ?? $key) }}" >{{ strtoupper($key) }}</a>
            @endif
        @endforeach

    </div>

@endif
