@if($brands->count())
    <div class="container brands">
        <h3>{{ $dictionary['ino_brands'] ?? 'Brendlərimiz '}}</h3>
        <div class="row">
            @foreach($brands as $brand)
                @if($brand->getFirstMedia())
                    <div class="col-xs-6 col-md-6"><div class="item">
                            <a href="{{ route('showPage',$brand->slug) }}">
                                <img src="{{asset( $brand->getFirstMedia()->getFullUrl()) }}"/>
                            </a>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@endif