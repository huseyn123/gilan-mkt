<h3>{{$dictionary['similar_news']}}</h3>
@foreach($similarArticles as $similarArticle)
    <div class="box">
        <span class="date">{{ blogDate($similarArticle->published_at) }}</span>
        <h2><a href="{{ route('showPage',[$similarArticle->slug,$similarArticle->article_slug]) }}" title="{{ $similarArticle->name }}">{{ $similarArticle->name }}</a></h2>
    </div>
@endforeach


