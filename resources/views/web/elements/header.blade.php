<!-- Header Begin -->
<header id="header">
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="list-unstyled topbar_contact">
                        <li>{{$dictionary['address']}}</li>
                        <li>{{$config['contact_phone']}}</li>
                        <li>{{$config['contact_email']}}</li>
                    </ul>
                    <div class="pull-right">
                        <!-- Search -->
                        <div class="search">
                            @include('web.elements.search')
                            <span class="close"></span>
                        </div>
                        <button type="button" class="btn_search"><i class="icofont-search-1"></i></button>
                        <!-- Lang -->
                        <div class="lang">
                            @include('web.elements.lang')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header_inner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <button type="button" class="btn_menu"></button>

                    <!-- Logo -->
                    <div class="logo">
                        <a href="{{route('home')}}" title=""><img src="{{asset("images/logo.svg")}}"></a>
                    </div>
                    <!-- Menu -->
                    <nav class="menu">

                        @include('web.elements.menu', ['parent' => null,'sub' => true,'lang' => $lang, 'hidden' => [0,3],'type' => 'header'])

                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Header End -->