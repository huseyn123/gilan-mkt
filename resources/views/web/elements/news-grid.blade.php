@foreach($articles as $article)

    <div class="col-sm-4  col-xs-12">
        <div class="box">
            <a href="{{ route('showPage',[$article->page_slug,$article->slug]) }}">

                @if($article->getFirstMedia())
                    <figure>
                        <img src="{{ asset($article->getFirstMedia()->getUrl('blade')) }}"  alt="{{$article->name}}" title="{{$article->image_title}}"/>
                    </figure>
                @endif

                <div class="body">
                    <h2>{{ $article->name }}</h2>
                    <p>{{ $article->summary }}</p>
                    <span class="date">{{ blogDate($article->published_at) }}</span>
                </div>
            </a>
        </div>
    </div>

@endforeach


