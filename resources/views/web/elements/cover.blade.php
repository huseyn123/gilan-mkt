@if(isset($page) && $page->getFirstMedia('cover'))

    <figure>
        <img src="{{ asset($page->getFirstMedia('cover')->getUrl('blade'))}}"  alt="{{$page->name}}" title="{{$page->image_title}}">
    </figure>

@endif
