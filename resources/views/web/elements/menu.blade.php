@if($type == 'header')

    <ul class="list-unstyled">
        @foreach($menu as $m)
            @if($m->parent_id == $parent && !in_array($m->visible, $hidden) && $m->lang == $lang && !in_array($m->template_id,[0]))

                <li @if(in_array($m->template_id,[1])) class="sub" @endif>

                    <a href="@if($m->forward_url){{$m->forward_url}}@elseif(in_array($m->template_id,[1])) JavaScript:Void(0);  @else {{route('showPage',[$m->slug])}} @endif"  title="{{ $m->name }}" @if($m->forward_url) target="_blank" @endif>{{ $m->name}} @if(in_array($m->template_id,[1]) && $sub == true)<i class="fav4 fa-angle-down"></i>@endif</a>

                    @if($sub == true && in_array($m->template_id,[1]))
                        @include('web.elements.menu',['type' => $type,'parent' => $m->tid,'template_id' => $m->template_id,'sub' => false,'hidden' =>[0,3]])
                    @endif

                </li>

            @endif
        @endforeach
    </ul>

@elseif($type == 'footer')

    <ul class="list-unstyled">

        @foreach($menu as $m)
            @if($m->parent_id == $parent && !in_array($m->visible, $hidden) && $m->lang == $lang)
                <li><a href="@if($m->forward_url){{$m->forward_url}}@else{{ route('showPage',[$m->slug]) }} @endif"  title="{{$m->name}}"  @if($m->forward_url) target="_blank" @endif>{{$m->name}}</a></li>
            @endif
        @endforeach

    </ul>

@elseif($type === 'footer12')

    <ul class="list-unstyled">
        @foreach($enterprise_menu as $en)
            @if($en->page_id == $parent)
                <li><a href="{{ route('showPage',[$en->page_slug,$en->slug]) }}"  title="{{$en->name}}" >{{$en->name}}</a></li>
            @endif
        @endforeach

    </ul>

@elseif($type === 'footer21')

    <ul class="list-unstyled">
        @foreach($product_menu as $pm)
            @if($pm->page_id == $parent)
                <li><a href="{{ route('showPage',[$pm->page_slug,$pm->slug]) }}"  title="{{$pm->name}}">{{$pm->name}}</a></li>
            @endif
        @endforeach

    </ul>


@endif