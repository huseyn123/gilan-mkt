@if($sliders->count())

    <!-- Slider Begin -->
    <section id="slider" class="owl-carousel">
        @foreach($sliders as $slider)

            <div class="item">
                @if($slider->getFirstMedia())
                    <img src="{{$slider->getFirstMedia()->getUrl('view')}}" />
                @endif
                <div class="caption">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="inner">
                                    @if($slider->title) <h1 class="frst">{{$slider->title}}</h1> @endif
                                    @if($slider->title2) <h1 class="lst">{{$slider->title2}}</h1>@endif
                                    <p>{!! convertName($slider->summary,'/') !!}</p>
                                    @if($slider->link)
                                        <a href="{{$slider->link}}" class="btn_more" target="_blank"><span>{{$dictionary['more']}}</span></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach

    </section>
    <!-- Slider End -->

@endif