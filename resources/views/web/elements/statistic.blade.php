<!-- CountSection Begin -->
<section class="count_section" style="background: url({{asset('storage/files/index/statistics/count-section-bg.png')}}) !important;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="inner">
                        <div class="body">
                            <figure>
                                <img src="{{asset('storage/files/index/statistics/icon-count1.svg')}}" >
                            </figure>
                            <span class="count">{{$config['count1']}}</span>
                            <span class="txt">{{$dictionary['experience']}}</span>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="inner">
                        <div class="body">
                            <figure>
                                <img src="{{asset('storage/files/index/statistics/icon-count2.svg')}}">
                            </figure>
                            <span class="count">{{$config['count2']}}</span>
                            <span class="txt">{{$dictionary['technique']}}</span>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="inner">
                        <div class="body">
                            <figure>
                                <img src="{{asset('storage/files/index/statistics/icon-count3.svg')}}" >
                            </figure>
                            <span class="count">{{$config['count3']}}</span>
                            <span class="txt">{{$dictionary['enterprise']}}</span>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="inner">
                        <div class="body">
                            <figure>
                                <img src="{{asset('storage/files/index/statistics/icon-count4.svg')}}">
                            </figure>
                            <span class="count">{{$config['count4']}}</span>
                            <span class="txt">{{$dictionary['product']}}</span>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="inner">
                        <div class="body">
                            <figure>
                                <img src="{{asset('storage/files/index/statistics/icon-count5.svg')}}">
                            </figure>
                            <span class="count">{{$config['count5']}}</span>
                            <span class="txt">{{$dictionary['area']}}</span>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="inner">
                        <div class="body">
                            <figure>
                                <img src="{{asset('storage/files/index/statistics/icon-count6.svg')}}">
                            </figure>
                            <span class="count">{{$config['count6']}}</span>
                            <span class="txt">{{$dictionary['harvest']}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- CountSection End -->