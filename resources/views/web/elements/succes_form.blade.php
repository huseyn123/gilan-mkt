<div class="success_form" id="{{$id}}">
    <div class="vertical-center">
        <i class="icon"></i>
        <p>{!! $dictionary['succes_message'] ?? 'Müraciətiniz qeydə alındı.<br>Sizinlə tezliklə əlaqə yaradılacaq' !!}</p>
    </div>
    <span class="back">{{$dictionary['go_back']}}</span>
</div>
