@foreach($products as $product)

    <div class="col-sm-6">
        <div class="product_box clr_green" @if($product->color_code) style="background:{{$product->color_code}} !important;" @endif>
            <div class="caption">
                <h2 @if($product->text_color_code) style="color:{{$product->text_color_code}} !important;" @endif>{{$product->name}}</h2>
                <p>{{$product->summary}}</p>
                <a href="{{route('showPage',[$product->page_slug,$product->slug])}}" title="{{$product->name}}" class="btn_more" @if($product->text_color_code) style="color:{{$product->text_color_code}} !important;" @endif><span>{{$dictionary['more']}}</span></a>
            </div>
            @if($product->getFirstMedia())
                <figure>
                    @if(substr($product->getFirstMedia()->getFullUrl(), -3) == 'svg')
                        <img src="{{asset($product->getFirstMedia()->getFullUrl())}}" alt="{{$product->name}}"  alt="{{$product->name}}" title="{{$product->image_title}}">
                    @else
                        <img src="{{asset($product->getFirstMedia()->getUrl('blade'))}}" alt="{{$product->name}}"  alt="{{$product->name}}" title="{{$product->image_title}}">
                    @endif
                </figure>
            @endif
        </div>
    </div>

@endforeach