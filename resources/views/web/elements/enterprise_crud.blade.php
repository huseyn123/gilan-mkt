@foreach($enterprises as $enterprise)
    @if($enterprise->getFirstMedia())
        <div class="{{$enterprise->linear == 1 ? 'col-md-8 col-xs-12' : 'col-md-4  col-xs-6' }}">
            <div class="box">
                <img src="{{asset($enterprise->getFirstMedia()->getUrl('blade'))}}" alt="{{$enterprise->name}}" alt="{{$enterprise->name}}" title="{{$enterprise->image_title}}">
                <div class="inner">
                    <h2>{{ $enterprise->name }}</h2>
                    <p>{!! enterprise_summary($enterprise->summary,'/') !!}</p>
                    <a href="{{route('showPage',[$enterprise->page_slug,$enterprise->slug])}}"class="btn_more_empty"><span>{{$dictionary['more']}}</span></a>
                </div>
            </div>
        </div>
    @endif
@endforeach