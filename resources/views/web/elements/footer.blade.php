    @foreach($menu as $m)

        @if(($m->lang == $lang) &&  (in_array($m->visible, [1,3])))

            @if($m->menu_children_count != 0 || in_array($m->template_id,[12,21]))
               @php
                    $footer_menu1[] = $m;
               @endphp
            @elseif(is_null($m->parent_id))
               @php
                    $footer_menu2[] = $m;
                @endphp
            @endif
        @endif

    @endforeach


<!-- Footer Begin -->
<footer id="footer">
    <div class="container">
        <div class="row">

            @if(isset($footer_menu1) && count($footer_menu1) > 0)

                @foreach($footer_menu1 as $fm1)
                        <div class="col-sm-3 col-xs-6 ">
                            <nav class="footer_menu">
                                <h4>{{$fm1->name}}</h4>
                                @include('web.elements.menu', ['parent' => $fm1->tid,'sub' => true,'lang' => $fm1->lang, 'hidden' => [0,2],'type' =>in_array($fm1->template_id,[12,21]) ? 'footer'.$fm1->template_id : 'footer'])
                            </nav>
                        </div>
                        @if($loop->iteration == 4)<div class="break_col"></div> @endif

                @endforeach
            @endif

            <div class="break_col visible-xs"></div>
            <div class="col-sm-3 col-xs-6">

                @if(isset($footer_menu2) && count($footer_menu2) > 0)
                    <nav class="footer_menu mb0">
                        @foreach($footer_menu2 as $fm2)
                            <h4><a href="@if($fm2->forward_url){{$fm2->forward_url}}@else{{route('showPage',[$fm2->slug])}}@endif" title="{{$fm2->name}}"  @if($fm2->forward_url)target="_blank"@endif> {{$fm2->name}} </a></h4>
                         @endforeach
                    </nav>
                @endif

                <!-- FooterSocial -->
                <nav class="footer_social">
                    @include('web.elements.social')
                </nav>
            </div>
            <div class="col-xs-12">
                <div class="bottom clearfix">
                    <p class="copyright">&copy; {{ $dictionary['copyright'] }} &copy; {{date('Y')}}</p>
                    <p class="author">{!! trans('locale.site_by', ['site' => '<a class="marcom" href="http://marcom.az" target="_blank">Marcom</a>']) !!}</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->
