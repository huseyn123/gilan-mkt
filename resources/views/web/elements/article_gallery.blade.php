@foreach($galleries as $gallery)
    <figure>
        <a href="{{ asset($gallery->getUrl('blade')) }}" class="zoom-photo"><img src="{{ asset($gallery->getUrl('blade')) }}"></a>
    </figure>
@endforeach
