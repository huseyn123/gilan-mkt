@extends ('layouts.web', ['page_heading' => $tender->name] )

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs',['data' => $tender])
                    <!-- BreadCrumbs End -->

                    <!-- TenderDetail Begin -->
                    <div class="tender_detail">
                        <h1 class="title">{{$tender->name}}</h1>
                        <div class="text">
                           {!! $tender->content !!}
                        </div>
                    </div>
                    <!-- TenderDetail End -->
                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->


@endsection

