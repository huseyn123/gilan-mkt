@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs')
                    <!-- BreadCrumbs End -->


                    <!-- About Begin -->
                    <div class="about">
                        <!-- HeadTitle Begin -->
                        <div class="head_title">
                            <h1>{{$dictionary['about_title']}}</h1>
                        </div>
                        <!-- HeadTitle End -->
                        <!-- AboutText Begin -->
                        <div class="about_text">
                            {!! $page->content !!}
                        </div>
                        <!-- AboutText End -->

                        @if($page->lang_media)
                            <a href="{{$page->lang_media->getFullUrl()}}" class="pdf_download" download>{{$dictionary['about_file_download']}}</a>
                        @endif
                        <!-- PDFDownload -->
                    </div>
                    <!-- About End -->
                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->

@endsection
