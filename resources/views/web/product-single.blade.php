@extends ('layouts.web', ['page_heading' => $product->name,'other_page'=>$product])

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- BreadCrumbs Begin -->
                    @include('web.elements.breadcrumbs',['data' => $product])
                    <!-- BreadCrumbs End -->

                    @include('web.products.product'.$product->template_id)

                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->


@endsection
