@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs')
                    <!-- BreadCrumbs End -->

                    <!-- Leader's Appeal Begin -->
                    <div class="leader_appeal">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="leader_info">
                                    @if($page->getFirstMedia())
                                        <figure>
                                            <img src="{{$page->getFirstMedia()->getUrl('blade')}}" alt="{{$page->name}}" title="{{$page->image_title}}"/>
                                        </figure>
                                    @endif
                                    <h2>{{$dictionary['company_leader'] ?? ''}}</h2>
                                    <span>{{$dictionary['head_of_company'] ?? ''}}</span>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <h1 class="title">{{$page->seo_title ? $page->seo_title:  $page->name}}</h1>
                                <div class="text">
                                    {!! $page->content !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Leader's Appeal End -->
                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->

@endsection