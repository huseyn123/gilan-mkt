{!! Form::open(['url'=>route('web.contact'), 'method'=>'POST', 'id' => 'product_offer_survey','files' => true]) !!}

    <div class="row">
            <div class="col-md-4">{!! Form::text('full_name', null, ["placeholder" => '*'.$dictionary['full_name'],'required' => 'required']) !!}</div>
            <div class="col-md-4">{!! Form::text('phone', null, ["placeholder" => '*'.$dictionary['phone'],'class' => 'phonenumber','required' => 'required']) !!}</div>
            <div class="col-md-4">{!! Form::email('email', null, ["placeholder" => '*'.$dictionary['email'],'required' => 'required']) !!}</div>
    </div>
    <div class="row">
        <div class="col-md-4">{!! Form::text('company_name', null, ["placeholder" => $dictionary['company_name'] ?? '* Firma adı ','required' => 'required']) !!}</div>
        <div class="col-md-4">{!! Form::text('brend_name', null, ["placeholder" => $dictionary['brend_name'] ?? '* Marka adı','required' => 'required']) !!}</div>
        <div class="col-md-4">{!! Form::text('product_name', null, ["placeholder" => $dictionary['product_name'] ?? '* Məhsul adı','required' => 'required']) !!}</div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::textarea('message', null, ["placeholder" => $dictionary['disclosure'] ?? '* Açıqlama','rows' => "9",'required' => 'required']) !!}
            {{ Form::hidden('method', 'product_offer_survey') }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="file_input">
                {!! Form::text(null, null, ['required' => 'required']) !!}
                <span class="button">{{$dictionary['insert_foto'] ?? '* Şəkil əlavə et'}}</span>
                {!! Form::file('image',['required' => 'required']) !!}
            </div>
            <div class="helper">{{$dictionary['image_format'] ?? 'Şəkilləri zip,pdf,png kimi əlavə edə bilərsiz'}}</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            @include('web.elements.error-message')
            {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
        </div>
    </div>

{!! Form::close() !!}