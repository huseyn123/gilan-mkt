{!! Form::open(['url'=>route('web.contact'), 'method'=>'POST','id' => 'comment_suggestion_form']) !!}
    <div class="item">
        <div class="row">
            <div class="col-sm-6 col-xs-12 xs-mb-20">
                {!! Form::text('full_name', null, ["class" => "ipt_style","placeholder" => $dictionary['full_name'],'required' => 'required']) !!}
            </div>
            <div class="col-sm-6 col-xs-12">
                {!! Form::text('phone', null, ['class' => 'ipt_style phonenumber',"placeholder" => '*'.$dictionary['phone'],'required' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="item">
        <div class="row">
            <div class="col-sm-6 col-xs-12 xs-mb-20">
                {!! Form::email('email', null, ['class' => 'ipt_style',"placeholder" => '*'.$dictionary['email'],'required' => 'required']) !!}
            </div>
            <div class="col-sm-6 col-xs-12">
                {{ Form::hidden('method', 'comment_suggestion') }}
                {!! Form::text('subject', null, ['class' => 'ipt_style',"placeholder" => '*'.$dictionary['subject'],'required' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="item clearfix">
        <span class="checkbox">
            {!! Form::radio('comment_suggestion',$dictionary['comment'] ?? 'İrad',true, ['id' => $dictionary['comment'] ?? 'İrad',"class" => 'css-checkbox']) !!}
            {!! Form::label($dictionary['comment'] ?? 'İrad', $dictionary['comment'] ?? 'İrad',['class' => 'css-label']) !!}
        </span>
        <span class="checkbox">
            {!! Form::radio('comment_suggestion',$dictionary['suggestion'] ?? 'Təklif',false, ['id' => $dictionary['suggestion'] ?? 'Təklif',"class" => 'css-checkbox']) !!}
            {!! Form::label($dictionary['suggestion'] ?? 'Təklif', $dictionary['suggestion'] ?? 'Təklif',['class' => 'css-label']) !!}
        </span>
    </div>
    <div class="item">
        {!! Form::textarea('message', null, ['class' => 'ipt_style h150',"placeholder" => $dictionary['message'],'required' => 'required']) !!}
    </div>
    <div class="item clearfix">
        @include('web.elements.error-message')
        {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
    </div>
{!! Form::close() !!}

@include('web.elements.succes_form', ['id' => 'comment_suggestion_form_succes'])
