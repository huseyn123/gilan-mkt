<h2>{{$dictionary['contact_form']}}</h2>
{!! Form::open(['url'=>route('web.contact'), 'method'=>'POST', 'id' => 'product_form']) !!}
    <div class="item">
        <div class="row">
            <div class="col-sm-6 col-xs-12  xs-mb-20">
                {!! Form::text('full_name', null, ["class" => "ipt_style","placeholder" => $dictionary['full_name'],'required' => 'required']) !!}
            </div>
            <div class="col-sm-6 col-xs-12  xs-mb-20">
                {!! Form::text('phone', null, ['class' => 'ipt_style phonenumber',"placeholder" => '*'.$dictionary['phone'],'required' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="item">
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                {!! Form::email('email', null, ['class' => 'ipt_style',"placeholder" => '*'.$dictionary['email'],'required' => 'required']) !!}
            </div>
            <div class="col-sm-6 col-xs-12">
                {{ Form::hidden('method', 'product') }}
                {{ Form::hidden('product_name',$product->name) }}
                {!! Form::text('subject', null, ['class' => 'ipt_style',"placeholder" => '*'.$dictionary['subject'],'required' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="item">
        {!! Form::textarea('message', null, ['class' => 'ipt_style h150',"placeholder" => $dictionary['message'],'required' => 'required']) !!}
    </div>
    <div class="item clearfix">
        @include('web.elements.error-message')
        {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
    </div>

{!! Form::close() !!}

@include('web.elements.succes_form', ['id' => 'product_form_succes'])

