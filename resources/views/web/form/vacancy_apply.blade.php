{!! Form::open(['url'=>route('web.contact'), 'method'=>'POST', 'id' => 'vacancy_apply','files' => true]) !!}

<div class="row">
    <div class="col-md-4">{!! Form::text('full_name', null, ["placeholder" => '*'.$dictionary['full_name'],'required' => 'required']) !!}</div>
    <div class="col-md-4">{!! Form::text('phone', null, ["placeholder" => '*'.$dictionary['phone'],'class' => 'phonenumber','required' => 'required']) !!}</div>
    <div class="col-md-4">{!! Form::email('email', null, ["placeholder" => '*'.$dictionary['email'],'required' => 'required']) !!}</div>
    {{ Form::hidden('method', 'vacany_apply') }}
    {{ Form::hidden('position',$vacancy->position) }}
</div>
<div class="row">
    <div class="col-md-12">
        <div class="file_input">
            {!! Form::text(null, null, ['required' => 'required']) !!}
            <span class="button">{{$dictionary['insert_cv'] ?? '* Cv  əlavə et'}}</span>
            {!! Form::file('cv',['required' => 'required']) !!}
        </div>
        <div class="helper">{{$dictionary['cv_formats'] ?? 'Cv-ni png,zip,pdf,docx kimi əlavə edə bilərsiz'}}</div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        @include('web.elements.error-message')
        {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
    </div>
</div>

{!! Form::close() !!}
