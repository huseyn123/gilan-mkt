<div class="form pt30">
    <h1>{{$dictionary['contact_form']}}</h1>
    {!! Form::open(['url'=>route('web.contact'), 'method'=>'POST', 'id' => 'enterprise_form']) !!}

        <div class="item">
            {!! Form::text('full_name', null, ["class" => "ipt_style","placeholder" => $dictionary['full_name'],'required' => 'required']) !!}
        </div>
        <div class="item">
            {!! Form::text('phone', null, ['class' => 'ipt_style phonenumber',"placeholder" => '*'.$dictionary['phone'],'required' => 'required']) !!}
        </div>
        <div class="item">
            {!! Form::email('email', null, ['class' => 'ipt_style',"placeholder" => '*'.$dictionary['email'],'required' => 'required']) !!}
        </div>
        <div class="item">
            {{ Form::hidden('method', 'enterprise') }}
            {{ Form::hidden('enterprise_name',$enterprise->name) }}
            {!! Form::text('subject', null, ['class' => 'ipt_style',"placeholder" => '*'.$dictionary['subject'],'required' => 'required']) !!}
        </div>
        <div class="item">
            {!! Form::textarea('message', null, ['class' => 'ipt_style h150',"placeholder" => $dictionary['message'],'required' => 'required']) !!}
        </div>
        <div class="item clearfix">
            {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
        </div>
    @include('web.elements.error-message')

    {!! Form::close() !!}
</div>
<!-- Form End -->

<!-- SuccessForm Begin -->
@include('web.elements.succes_form', ['id' => 'enterprise_form_succes'])
<!-- SuccessForm End -->