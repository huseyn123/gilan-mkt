@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs')
                    <!-- BreadCrumbs End -->

                    <!-- HeadTitle Begin -->
                    <div class="head_title">
                        <h1 class="kv_title">{{$dictionary[$type.'_seo_title']}}</h1>
                    </div>
                    <!-- HeadTitle End -->

                    <!-- PageMenu Begin -->
                    <nav class="page_menu clearfix">
                        @foreach(config('config.kiv_types') as $key => $value)

                            <a @if($type == $key) class="active" @endif href="{{route('showPage',[$page->slug])}}?type={{$key}}">{{$dictionary['company_'.$key] ?? $key}}</a>

                        @endforeach

                    </nav>
                    <!-- PageMenu End -->

                    @include('web.kiv.kiv-'.$type)

                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->

@endsection
