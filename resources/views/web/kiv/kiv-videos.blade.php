<!-- CompanyVideos Begin -->
<div class="company_videos">
    <div class="row">

        @foreach($kivs as $kiv)

            <div class="col-md-3 col-sm-4 col-xs-6 col-mob-12">
            <div class="box">
                <a  @if($kiv->youtube_id) href="{{$kiv->youtube_id}}"  class="zoom-video"  @endif>

                    @if($kiv->getFirstMedia())
                        <figure>
                            <img src="{{asset($kiv->getFirstMedia()->getUrl('blade'))}}">
                        </figure>
                    @endif

                    <div class="body">
                        <h2>{{$kiv->title}}</h2>
                    </div>

                </a>
            </div>
        </div>

         @endforeach

    </div>
</div>
<!-- CompanyVideos End -->