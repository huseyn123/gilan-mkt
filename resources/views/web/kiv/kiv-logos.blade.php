<!-- CompanyLogos Begin -->
<div class="company_logos">
    <div class="row">

        @foreach($kivs as $kiv)

            <div class="col-md-3 col-sm-4 col-xs-6 col-mob-12">
                <div class="box">

                    @if($kiv->getFirstMedia('logo_image'))
                        <figure>
                            <img src="{{ asset($kiv->getFirstMedia('logo_image')->getFullUrl()) }}" alt="{{$dictionary['company_logos'] ?? 'logos'}}" title="{{$kiv->image_title}}">
                        </figure>
                    @endif

                    <div class="types clearfix">
                        @if($kiv->getFirstMedia('eps_file'))<a href="{{asset($kiv->getFirstMedia('eps_file')->getFullUrl())}}" download>EPS</a>@endif
                        @if($kiv->getFirstMedia('png_file'))
                          <span class="line"></span>
                          <a href="{{asset($kiv->getFirstMedia('png_file')->getFullUrl())}}" download>PNG</a>
                        @endif
                    </div>
                </div>
            </div>

        @endforeach

    </div>
</div>
<!-- CompanyLogos End -->