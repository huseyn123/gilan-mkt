<!-- CompanyGallery Begin -->
<div class="company_gallery">
    <div class="row">

        @foreach($page->getMedia('gallery') as $gallery)

            <div class="col-md-3 col-sm-4 col-xs-6 col-mob-12">
                <div class="box">
                    <figure>
                        <img src="{{asset($gallery->getUrl('blade'))}}">
                    </figure>
                    <div class="body">
                        <a href="{{$gallery->getUrl('blade')}}" download>{{$dictionary['download']}}</a>
                    </div>
                </div>
            </div>

        @endforeach

    </div>
</div>
<!-- CompanyGallery End -->
