@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs')
                    <!-- BreadCrumbs End -->

                    <div class="row pt10">
                        <div class="col-sm-6 col-xs-12">
                            <!-- Conditions Begin -->
                            <div class="conditions">
                                <h1 class="title">{{$dictionary['conditions']}}</h1>
                                {!! $page->content !!}
                            </div>
                            <!-- Conditions End -->
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <!-- Form Begin -->
                            <div class="form pt30">
                                @include('web.form.wholesale')
                            </div>
                            <!-- Form End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->


@endsection

@push('scripts')
    <script>
        $('.conditions ul').addClass('list-unstyled')
    </script>
@endpush