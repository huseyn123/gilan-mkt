@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs')
                    <!-- BreadCrumbs End -->

                    <!-- Tenders Begin -->
                    <div class="tenders">
                        <!-- HeadTitle Begin -->
                        <div class="head_title small mb20">
                            <h1>{{$page->name}}</h1>
                        </div>
                        <!-- HeadTitle End -->
                        <!-- Table Begin -->
                        <div class="table_style responsive-table">
                            <table>
                                <thead>
                                <tr>
                                    <td class="num">#</td>
                                    <td>{{$dictionary['tenders_name']}}</td>
                                    <td>{{$dictionary['tenders_published_at']}}</td>
                                    <td>{{$dictionary['tenders_end_date']}}</td>
                                    <td>{{$dictionary['more']}}</td>
                                </tr>
                                </thead>

                                <tbody>

                                    @foreach($tenders as $tender)
                                        <tr>
                                            <td class="num">{{$loop->iteration}}</td>
                                            <td>{{$tender->name}}</td>
                                            <td>{{blogDate($tender->start_date)}}</td>
                                            <td>{{blogDate($tender->end_date)}}</td>
                                            <td class="btn_td"><a href="{{route('showPage',[$page->slug,$tender->slug])}}" title="" class="more">{{$dictionary['details']}}</a></td>
                                        </tr>
                                    @endforeach

                                </tbody>

                            </table>
                        </div>
                        <!-- Table End -->
                    </div>
                    <!-- Tenders End -->


                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->



@endsection