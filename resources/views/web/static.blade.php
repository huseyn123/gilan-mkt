@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    <!-- PageWrapper Begin -->
    <section class="page_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- BreadCrumbs Begin -->
                        @include('web.elements.breadcrumbs')
                    <!-- BreadCrumbs End -->

                    <!-- GreyTextBlock Begin -->
                    <div class="grey_text_block mt10">
                        <div class="text_block">
                            <h1 class="title">{{$page->name}}</h1>
                            <div class="text">
                               {!! $page->content !!}
                            </div>
                        </div>
                    </div>
                    <!-- GreyTextBlock End -->
                </div>
            </div>
        </div>
    </section>
    <!-- PageWrapper End -->

@endsection