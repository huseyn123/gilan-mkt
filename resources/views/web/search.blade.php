@extends ('layouts.web', ['page_heading' => $dictionary['search_page'] ?? 'Axtarışın nəticələri'])

@section ('content')

	<!-- PageWrapper Begin -->
	<section class="page_wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<!-- BreadCrumbs Begin -->
					<div class="breadcrumbs">
						<div class="inner">
							<a href="{{ route('home') }}">Ana səhifə</a>
							<i class="fav4 fa-angle-right"></i>
							<span>Axtarışın nəticəsi</span>
						</div>
					</div>
					<!-- BreadCrumbs End -->

					<!-- PageSearch Begin -->
					<div class="page_search">
						<div class="row">
							<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
								@include('web.elements.search')
							</div>
						</div>
						<span class="search_word">"{{$keyword}}"</span>
						<span class="result_count">{{$pages->total()}} nəticə tapıldı</span>
					</div>
					<!-- PageSearch End -->
					<!-- SearchResults Begin -->
					<div class="search_results">
						<div class="row">

							@foreach($pages as $page)
								<div class="col-sm-6 col-xs-12">
									<div class="box">
										<h2><a href="{{route('showPage',$page->slug)}}" title="{{$page->name}}">{{$page->name}}</a></h2>
										@if($page->summary) <p>{{str_limit( $page->summary, 200) }}</p> @endif
										<div class="bottom">
											<a href="{{route('showPage',$page->slug)}}" title="">Ətraflı<i class="fav4 fa-angle-right"></i></a>
										</div>
									</div>
								</div>
							@endforeach
						</div>
					</div>
					<!-- SearchResults End -->


					<!-- Pagination Begin -->
						{!! $pages->appends(request()->input())->links('vendor.pagination.news') !!}
					<!-- Pagination End -->
				</div>
			</div>
		</div>
	</section>
	<!-- PageWrapper End -->

@endsection



