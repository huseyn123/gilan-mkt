@component('mail::message')


    İstifadəçinin məlumatları:

    @if(isset($content['full_name']))
        - Ad, soyad: {{$content['full_name']}}
    @endif

    @if(isset($content['name']))
        - Ad: {{$content['name']}}
    @endif

    @if(isset($content['phone']))
        - Telefon: {{$content['phone']}}
    @endif

    @if(isset($content['surname']))
        - Soyad: {{$content['surname']}}
    @endif

    @if(isset($content['email']))
        - Email: {{$content['email']}}
    @endif

    @if(isset($content['subject']))
        - Mövzu: {{$content['subject']}}
    @endif

    @if(isset($content['comment_suggestion']))
        - İrad və ya təklif: {{$content['comment_suggestion']}}
    @endif

    @if(isset($content['product_name']))
        - Məhsul adı: {{$content['product_name']}}
    @endif

    @if(isset($content['enterprise_name']))
        - Müəssisə adı: {{$content['enterprise_name']}}
    @endif

    @if(isset($content['address']))
        - Təklif etdiyiniz yerin ünvanı: {{$content['address']}}
    @endif

    @if(isset($content['area']))
        - Yerin sahəsiı: {{$content['area']}}
    @endif

    @if(isset($content['depot']))
        - Anbar sahəsi: {{$content['depot']}}
    @endif

    @if(isset($content['parking']))
        - Avtopark: {{$content['parking']}}
    @endif

    @if(isset($content['shop']))
        - Market: {{$content['shop']}}
    @endif


    @if(isset($content['s_shop_price']))
        - Mağazalarımızda qiymətlər sizin fikrinizcə necədir?: {{$content['s_shop_price']}}
    @endif

    @if(isset($content['s_product_type']))
        - Məhsul çeşidlərindən razısınızmı?: {{$content['s_product_type']}}
    @endif

    @if(isset($content['s_product_quality']))
        - Məhsul keyfiyyəti və təzəliyi necədir?: {{$content['s_product_quality']}}
    @endif

    @if(isset($content['s_product_find']))
        - Rəflərdən istədiyiniz məhsulu rahatlıqla tapa bilirsinizmi?: {{$content['s_product_find']}}
    @endif

    @if(isset($content['s_product_followed']))
        - Aylıq kataloqlarımızı izləyirsinizmi?: {{$content['s_product_followed']}}
    @endif

    @if(isset($content['s_shop_employee']))
        - İşçilərimiz sizə qarşı nəzakətlidirlərmi?: {{$content['s_shop_employee']}}
    @endif

    @if(isset($content['s_shop_till']))
        - Kassalarda gözləmə necədir: {{$content['s_shop_till']}}
    @endif



    @if(isset($content['ads_type']))
        - Reklam Növü: {{$content['ads_type']}}
    @endif

    @if(isset($content['f_type']))
        - Müraciət növü: {{$content['f_type']}}
    @endif

    @if(isset($content['message']))
        - Mesaj: {{$content['message']}}
    @endif


@endcomponent