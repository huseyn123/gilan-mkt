@if ($paginator->hasPages())
    <!-- Pagination Begin -->
    <nav class="pag">
        <ul class="list-unstyled">

            {{-- Previous Page Link --}}
            <li class="disabled prev"><a   @if (!$paginator->onFirstPage()) href="{{ $paginator->previousPageUrl() }}"   @endif><i class="fav4 fa-angle-left"></i></li>


            @foreach ($elements as $element)
                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active"><a >{{ $page }}</a></li>
                        @else
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            <li class="next"><a @if ($paginator->hasMorePages()) href="{{ $paginator->nextPageUrl() }}"  @endif><i class="fav4 fa-angle-right"></i></a></li>

        </ul>
    </nav>
    <!-- Pagination End -->


@endif


