@extends('layouts.guest')
@section('title', trans('admin.password_reset'))

@section('content')

    <div class="login-box">
        <div class="login-box-body">
            @include('admin.components.login-logo')
            <p class="login-box-msg">{{ trans('admin.password_reset') }}</p>
            <form method="post" action="{{ route('admin.password.email') }}">
                @csrf
                <div class="form-group has-feedback @if($errors->has('email')) has-error @endif">
                    <input type="email" placeholder="Email" name="email" autofocus="autofocus" class="form-control" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <label>{{ $errors->first('email') }}</label>
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-2"></div>
                    <div class="col-xs-8">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('admin.password_reset_link') }}</button>
                    </div>
                    <div class="col-xs-2"></div>
                </div>
            </form>
            <a href="{{ route('admin.login') }}">{{ trans('admin.login') }}</a><br>
        </div>
    </div>

@endsection
