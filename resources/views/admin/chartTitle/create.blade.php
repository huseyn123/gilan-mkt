@extends ('layouts.modal', ['script' => true, 'editor' => true])
@section ('title', $create_title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        @include('widgets.lang-tab', ['input' => 'text', 'name' => 'title','tab_title' => 'Ad'])

        {!! $fields !!}


    </div>

@endsection
