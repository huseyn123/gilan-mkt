@extends('layouts.modal', ['route' => $route, 'method' => 'PUT','script' => true, 'editor' => true])
@section('title', $data->name ?? $title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        @include('widgets.lang-tab', ['input' => 'text', 'info' => $data,'name' => 'title'])

        {!! $fields !!}


    </div>


@endsection
