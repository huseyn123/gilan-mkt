@extends ('layouts.modal', ['script' => true, 'editor' => true])
@section ('title', $create_title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        {!! $fields !!}

        @include('widgets.lang-tab', ['input' => 'text', 'name' => 'title','tab_title' => 'Ad'])

        @include('widgets.lang-tab', ['input' => 'textarea', 'name' => 'content','tab_title' => 'Məzmun','editor' => 1])


    </div>

@endsection

@push('scripts')
    <script>
            CKEDITOR.replace('editor-az');
            CKEDITOR.replace('editor-en');
            CKEDITOR.replace('editor-ru');
    </script>
@endpush