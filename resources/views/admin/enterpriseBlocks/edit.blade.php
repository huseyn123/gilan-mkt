@extends('layouts.modal', ['route' => $route, 'method' => 'PUT','script' => true, 'editor' => true])
@section('title', $data->name ?? $title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        {!! $fields !!}

        @include('widgets.lang-tab', ['input' => 'text', 'info' => $data,'name' => 'title'])

        @include('widgets.lang-tab', ['input' => 'textarea', 'info' => $data,'name' => 'content','editor' => true])


    </div>


@endsection


@push('scripts')
    <script>
        CKEDITOR.replace('editor-az');
        CKEDITOR.replace('editor-en');
        CKEDITOR.replace('editor-ru');
    </script>
@endpush
