<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <div class="user-panel">
        <div class="pull-left image">
            @include('widgets.profile-picture', ['class' => 'img-circle'])
        </div>
        <div class="pull-left info">
            <p>{{ auth()->guard('admin')->user()->name }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search..." autocomplete="off">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
          </span>
        </div>
    </form>
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
        <li {{ activeUrl(route('admin.dashboard')) }}><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> <span>Dashboard</span></a></li>
        <li {{ activeUrl(route('admins.index')) }}><a href="{{ route('admins.index') }}"><i class="fa fa-user-plus fa-fw"></i> <span>Adminlər</span></a></li>
        <li {{ activeUrl(route('slider.index')) }}><a href="{{ route('slider.index') }}"><i class="fa fa-image fa-fw"></i> <span>Slider</span></a></li>
        <li class="treeview">
            <a href="#"><i class="fa fa-file fa-fw"></i> <span>Səhifələr</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(route('page.index')) }}><a href="{{ route('page.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Səhifələr</span></a></li>
                <li {{ activeUrl(route('pageTranslation.order')) }}><a href="{{ route('pageTranslation.order') }}?lang=az"><i class="fa fa-circle-o fa-fw"></i> <span>Ardıcıllıq</span></a></li>
            </ul>
        </li>

        <li {{ activeUrl(route('articles.index')) }}><a href="{{ route('articles.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Xəbərlər</span></a></li>

        <li class="treeview">
            <a href="#"><i class="fa fa-circle"></i> <span>Məhsullar</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(route('products.index')) }}><a href="{{ route('products.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Məhsullar</span></a></li>
                <li {{ activeUrl(route('productBlocks.index')) }}><a href="{{ route('productBlocks.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Blok</span></a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#"><i class="fa fa-circle"></i> <span>Chart</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(route('chartTitle.index')) }}><a href="{{ route('chartTitle.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>ChartTitle</span></a></li>
                <li {{ activeUrl(route('chartTitleLineX.index')) }}><a href="{{ route('chartTitleLineX.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>ChartTitleLineX</span></a></li>
                <li {{ activeUrl(route('chartStatistic.index')) }}><a href="{{ route('chartStatistic.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Statistika</span></a></li>
                <li {{ activeUrl(route('mahlic_chart.index')) }}><a href="{{ route('mahlic_chart.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>MahlicChart</span></a></li>
                <li {{ activeUrl(route('chartTitle.order')) }}><a href="{{ route('chartTitle.order') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Chart-Title-Ardıcıllıq</span></a></li>

            </ul>
        </li>

        <li {{ activeUrl(route('branch_maps.index')) }}><a href="{{ route('branch_maps.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Filial</span></a></li>

        <li class="treeview">
            <a href="#"><i class="fa fa-circle"></i> <span>Müəssisələr</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(route('enterprises.index')) }}><a href="{{ route('enterprises.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Müəssisələr</span></a></li>
                <li {{ activeUrl(route('enterpriseBlocks.index')) }}><a href="{{ route('enterpriseBlocks.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Blok</span></a></li>
                <li {{ activeUrl(route('enterpriseTranslation.order')) }}><a href="{{ route('enterpriseTranslation.order') }}?lang=az"><i class="fa fa-circle-o fa-fw"></i> <span>Ardıcıllıq</span></a></li>
            </ul>
        </li>


        <li {{ activeUrl(route('tenders.index')) }}><a href="{{ route('tenders.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Tenderlər</span></a></li>

        <li {{ activeUrl(route('certificate.index')) }}><a href="{{ route('certificate.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Sertifikatlar</span></a></li>

        <li {{ activeUrl(route('faq.index')) }}><a href="{{ route('faq.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Faq</span></a></li>

        <li class="treeview">
            <a href="#"><i class="fa fa-cogs fa-fw"></i> <span>Kiv</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(route('kivLogo.index')) }}><a href="{{ route('kivLogo.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Şirkətin loqosu</span></a></li>
                <li {{ activeUrl(route('kivVideo.index')) }}><a href="{{ route('kivVideo.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Reklam çarxları</span></a></li>
            </ul>
        </li>

        {{--<li class="treeview">--}}
            {{--<a href="#"><i class="fa fa-circle-o fa-fw"></i> <span>Əhatə Dairəmiz</span>--}}
                {{--<span class="pull-right-container">--}}
                    {{--<i class="fa fa-angle-left pull-right"></i>--}}
                {{--</span>--}}
            {{--</a>--}}
            {{--<ul class="treeview-menu">--}}
                {{--<li {{ activeUrl(route('branch.index')) }}><a href="{{ route('branch.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Əhatə dairəsi</span></a></li>--}}
                {{--<li {{ activeUrl(route('region.index')) }}><a href="{{ route('region.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Region</span></a></li>--}}
                {{--<li {{ activeUrl(route('network.index')) }}><a href="{{ route('network.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Topdançı</span></a></li>--}}
            {{--</ul>--}}
        {{--</li>--}}

        <li {{ activeUrl(route('subscribers.index')) }}><a href="{{ route('subscribers.index') }}"><i class="fa fa-users fa-fw"></i> <span>İzləyicilər</span></a></li>
        <li {{ activeUrl(route('dictionary.index')) }}><a href="{{ route ('dictionary.index') }}"><i class="fa fa-text-height fa-fw"></i> <span>Lüğət</span></a></li>
        <li {{ activeUrl(route('partners.index')) }}><a href="{{ route('partners.index') }}"><i class="fa fa-briefcase fa-fw"></i> <span>Tərəfdaşlar</span></a></li>

        <li class="treeview">
            <a href="#"><i class="fa fa-cogs fa-fw"></i> <span>Advanced</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(url('api/filemanager')) }}><a href="{{ url('api/filemanager') }}"><i class="fa fa-circle-o fa-fw"></i> <span>File Manager</span></a></li>
                <li {{ activeUrl(route('config.index')) }}><a href="{{ route('config.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Konfiqurasiya</span></a></li>
                <li {{ activeUrl(route('analytic.index')) }}><a href="{{ route('analytic.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Google Analytics</span></a></li>
                <li {{ activeUrl(route('sitemap.index')) }}><a href="{{ route('sitemap.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Sitemap</span></a></li>
            </ul>
        </li>
    </ul>
    <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->