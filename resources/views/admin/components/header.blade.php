<!-- Header Navbar -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    @include('widgets.profile-picture', ['class' => 'user-image'])
                    <span class="hidden-xs">{{ Auth::guard('admin')->user()->name }}</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- The user image in the menu -->
                    <li class="user-header">
                        @include('widgets.profile-picture', ['class' => 'img-circle'])
                        <p>{{ Auth::guard('admin')->user()->name }}<small>Qeydiyyat tarixi: {{ Auth::guard('admin')->user()->created_at }}</small></p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="{{ route($profileRoute) }}" class="btn btn-default btn-flat">{{ trans('admin.profile') }}</a>
                        </div>
                        <div class="pull-right">

                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">
                                {{ trans('admin.logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ url('/') }}" target="_blank"><i class="fa fa-globe"></i></a>
            </li>
        </ul>
    </div>
</nav>