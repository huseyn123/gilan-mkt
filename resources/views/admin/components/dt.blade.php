<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                @if(isset($tabs) && is_array($tabs))
                    <ul class="nav nav-pills pull-left">
                    </ul>
                @endif

                @if(isset($create))
                    <div class="box-header pull-right">
                        <h3 class="box-title"><button class="btn btn-success btn-block btn-flat open-modal-dialog" data-link="{{ route($create) }}" data-large="{{ $largeModal ?? false }}"><span class="fa fa-plus"></span> {{ trans('locale.create') }}</button></h3>
                    </div>
                @endif

                @if(isset($locale) && count(config('app.locales')) > 1)
                    <div class="box-header pull-right">
                        {!! Form::select('lang', array_merge(config('app.locales'), ['all' => 'Bütün']), array_first(config('app.locales')), ['id' => 'change-lang', 'class' => 'form-control']) !!}
                    </div>
                @endif

                @if(isset($filter))
                    <div class="box-header pull-right">
                        {!! Form::select('type', $filter, 1, ['id' => 'filter-type', 'class' => 'form-control']) !!}
                    </div>
                @endif

                @if(isset($custom_filter))
                    <div class="box-header pull-right">
                        {!! Form::select('custom_filter', $custom_filter, 1, ['id' => 'custom_filter', 'class' => 'form-control']) !!}
                    </div>
                @endif

                @if(isset($route))
                    <div class="clearfix"></div>
                @endif

                <div class="tab-content">
                    <div class="tab-pane active"><br>
                        @if(isset($filtering))
                            <div class="dataTables_wrapper">
                                <div class="col-md-12 text-center">
                                    @include('filter.'.$filtering)
                                </div>
                            </div>
                        @endif
                        {{ $table }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>