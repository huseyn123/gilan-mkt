@extends ('layouts.modal', ['script' => true])
@section ('title', $create_title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        @include('widgets.lang-tab', ['input' => 'text', 'name' => 'region','tab_title' => 'Region'])

        @include('widgets.lang-tab', ['input' => 'text', 'name' => 'branch_title','tab_title' => 'Filial'])

        {!! $fields !!}



    </div>

@endsection
