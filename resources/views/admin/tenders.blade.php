@extends ('layouts.admin', ['table' => 'tenders'])
@section ('title', $title)

@section('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'tenders.create','filter' => config('config.filter-type'),'route' => 'tenders', 'largeModal' => true, 'locale' => true,])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'tenders', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush