@if ($breadcrumbs)
    @foreach ($breadcrumbs as $breadcrumb)
            @if (!$breadcrumb->last)
                <a title="{{$breadcrumb->title}}" href="{{ $breadcrumb->url }}"> {{ removeSymbol($breadcrumb->title ,'/') }}</a>
                <i class="fav4 fa-angle-right"></i>
            @else
                <span> {{ removeSymbol($breadcrumb->title ,'/') }}</span>
            @endif
    @endforeach
@endif