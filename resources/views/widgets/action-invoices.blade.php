@if($status == 0)
<a href="#" class="btn btn-xs btn-default" onclick="event.preventDefault(); datatableAction(document.getElementById('approve-{{ $id }}'));">
    <i class="fa fa-check-circle"></i>
    {!! Form::open(['method' => 'PUT', 'url' => route('invoices.update', $id), 'id' => "approve-$id", 'style' => 'display:none']) !!}
    {!! Form::close() !!}
</a>
<a href="#" class="btn btn-xs btn-danger" data-action="{{route('invoices.destroy', $id)}}" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-remove"></i></a>
@endif
