@extends ('layouts.web', [
'error_c' => true,
'src' => 'img/404.jpg',
'page_heading' => Cache::get('dictionary_'.app()->getLocale())['nothing_found_title'],
'menuWithoutSlug' => true,
'menu' => \App\Logic\Menu::all(),
'config' => \App\Logic\WebCache::config()->pluck('value', 'key'),
'dictionary' => Cache::get('dictionary_'.app()->getLocale()),
'social' => Cache::get('social'),
'lang' => app()->getLocale(),

])

@section ('content')

    <!-- Error404 Begin -->
    <section class="error404">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- BreadCrumbs Begin -->
                    <div class="breadcrumbs">
                        <div class="inner">
                            <a href="{{ route('home') }}" >{{ $dictionary['home_page'] }}</a>
                            <i class="fav4 fa-angle-right"></i>
                            <span>Error 404</span>
                        </div>
                    </div>
                    <!-- BreadCrumbs End -->
                    <div class="error404_inner">
                        <img src="{{asset('images/error404.svg')}}" >
                        <p>{{ $dictionary['nothing_found_heading'] }}</p>
                        <a href="{{ route('home') }}" class="btn_more"><span>{{ $dictionary['home_page'] }}</span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Error404 End -->


@endsection

