
$(function() {

    $.mask.definitions['9'] = '';
    $.mask.definitions['d'] = '[0-9]';
    $(".phonenumber").mask("+(994 dd) ddd dd dd");

});


    /** subscribe */
$("#subscribe-form").on("submit",function (event){

    event.preventDefault();
    subscribe($(this));
});

$('.report .tabs a').click(function () {
    var route = $(this).attr('href');

    $.ajax({
        url: route,
        beforeSend: function(response) {
        }
    })
    .done(function(response) {
        $('.report  .tabs_content').html(response.html);
    })
    .fail(function(xhr, ajaxOptions, thrownError) {
        requestSent = false;
        var json = JSON.parse(xhr.responseText);
    });
})


function subscribe(action){

    if (action.data('submitted') === true) {
        return false;
    }
    else
    {
        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            data: action.serialize(),
            dataType: "json",

            beforeSend: function(response) {
                $("#loadingSubButton").button('loading');
            },

            success: function(response){
                $("#loadingSubButton").button('reset');

                if(response.code == 0) {

                    $('#subscribe-form')[0].reset();
                    setTimeout(function(){ $("#subscribeBox").html('');}, 3000);
                }

                $("#subscribeBox").html(response.msg);
            },

            error: function(res){
                $("#loadingSubButton").button('reset');
                $("#subscribeBox").html('<p>Unexpected Error!</p>');
            }
        });
    }
}



$(function(){

    function preventNumberInput(e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode > 47 && keyCode < 58) {
            e.preventDefault();
        }
    }
    function preventOnlyNumberInput(e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode > 31 && (keyCode < 48 || keyCode > 57)){
            e.preventDefault();
        }
    }

    $('.alphaonly').keypress(function(e) {
        preventNumberInput(e);
    });

    $('.alphaonlynumb').keypress(function(e) {
        preventOnlyNumberInput(e);
    });



});

//Rezervation
function g_form(action,type){
    $.ajax({
        url:  action.attr('action'),
        type: action.attr('method'),
        data: new FormData(action[0]),
        dataType: "json",
        processData: false,
        contentType: false,
        beforeSend: function(response) {
            $(".msg").html('');
            $("#loadingButton").button('loading');
        },

        success: function(response){
            $("#loadingButton").button('reset');

            if(response.code == 1) {

                $('button[type=submit]').attr('disabled',true);

                action.data('submitted', true);
                $(".msg").html(response.msg);
            }
            else{
                $("#"+type)[0].reset();
                $(".msg").html('<i class="fa fa-check" aria-hidden="true"></i>'+response.msg);
            }

        },

        error: function(res){
            $("#loadingButton").button('reset');
            $(".msg").html('Unexpected Error!');
        }
    });
}

function MainForm(action,type)  {
    $.ajax({
        url:  action.attr('action'),
        type: action.attr('method'),
        data: new FormData(action[0]),
        dataType: "json",
        processData: false,
        contentType: false,
        beforeSend: function(response) {
            $(".loadingButton").button('loading');
            $("#"+type+"_message").html('');
        },

        success: function(response){

            $(".loadingButton").button('reset');

            if(response.code == 1) {
                $("#"+type+" span.error_message").html(response.msg);
            }
            else{
                $('#'+type)[0].reset();
                $('#'+type+'_succes').addClass('show');
            }
        },

        error: function(res){
            $(".loadingButton").button('reset');
            $("#"+type+" span.error_message").html('Unexpected Error!');
        }
    });
}

$('.success_form .back').click(function () {
    $('span.error_message').empty();

    $(this).parent('.success_form').removeClass('show');
});

$( "#enterprise_form" ).submit(function( e ) {
    e.preventDefault();
    MainForm($(this),'enterprise_form');
});

$( "#product_form" ).submit(function( e ) {
    e.preventDefault();
    MainForm($(this),'product_form');
});

$( "#wholesale_form" ).submit(function( e ) {
    e.preventDefault();
    MainForm($(this),'wholesale_form');
});

$( "#contact_form" ).submit(function( e ) {
    e.preventDefault();
    MainForm($(this),'contact_form');
});

$( "#comment_suggestion_form" ).submit(function( e ) {
    e.preventDefault();
    MainForm($(this),'comment_suggestion_form');
});


$(function(){
    $.validator.addMethod("valueNotEquals", function(value, element, arg){
        return arg !== value;
    });

    $("#product_offer_survey").validate({
        rules: {
            full_name: {
                required: true,
            },
            phone: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            company_name: {
                required: true,
            },
            brend_name: {
                required: true,
            },
            product_name :{
                required: true,
            },
            message:{
                required:true,
                minlength: 10,
                maxlength: 300,
            },
            image :{
                required: true,
            }
        },

        success: function(label) {
            label.html('').removeClass('error').addClass('ok');
        },
        errorPlacement: function(error, element) {
            $(element).addClass( "error-item" );
        },
        highlight: function ( element, errorClass, validClass ) {
            $(element).addClass( "error-item" ).removeClass( "valid-item" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass( "valid-item" ).removeClass( "error-item" );
        },
        submitHandler: function (form) {
            MainForm($(form),'product_offer_survey');
        }
    });

    $("#store_location_survey").validate({
        rules: {
            full_name: {
                required: true,
            },
            phone: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            address: {
                required: true,
            },
            area: {
                required: true,
            },
            depot :{
                required: true,
            },
            parking :{
                required: true,
            },
            image :{
                required: true,
            },
            message:{
                required:true,
                minlength: 10,
                maxlength: 300,
            }
        },

        success: function(label) {
            label.html('').removeClass('error').addClass('ok');
        },
        errorPlacement: function(error, element) {
            $(element).addClass( "error-item" );
        },
        highlight: function ( element, errorClass, validClass ) {
            $(element).addClass( "error-item" ).removeClass( "valid-item" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass( "valid-item" ).removeClass( "error-item" );
        },
        submitHandler: function (form) {
            MainForm($(form),'store_location_survey');
        }
    });

    $("#satisfaction_survey").validate({
        rules: {
            full_name: {
                required: true,
            },
            phone: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            shop: {
                required: true,
            },

            s_shop_price: {
                required: true,
            },
            s_product_type: {
                required: true,
            },
            s_product_quality :{
                required: true,
            },
            s_product_find :{
                required: true,
            },
            s_product_followed :{
                required: true,
            },
            s_shop_employee :{
                required: true,
            },
            s_shop_till :{
                required: true,
            }
        },

        success: function(label) {
            label.html('').removeClass('error').addClass('ok');
        },
        errorPlacement: function(error, element) {
            $(element).addClass( "error-item" );
        },
        highlight: function ( element, errorClass, validClass ) {
            $(element).addClass( "error-item" ).removeClass( "valid-item" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass( "valid-item" ).removeClass( "error-item" );
        },
        submitHandler: function (form) {
            MainForm($(form),'satisfaction_survey');
        }
    });

    $("#feedback_form").validate({
        rules: {
            full_name: {
                required: true,
            },
            phone: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            f_type: {
                required: true,
            },
            shop: {
                required: true,
            },
            message:{
                required:true,
                minlength: 10,
                maxlength: 300,
            }
        },

        success: function(label) {
            label.html('').removeClass('error').addClass('ok');
        },
        errorPlacement: function(error, element) {
            $(element).addClass( "error-item" );
        },
        highlight: function ( element, errorClass, validClass ) {
            $(element).addClass( "error-item" ).removeClass( "valid-item" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass( "valid-item" ).removeClass( "error-item" );
        },
        submitHandler: function (form) {
            MainForm($(form),'feedback_form');
        }
    });

    //Vacancy Apply
    $("#vacancy_apply").validate({
        rules: {
            full_name: {
                required: true,
            },
            phone: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            image :{
                required: true,
            }
        },

        success: function(label) {
            label.html('').removeClass('error').addClass('ok');
        },
        errorPlacement: function(error, element) {
            $(element).addClass( "error-item" );
        },
        highlight: function ( element, errorClass, validClass ) {
            $(element).addClass( "error-item" ).removeClass( "valid-item" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass( "valid-item" ).removeClass( "error-item" );
        },
        submitHandler: function (form) {
            MainForm($(form),'vacancy_apply');
        }
    });

    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
    }, "Only alphabetical characters");

});





