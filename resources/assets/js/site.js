$(window).on("load", function (e) {
    //EqualHeights
    eqHeight(".eq", 767);
    eqHeight(".veq", 991);
    eqHeight(".contact_eq", 991);
    eqHeight(".enterprises .box", 767);
    eqHeight(".company_gallery .box", 480);
    eqHeight(".company_videos .box", 480);
    eqHeight(".techique_list .box", 480);
    eqHeight(".enterprise_about_blocks .block", 767);
    eqHeight(".news_list .box .body", 767);

});

$(document).ready(function () {
    //Counter
    $('.count_section .count').counterUp({
        delay: 10,
        time: 1000
    });

});

$(function(){

    //Menu
    $(".btn_menu").click(function(){
        if($(this).hasClass("open")){
            $(".mobile_nav").stop(true, true).removeClass("is-open");
            $("body").stop(true, true).removeClass("noscroll");
            $(this).stop(true, true).removeClass("open");
        } else {
            $(this).stop(true, true).addClass("open");
            $("body").stop(true, true).addClass("noscroll");
            $(".mobile_nav").stop(true, true).addClass("is-open");
        }
        return false;
    });

    $(".mobile_nav li.sub>a").click(function(){
        if($(this).parent().hasClass("active")){
            $(".mobile_nav li.sub ul").stop(true, true).slideUp();
            $(this).parent().stop(true, true).removeClass("active");
        }
        else {
            $(".mobile_nav li.sub ul").stop(true, true).slideUp("normal");
            $(this).siblings("ul").stop(true, true).slideDown("normal");
            $(".mobile_nav li").stop(true, true).removeClass("active");
            $(this).parent().stop(true, true).addClass("active");
        }
        return false
    });

    $(".mobile_nav span.close").click(function(){
        $(".mobile_nav").stop(true, true).removeClass("is-open");
        $("body").stop(true, true).removeClass("noscroll");
        $(".btn_menu").stop(true, true).removeClass("open");
    });

    //Lang
    $(".lang span").click(function(){
        if($(this).hasClass("open")){
            $(this).stop(true, true).removeClass("open");
            $(".lang_list").stop(true, true).removeClass("open");
        } else {
            $(this).stop(true, true).addClass("open");
            $(".lang_list").stop(true, true).addClass("open");
        }
    });

    //Search
    $(".btn_search").click(function(){
        $("#header").stop(true, true).addClass("is-open");
    });
    $(".search .close").click(function(){
        $("#header").stop(true, true).removeClass("is-open");
    });

    //MaskInput
    $.mask.definitions['9'] = '';
    $.mask.definitions['d'] = '[0-9]';
    $(".phonenumber").mask("+994 dd ddd dd dd");

    //Slider
    $("#slider").owlCarousel({
        lazyLoad: false,
        dots: true,
        nav: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        loop: false,
        margin: 20,
        navText : ["<i class='fav4 fa-angle-left'></i>","<i class='fav4 fa-angle-right'></i>"],
        items: 1
    });

    //FAQ
    $(".faq .head").on("click", function(){
        if($(this).parent().hasClass("active")){
            $(this).parent().stop().removeClass("active");
            $(this).siblings(".body").stop().slideUp();
        }
        else{
            $(".faq .item").stop().removeClass("active");
            $(this).parent().stop().addClass("active");
            $(".faq .body").stop().slideUp();
            $(this).siblings(".body").stop().slideDown();
        }
    });

    //ProductsCarousel
    $("#productscarousel").owlCarousel({
        lazyLoad: false,
        dots: false,
        nav: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        loop: false,
        margin: 20,
        navText : ["<i class='fav4 fa-angle-left'></i>","<i class='fav4 fa-angle-right'></i>"],
        responsive : {
            0 : {
                items: 1
            },
            768 : {
                items: 2
            },
            992 : {
                items: 2
            },
            1200 : {
                items: 2
            }
        }
    });


    $("#other_products").owlCarousel({
        lazyLoad: false,
        dots: false,
        nav: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        loop: false,
        margin: 20,
        navText : ["<i class='fav4 fa-angle-left'></i>","<i class='fav4 fa-angle-right'></i>"],
        responsive : {
            0 : {
                items: 1
            },
            768 : {
                items: 2
            },
            992 : {
                items: 2
            },
            1200 : {
                items: 2
            }
        }
    });

    //Partners
    $("#partners").owlCarousel({
        dots: false,
        nav: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        loop: true,
        margin: 0,
        smartSpeed: 450,
        navText : ["<i class='fav4 fa-angle-left'></i>","<i class='fav4 fa-angle-right'></i>"],
        responsive : {
            // breakpoint from 0 up
            0 : {
                items: 1
            },
            // breakpoint from 480 up
            480 : {
                items: 2
            },
            // breakpoint from 768 up
            768 : {
                items: 3
            },
            // breakpoint from 992 up
            992 : {
                items: 5
            },
            // breakpoint from 1200 up
            1200 : {
                items: 5
            }
        }
    });

    //NewsGallery
    $("#news_gallery").owlCarousel({
        lazyLoad: true,
        dots: false,
        nav: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        loop: false,
        margin: 20,
        navText : ["<i class='fav4 fa-angle-left'></i>","<i class='fav4 fa-angle-right'></i>"],
        responsive : {
            0 : {
                items: 2
            },
            768 : {
                items: 2
            },
            992 : {
                items: 3
            },
            1200 : {
                items: 3
            }
        }
    });

    //Branch
    $(".map path.active").click(function() {
        var id = $(this).attr("id");

        $(".map path").stop(true, true).removeClass("show");
        $(".map path#"+id).stop(true, true).addClass("show");

        $(".branch_info .block").stop(true, true).removeClass("show");
        $(".branch_info .block[data-type="+id+"]").stop(true, true).addClass("show");
    });

    //Video
    $(".zoom-video").magnificPopup({
        disableOn: 700,
        type: "iframe",
        mainClass: "mfp-fade",
        removalDelay: 160,
        preloader: false,
        fixedContentPos: true,
        gallery:{
            enabled:true
        }
    });

    //Gallery
    if( $(".zoom-photo").length ){
        $(".zoom-photo").magnificPopup({
            type: "image",
            tLoading: "Loading image #%curr%...",
            mainClass: "mfp-img-mobile",
            gallery: {
                enabled: true,
                navigateByImgClick: false,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            }
        });
    }
});

function eqHeight(param, sizes) {
    if ($(param).length){
        var resizeTimer;
        $( window ).resize(function() {
            var width = $(window).width();
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(func, 500);
        });
        func()
    }

    function func(){
        var width = $(window).width();
        $(param).removeAttr('style');
        if (width < sizes){
            $(param).height("auto");
        }else{
            $(param).equalHeights();
        }
    }
}