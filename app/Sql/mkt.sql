/*
 Navicat Premium Data Transfer

 Source Server         : Mkt
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3306
 Source Schema         : mkt

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 06/02/2020 23:28:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admins_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES (1, 'Huseyn Huseynli', 'huseyn.h@gmail.com', NULL, '$2y$10$sqDyqzYfmQdUZY4/xl4gnuPacBft3Bqj5tw9tQaP4X5RUCa08hjym', NULL, '2020-01-14 12:14:11', '2020-01-14 12:14:11', NULL);
INSERT INTO `admins` VALUES (3, 'Taleh Kerimov', 'taleh@marcom.az', NULL, '$2y$10$qkwt4zQgCLQ.aqYb4SjxcuOj8NWrCN4todtcyXstEsvUJtpKZtH1q', 'FkFS3NJcpe3URAZjegzs2ZMierBLOiL8fUYeVuRWqSzX42uBhKRZ81rhDKaq', '2020-01-20 09:33:32', '2020-01-20 09:33:32', NULL);

-- ----------------------------
-- Table structure for article_translations
-- ----------------------------
DROP TABLE IF EXISTS `article_translations`;
CREATE TABLE `article_translations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `article_id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `view_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `article_translations_article_id_lang_unique`(`article_id`, `lang`) USING BTREE,
  UNIQUE INDEX `article_translations_slug_unique`(`slug`) USING BTREE,
  INDEX `article_translations_page_id_foreign`(`page_id`) USING BTREE,
  CONSTRAINT `article_translations_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `article_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `page_translations` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article_translations
-- ----------------------------
INSERT INTO `article_translations` VALUES (1, 1, 12, 'Əsas ixracatçı şirkətlər siyahısında “MKT İstehsalat Kommersiya” MMC-nin də adı var.', 'esas-ixracatci-sirketler-siyahisinda-mkt-istehsalat-kommersiya-mmc-nin-de-adi-var', 'İqtisadi İslahatların Təhlili və Kommunikasiya Mərkəzinin “İxrac İcmalı”nın 2019-cu ilin noyabr sayı təqdim olunub.', '<p style=\"text-align: justify;\"><strong>&ldquo;İxrac İcmalı&rdquo;nın noyabr sayında qeyri-neft sektoru &uuml;zrə əsas ixracat&ccedil;ı şirkətlərin reytinqi verilib.</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Bu ilin <strong>yanvar - oktyabr ayları</strong> &uuml;zrə hazırlanan qeyri-d&ouml;vlət ixracat&ccedil;ı subyektlərin reytinqində ilk onluqda aşağıdakı şirkətlər təmsil olunurlar: &quot;Azərbaycan İnterneyşnl Mayninq Kompani Limited Şirkəti&quot;nin Azərbaycan Respublikasındakı n&uuml;mayəndəliyi, &ldquo;Socar Polymer&rdquo; MMC, &quot;MKT İstehsalat Kommersiya&quot; MMC, &quot;Nine Climate&quot; MMC, &quot;Global Export Fruits&quot; MMC, &ldquo;Sun Food&quot; MMC, &quot;Baku Steel Company&quot;, &quot;Azərbaycan Şəkər İstehsalat Birliyi &quot; MMC, &quot;Caspian enerji-2020&quot; MMC, &quot;Ram Beynəlxalq Nəqliyyat və Ticarət Ltd&quot; MMC.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Qeyri-neft sektoru &uuml;zrə</strong> ixrac əməliyyatlarında iştirak edən d&ouml;vlətə məxsus şirkətlərin siyahısına isə ARDNŞ-nin &ldquo;Marketinq və İqtisadi Əməliyyatlar İdarəsi&rdquo; baş&ccedil;ılıq edir.</p>\r\n\r\n<p style=\"text-align: justify;\">Daha sonra bu siyahıda aşağıdakı şirkətlər təmsil olunurlar: &quot;Azəral&uuml;minium&quot; MMC, &quot;Azergold&quot; QSC, &quot;Azərenerji&quot; ATSC, &quot;Azərbaycan Hava Yolları&quot; QSC, &quot;Azərpambıq Aqrar Sənaye Kompleksi&quot; MMC, &ldquo;CTS-Agro&rdquo; MMC, &quot;Azərt&uuml;t&uuml;n Aqrar Sənaye Kompleksi&quot; MMC, &quot;Azərikimya&quot; İB, &quot;Bakı Neft Maşinqayırma Zavodu&quot; TASC.</p>', 'az', 0, '2020-01-19 12:14:29', '2020-01-27 13:41:34', NULL);
INSERT INTO `article_translations` VALUES (2, 2, 12, '\"Pambığın yarıdan çoxunu bizim şirkət tədarük edib.\"', 'pambigin-yaridan-coxunu-bizim-sirket-tedaruk-edib', '“Respublikada 2017-ci ildə 135 min hektardan artıq, o cümlədən “MKT İstehsalat Kommersiya” MMC üzrə 70 min 563 hektar sahədə pambıq əkini üzrə çıxış alınıb.', '<p style=\"text-align: justify;\">&ldquo;Respublikada 2017-ci ildə 135 min hektardan artıq, o c&uuml;mlədən &ldquo;MKT İstehsalat Kommersiya&rdquo; MMC &uuml;zrə 70 min 563 hektar sahədə pambıq əkini &uuml;zrə &ccedil;ıxış alınıb, &ouml;lkə &uuml;zrə kondisiya şəklində &ccedil;əkidə 207 min tondan artıq, o c&uuml;mlədən cəmiyyətimiz &uuml;zrə 130 min 679 ton xam pambıq tədar&uuml;k edilib&rdquo; (&ouml;lkə &uuml;zrə pambığın 63%-i).</p>\r\n\r\n<p style=\"text-align: justify;\">Bunu &ldquo;MKT İstehsalat Kommersiya&rdquo; MMC-nin baş direktorunun m&uuml;avini R&ouml;vşən Həsənov bildirib.&nbsp;Onun s&ouml;zlərinə g&ouml;rə, tədar&uuml;k edilmiş xam pambığın 114 min tonu artıq emal olunub, 45 min ton mahlıc, 55 min ton &ccedil;iyid, 1540 ton k&ouml;mək&ccedil;i məhsullar alınıb. Bundan əlavə, cəmiyyətimizin istehsal m&uuml;əssisələrində 3300 ton pambıq ipliyi, 3 min ton rafinə olunmuş pambıq yağı və 22 min ton jmıx istehsal olunub.</p>\r\n\r\n<p style=\"text-align: justify;\">2017-ci ildə istehsal olunan məhsullardan yerli istehsal tələbatı daxil olmaqla 26 milyon ABŞ dolları dəyərində mal satılıb ki, bunun da 17,7 milyon dolları ixracın payına d&uuml;ş&uuml;r. Hazırda məhsulların istehsal və satış prosesi davam edir.</p>\r\n\r\n<p style=\"text-align: justify;\">R&ouml;vşən Həsənov qeyd edib ki, bu il &ldquo;MKT İstehsalat Kommersiya&rdquo; &nbsp;tərəfindən 1816 hektarda buğda, 762 hektarda arpa əkilib, 50 min hektaradək sahədə pambıq əkini nəzərdə tutulub və bu istiqamətdə işlərə başlanılıb: &ldquo;Belə ki, cəmiyyətimizə məxsus 5056 hektar sahədə pambıq əkini nəzərdə tutulub ki, bundan 4235 hektarı m&uuml;asir və səmərəli suvarma sistemlərindən olan pivot suvarma sistemi ilə təchiz edilib.</p>\r\n\r\n<p style=\"text-align: justify;\">Pambıq&ccedil;ılıqla məşğul olan 8700 fermerlə 40 min 500 hektar sahədə əkin &uuml;&ccedil;&uuml;n m&uuml;qavilə bağlanıb və proses davam edir.&nbsp;42 min hektarda şum aparılıb, 16 min hektar sahə arata qoyulub, fermerlərə 2 milyon manatdan artıq avans verilib.&nbsp;B&uuml;t&uuml;n rayonlar &uuml;zrə ayrı-ayrılıqda texnoloji xəritələr hazırlanıb.</p>\r\n\r\n<p style=\"text-align: justify;\">500 ton lifli, 1035 ton lifsiz y&uuml;ksək reproduksiyalı pambıq toxumu tədar&uuml;k edilib və tələbat tam &ouml;dənilib.&nbsp;Beş min ton ammonium nitrat, 952 ton karbamid g&uuml;brəsi tədar&uuml;k edilib, əlavə olaraq 3500 ton ammonium nitrat, 4 min ton ammofos g&uuml;brəsi və 138 ton bitki m&uuml;hafizə vasitələri sifariş edilib&rdquo;.</p>\r\n\r\n<p style=\"text-align: justify;\">&ldquo;MKT İstehsalat Kommersiya&rdquo; MMC hazırda 6146 texnika və avadanlıq, o c&uuml;mlədən 188 kombayn, 802 traktor, 5156 m&uuml;xtəlif təyinatlı qoşqu və avadanlıqlar vasitəsilə fermerlərə xidmət g&ouml;stərməkdədir. Qeyd etmək istəyirəm ki, fermerlərə vaxtlı-vaxtında, y&uuml;ksək keyfiyyətli xidmət g&ouml;stərə bilmək &uuml;&ccedil;&uuml;n aqrotexniki bazamız tam formalaşıb.</p>\r\n\r\n<p style=\"text-align: justify;\">Xam pambığın 24 qəbul məntəqəsində tədar&uuml;k&uuml;, 13 pambıqtəmizləmə zavodunda emal edilməsi planlaşdırılır. Şirkətin məntəqə və zavodlarında &ouml;tən il əsaslı təmirə başlanılıb, proses yekunlaşmaq &uuml;zrədir.&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">&ldquo;Cari m&ouml;vs&uuml;mdə cəmiyyətimiz tərəfindən sahələrdən hər hektara orta hesabla 24 sentner olmaqla 120 min ton xam pambığın toplanması g&ouml;zlənilir. Tədar&uuml;k ediləcək xam pambığın emalından 48 min ton mahlıc, 60 min ton &ccedil;iyid, 2 min ton k&ouml;mək&ccedil;i məhsullar, &ccedil;iyidin emalından isə 7 min ton rafinə edilmiş pambıq yağı və 42 min ton jmıx alınması proqnozlaşdırılır.</p>\r\n\r\n<p style=\"text-align: justify;\">H&ouml;rmətli cənab Prezident, &ccedil;oxsaylı və zəhmətkeş kollektivimiz adından bizlərə g&ouml;stərdiyiniz x&uuml;susi diqqət və qayğıya g&ouml;rə Sizə təşəkk&uuml;r edirik. Bu şərəfli işdə qarşıya qoyduğunuz hədəflərə &ccedil;atmaq &uuml;&ccedil;&uuml;n əlimizdən gələni əsirgəməyəcəyimizə s&ouml;z veririk&rdquo;, - baş direktorunun m&uuml;avini deyib.</p>', 'az', 0, '2020-01-19 12:15:21', '2020-01-27 14:06:10', NULL);
INSERT INTO `article_translations` VALUES (3, 3, 12, '“MKT İstehsalat Kommersiya” pambıqçılıq üzrə rekord məhsuldarlıq əldə edib.', 'mkt-istehsalat-kommersiya-pambiqciliq-uzre-rekord-mehsuldarliq-elde-edib', 'Dövlət başçısının ölkədə pambıqçılığın inkişafı ilə əlaqədar verdiyi tövsiyə və tapşırıqlara müvafiq olaraq son illərdə Azərbaycanda bu sahadə yüksək nəticələr əldə olunur.', '<p style=\"text-align: justify;\">D&ouml;vlət baş&ccedil;ısının &ouml;lkədə pambıq&ccedil;ılığın inkişafı ilə əlaqədar verdiyi t&ouml;vsiyə və tapşırıqlara m&uuml;vafiq olaraq son illərdə Azərbaycanda bu sahadə y&uuml;ksək nəticələr əldə olunur. Respublika ərazisində pambıq əkin sahələri genişləndirilir, yeni sortlar yetişdirilərək istehsalatda tətbiq olunur və m&uuml;asir avadanlıq vasitəsilə emalı, daxili və xarici bazarlara &ccedil;atdırılması təmin edilir.</p>\r\n\r\n<p style=\"text-align: justify;\">&Ouml;lkənin pambıq və pambıq məhsulları istehsalı və satışı ilə məşğul olan aparıcı şirkəti &ldquo;MKT İstehsalat Kommersiya&rdquo; MMC pambıq&ccedil;ılığın inkişafına &ouml;z t&ouml;hfəsini vermişdir. 1995-ci ildən fəaliyyətə başlayan Cəmiyyət &ouml;lkənin pambıq&ccedil;ılıqla məşğul olan qabaqcıl rayonlarında (Ağcabədi, Ağdam, Ağdaş, Biləsuvar, Beyləqan, Bərdə, Neft&ccedil;ala, Saatlı, Sabirabad, Salyan, K&uuml;rdəmir, Tərtər, Goranboy, İmişli, Ucar, Yevlax, Zərdab) yeni pambıq sortlarının əkilməsi, məhsuldarlığın artması, əlverişli infrastrukturun yaradılması istiqamətində işlər aparmaqda davam edir.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>2018-ci ildə yeni texnologiyanın tətbiqi sayəsində 45,000 hektar sahədə, o c&uuml;mlədən 8800 nəfərdən &ccedil;ox fermerə məxsus 40,000 hektar və şirkətə məxsus 5,000 hektar sahədə pambıq əkilib becərilmişdir. Cari ilin sentyabr ayından etibarən şirkət filial və x&uuml;susi məntəqələrində pambıq qəbuluna başlayıb. Tədar&uuml;k m&ouml;vs&uuml;m&uuml;n&uuml;n başlanmasından etibarən &ldquo;Ağ qızıl&rdquo; sortları &uuml;zrə 118 min tondan &ccedil;ox xam pambıq artıq qəbul edilib. M&ouml;vs&uuml;m&uuml;n sonunadək filiallarda və pambıq qəbulu məntəqələrində 120 min tondan artıq xam pambığın qəbulu g&ouml;zlənilir. Bu isə 1 hektar &uuml;&ccedil;&uuml;n 26.7 sentner orta məhsuldarlıq deməkdir. Qeyd edək ki, bu nəticə &ouml;tən ilin g&ouml;stəricisi ilə m&uuml;qayisədə 8 sentner &ccedil;oxdur.</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Fermerlərlə təşkil edilmiş qarşılıqlı işg&uuml;zar əlaqələr nəticəsində, onlara bu g&uuml;nə qədər pambığın dəyəri olaraq <strong>36 milyon 600 min manat vəsait &ouml;dənilmişdir ki, bu da hər hektara 914 AZN məbləğində qazanc deməkdir.</strong> Bu m&uuml;nasibətlər getdikcə zənginləşir və doğmalaşır.</p>\r\n\r\n<p style=\"text-align: justify;\">Qeyd edək ki, İqtisadi İslahatların Təhlili və Kommunikasiya Mərkəzinin &ldquo;İxrac İcmalı&rdquo;nın 2018-ci ilin sentyabr ayında qeyri-neft sektoru &uuml;zrə malların ixracına dair dərc olunan reytinqə əsasən, &ldquo;MKT İstehsalat Kommersiya&rdquo; MMC 2018-ci ilin yanvar-avqust aylarında qeyri-neft sektoru &uuml;zrə qeyri-d&ouml;vlət ixracat&ccedil;ı subyektlərinin siyahısında 2-ci yer tutub. 2017-ci ilin m&uuml;vafiq d&ouml;vr&uuml; ilə m&uuml;qayisədə şirkət ixrac gəlirlərini 28.1 milyon artıraraq 48.1 milyon ABŞ dolları məbləğinə &ccedil;atdırıb.</p>', 'az', 0, '2020-01-27 14:00:45', '2020-01-27 14:01:32', NULL);
INSERT INTO `article_translations` VALUES (4, 4, 12, '“MKT İstehsalat Kommersiya” MMC 2017-ci ildə pambıq satışından gəlirlərini 15,5% artırıb.', 'mkt-istehsalat-kommersiya-mmc-2017-ci-ilde-pambiq-satisindan-gelirlerini-15-5-artirib', '«MKT Istehsalat Kommersiya» MMC 2017-ci ildə emal olunmuş pambıq satışından 26 mln. ABŞ Dolları gəlir əldə edib.', '<p style=\"text-align: justify;\">&laquo;MKT Istehsalat Kommersiya&raquo; MMC 2017-ci ildə emal olunmuş pambıq satışından $26 mln. gəlir əldə edib. Bunu şirkətin baş direktorunun m&uuml;avini R&ouml;vşən Həsənov bu g&uuml;n pambıq&ccedil;ılığın inkişafına dair &uuml;mumrespublika m&uuml;şavirəsində &ccedil;ıxışı zamanı deyib.</p>\r\n\r\n<p style=\"text-align: justify;\">&laquo;2017-ci ildə istehsal olunan məhsullardan yerli istehsal tələbatı daxil olmaqla 26 mln. ABŞ dolları dəyərində mal satılıb ki, bunun da 17,7 mln. dolları ixracın payına d&uuml;ş&uuml;r. Hazırda məhsulların istehsal və satış prosesi davam edir&raquo;, - deyə o bildirib.</p>\r\n\r\n<p style=\"text-align: justify;\">R.Həsənovun s&ouml;zlərinə g&ouml;rə, &ouml;tən il respublikada 135 min hektardan artıq, o c&uuml;mlədən &ldquo;MKT İstehsalat Kommersiya&rdquo; MMC &uuml;zrə 70 min 563 hektar sahədə pambıq əkini &uuml;zrə &ccedil;ıxış alınıb, &ouml;lkə &uuml;zrə kondisiya şəklində &ccedil;əkidə 207 min tondan artıq, o c&uuml;mlədən cəmiyyət &uuml;zrə 130 min 679 ton xam pambıq tədar&uuml;k edilib.</p>\r\n\r\n<p style=\"text-align: justify;\">&ldquo;Cari m&ouml;vs&uuml;mdə cəmiyyətimiz tərəfindən sahələrdən hər hektara orta hesabla 24 sentner olmaqla 120 min ton xam pambığın toplanması g&ouml;zlənilir. Pambıq&ccedil;ılıqla məşğul olan 8700 fermerlə 40 min 500 hektar sahədə əkin &uuml;&ccedil;&uuml;n m&uuml;qavilə bağlanıb və proses davam edir. Fermerlərə 2 mln. manatdan artıq avans verilib&rdquo;, - deyə baş direktorun m&uuml;avini qeyd edib.</p>\r\n\r\n<p style=\"text-align: justify;\">O, həm&ccedil;inin əlavə edib ki, b&uuml;t&uuml;n rayonlar &uuml;zrə ayrı-ayrılıqda texnoloji xəritələr hazırlanıb. 500 ton lifli, 1035 ton lifsiz y&uuml;ksək reproduksiyalı pambıq toxumu tədar&uuml;k edilib və tələbat tam &ouml;dənilib.</p>', 'az', 0, '2020-01-27 14:09:19', '2020-01-27 14:09:19', NULL);
INSERT INTO `article_translations` VALUES (5, 5, 12, 'MKT İstehsalat Kommersiya MMC üçün hazırlanan video çarx təqdim olundu.', 'mkt-istehsalat-kommersiya-mmc-ucun-hazirlanan-video-carx-teqdim-olundu', 'Təqdimat çarxının çəkilişləri 4 ay ərzində Bakı, Şirvan, Yevlax şəhərlərində və Ağcabədi rayonunda aparılmışdır.', '<p style=\"text-align: justify;\">Marcom Marketinq və Kommunikasiyalar Şirkətinin&nbsp;İcra&ccedil;ı Direktoru Ceyhun Fərzəliyev təqdim olunmuş tanıtım &ccedil;arxı ilə bağlı məlumat verərək bildirdi ki, video &ccedil;əkilişlər bir ne&ccedil;ə məkanlarda baş tutub:</p>\r\n\r\n<p style=\"text-align: justify;\">&ldquo;Təqdimat &ccedil;arxının &ccedil;əkilişləri 4 ay ərzində Bakı, Şirvan, Yevlax şəhərlərində və Ağcabədi rayonunda aparılmışdır. Sifariş&ccedil;i tərəfindən qarşımıza qoyulan məqsəd şirkətin g&ouml;rd&uuml;y&uuml; işi, əhatə etdiyi əraziləri, texniki imkanlarını, yığım və istehsal prosesini əks etdirmək idi ki, biz də bunun &ouml;hdəsindən layiqincə gəlmiş olduq. Şirkətin gələcək dizayn işlərində istifadəsi &uuml;&ccedil;&uuml;n &ccedil;əkilişlərə foto işləri daxil edilmişdir.&rdquo;</p>\r\n\r\n<p style=\"text-align: justify;\">&ldquo;MKT İstehsalat Kommersiya&rdquo; MMC şirkətinin İdarə Heyətinin sədri R&ouml;vşən Həsənov g&ouml;r&uuml;lm&uuml;ş işlərlə bağlı fikir bildirərək s&ouml;yləmişdir: &ldquo;&ldquo;Marcom&rdquo; şirkəti ilə olan iş birliyimizi y&uuml;ksək qiymətləndiririk. Şadam ki, video &ccedil;əkiliş işlərində bizə dəstək olmaqla yanaşı, &ccedil;əkilişin daha keyfiyyətli olması &uuml;&ccedil;&uuml;n bizim əməkdaşlara da m&uuml;vafiq istiqamətlər verməkdə yardım&ccedil;ı oldular. Hal-hazırda &ldquo;Marcom&rdquo; şirkəti ilə əməkdaşlığımızın yeni istiqamətlərini m&uuml;zakirə edirik.&rdquo;</p>\r\n\r\n<p style=\"text-align: justify;\">Qeyd edək ki, yaxın g&uuml;nlərdə &ldquo;MKT İstehsalat Kommersiya&rdquo; MMC-nin veb platforması da&nbsp;təqdim olunacaq.</p>', 'az', 0, '2020-01-27 14:21:09', '2020-01-27 14:22:56', NULL);

-- ----------------------------
-- Table structure for articles
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) UNSIGNED NOT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `published_by` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_at` date NULL DEFAULT NULL,
  `show_index` tinyint(1) NOT NULL DEFAULT 0,
  `youtube_link` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `articles_status_index`(`status`) USING BTREE,
  INDEX `articles_published_at_index`(`published_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES (1, 1, 0, '', '2019-11-28', 1, NULL, '2020-01-19 12:14:29', '2020-01-27 14:02:27', NULL);
INSERT INTO `articles` VALUES (2, 1, 0, '', '2018-03-26', 0, NULL, '2020-01-19 12:15:21', '2020-01-28 05:21:51', NULL);
INSERT INTO `articles` VALUES (3, 1, 0, '', '2018-11-29', 0, NULL, '2020-01-27 14:00:45', '2020-01-27 14:09:39', NULL);
INSERT INTO `articles` VALUES (4, 1, 0, '', '2018-03-27', 0, NULL, '2020-01-27 14:09:19', '2020-01-27 14:09:19', NULL);
INSERT INTO `articles` VALUES (5, 1, 0, '', '2019-12-11', 1, NULL, '2020-01-27 14:21:09', '2020-01-27 14:21:09', NULL);

-- ----------------------------
-- Table structure for branch_elements
-- ----------------------------
DROP TABLE IF EXISTS `branch_elements`;
CREATE TABLE `branch_elements`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title_az` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `title_ru` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `type` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for branch_maps
-- ----------------------------
DROP TABLE IF EXISTS `branch_maps`;
CREATE TABLE `branch_maps`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `location` tinyint(1) UNSIGNED NOT NULL,
  `region_az` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `region_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `region_ru` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `branch_title_az` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_title_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `branch_title_ru` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `farmers_count` int(11) NOT NULL,
  `sown_area` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_at` date NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `branch_maps_published_at_index`(`published_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of branch_maps
-- ----------------------------
INSERT INTO `branch_maps` VALUES (3, 48, 'Ağcabədi', 'Ağcabədi', 'Ağcabədi', 'MKT - Ağcabədi', 'MKT - Ağcabədi', 'MKT - Ağcabədi', 762, '4551,33', '2008-01-28', '2020-01-24 13:26:15', '2020-01-24 14:52:17');
INSERT INTO `branch_maps` VALUES (4, 48, 'Ağcabədi', 'Ağcabədi', 'Ağcabədi', 'Ağcabədi - İnnovasiya', 'Ağcabədi - İnnovasiya', 'Ağcabədi - İnnovasiya', 0, '850', '2013-09-30', '2020-01-24 13:27:26', '2020-01-24 14:52:41');
INSERT INTO `branch_maps` VALUES (5, 49, 'Bərdə', 'Bərdə', 'Bərdə', 'MKT - Bərdə', 'MKT - Bərdə', 'MKT - Bərdə', 1160, '5020', '2008-01-28', '2020-01-24 13:36:19', '2020-01-24 14:54:41');
INSERT INTO `branch_maps` VALUES (6, 21, 'Biləsuvar', 'Biləsuvar', 'Biləsuvar', 'MKT - Biləsuvar', 'MKT - Biləsuvar', 'MKT - Biləsuvar', 900, '4100', '2008-01-28', '2020-01-24 13:37:39', '2020-01-24 14:58:48');
INSERT INTO `branch_maps` VALUES (7, 23, 'Saatlı', 'Saatlı', 'Saatlı', 'MKT - Saatlı', 'MKT - Saatlı', 'MKT - Saatlı', 1205, '4091', '2008-01-28', '2020-01-24 13:38:42', '2020-01-24 14:51:33');
INSERT INTO `branch_maps` VALUES (8, 23, 'Saatlı', 'Saatlı', 'Saatlı', 'MKT - Saatlı İnnovasiya', 'MKT - Saatlı İnnovasiya', 'MKT - Saatlı İnnovasiya', 0, '1395', '2012-05-23', '2020-01-24 13:39:28', '2020-01-24 14:51:04');
INSERT INTO `branch_maps` VALUES (9, 17, 'Sabirabad', 'Sabirabad', 'Sabirabad', 'MKT - Sabirabad', 'MKT - Sabirabad', 'MKT - Sabirabad', 982, '2900,73', '2012-12-30', '2020-01-24 13:40:29', '2020-01-24 14:57:36');
INSERT INTO `branch_maps` VALUES (10, 15, 'Salyan', 'Salyan', 'Salyan', 'MKT - Salyan', 'MKT - Salyan', 'MKT - Salyan', 407, '2138,18', '2008-01-28', '2020-01-24 13:42:28', '2020-01-24 14:57:58');
INSERT INTO `branch_maps` VALUES (11, 50, 'Tərtər', 'Tərtər', 'Tərtər', 'MKT - Tərtər', 'MKT - Tərtər', 'MKT - Tərtər', 837, '2017,18', '2012-12-30', '2020-01-24 13:43:32', '2020-01-24 14:55:18');
INSERT INTO `branch_maps` VALUES (12, 66, 'Yevlax', 'Yevlax', 'Yevlax', 'MKT - Yevlax', 'MKT - Yevlax', 'MKT - Yevlax', 117, '906,75', '2008-01-28', '2020-01-24 13:44:27', '2020-01-24 14:53:04');
INSERT INTO `branch_maps` VALUES (13, 66, 'Yevlax', 'Yevlax', 'Yevlax', 'Yevlax - İnnovasiya', 'Yevlax - İnnovasiya', 'Yevlax - İnnovasiya', 0, '965', '2006-05-01', '2020-01-24 13:45:28', '2020-01-24 13:45:28');
INSERT INTO `branch_maps` VALUES (14, 22, 'İmişli', 'İmişli', 'İmişli', 'MKT - İmişli', 'MKT - İmişli', 'MKT - İmişli', 665, '3469,66', '2010-12-20', '2020-01-24 13:46:30', '2020-01-24 14:59:54');
INSERT INTO `branch_maps` VALUES (15, 16, 'Neftçala', 'Neftçala', 'Neftçala', 'MKT - Neftçala', 'MKT - Neftçala', 'MKT - Neftçala', 710, '3272', '2017-02-27', '2020-01-24 13:47:25', '2020-01-24 14:58:18');
INSERT INTO `branch_maps` VALUES (16, 32, 'Beyləqan', 'Beyləqan', 'Beyləqan', 'MKT - Beyləqan', 'MKT - Beyləqan', 'MKT - Beyləqan', 973, '4373,47', '2008-01-28', '2020-01-24 14:16:19', '2020-01-24 14:47:48');
INSERT INTO `branch_maps` VALUES (17, 32, 'Beyləqan', 'Beyləqan', 'Beyləqan', 'Beyləqan - İnnovasiya', 'Beyləqan - İnnovasiya', 'Beyləqan - İnnovasiya', 0, '1105', '2014-04-21', '2020-01-24 14:17:15', '2020-01-24 14:50:04');
INSERT INTO `branch_maps` VALUES (18, 70, 'Şirvan', 'Şirvan', 'Şirvan', 'Şirvan Yağ - Piy', 'Şirvan Yağ - Piy', 'Şirvan Yağ - Piy', 0, '0', '2009-05-01', '2020-01-24 14:26:14', '2020-01-24 14:26:14');
INSERT INTO `branch_maps` VALUES (19, 31, 'Zərdab', 'Zərdab', 'Zərdab', 'MKT - Zərdab', 'MKT - Zərdab', 'MKT - Zərdab', 339, '1014', '2017-07-27', '2020-01-24 14:27:17', '2020-01-24 14:53:44');
INSERT INTO `branch_maps` VALUES (20, 51, 'Goranboy', 'Goranboy', 'Goranboy', 'MKT - Goranboy', 'MKT - Goranboy', 'MKT - Goranboy', 330, '1300,01', '2018-08-06', '2020-01-24 14:28:06', '2020-01-24 14:55:56');
INSERT INTO `branch_maps` VALUES (21, 8, 'Ağdam', 'Ağdam', 'Ağdam', 'MKT - Ağdam', 'MKT - Ağdam', 'MKT - Ağdam', 526, '1350,01', '2017-06-05', '2020-01-24 14:29:44', '2020-01-24 14:56:55');
INSERT INTO `branch_maps` VALUES (22, 22, 'İmişli', 'İmişli', 'İmişli', 'İmişli - İnnovasiya', 'İmişli - İnnovasiya', 'İmişli - İnnovasiya', 0, '2395', '2013-05-01', '2020-01-24 14:30:35', '2020-01-24 14:30:35');
INSERT INTO `branch_maps` VALUES (23, 24, 'Kürdəmir', 'Kürdəmir', 'Kürdəmir', 'MKT - Kürdəmir', 'MKT - Kürdəmir', 'MKT - Kürdəmir', 93, '724,15', '2017-06-05', '2020-01-24 14:31:17', '2020-01-24 14:54:21');
INSERT INTO `branch_maps` VALUES (24, 49, 'Bərdə', 'Bərdə', 'Bərdə', 'Lənbəran İnnovasiya', 'Lənbəran İnnovasiya', 'Lənbəran İnnovasiya', 0, '450', '2016-05-01', '2020-01-24 14:32:26', '2020-01-24 14:32:26');

-- ----------------------------
-- Table structure for branches
-- ----------------------------
DROP TABLE IF EXISTS `branches`;
CREATE TABLE `branches`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `phone` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `region_id` int(10) UNSIGNED NOT NULL,
  `network_id` int(10) UNSIGNED NOT NULL,
  `location_az` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `location_ru` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `branches_region_id_foreign`(`region_id`) USING BTREE,
  INDEX `branches_network_id_foreign`(`network_id`) USING BTREE,
  CONSTRAINT `branches_network_id_foreign` FOREIGN KEY (`network_id`) REFERENCES `branch_elements` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `branches_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `branch_elements` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for certificates
-- ----------------------------
DROP TABLE IF EXISTS `certificates`;
CREATE TABLE `certificates`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `no` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_id` int(10) UNSIGNED NOT NULL,
  `title_az` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `title_ru` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for chart_statistics
-- ----------------------------
DROP TABLE IF EXISTS `chart_statistics`;
CREATE TABLE `chart_statistics`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `template_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `product_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `value` int(11) NULL DEFAULT NULL,
  `year` int(11) NULL DEFAULT NULL,
  `percent` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chart_statistics
-- ----------------------------
INSERT INTO `chart_statistics` VALUES (1, 10, 1, 2, NULL, NULL, 10, '2020-01-19 13:32:06', '2020-01-23 13:33:20');
INSERT INTO `chart_statistics` VALUES (2, 2, 3, NULL, 63, 2019, NULL, '2020-01-21 14:51:22', '2020-01-21 14:51:22');
INSERT INTO `chart_statistics` VALUES (3, 2, 4, NULL, 19, 2019, NULL, '2020-01-21 14:51:50', '2020-01-21 14:51:50');
INSERT INTO `chart_statistics` VALUES (4, 2, 5, NULL, 44, 2019, NULL, '2020-01-21 14:52:23', '2020-01-21 14:52:23');
INSERT INTO `chart_statistics` VALUES (5, 1, NULL, NULL, 34, 2019, NULL, '2020-01-21 15:01:27', '2020-01-21 15:01:27');
INSERT INTO `chart_statistics` VALUES (6, 10, 7, 8, NULL, NULL, 50, '2020-01-23 13:21:28', '2020-01-23 13:32:46');
INSERT INTO `chart_statistics` VALUES (7, 10, 1, 8, NULL, NULL, 50, '2020-01-23 13:32:22', '2020-01-23 13:32:41');
INSERT INTO `chart_statistics` VALUES (8, 10, 14, 2, NULL, NULL, 30, '2020-01-23 13:33:38', '2020-01-23 13:33:38');
INSERT INTO `chart_statistics` VALUES (9, 10, 7, 2, NULL, NULL, 10, '2020-01-23 13:34:08', '2020-01-23 13:34:08');
INSERT INTO `chart_statistics` VALUES (10, 10, 17, 2, NULL, NULL, 10, '2020-01-23 13:34:19', '2020-01-23 13:34:19');
INSERT INTO `chart_statistics` VALUES (11, 10, 18, 2, NULL, NULL, 10, '2020-01-23 13:34:30', '2020-01-23 13:34:30');
INSERT INTO `chart_statistics` VALUES (12, 10, 1, 9, NULL, NULL, 60, '2020-01-23 13:35:22', '2020-01-23 13:35:22');
INSERT INTO `chart_statistics` VALUES (13, 10, 7, 9, NULL, NULL, 30, '2020-01-23 13:35:32', '2020-01-23 13:35:32');
INSERT INTO `chart_statistics` VALUES (14, 10, 16, 9, NULL, NULL, 10, '2020-01-23 13:35:44', '2020-01-23 13:35:44');
INSERT INTO `chart_statistics` VALUES (15, 10, 15, 10, NULL, NULL, 100, '2020-01-23 13:36:37', '2020-01-23 13:36:37');
INSERT INTO `chart_statistics` VALUES (16, 10, 1, 11, NULL, NULL, 50, '2020-01-23 13:36:55', '2020-01-23 13:36:55');
INSERT INTO `chart_statistics` VALUES (17, 10, 19, 11, NULL, NULL, 50, '2020-01-23 13:37:07', '2020-01-23 13:37:07');
INSERT INTO `chart_statistics` VALUES (18, 10, 1, 12, NULL, NULL, 50, '2020-01-23 13:38:02', '2020-01-23 13:38:02');
INSERT INTO `chart_statistics` VALUES (19, 10, 20, 12, NULL, NULL, 50, '2020-01-23 13:38:24', '2020-01-23 13:38:24');
INSERT INTO `chart_statistics` VALUES (20, 10, 1, 13, NULL, NULL, 20, '2020-01-23 13:39:15', '2020-01-23 13:39:15');
INSERT INTO `chart_statistics` VALUES (21, 10, 14, 13, NULL, NULL, 40, '2020-01-23 13:39:31', '2020-01-23 13:39:31');
INSERT INTO `chart_statistics` VALUES (22, 10, 19, 13, NULL, NULL, 40, '2020-01-23 13:39:45', '2020-01-23 13:39:45');
INSERT INTO `chart_statistics` VALUES (23, 3, 24, NULL, 3050, 2019, NULL, '2020-01-28 12:28:02', '2020-01-28 13:32:08');
INSERT INTO `chart_statistics` VALUES (24, 3, 25, NULL, 7140, 2019, NULL, '2020-01-28 12:28:45', '2020-01-28 13:32:03');

-- ----------------------------
-- Table structure for chart_titles
-- ----------------------------
DROP TABLE IF EXISTS `chart_titles`;
CREATE TABLE `chart_titles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(10) UNSIGNED NOT NULL,
  `order` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `title_az` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `title_ru` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `color` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'title',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chart_titles
-- ----------------------------
INSERT INTO `chart_titles` VALUES (1, 10, 7, 'Türkiyə', 'Turkey', 'Turkey', '#458fd2', 'title', '2020-01-19 13:31:22', '2020-01-23 13:41:58');
INSERT INTO `chart_titles` VALUES (2, 10, 1, 'İplik', 'Cotton Yarn', 'Cotton Yarn', NULL, 'line_x', '2020-01-19 13:33:22', '2020-01-28 12:20:27');
INSERT INTO `chart_titles` VALUES (3, 2, 0, 'Pambıq Çiyidi İstehsalı', 'Pambıq Çiyidi İstehsalı', 'Pambıq Çiyidi İstehsalı', '#5471d1', 'title', '2020-01-21 14:47:54', '2020-01-23 13:40:28');
INSERT INTO `chart_titles` VALUES (4, 2, 1, 'Yerli və Xarici bazara satılması', 'Yerli və Xarici bazara satılması', 'Yerli və Xarici bazara satılması', '#a60cc4', 'title', '2020-01-21 14:48:40', '2020-01-23 13:40:28');
INSERT INTO `chart_titles` VALUES (5, 2, 2, 'Pambıq Yağı Emalı', 'Pambıq Yağı Emalı', 'Pambıq Yağı Emalı', '#00d9b8', 'title', '2020-01-21 14:50:24', '2020-01-23 13:40:28');
INSERT INTO `chart_titles` VALUES (6, 1, 3, 'Jmıx istehsalı', 'Jmıx istehsalı', 'Jmıx istehsalı', '#0089d9', 'title', '2020-01-21 15:01:06', '2020-01-23 13:40:28');
INSERT INTO `chart_titles` VALUES (7, 10, 8, 'İran', 'Iran', 'İran', '#9c9c9c', 'title', '2020-01-23 13:17:38', '2020-01-23 13:41:58');
INSERT INTO `chart_titles` VALUES (8, 10, 1, 'Mahlıc', 'Cotton Fiber', 'Cotton Fiber', NULL, 'line_x', '2020-01-23 13:18:16', '2020-01-28 12:20:47');
INSERT INTO `chart_titles` VALUES (9, 10, 1, 'Pambıq Cecəsi', 'Cotton mill cake', 'Cotton mill cake', NULL, 'line_x', '2020-01-23 13:18:55', '2020-01-28 12:21:10');
INSERT INTO `chart_titles` VALUES (10, 10, 1, 'Pambıq Yağı', 'Cotton Oil', 'Cotton Oil', NULL, 'line_x', '2020-01-23 13:19:25', '2020-01-28 12:21:18');
INSERT INTO `chart_titles` VALUES (11, 10, 1, 'Tiftik', 'Cotton Down', 'Cotton Down', NULL, 'line_x', '2020-01-23 13:19:37', '2020-01-28 12:21:34');
INSERT INTO `chart_titles` VALUES (12, 10, 1, 'Pambıq Çiyidi', 'Cotton Seed', 'Cotton Seed', NULL, 'line_x', '2020-01-23 13:19:48', '2020-01-28 12:22:50');
INSERT INTO `chart_titles` VALUES (13, 10, 1, 'Lint', 'Lint', 'Lint', NULL, 'line_x', '2020-01-23 13:19:59', '2020-01-23 13:19:59');
INSERT INTO `chart_titles` VALUES (14, 10, 4, 'Rusiya', 'Russian', 'Russian', '#eb8500', 'title', '2020-01-23 13:26:12', '2020-01-23 13:41:04');
INSERT INTO `chart_titles` VALUES (15, 10, 9, 'Tacikistan', 'Tajikistan', 'Tajikistan', '#ffe600', 'title', '2020-01-23 13:27:27', '2020-01-23 13:41:58');
INSERT INTO `chart_titles` VALUES (16, 10, 10, 'Qazaxıstan', 'Qazaxıstan', 'Qazaxıstan', '#006aff', 'title', '2020-01-23 13:29:26', '2020-01-23 13:40:51');
INSERT INTO `chart_titles` VALUES (17, 10, 11, 'Ukrayna', 'Ukraine', 'Ukraine', '#00c210', 'title', '2020-01-23 13:30:04', '2020-01-23 13:40:51');
INSERT INTO `chart_titles` VALUES (18, 10, 12, 'Belarusiya', 'Belarusiya', 'Belarusiya', '#001391', 'title', '2020-01-23 13:30:37', '2020-01-23 13:40:51');
INSERT INTO `chart_titles` VALUES (19, 10, 6, 'Gürcüstan', 'Georgia', 'Georgia', '#8a4e00', 'title', '2020-01-23 13:31:14', '2020-01-23 13:42:01');
INSERT INTO `chart_titles` VALUES (20, 10, 5, 'İraq', 'İraq', 'İraq', '#595959', 'title', '2020-01-23 13:31:49', '2020-01-23 13:42:01');
INSERT INTO `chart_titles` VALUES (24, 3, 13, 'Arpa', 'Arpa', 'Arpa', '#329ea8', 'title', '2020-01-28 12:31:54', '2020-01-28 12:31:54');
INSERT INTO `chart_titles` VALUES (25, 3, 14, 'Buğda', 'Buğda', 'Buğda', '#9032a8', 'title', '2020-01-28 12:32:42', '2020-01-28 12:32:42');

-- ----------------------------
-- Table structure for dictionaries
-- ----------------------------
DROP TABLE IF EXISTS `dictionaries`;
CREATE TABLE `dictionaries`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(600) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `editor` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dictionaries_keyword_lang_id_unique`(`keyword`, `lang_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 602 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dictionaries
-- ----------------------------
INSERT INTO `dictionaries` VALUES (2, 'az', 'nothing_found_heading', 'Axtardığınız Səhifə Tapılmadı', 0, '2017-10-16 11:25:51', '2019-05-23 08:27:34');
INSERT INTO `dictionaries` VALUES (387, 'en', 'statistic_experience', 'İLLİK TƏCRÜBƏ', 0, '2019-05-25 20:24:43', '2019-05-25 20:24:43');
INSERT INTO `dictionaries` VALUES (3, 'az', 'nothing_found_title', 'AXTARDIĞINIZ SƏHİFƏ MÖVCUD DEYİL', 0, '2017-10-16 11:25:51', '2018-01-31 07:40:47');
INSERT INTO `dictionaries` VALUES (4, 'az', 'nothing_found_content', 'Axtardığınız məlumatları səhifəmizin üst hissəsində yerləşən MENYU vasitəsilə əldə edə bilərsiniz.', 0, '2017-10-16 11:25:51', '2018-01-31 07:33:36');
INSERT INTO `dictionaries` VALUES (5, 'az', 'copyright', 'Bütün hüquqlar qorunur!', 0, '2017-10-16 11:25:51', '2017-10-26 20:29:45');
INSERT INTO `dictionaries` VALUES (6, 'az', 'full_name', 'Ad, Soyad', 0, '2017-10-16 11:25:51', '2017-10-26 20:39:09');
INSERT INTO `dictionaries` VALUES (7, 'az', 'subject', 'Mövzu', 0, '2017-10-16 11:25:51', '2017-10-26 20:40:51');
INSERT INTO `dictionaries` VALUES (8, 'az', 'text', 'Məktub', 0, '2017-10-16 11:25:51', '2017-10-26 20:40:38');
INSERT INTO `dictionaries` VALUES (9, 'az', 'address', 'AZ 1029, Azərbaycan Respublikası, Bakı şəhəri, H.Əliyev prospekti, 95', 0, '2017-10-16 11:25:51', '2020-01-27 14:11:07');
INSERT INTO `dictionaries` VALUES (10, 'az', 'date', 'Tarix', 0, '2017-10-16 11:25:51', '2017-10-26 20:37:54');
INSERT INTO `dictionaries` VALUES (11, 'az', 'search', 'Axtarış', 0, '2017-10-16 11:25:51', '2017-10-26 20:41:52');
INSERT INTO `dictionaries` VALUES (12, 'az', 'email_sent', 'Məktubunuz göndərildi, təşəkkür edirik', 0, '2017-10-16 11:25:51', '2017-10-26 20:38:57');
INSERT INTO `dictionaries` VALUES (13, 'az', 'print', 'Çap üçün', 0, '2017-10-16 11:25:51', '2017-10-26 20:42:47');
INSERT INTO `dictionaries` VALUES (14, 'az', 'send', 'Göndər', 0, '2017-10-16 11:25:51', '2017-10-26 20:41:36');
INSERT INTO `dictionaries` VALUES (15, 'az', 'cancel', 'Ləğv et', 0, '2017-10-16 11:25:51', '2017-10-26 20:37:29');
INSERT INTO `dictionaries` VALUES (16, 'az', 'az', 'AZ', 0, '2017-10-16 11:25:51', '2018-01-31 08:17:17');
INSERT INTO `dictionaries` VALUES (17, 'az', 'en', 'EN', 0, '2017-10-16 11:25:51', '2018-01-31 08:17:29');
INSERT INTO `dictionaries` VALUES (18, 'az', 'ru', 'RU', 0, '2017-10-16 11:25:51', '2018-01-31 08:17:39');
INSERT INTO `dictionaries` VALUES (19, 'az', 'email', 'E-poçt', 0, '2017-10-16 11:25:51', '2017-10-26 20:38:42');
INSERT INTO `dictionaries` VALUES (20, 'az', 'phone', 'Əlaqə nömrəsi', 0, '2017-10-16 11:25:51', '2017-10-26 20:40:22');
INSERT INTO `dictionaries` VALUES (21, 'az', 'published', 'Dərc edildi', 0, '2017-10-16 11:25:51', '2017-10-26 20:42:34');
INSERT INTO `dictionaries` VALUES (22, 'az', 'read_more', 'Daha ətraflı', 0, '2017-10-16 11:25:51', '2017-10-26 20:43:05');
INSERT INTO `dictionaries` VALUES (23, 'az', 'gallery', '<strong>Foto</strong> qalereya', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (24, 'az', 'contact_us', 'BİZƏ MÜRACİƏT ET', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (25, 'az', 'send_by_email', 'Poçtla göndər', 0, '2017-10-16 11:25:51', '2017-10-26 20:41:17');
INSERT INTO `dictionaries` VALUES (62, 'en', 'address', NULL, 0, '2017-10-26 20:36:56', '2020-01-27 14:11:07');
INSERT INTO `dictionaries` VALUES (63, 'ru', 'address', NULL, 0, '2017-10-26 20:36:56', '2020-01-27 14:11:07');
INSERT INTO `dictionaries` VALUES (27, 'az', 'video_title', '<span>Reklam</span> filmlərimiz', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (28, 'az', 'video_summary', 'İzahlı videonu buradan izləyə bilərsiniz', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (29, 'az', 'contact_us_description', 'Aşağıdakı formanı doldurmaqla<br>bizə müraciət edə bilərsiniz', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (30, 'az', 'contact_us_summary', '* Biz sizinlə ən tez zamanda əlaqə saxlayacayıq', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (31, 'az', 'contact_us_button', 'MÜRACİƏT ET', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (32, 'az', 'subscribe', 'ABUNƏ OL', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (33, 'az', 'catalog', 'E-KATALOQ', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (35, 'az', 'subscribe_text', 'ATENA süd və süd məhsulları bazarına ilkin olaraq 32 növ məhsulla daxil olu', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (36, 'az', 'subscribe_title', '<h4>Atena xəbər və gündəlik yeniliklərinə</h4><h3>Abunə ol</h3>', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (37, 'az', 'expire_date', 'Saxlanma müddəti', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (38, 'az', 'category', 'Kateqoriya', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (39, 'az', 'day', 'gün', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (40, 'az', 'call_us', 'Bizə zəng et', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (41, 'az', 'contact_us_tab', 'Bizimlə əlaqə', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (42, 'az', 'customer_service_tab', 'Müştəri Xidmətləri', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (43, 'az', 'hr_tab', 'İnsan Resursları', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (44, 'az', 'work_hours', '<strong>İş</strong> saatları', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (45, 'az', 'subscribe_success', 'Yeniliklərə abunə oldunuz', 1, '2017-10-16 11:25:51', '2017-10-16 11:25:51');
INSERT INTO `dictionaries` VALUES (46, 'az', 'go_back', 'Geriyə qayıt', 0, '2017-10-16 11:27:08', '2017-10-24 10:30:36');
INSERT INTO `dictionaries` VALUES (47, 'az', 'search_article', 'Xəbəri ilinə görə axtarın', 1, '2017-10-17 19:35:28', '2017-10-17 19:35:28');
INSERT INTO `dictionaries` VALUES (51, 'az', 'similar_news', 'DİGƏR XƏBƏRLƏR', 0, '2017-10-24 10:26:45', '2017-10-24 10:26:45');
INSERT INTO `dictionaries` VALUES (52, 'en', 'similar_news', 'DİGƏR XƏBƏRLƏR', 0, '2017-10-24 10:26:45', '2017-10-24 10:26:45');
INSERT INTO `dictionaries` VALUES (53, 'ru', 'similar_news', 'DİGƏR XƏBƏRLƏR', 0, '2017-10-24 10:26:45', '2017-10-24 10:26:45');
INSERT INTO `dictionaries` VALUES (55, 'en', 'go_back', 'Geriyə qayıt', 0, '2017-10-24 10:30:36', '2017-10-24 10:30:36');
INSERT INTO `dictionaries` VALUES (56, 'ru', 'go_back', 'Geriyə qayıt', 0, '2017-10-24 10:30:36', '2017-10-24 10:30:36');
INSERT INTO `dictionaries` VALUES (58, 'en', 'copyright', 'All rights reserved!', 0, '2017-10-26 20:29:45', '2017-10-26 20:29:45');
INSERT INTO `dictionaries` VALUES (59, 'ru', 'copyright', 'Все права защищены', 0, '2017-10-26 20:29:45', '2017-10-26 20:29:45');
INSERT INTO `dictionaries` VALUES (65, 'en', 'cancel', 'Cancel', 0, '2017-10-26 20:37:29', '2017-10-26 20:37:29');
INSERT INTO `dictionaries` VALUES (66, 'ru', 'cancel', 'Отмена', 0, '2017-10-26 20:37:29', '2017-10-26 20:37:29');
INSERT INTO `dictionaries` VALUES (68, 'en', 'date', 'Date', 0, '2017-10-26 20:37:54', '2017-10-26 20:37:54');
INSERT INTO `dictionaries` VALUES (69, 'ru', 'date', 'Дата', 0, '2017-10-26 20:37:54', '2017-10-26 20:37:54');
INSERT INTO `dictionaries` VALUES (71, 'en', 'email', 'E-mail', 0, '2017-10-26 20:38:42', '2017-10-26 20:38:42');
INSERT INTO `dictionaries` VALUES (72, 'ru', 'email', 'Эл. Почта', 0, '2017-10-26 20:38:42', '2017-10-26 20:38:42');
INSERT INTO `dictionaries` VALUES (74, 'en', 'email_sent', 'Məktubunuz göndərildi, təşəkkür edirik', 0, '2017-10-26 20:38:57', '2017-10-26 20:38:57');
INSERT INTO `dictionaries` VALUES (75, 'ru', 'email_sent', 'Məktubunuz göndərildi, təşəkkür edirik', 0, '2017-10-26 20:38:57', '2017-10-26 20:38:57');
INSERT INTO `dictionaries` VALUES (77, 'en', 'full_name', 'Ad, Soyad', 0, '2017-10-26 20:39:09', '2017-10-26 20:39:09');
INSERT INTO `dictionaries` VALUES (78, 'ru', 'full_name', 'Ad, Soyad', 0, '2017-10-26 20:39:09', '2017-10-26 20:39:09');
INSERT INTO `dictionaries` VALUES (80, 'en', 'nothing_found_content', 'Axtardığınız məlumatları səhifəmizin üst hissəsində yerləşən MENYU vasitəsilə əldə edə bilərsiniz.', 0, '2017-10-26 20:39:33', '2018-01-31 07:33:36');
INSERT INTO `dictionaries` VALUES (81, 'ru', 'nothing_found_content', 'Axtardığınız məlumatları səhifəmizin üst hissəsində yerləşən MENYU vasitəsilə əldə edə bilərsiniz.', 0, '2017-10-26 20:39:33', '2018-01-31 07:33:36');
INSERT INTO `dictionaries` VALUES (83, 'en', 'nothing_found_heading', 'Axtardığınız Səhifə Tapılmadı', 0, '2017-10-26 20:39:53', '2019-05-23 08:27:34');
INSERT INTO `dictionaries` VALUES (84, 'ru', 'nothing_found_heading', 'Axtardığınız Səhifə Tapılmadı', 0, '2017-10-26 20:39:53', '2019-05-23 08:27:34');
INSERT INTO `dictionaries` VALUES (386, 'az', 'statistic_experience', 'İLLİK TƏCRÜBƏ', 0, '2019-05-25 20:24:43', '2019-05-25 20:24:43');
INSERT INTO `dictionaries` VALUES (86, 'en', 'nothing_found_title', 'AXTARDIĞINIZ SƏHİFƏ MÖVCUD DEYİL', 0, '2017-10-26 20:40:09', '2018-01-31 07:40:47');
INSERT INTO `dictionaries` VALUES (87, 'ru', 'nothing_found_title', 'AXTARDIĞINIZ SƏHİFƏ MÖVCUD DEYİL', 0, '2017-10-26 20:40:09', '2018-01-31 07:40:47');
INSERT INTO `dictionaries` VALUES (89, 'en', 'phone', 'Əlaqə nömrəsi', 0, '2017-10-26 20:40:22', '2017-10-26 20:40:22');
INSERT INTO `dictionaries` VALUES (90, 'ru', 'phone', 'Əlaqə nömrəsi', 0, '2017-10-26 20:40:22', '2017-10-26 20:40:22');
INSERT INTO `dictionaries` VALUES (92, 'en', 'text', 'Məktub', 0, '2017-10-26 20:40:38', '2017-10-26 20:40:38');
INSERT INTO `dictionaries` VALUES (93, 'ru', 'text', 'Məktub', 0, '2017-10-26 20:40:38', '2017-10-26 20:40:38');
INSERT INTO `dictionaries` VALUES (95, 'en', 'subject', 'Mövzu', 0, '2017-10-26 20:40:51', '2017-10-26 20:40:51');
INSERT INTO `dictionaries` VALUES (96, 'ru', 'subject', 'Mövzu', 0, '2017-10-26 20:40:51', '2017-10-26 20:40:51');
INSERT INTO `dictionaries` VALUES (98, 'en', 'send_by_email', 'Poçtla göndər', 0, '2017-10-26 20:41:17', '2017-10-26 20:41:17');
INSERT INTO `dictionaries` VALUES (99, 'ru', 'send_by_email', 'Poçtla göndər', 0, '2017-10-26 20:41:17', '2017-10-26 20:41:17');
INSERT INTO `dictionaries` VALUES (101, 'en', 'send', 'Göndər', 0, '2017-10-26 20:41:36', '2017-10-26 20:41:36');
INSERT INTO `dictionaries` VALUES (102, 'ru', 'send', 'Göndər', 0, '2017-10-26 20:41:36', '2017-10-26 20:41:36');
INSERT INTO `dictionaries` VALUES (104, 'en', 'search', 'Axtarış', 0, '2017-10-26 20:41:52', '2017-10-26 20:41:52');
INSERT INTO `dictionaries` VALUES (105, 'ru', 'search', 'Axtarış', 0, '2017-10-26 20:41:52', '2017-10-26 20:41:52');
INSERT INTO `dictionaries` VALUES (107, 'en', 'published', 'Dərc edildi', 0, '2017-10-26 20:42:34', '2017-10-26 20:42:34');
INSERT INTO `dictionaries` VALUES (108, 'ru', 'published', 'Dərc edildi', 0, '2017-10-26 20:42:34', '2017-10-26 20:42:34');
INSERT INTO `dictionaries` VALUES (110, 'en', 'print', 'Çap üçün', 0, '2017-10-26 20:42:47', '2017-10-26 20:42:47');
INSERT INTO `dictionaries` VALUES (111, 'ru', 'print', 'Çap üçün', 0, '2017-10-26 20:42:47', '2017-10-26 20:42:47');
INSERT INTO `dictionaries` VALUES (113, 'en', 'read_more', 'Daha ətraflı', 0, '2017-10-26 20:43:05', '2017-10-26 20:43:05');
INSERT INTO `dictionaries` VALUES (114, 'ru', 'read_more', 'Daha ətraflı', 0, '2017-10-26 20:43:05', '2017-10-26 20:43:05');
INSERT INTO `dictionaries` VALUES (116, 'az', 'home_page', 'Ana Səhifə', 0, '2018-01-31 07:34:11', '2018-01-31 07:34:41');
INSERT INTO `dictionaries` VALUES (117, 'en', 'home_page', 'Home Page', 0, '2018-01-31 07:34:11', '2018-01-31 07:34:41');
INSERT INTO `dictionaries` VALUES (118, 'ru', 'home_page', 'Главная Страница', 0, '2018-01-31 07:34:11', '2018-01-31 07:34:41');
INSERT INTO `dictionaries` VALUES (119, 'en', 'az', 'AZ', 0, '2018-01-31 08:17:17', '2018-01-31 08:17:17');
INSERT INTO `dictionaries` VALUES (120, 'ru', 'az', 'AZ', 0, '2018-01-31 08:17:17', '2018-01-31 08:17:17');
INSERT INTO `dictionaries` VALUES (121, 'en', 'en', 'EN', 0, '2018-01-31 08:17:29', '2018-01-31 08:17:29');
INSERT INTO `dictionaries` VALUES (122, 'ru', 'en', 'EN', 0, '2018-01-31 08:17:29', '2018-01-31 08:17:29');
INSERT INTO `dictionaries` VALUES (123, 'en', 'ru', 'RU', 0, '2018-01-31 08:17:39', '2018-01-31 08:17:39');
INSERT INTO `dictionaries` VALUES (124, 'ru', 'ru', 'RU', 0, '2018-01-31 08:17:39', '2018-01-31 08:17:39');
INSERT INTO `dictionaries` VALUES (143, 'az', 'photo_gallery', 'Foto Qalereya', 0, '2018-01-31 10:16:10', '2018-01-31 10:16:10');
INSERT INTO `dictionaries` VALUES (144, 'en', 'photo_gallery', 'Foto Qalereya', 0, '2018-01-31 10:16:10', '2018-01-31 10:16:10');
INSERT INTO `dictionaries` VALUES (145, 'ru', 'photo_gallery', 'Foto Qalereya', 0, '2018-01-31 10:16:10', '2018-01-31 10:16:10');
INSERT INTO `dictionaries` VALUES (146, 'az', 'nav_back', 'Geri', 0, '2018-01-31 12:23:38', '2018-01-31 12:23:38');
INSERT INTO `dictionaries` VALUES (147, 'en', 'nav_back', 'Geri', 0, '2018-01-31 12:23:38', '2018-01-31 12:23:38');
INSERT INTO `dictionaries` VALUES (148, 'ru', 'nav_back', 'Geri', 0, '2018-01-31 12:23:38', '2018-01-31 12:23:38');
INSERT INTO `dictionaries` VALUES (149, 'az', 'nav_forward', 'İrəli', 0, '2018-01-31 12:23:59', '2018-01-31 12:23:59');
INSERT INTO `dictionaries` VALUES (150, 'en', 'nav_forward', 'İrəli', 0, '2018-01-31 12:23:59', '2018-01-31 12:23:59');
INSERT INTO `dictionaries` VALUES (151, 'ru', 'nav_forward', 'İrəli', 0, '2018-01-31 12:23:59', '2018-01-31 12:23:59');
INSERT INTO `dictionaries` VALUES (152, 'az', 'weight', 'Çəkisi', 0, '2018-01-31 12:26:15', '2018-01-31 12:26:15');
INSERT INTO `dictionaries` VALUES (153, 'en', 'weight', 'Çəkisi', 0, '2018-01-31 12:26:15', '2018-01-31 12:26:15');
INSERT INTO `dictionaries` VALUES (154, 'ru', 'weight', 'Çəkisi', 0, '2018-01-31 12:26:15', '2018-01-31 12:26:15');
INSERT INTO `dictionaries` VALUES (161, 'az', 'duration_use', 'İstifadə müddəti', 0, '2018-01-31 12:38:48', '2018-01-31 12:38:48');
INSERT INTO `dictionaries` VALUES (162, 'en', 'duration_use', 'İstifadə müddəti', 0, '2018-01-31 12:38:48', '2018-01-31 12:38:48');
INSERT INTO `dictionaries` VALUES (163, 'ru', 'duration_use', 'İstifadə müddəti', 0, '2018-01-31 12:38:48', '2018-01-31 12:38:48');
INSERT INTO `dictionaries` VALUES (164, 'az', 'pressure_resistance', 'Təzyiqə müqaviməti', 0, '2018-01-31 12:39:08', '2018-01-31 12:39:08');
INSERT INTO `dictionaries` VALUES (165, 'en', 'pressure_resistance', 'Təzyiqə müqaviməti', 0, '2018-01-31 12:39:08', '2018-01-31 12:39:08');
INSERT INTO `dictionaries` VALUES (166, 'ru', 'pressure_resistance', 'Təzyiqə müqaviməti', 0, '2018-01-31 12:39:08', '2018-01-31 12:39:08');
INSERT INTO `dictionaries` VALUES (173, 'az', 'storage_duration', 'Saxlama müddəti', 0, '2018-01-31 12:40:55', '2018-01-31 12:40:55');
INSERT INTO `dictionaries` VALUES (174, 'en', 'storage_duration', 'Saxlama müddəti', 0, '2018-01-31 12:40:55', '2018-01-31 12:40:55');
INSERT INTO `dictionaries` VALUES (175, 'ru', 'storage_duration', 'Saxlama müddəti', 0, '2018-01-31 12:40:55', '2018-01-31 12:40:55');
INSERT INTO `dictionaries` VALUES (176, 'az', 'designation', 'Təyinatı', 0, '2018-01-31 12:41:29', '2018-01-31 12:41:29');
INSERT INTO `dictionaries` VALUES (177, 'en', 'designation', 'Təyinatı', 0, '2018-01-31 12:41:29', '2018-01-31 12:41:29');
INSERT INTO `dictionaries` VALUES (178, 'ru', 'designation', 'Təyinatı', 0, '2018-01-31 12:41:29', '2018-01-31 12:41:29');
INSERT INTO `dictionaries` VALUES (179, 'az', 'ingredients', 'Tərkibi', 0, '2018-01-31 12:41:47', '2018-01-31 12:41:47');
INSERT INTO `dictionaries` VALUES (180, 'en', 'ingredients', 'Tərkibi', 0, '2018-01-31 12:41:47', '2018-01-31 12:41:47');
INSERT INTO `dictionaries` VALUES (181, 'ru', 'ingredients', 'Tərkibi', 0, '2018-01-31 12:41:47', '2018-01-31 12:41:47');
INSERT INTO `dictionaries` VALUES (182, 'az', 'color', 'Rəng', 0, '2018-01-31 12:42:12', '2018-01-31 12:42:12');
INSERT INTO `dictionaries` VALUES (183, 'en', 'color', 'Rəng', 0, '2018-01-31 12:42:12', '2018-01-31 12:42:12');
INSERT INTO `dictionaries` VALUES (184, 'ru', 'color', 'Rəng', 0, '2018-01-31 12:42:12', '2018-01-31 12:42:12');
INSERT INTO `dictionaries` VALUES (185, 'az', 'description', 'Məlumat', 0, '2018-01-31 12:42:32', '2018-01-31 12:42:32');
INSERT INTO `dictionaries` VALUES (186, 'en', 'description', 'Məlumat', 0, '2018-01-31 12:42:32', '2018-01-31 12:42:32');
INSERT INTO `dictionaries` VALUES (187, 'ru', 'description', 'Məlumat', 0, '2018-01-31 12:42:32', '2018-01-31 12:42:32');
INSERT INTO `dictionaries` VALUES (188, 'az', 'technical_indicators', 'Texniki göstəriciləri', 0, '2018-01-31 12:42:55', '2018-01-31 12:42:55');
INSERT INTO `dictionaries` VALUES (189, 'en', 'technical_indicators', 'Texniki göstəriciləri', 0, '2018-01-31 12:42:55', '2018-01-31 12:42:55');
INSERT INTO `dictionaries` VALUES (190, 'ru', 'technical_indicators', 'Texniki göstəriciləri', 0, '2018-01-31 12:42:55', '2018-01-31 12:42:55');
INSERT INTO `dictionaries` VALUES (194, 'az', 'product_code', 'Məhsulun kodu', 0, '2018-01-31 12:47:08', '2018-01-31 12:47:08');
INSERT INTO `dictionaries` VALUES (195, 'en', 'product_code', 'Məhsulun kodu', 0, '2018-01-31 12:47:08', '2018-01-31 12:47:08');
INSERT INTO `dictionaries` VALUES (196, 'ru', 'product_code', 'Məhsulun kodu', 0, '2018-01-31 12:47:08', '2018-01-31 12:47:08');
INSERT INTO `dictionaries` VALUES (197, 'az', 'type', 'Məhsulun növü', 0, '2018-01-31 12:47:26', '2018-01-31 12:47:26');
INSERT INTO `dictionaries` VALUES (198, 'en', 'type', 'Məhsulun növü', 0, '2018-01-31 12:47:26', '2018-01-31 12:47:26');
INSERT INTO `dictionaries` VALUES (199, 'ru', 'type', 'Məhsulun növü', 0, '2018-01-31 12:47:26', '2018-01-31 12:47:26');
INSERT INTO `dictionaries` VALUES (200, 'az', 'size', 'Məhsulun ölçüləri', 0, '2018-01-31 12:47:45', '2018-01-31 12:47:45');
INSERT INTO `dictionaries` VALUES (201, 'en', 'size', 'Məhsulun ölçüləri', 0, '2018-01-31 12:47:45', '2018-01-31 12:47:45');
INSERT INTO `dictionaries` VALUES (202, 'ru', 'size', 'Məhsulun ölçüləri', 0, '2018-01-31 12:47:45', '2018-01-31 12:47:45');
INSERT INTO `dictionaries` VALUES (203, 'az', 'tecnical', 'Texniki göstəriciləri', 0, '2018-01-31 12:48:13', '2018-01-31 12:48:13');
INSERT INTO `dictionaries` VALUES (204, 'en', 'tecnical', 'Texniki göstəriciləri', 0, '2018-01-31 12:48:13', '2018-01-31 12:48:13');
INSERT INTO `dictionaries` VALUES (205, 'ru', 'tecnical', 'Texniki göstəriciləri', 0, '2018-01-31 12:48:13', '2018-01-31 12:48:13');
INSERT INTO `dictionaries` VALUES (206, 'az', 'rule', 'İstifadə qaydaları', 0, '2018-01-31 12:48:51', '2018-01-31 12:48:51');
INSERT INTO `dictionaries` VALUES (207, 'en', 'rule', 'İstifadə qaydaları', 0, '2018-01-31 12:48:51', '2018-01-31 12:48:51');
INSERT INTO `dictionaries` VALUES (208, 'ru', 'rule', 'İstifadə qaydaları', 0, '2018-01-31 12:48:51', '2018-01-31 12:48:51');
INSERT INTO `dictionaries` VALUES (213, 'en', 'similar_articles', '<strong>Oxşar</strong> Xəbərlər', 0, '2018-01-31 12:57:33', '2018-01-31 12:57:33');
INSERT INTO `dictionaries` VALUES (212, 'az', 'similar_articles', '<strong>Oxşar</strong> Xəbərlər', 0, '2018-01-31 12:57:33', '2018-01-31 12:57:33');
INSERT INTO `dictionaries` VALUES (214, 'ru', 'similar_articles', '<strong>Oxşar</strong> Xəbərlər', 0, '2018-01-31 12:57:33', '2018-01-31 12:57:33');
INSERT INTO `dictionaries` VALUES (215, 'az', 'name', 'Adınız', 0, '2018-01-31 13:44:33', '2018-01-31 13:44:33');
INSERT INTO `dictionaries` VALUES (216, 'en', 'name', 'Adınız', 0, '2018-01-31 13:44:33', '2018-01-31 13:44:33');
INSERT INTO `dictionaries` VALUES (217, 'ru', 'name', 'Adınız', 0, '2018-01-31 13:44:33', '2018-01-31 13:44:33');
INSERT INTO `dictionaries` VALUES (218, 'az', 'surname', 'Soyadınız', 0, '2018-01-31 13:45:10', '2018-01-31 13:45:10');
INSERT INTO `dictionaries` VALUES (219, 'en', 'surname', 'Soyadınız', 0, '2018-01-31 13:45:10', '2018-01-31 13:45:10');
INSERT INTO `dictionaries` VALUES (220, 'ru', 'surname', 'Soyadınız', 0, '2018-01-31 13:45:10', '2018-01-31 13:45:10');
INSERT INTO `dictionaries` VALUES (226, 'ru', 'new_dict', 'ru', 1, '2018-02-01 07:20:06', '2018-02-01 07:21:59');
INSERT INTO `dictionaries` VALUES (225, 'en', 'new_dict', 'en', 1, '2018-02-01 07:20:06', '2018-02-01 07:21:59');
INSERT INTO `dictionaries` VALUES (224, 'az', 'new_dict', 'aaa', 1, '2018-02-01 07:20:06', '2018-02-01 07:21:59');
INSERT INTO `dictionaries` VALUES (227, 'az', 'categories', 'Kateqoriyalar', 0, '2018-04-17 07:48:13', '2018-04-17 07:48:13');
INSERT INTO `dictionaries` VALUES (228, 'en', 'categories', 'Kateqoriyalar', 0, '2018-04-17 07:48:13', '2018-04-17 07:48:13');
INSERT INTO `dictionaries` VALUES (229, 'ru', 'categories', 'Kateqoriyalar', 0, '2018-04-17 07:48:13', '2018-04-17 07:48:13');
INSERT INTO `dictionaries` VALUES (230, 'az', 'enter_email', 'E-poçt daxil edin', 0, '2018-04-17 07:48:46', '2018-04-17 07:48:46');
INSERT INTO `dictionaries` VALUES (231, 'en', 'enter_email', 'E-poçt daxil edin', 0, '2018-04-17 07:48:46', '2018-04-17 07:48:46');
INSERT INTO `dictionaries` VALUES (232, 'ru', 'enter_email', 'E-poçt daxil edin', 0, '2018-04-17 07:48:46', '2018-04-17 07:48:46');
INSERT INTO `dictionaries` VALUES (233, 'az', 'repeat_email', 'E-poçtunuzu təkrarən daxil edin', 0, '2018-04-17 07:49:05', '2018-04-17 07:49:05');
INSERT INTO `dictionaries` VALUES (234, 'en', 'repeat_email', 'E-poçtunuzu təkrarən daxil edin', 0, '2018-04-17 07:49:05', '2018-04-17 07:49:05');
INSERT INTO `dictionaries` VALUES (235, 'ru', 'repeat_email', 'E-poçtunuzu təkrarən daxil edin', 0, '2018-04-17 07:49:05', '2018-04-17 07:49:05');
INSERT INTO `dictionaries` VALUES (236, 'az', 'country', 'Ölkə', 0, '2018-04-17 07:49:24', '2018-04-17 07:49:24');
INSERT INTO `dictionaries` VALUES (237, 'en', 'country', 'Ölkə', 0, '2018-04-17 07:49:24', '2018-04-17 07:49:24');
INSERT INTO `dictionaries` VALUES (238, 'ru', 'country', 'Ölkə', 0, '2018-04-17 07:49:24', '2018-04-17 07:49:24');
INSERT INTO `dictionaries` VALUES (239, 'az', 'sex', 'Cinsi', 0, '2018-04-17 07:49:42', '2018-04-17 07:49:42');
INSERT INTO `dictionaries` VALUES (240, 'en', 'sex', 'Cinsi', 0, '2018-04-17 07:49:42', '2018-04-17 07:49:42');
INSERT INTO `dictionaries` VALUES (241, 'ru', 'sex', 'Cinsi', 0, '2018-04-17 07:49:42', '2018-04-17 07:49:42');
INSERT INTO `dictionaries` VALUES (242, 'az', 'male', 'Kişi', 0, '2018-04-17 07:50:02', '2018-04-17 07:50:02');
INSERT INTO `dictionaries` VALUES (243, 'en', 'male', 'Kişi', 0, '2018-04-17 07:50:02', '2018-04-17 07:50:02');
INSERT INTO `dictionaries` VALUES (244, 'ru', 'male', 'Kişi', 0, '2018-04-17 07:50:02', '2018-04-17 07:50:02');
INSERT INTO `dictionaries` VALUES (245, 'az', 'female', 'Qadın', 0, '2018-04-17 07:50:26', '2018-04-17 07:50:26');
INSERT INTO `dictionaries` VALUES (246, 'en', 'female', 'Qadın', 0, '2018-04-17 07:50:26', '2018-04-17 07:50:26');
INSERT INTO `dictionaries` VALUES (247, 'ru', 'female', 'Qadın', 0, '2018-04-17 07:50:26', '2018-04-17 07:50:26');
INSERT INTO `dictionaries` VALUES (248, 'az', 'street', 'Küçə', 0, '2018-04-17 07:50:45', '2018-04-17 07:50:45');
INSERT INTO `dictionaries` VALUES (249, 'en', 'street', 'Küçə', 0, '2018-04-17 07:50:45', '2018-04-17 07:50:45');
INSERT INTO `dictionaries` VALUES (250, 'ru', 'street', 'Küçə', 0, '2018-04-17 07:50:45', '2018-04-17 07:50:45');
INSERT INTO `dictionaries` VALUES (251, 'az', 'city', 'Şəhər', 0, '2018-04-17 07:51:04', '2018-04-17 07:51:04');
INSERT INTO `dictionaries` VALUES (252, 'en', 'city', 'Şəhər', 0, '2018-04-17 07:51:04', '2018-04-17 07:51:04');
INSERT INTO `dictionaries` VALUES (253, 'ru', 'city', 'Şəhər', 0, '2018-04-17 07:51:04', '2018-04-17 07:51:04');
INSERT INTO `dictionaries` VALUES (254, 'az', 'zip_code', 'ZIP kod', 0, '2018-04-17 07:51:23', '2018-04-17 07:51:23');
INSERT INTO `dictionaries` VALUES (255, 'en', 'zip_code', 'ZIP kod', 0, '2018-04-17 07:51:23', '2018-04-17 07:51:23');
INSERT INTO `dictionaries` VALUES (256, 'ru', 'zip_code', 'ZIP kod', 0, '2018-04-17 07:51:23', '2018-04-17 07:51:23');
INSERT INTO `dictionaries` VALUES (260, 'az', 'vacancy_lang_az', 'Azərbaycan dili', 0, '2018-10-13 12:33:08', '2018-10-13 12:33:08');
INSERT INTO `dictionaries` VALUES (261, 'en', 'vacancy_lang_az', 'Azərbaycan dili', 0, '2018-10-13 12:33:08', '2018-10-13 12:33:08');
INSERT INTO `dictionaries` VALUES (262, 'ru', 'vacancy_lang_az', 'Azərbaycan dili', 0, '2018-10-13 12:33:08', '2018-10-13 12:33:08');
INSERT INTO `dictionaries` VALUES (263, 'az', 'vacancy_lang_ru', 'Rus dili', 0, '2018-10-13 12:35:20', '2018-10-13 12:35:20');
INSERT INTO `dictionaries` VALUES (264, 'en', 'vacancy_lang_ru', 'Rus dili', 0, '2018-10-13 12:35:20', '2018-10-13 12:35:20');
INSERT INTO `dictionaries` VALUES (265, 'ru', 'vacancy_lang_ru', 'Rus dili', 0, '2018-10-13 12:35:20', '2018-10-13 12:35:20');
INSERT INTO `dictionaries` VALUES (266, 'az', 'weak', 'Zəif', 0, '2018-10-13 12:36:54', '2018-10-13 12:36:54');
INSERT INTO `dictionaries` VALUES (267, 'en', 'weak', 'Zəif', 0, '2018-10-13 12:36:54', '2018-10-13 12:36:54');
INSERT INTO `dictionaries` VALUES (268, 'ru', 'weak', 'Zəif', 0, '2018-10-13 12:36:54', '2018-10-13 12:36:54');
INSERT INTO `dictionaries` VALUES (269, 'az', 'middle', 'Orta', 0, '2018-10-13 12:37:56', '2018-10-13 12:37:56');
INSERT INTO `dictionaries` VALUES (270, 'en', 'middle', 'Orta', 0, '2018-10-13 12:37:56', '2018-10-13 12:37:56');
INSERT INTO `dictionaries` VALUES (271, 'ru', 'middle', 'Orta', 0, '2018-10-13 12:37:56', '2018-10-13 12:37:56');
INSERT INTO `dictionaries` VALUES (275, 'az', 'the_best', 'Ana dili', 0, '2018-10-13 12:39:31', '2018-10-13 12:39:31');
INSERT INTO `dictionaries` VALUES (276, 'en', 'the_best', 'Ana dili', 0, '2018-10-13 12:39:31', '2018-10-13 12:39:31');
INSERT INTO `dictionaries` VALUES (277, 'ru', 'the_best', 'Ana dili', 0, '2018-10-13 12:39:31', '2018-10-13 12:39:31');
INSERT INTO `dictionaries` VALUES (278, 'az', 'vacancy_lang_en', 'English', 0, '2018-10-13 12:43:12', '2018-10-13 12:43:12');
INSERT INTO `dictionaries` VALUES (279, 'en', 'vacancy_lang_en', 'English', 0, '2018-10-13 12:43:12', '2018-10-13 12:43:12');
INSERT INTO `dictionaries` VALUES (280, 'ru', 'vacancy_lang_en', 'English', 0, '2018-10-13 12:43:12', '2018-10-13 12:43:12');
INSERT INTO `dictionaries` VALUES (281, 'az', 'single', 'Subay', 0, '2018-10-14 16:18:20', '2018-10-14 16:18:20');
INSERT INTO `dictionaries` VALUES (282, 'en', 'single', 'Subay', 0, '2018-10-14 16:18:20', '2018-10-14 16:18:20');
INSERT INTO `dictionaries` VALUES (283, 'ru', 'single', 'Subay', 0, '2018-10-14 16:18:20', '2018-10-14 16:18:20');
INSERT INTO `dictionaries` VALUES (284, 'az', 'married', 'Evli', 0, '2018-10-14 16:19:42', '2018-10-14 16:19:42');
INSERT INTO `dictionaries` VALUES (285, 'en', 'married', 'Evli', 0, '2018-10-14 16:19:42', '2018-10-14 16:19:42');
INSERT INTO `dictionaries` VALUES (286, 'ru', 'married', 'Evli', 0, '2018-10-14 16:19:42', '2018-10-14 16:19:42');
INSERT INTO `dictionaries` VALUES (287, 'az', 'widow', 'Dul', 0, '2018-10-14 16:30:10', '2018-10-14 16:30:10');
INSERT INTO `dictionaries` VALUES (288, 'en', 'widow', 'Dul', 0, '2018-10-14 16:30:10', '2018-10-14 16:30:10');
INSERT INTO `dictionaries` VALUES (289, 'ru', 'widow', 'Dul', 0, '2018-10-14 16:30:10', '2018-10-14 16:30:10');
INSERT INTO `dictionaries` VALUES (290, 'az', 'divorced', 'Boşanmış', 0, '2018-10-14 16:31:03', '2018-10-14 16:31:03');
INSERT INTO `dictionaries` VALUES (291, 'en', 'divorced', 'Boşanmış', 0, '2018-10-14 16:31:03', '2018-10-14 16:31:03');
INSERT INTO `dictionaries` VALUES (292, 'ru', 'divorced', 'Boşanmış', 0, '2018-10-14 16:31:03', '2018-10-14 16:31:03');
INSERT INTO `dictionaries` VALUES (293, 'az', 'military_service1', 'Mükəlləfiyyətli', 0, '2018-10-14 16:38:04', '2018-10-14 16:38:04');
INSERT INTO `dictionaries` VALUES (294, 'en', 'military_service1', 'Mükəlləfiyyətli', 0, '2018-10-14 16:38:04', '2018-10-14 16:38:04');
INSERT INTO `dictionaries` VALUES (295, 'ru', 'military_service1', 'Mükəlləfiyyətli', 0, '2018-10-14 16:38:04', '2018-10-14 16:38:04');
INSERT INTO `dictionaries` VALUES (296, 'az', 'military_service2', 'Mükəlləfiyyətsiz', 0, '2018-10-14 16:38:45', '2018-10-14 16:38:45');
INSERT INTO `dictionaries` VALUES (297, 'en', 'military_service2', 'Mükəlləfiyyətsiz', 0, '2018-10-14 16:38:45', '2018-10-14 16:38:45');
INSERT INTO `dictionaries` VALUES (298, 'ru', 'military_service2', 'Mükəlləfiyyətsiz', 0, '2018-10-14 16:38:45', '2018-10-14 16:38:45');
INSERT INTO `dictionaries` VALUES (299, 'az', 'have', 'Var', 0, '2018-10-14 16:44:48', '2018-10-14 16:44:48');
INSERT INTO `dictionaries` VALUES (300, 'en', 'have', 'Var', 0, '2018-10-14 16:44:48', '2018-10-14 16:44:48');
INSERT INTO `dictionaries` VALUES (301, 'ru', 'have', 'Var', 0, '2018-10-14 16:44:48', '2018-10-14 16:44:48');
INSERT INTO `dictionaries` VALUES (302, 'az', 'have\'t', 'Yoxdur', 0, '2018-10-14 16:48:48', '2018-10-14 16:48:48');
INSERT INTO `dictionaries` VALUES (303, 'en', 'have\'t', 'Yoxdur', 0, '2018-10-14 16:48:48', '2018-10-14 16:48:48');
INSERT INTO `dictionaries` VALUES (304, 'ru', 'have\'t', 'Yoxdur', 0, '2018-10-14 16:48:48', '2018-10-14 16:48:48');
INSERT INTO `dictionaries` VALUES (305, 'az', 'search_result', ':keyword sözü üzrə :count nəticə tapıldı', 0, '2018-10-15 12:34:47', '2018-10-15 12:34:47');
INSERT INTO `dictionaries` VALUES (306, 'en', 'search_result', ':keyword sözü üzrə :count nəticə tapıldı', 0, '2018-10-15 12:34:47', '2018-10-15 12:34:47');
INSERT INTO `dictionaries` VALUES (307, 'ru', 'search_result', ':keyword sözü üzrə :count nəticə tapıldı', 0, '2018-10-15 12:34:47', '2018-10-15 12:34:47');
INSERT INTO `dictionaries` VALUES (314, 'az', 'employee_text', 'Peşəkar işçi heyəti', 0, '2018-10-16 09:23:01', '2018-10-16 09:23:01');
INSERT INTO `dictionaries` VALUES (313, 'ru', 'office_text', 'ofisimizlə Xalqımıza xidmət', 0, '2018-10-16 08:08:03', '2018-10-16 08:08:03');
INSERT INTO `dictionaries` VALUES (312, 'en', 'office_text', 'ofisimizlə Xalqımıza xidmət', 0, '2018-10-16 08:08:03', '2018-10-16 08:08:03');
INSERT INTO `dictionaries` VALUES (311, 'az', 'office_text', 'ofisimizlə Xalqımıza xidmət', 0, '2018-10-16 08:08:03', '2018-10-16 08:08:03');
INSERT INTO `dictionaries` VALUES (315, 'en', 'employee_text', 'Peşəkar işçi heyəti', 0, '2018-10-16 09:23:01', '2018-10-16 09:23:01');
INSERT INTO `dictionaries` VALUES (316, 'ru', 'employee_text', 'Peşəkar işçi heyəti', 0, '2018-10-16 09:23:01', '2018-10-16 09:23:01');
INSERT INTO `dictionaries` VALUES (317, 'az', 'product_text', 'Keyfiyyətli marka sayı', 0, '2018-10-16 09:23:57', '2018-10-16 09:23:57');
INSERT INTO `dictionaries` VALUES (318, 'en', 'product_text', 'Keyfiyyətli marka sayı', 0, '2018-10-16 09:23:57', '2018-10-16 09:23:57');
INSERT INTO `dictionaries` VALUES (319, 'ru', 'product_text', 'Keyfiyyətli marka sayı', 0, '2018-10-16 09:23:57', '2018-10-16 09:23:57');
INSERT INTO `dictionaries` VALUES (325, 'ru', 'redirect_home_page', 'Ana səhifəyə geri dön', 0, '2019-01-06 21:19:16', '2019-01-06 21:19:16');
INSERT INTO `dictionaries` VALUES (324, 'en', 'redirect_home_page', 'Ana səhifəyə geri dön', 0, '2019-01-06 21:19:16', '2019-01-06 21:19:16');
INSERT INTO `dictionaries` VALUES (323, 'az', 'redirect_home_page', 'Ana səhifəyə geri dön', 0, '2019-01-06 21:19:16', '2019-01-06 21:19:16');
INSERT INTO `dictionaries` VALUES (326, 'az', 'password', 'Şifrə', 0, '2019-01-11 13:50:25', '2019-01-11 13:50:25');
INSERT INTO `dictionaries` VALUES (327, 'en', 'password', 'Şifrə', 0, '2019-01-11 13:50:25', '2019-01-11 13:50:25');
INSERT INTO `dictionaries` VALUES (328, 'ru', 'password', 'Şifrə', 0, '2019-01-11 13:50:25', '2019-01-11 13:50:25');
INSERT INTO `dictionaries` VALUES (329, 'az', 'repeat_password', 'Şifrə təkrar', 0, '2019-01-11 13:50:49', '2019-01-11 13:50:49');
INSERT INTO `dictionaries` VALUES (330, 'en', 'repeat_password', 'Şifrə təkrar', 0, '2019-01-11 13:50:49', '2019-01-11 13:50:49');
INSERT INTO `dictionaries` VALUES (331, 'ru', 'repeat_password', 'Şifrə təkrar', 0, '2019-01-11 13:50:49', '2019-01-11 13:50:49');
INSERT INTO `dictionaries` VALUES (332, 'az', 'hotels_type', 'Otağ növü', 0, '2019-02-12 08:36:12', '2019-02-12 08:36:12');
INSERT INTO `dictionaries` VALUES (333, 'en', 'hotels_type', 'Otağ növü', 0, '2019-02-12 08:36:13', '2019-02-12 08:36:13');
INSERT INTO `dictionaries` VALUES (334, 'ru', 'hotels_type', 'Otağ növü', 0, '2019-02-12 08:36:13', '2019-02-12 08:36:13');
INSERT INTO `dictionaries` VALUES (335, 'az', 'breakfast', 'Səhər yeməyi!', 0, '2019-02-12 13:05:56', '2019-02-12 13:09:30');
INSERT INTO `dictionaries` VALUES (336, 'en', 'breakfast', 'Səhər yeməyi', 0, '2019-02-12 13:05:56', '2019-02-12 13:09:30');
INSERT INTO `dictionaries` VALUES (337, 'ru', 'breakfast', 'Səhər yeməyi', 0, '2019-02-12 13:05:56', '2019-02-12 13:09:30');
INSERT INTO `dictionaries` VALUES (338, 'az', 'lunch', 'Gündüz yeməyi', 0, '2019-02-12 13:06:30', '2019-02-12 13:06:30');
INSERT INTO `dictionaries` VALUES (339, 'en', 'lunch', 'Gündüz yeməyi', 0, '2019-02-12 13:06:30', '2019-02-12 13:06:30');
INSERT INTO `dictionaries` VALUES (340, 'ru', 'lunch', 'Gündüz yeməyi', 0, '2019-02-12 13:06:31', '2019-02-12 13:06:31');
INSERT INTO `dictionaries` VALUES (341, 'az', 'supper', 'Axşam yeməyi', 0, '2019-02-12 13:06:47', '2019-02-12 13:06:47');
INSERT INTO `dictionaries` VALUES (342, 'en', 'supper', 'Axşam yeməyi', 0, '2019-02-12 13:06:47', '2019-02-12 13:06:47');
INSERT INTO `dictionaries` VALUES (343, 'ru', 'supper', 'Axşam yeməyi', 0, '2019-02-12 13:06:47', '2019-02-12 13:06:47');
INSERT INTO `dictionaries` VALUES (344, 'az', 'company_project_title', 'Şirkət layihələri aşağıdakı fəlsəfə əsasında həyata keçirir:', 0, '2019-05-18 17:29:03', '2019-05-18 17:29:03');
INSERT INTO `dictionaries` VALUES (345, 'en', 'company_project_title', 'Şirkət layihələri aşağıdakı fəlsəfə əsasında həyata keçirir:', 0, '2019-05-18 17:29:03', '2019-05-18 17:29:03');
INSERT INTO `dictionaries` VALUES (346, 'ru', 'company_project_title', 'Şirkət layihələri aşağıdakı fəlsəfə əsasında həyata keçirir:', 0, '2019-05-18 17:29:03', '2019-05-18 17:29:03');
INSERT INTO `dictionaries` VALUES (347, 'az', 'company_project_description', '<ul>\r\n	<li>Sifariş&ccedil;inin tələblərinə uyğun olaraq resursların planlamasını təşkil etmək</li>\r\n	<li>Tərəflər arası koordinasiyanı qurmaq və inkişaf etdirmək</li>\r\n	<li>G&ouml;r&uuml;lən işlərin keyfiyyətinə diqqət etmək</li>\r\n	<li>Layihəni vaxtında t', 1, '2019-05-18 17:29:36', '2019-05-18 17:30:17');
INSERT INTO `dictionaries` VALUES (348, 'en', 'company_project_description', '<ul>\r\n	<li>Sifariş&ccedil;inin tələblərinə uyğun olaraq resursların planlamasını təşkil etmək</li>\r\n	<li>Tərəflər arası koordinasiyanı qurmaq və inkişaf etdirmək</li>\r\n	<li>G&ouml;r&uuml;lən işlərin keyfiyyətinə diqqət etmək</li>\r\n	<li>Layihəni vaxtında t', 1, '2019-05-18 17:29:36', '2019-05-18 17:30:17');
INSERT INTO `dictionaries` VALUES (349, 'ru', 'company_project_description', '<ul>\r\n	<li>Sifariş&ccedil;inin tələblərinə uyğun olaraq resursların planlamasını təşkil etmək</li>\r\n	<li>Tərəflər arası koordinasiyanı qurmaq və inkişaf etdirmək</li>\r\n	<li>G&ouml;r&uuml;lən işlərin keyfiyyətinə diqqət etmək</li>\r\n	<li>Layihəni vaxtında t', 1, '2019-05-18 17:29:36', '2019-05-18 17:30:17');
INSERT INTO `dictionaries` VALUES (350, 'az', 'statistics_text', '<p>&ldquo;İmport Group AZ&rdquo; MMC şirkəti 2007-ci ildən Azərbaycanda tikinti, quraşdırma və layihələndirmə sektoru &uuml;zrə fəaliyyət g&ouml;stərir. Şirkətin əsas fəaliyyət istiqaməti daxili resurslarından istifadə etməklə m&uuml;xtəlif tikinti işləri yerinə yetirməkdir. Şirkətdə &ccedil;alışan əməkdaşlar qabaqcıl texnologiya və d&uuml;nya təcr&uuml;bəsini layihələrə tətbiq etməklə tikinti sektorunda fərqli dəsti xətt yaratmaqda davam edirlər.</p>', 1, '2019-05-18 17:35:15', '2019-05-18 17:38:03');
INSERT INTO `dictionaries` VALUES (351, 'en', 'statistics_text', '<p>&ldquo;İmport Group AZ&rdquo; MMC şirkəti 2007-ci ildən Azərbaycanda tikinti, quraşdırma və layihələndirmə sektoru &uuml;zrə fəaliyyət g&ouml;stərir. Şirkətin əsas fəaliyyət istiqaməti daxili resurslarından istifadə etməklə m&uuml;xtəlif tikinti işləri yerinə yetirməkdir. Şirkətdə &ccedil;alışan əməkdaşlar qabaqcıl texnologiya və d&uuml;nya təcr&uuml;bəsini layihələrə tətbiq etməklə tikinti sektorunda fərqli dəsti xətt yaratmaqda davam edirlər.</p>', 1, '2019-05-18 17:35:15', '2019-05-18 17:38:03');
INSERT INTO `dictionaries` VALUES (352, 'ru', 'statistics_text', '<p>&ldquo;İmport Group AZ&rdquo; MMC şirkəti 2007-ci ildən Azərbaycanda tikinti, quraşdırma və layihələndirmə sektoru &uuml;zrə fəaliyyət g&ouml;stərir. Şirkətin əsas fəaliyyət istiqaməti daxili resurslarından istifadə etməklə m&uuml;xtəlif tikinti işləri yerinə yetirməkdir. Şirkətdə &ccedil;alışan əməkdaşlar qabaqcıl texnologiya və d&uuml;nya təcr&uuml;bəsini layihələrə tətbiq etməklə tikinti sektorunda fərqli dəsti xətt yaratmaqda davam edirlər.</p>', 1, '2019-05-18 17:35:15', '2019-05-18 17:38:03');
INSERT INTO `dictionaries` VALUES (353, 'az', 'message', 'Mesajınız', 0, '2019-05-18 18:21:05', '2019-05-18 18:21:05');
INSERT INTO `dictionaries` VALUES (354, 'en', 'message', 'Mesajınız', 0, '2019-05-18 18:21:05', '2019-05-18 18:21:05');
INSERT INTO `dictionaries` VALUES (355, 'ru', 'message', 'Mesajınız', 0, '2019-05-18 18:21:05', '2019-05-18 18:21:05');
INSERT INTO `dictionaries` VALUES (356, 'az', 'quality_control_blog_title', 'İmport Group-un keyfiyyət sahəsində fəaliyyəti:', 0, '2019-05-18 19:48:05', '2019-05-18 19:48:05');
INSERT INTO `dictionaries` VALUES (357, 'en', 'quality_control_blog_title', 'İmport Group-un keyfiyyət sahəsində fəaliyyəti:', 0, '2019-05-18 19:48:05', '2019-05-18 19:48:05');
INSERT INTO `dictionaries` VALUES (358, 'ru', 'quality_control_blog_title', 'İmport Group-un keyfiyyət sahəsində fəaliyyəti:', 0, '2019-05-18 19:48:05', '2019-05-18 19:48:05');
INSERT INTO `dictionaries` VALUES (359, 'az', 'logistic_service_title', 'Sizə aşağıdakı məsələlərdə dəstək olmağa hazırıq:', 0, '2019-05-20 08:26:59', '2019-05-20 08:26:59');
INSERT INTO `dictionaries` VALUES (360, 'en', 'logistic_service_title', 'Sizə aşağıdakı məsələlərdə dəstək olmağa hazırıq:', 0, '2019-05-20 08:26:59', '2019-05-20 08:26:59');
INSERT INTO `dictionaries` VALUES (361, 'ru', 'logistic_service_title', 'Sizə aşağıdakı məsələlərdə dəstək olmağa hazırıq:', 0, '2019-05-20 08:26:59', '2019-05-20 08:26:59');
INSERT INTO `dictionaries` VALUES (362, 'az', 'logistic_service_description', '<ul>\r\n	<li>B&uuml;t&uuml;n zәruri nәqliyyat sәnәdlәrinin hazırlanması</li>\r\n	<li>Konteynerin verilmәsi və y&uuml;klәmә işlәrinin icrası</li>\r\n	<li>Konteynerin dәniz limanına daşınması</li>\r\n	<li>G&ouml;mr&uuml;k terminalında &ccedil;atdırılan y&uuml;k&uuml;n g&ouml;mr&uuml;k rәsmilәşdirilmәsi</li>\r\n	<li>Konteynerin boşaldılması, yaxud onun alıcının anbarına &ccedil;atdırılması</li>\r\n	<li>Y&uuml;k&uuml;n g&ouml;ndәrmә limanından tәyinat limanına &ccedil;atdırılması</li>\r\n	<li>Darida</li>\r\n</ul>', 1, '2019-05-20 08:28:20', '2019-05-20 08:29:13');
INSERT INTO `dictionaries` VALUES (363, 'en', 'logistic_service_description', '<ul>\r\n	<li>B&uuml;t&uuml;n zәruri nәqliyyat sәnәdlәrinin hazırlanması</li>\r\n	<li>Konteynerin verilmәsi və y&uuml;klәmә işlәrinin icrası</li>\r\n	<li>Konteynerin dәniz limanına daşınması</li>\r\n	<li>G&ouml;mr&uuml;k terminalında &ccedil;atdırılan y&uuml;k&uuml;n g&ouml;mr&uuml;k rәsmilәşdirilmәsi</li>\r\n	<li>Konteynerin boşaldılması, yaxud onun alıcının anbarına &ccedil;atdırılması</li>\r\n	<li>Y&uuml;k&uuml;n g&ouml;ndәrmә limanından tәyinat limanına &ccedil;atdırılması</li>\r\n</ul>', 1, '2019-05-20 08:28:20', '2019-05-20 08:29:13');
INSERT INTO `dictionaries` VALUES (364, 'ru', 'logistic_service_description', '<ul>\r\n	<li>B&uuml;t&uuml;n zәruri nәqliyyat sәnәdlәrinin hazırlanması</li>\r\n	<li>Konteynerin verilmәsi və y&uuml;klәmә işlәrinin icrası</li>\r\n	<li>Konteynerin dәniz limanına daşınması</li>\r\n	<li>G&ouml;mr&uuml;k terminalında &ccedil;atdırılan y&uuml;k&uuml;n g&ouml;mr&uuml;k rәsmilәşdirilmәsi</li>\r\n	<li>Konteynerin boşaldılması, yaxud onun alıcının anbarına &ccedil;atdırılması</li>\r\n	<li>Y&uuml;k&uuml;n g&ouml;ndәrmә limanından tәyinat limanına &ccedil;atdırılması</li>\r\n</ul>', 1, '2019-05-20 08:28:20', '2019-05-20 08:29:13');
INSERT INTO `dictionaries` VALUES (365, 'az', 'custom_works', 'Gömrük İşləri', 0, '2019-05-21 11:42:37', '2019-05-21 11:42:37');
INSERT INTO `dictionaries` VALUES (366, 'en', 'custom_works', 'Gömrük İşləri', 0, '2019-05-21 11:42:37', '2019-05-21 11:42:37');
INSERT INTO `dictionaries` VALUES (367, 'ru', 'custom_works', 'Gömrük İşləri', 0, '2019-05-21 11:42:37', '2019-05-21 11:42:37');
INSERT INTO `dictionaries` VALUES (368, 'az', 'custom_cover_name', 'YÜKÜNÜZÜ GÖMRÜKDƏ DEYİL, ANBARDA SAXLAYIN', 0, '2019-05-21 11:43:28', '2019-05-21 11:43:28');
INSERT INTO `dictionaries` VALUES (369, 'en', 'custom_cover_name', 'YÜKÜNÜZÜ GÖMRÜKDƏ DEYİL, ANBARDA SAXLAYIN', 0, '2019-05-21 11:43:28', '2019-05-21 11:43:28');
INSERT INTO `dictionaries` VALUES (370, 'ru', 'custom_cover_name', 'YÜKÜNÜZÜ GÖMRÜKDƏ DEYİL, ANBARDA SAXLAYIN', 0, '2019-05-21 11:43:28', '2019-05-21 11:43:28');
INSERT INTO `dictionaries` VALUES (371, 'az', 'custom_services_advantages', 'İmport Group-la gömrük xidmətlərinin üstünlükləri', 0, '2019-05-21 11:44:38', '2019-05-21 11:44:38');
INSERT INTO `dictionaries` VALUES (372, 'en', 'custom_services_advantages', 'İmport Group-la gömrük xidmətlərinin üstünlükləri', 0, '2019-05-21 11:44:38', '2019-05-21 11:44:38');
INSERT INTO `dictionaries` VALUES (373, 'ru', 'custom_services_advantages', 'İmport Group-la gömrük xidmətlərinin üstünlükləri', 0, '2019-05-21 11:44:38', '2019-05-21 11:44:38');
INSERT INTO `dictionaries` VALUES (374, 'az', 'consulting_check', 'Şirkətin aşağıdakı xidmətləri ilə bağlı ödənişsiz konsultasiya ala bilərsiniz:', 0, '2019-05-21 13:03:35', '2019-05-21 13:03:35');
INSERT INTO `dictionaries` VALUES (375, 'en', 'consulting_check', 'Şirkətin aşağıdakı xidmətləri ilə bağlı ödənişsiz konsultasiya ala bilərsiniz:', 0, '2019-05-21 13:03:35', '2019-05-21 13:03:35');
INSERT INTO `dictionaries` VALUES (376, 'ru', 'consulting_check', 'Şirkətin aşağıdakı xidmətləri ilə bağlı ödənişsiz konsultasiya ala bilərsiniz:', 0, '2019-05-21 13:03:35', '2019-05-21 13:03:35');
INSERT INTO `dictionaries` VALUES (377, 'az', 'professionals_consultation', 'PEŞƏKARLARDAN MƏSLƏHƏT ALIN', 0, '2019-05-21 13:06:59', '2019-05-21 13:06:59');
INSERT INTO `dictionaries` VALUES (378, 'en', 'professionals_consultation', 'PEŞƏKARLARDAN MƏSLƏHƏT ALIN', 0, '2019-05-21 13:06:59', '2019-05-21 13:06:59');
INSERT INTO `dictionaries` VALUES (379, 'ru', 'professionals_consultation', 'PEŞƏKARLARDAN MƏSLƏHƏT ALIN', 0, '2019-05-21 13:06:59', '2019-05-21 13:06:59');
INSERT INTO `dictionaries` VALUES (380, 'az', 'consultation_services_advantages', 'İmport Group-da Konsultasiya xidmətinin üstünlükləri:', 0, '2019-05-21 13:08:54', '2019-05-21 13:08:54');
INSERT INTO `dictionaries` VALUES (381, 'en', 'consultation_services_advantages', 'İmport Group-da Konsultasiya xidmətinin üstünlükləri:', 0, '2019-05-21 13:08:54', '2019-05-21 13:08:54');
INSERT INTO `dictionaries` VALUES (382, 'ru', 'consultation_services_advantages', 'İmport Group-da Konsultasiya xidmətinin üstünlükləri:', 0, '2019-05-21 13:08:54', '2019-05-21 13:08:54');
INSERT INTO `dictionaries` VALUES (383, 'az', 'leader_appeal_text_name', 'Bizim əsas prioritetimiz müştəri məmnuniyyətini və tikinti sektorunda keyfiyyətin təmin olunmasıdır.', 0, '2019-05-21 13:55:41', '2019-05-21 13:55:41');
INSERT INTO `dictionaries` VALUES (384, 'en', 'leader_appeal_text_name', 'Bizim əsas prioritetimiz müştəri məmnuniyyətini və tikinti sektorunda keyfiyyətin təmin olunmasıdır.', 0, '2019-05-21 13:55:41', '2019-05-21 13:55:41');
INSERT INTO `dictionaries` VALUES (385, 'ru', 'leader_appeal_text_name', 'Bizim əsas prioritetimiz müştəri məmnuniyyətini və tikinti sektorunda keyfiyyətin təmin olunmasıdır.', 0, '2019-05-21 13:55:41', '2019-05-21 13:55:41');
INSERT INTO `dictionaries` VALUES (388, 'ru', 'statistic_experience', 'İLLİK TƏCRÜBƏ', 0, '2019-05-25 20:24:43', '2019-05-25 20:24:43');
INSERT INTO `dictionaries` VALUES (389, 'az', 'statistic_service', 'XİDMƏT SAYI', 0, '2019-05-25 20:25:07', '2019-05-25 20:25:07');
INSERT INTO `dictionaries` VALUES (390, 'en', 'statistic_service', 'XİDMƏT SAYI', 0, '2019-05-25 20:25:07', '2019-05-25 20:25:07');
INSERT INTO `dictionaries` VALUES (391, 'ru', 'statistic_service', 'XİDMƏT SAYI', 0, '2019-05-25 20:25:07', '2019-05-25 20:25:07');
INSERT INTO `dictionaries` VALUES (392, 'az', 'statistic_project', 'LAYİHƏ SAYI', 0, '2019-05-25 20:25:29', '2019-05-25 20:25:29');
INSERT INTO `dictionaries` VALUES (393, 'en', 'statistic_project', 'LAYİHƏ SAYI', 0, '2019-05-25 20:25:29', '2019-05-25 20:25:29');
INSERT INTO `dictionaries` VALUES (394, 'ru', 'statistic_project', 'LAYİHƏ SAYI', 0, '2019-05-25 20:25:29', '2019-05-25 20:25:29');
INSERT INTO `dictionaries` VALUES (395, 'az', 'service_experience', 'XİDMƏT SAYI', 0, '2019-05-25 20:30:24', '2019-05-25 20:30:24');
INSERT INTO `dictionaries` VALUES (396, 'en', 'service_experience', 'XİDMƏT SAYI', 0, '2019-05-25 20:30:24', '2019-05-25 20:30:24');
INSERT INTO `dictionaries` VALUES (397, 'ru', 'service_experience', 'XİDMƏT SAYI', 0, '2019-05-25 20:30:24', '2019-05-25 20:30:24');
INSERT INTO `dictionaries` VALUES (398, 'az', 'project_experience', 'LAYİHƏ SAYI', 0, '2019-05-25 20:30:43', '2019-05-25 20:30:43');
INSERT INTO `dictionaries` VALUES (399, 'en', 'project_experience', 'LAYİHƏ SAYI', 0, '2019-05-25 20:30:43', '2019-05-25 20:30:43');
INSERT INTO `dictionaries` VALUES (400, 'ru', 'project_experience', 'LAYİHƏ SAYI', 0, '2019-05-25 20:30:43', '2019-05-25 20:30:43');
INSERT INTO `dictionaries` VALUES (401, 'az', 'post_meeting_subject', '\"Bizdən Görüş Al\" bölməsindən göndərilən məktub', 0, '2019-05-26 20:49:01', '2019-05-26 20:49:01');
INSERT INTO `dictionaries` VALUES (402, 'en', 'post_meeting_subject', '\"Bizdən Görüş Al\" bölməsindən göndərilən məktub', 0, '2019-05-26 20:49:01', '2019-05-26 20:49:01');
INSERT INTO `dictionaries` VALUES (403, 'ru', 'post_meeting_subject', '\"Bizdən Görüş Al\" bölməsindən göndərilən məktub', 0, '2019-05-26 20:49:01', '2019-05-26 20:49:01');
INSERT INTO `dictionaries` VALUES (404, 'az', 'post_contact_subject', '\"Əlaqə\" bölməsindən göndərilən məktub', 0, '2019-05-26 21:25:09', '2019-05-26 21:25:09');
INSERT INTO `dictionaries` VALUES (405, 'en', 'post_contact_subject', '\"Əlaqə\" bölməsindən göndərilən məktub', 0, '2019-05-26 21:25:09', '2019-05-26 21:25:09');
INSERT INTO `dictionaries` VALUES (406, 'ru', 'post_contact_subject', '\"Əlaqə\" bölməsindən göndərilən məktub', 0, '2019-05-26 21:25:09', '2019-05-26 21:25:09');
INSERT INTO `dictionaries` VALUES (407, 'az', 'post_work_subject', '\"İşçi̇ Qüvvəsi̇\" bölməsindən göndərilən məktub', 0, '2019-05-27 09:18:34', '2019-05-27 09:18:34');
INSERT INTO `dictionaries` VALUES (408, 'en', 'post_work_subject', '\"İşçi̇ Qüvvəsi̇\" bölməsindən göndərilən məktub', 0, '2019-05-27 09:18:34', '2019-05-27 09:18:34');
INSERT INTO `dictionaries` VALUES (409, 'ru', 'post_work_subject', '\"İşçi̇ Qüvvəsi̇\" bölməsindən göndərilən məktub', 0, '2019-05-27 09:18:34', '2019-05-27 09:18:34');
INSERT INTO `dictionaries` VALUES (410, 'az', 'order_butom', 'Sifariş et', 0, '2019-05-27 10:22:23', '2019-05-27 10:22:23');
INSERT INTO `dictionaries` VALUES (411, 'en', 'order_butom', 'Sifariş et', 0, '2019-05-27 10:22:23', '2019-05-27 10:22:23');
INSERT INTO `dictionaries` VALUES (412, 'ru', 'order_butom', 'Sifariş et', 0, '2019-05-27 10:22:23', '2019-05-27 10:22:23');
INSERT INTO `dictionaries` VALUES (413, 'az', 'post_order_subject', 'Yeni sifarış', 0, '2019-05-27 11:07:14', '2019-05-27 11:07:14');
INSERT INTO `dictionaries` VALUES (414, 'en', 'post_order_subject', 'Yeni sifarış', 0, '2019-05-27 11:07:14', '2019-05-27 11:07:14');
INSERT INTO `dictionaries` VALUES (415, 'ru', 'post_order_subject', 'Yeni sifarış', 0, '2019-05-27 11:07:14', '2019-05-27 11:07:14');
INSERT INTO `dictionaries` VALUES (416, 'az', 'post_service_subject', '\"Xidmətlər\" bölməsindən göndərilən məktub', 0, '2019-05-27 12:10:33', '2019-05-27 12:10:33');
INSERT INTO `dictionaries` VALUES (417, 'en', 'post_service_subject', '\"Xidmətlər\" bölməsindən göndərilən məktub', 0, '2019-05-27 12:10:33', '2019-05-27 12:10:33');
INSERT INTO `dictionaries` VALUES (418, 'ru', 'post_service_subject', '\"Xidmətlər\" bölməsindən göndərilən məktub', 0, '2019-05-27 12:10:33', '2019-05-27 12:10:33');
INSERT INTO `dictionaries` VALUES (419, 'az', 'experience', 'Təcrübə', 0, '2019-12-09 14:08:23', '2020-01-22 20:20:35');
INSERT INTO `dictionaries` VALUES (420, 'en', 'experience', 'd', 0, '2019-12-09 14:08:23', '2020-01-22 20:20:35');
INSERT INTO `dictionaries` VALUES (421, 'ru', 'experience', 'd', 0, '2019-12-09 14:08:23', '2020-01-22 20:20:35');
INSERT INTO `dictionaries` VALUES (422, 'az', 'technique', 'Texnika', 0, '2019-12-09 14:08:55', '2020-01-22 20:21:41');
INSERT INTO `dictionaries` VALUES (423, 'en', 'technique', 'd', 0, '2019-12-09 14:08:55', '2020-01-22 20:21:41');
INSERT INTO `dictionaries` VALUES (424, 'ru', 'technique', 'd', 0, '2019-12-09 14:08:55', '2020-01-22 20:21:41');
INSERT INTO `dictionaries` VALUES (425, 'az', 'enterprise', 'Müəssisə', 0, '2019-12-09 14:09:06', '2020-01-22 20:22:11');
INSERT INTO `dictionaries` VALUES (426, 'en', 'enterprise', 'd', 0, '2019-12-09 14:09:06', '2020-01-22 20:22:11');
INSERT INTO `dictionaries` VALUES (427, 'ru', 'enterprise', 'd', 0, '2019-12-09 14:09:06', '2020-01-22 20:22:11');
INSERT INTO `dictionaries` VALUES (428, 'az', 'product', 'Məhsul', 0, '2019-12-09 14:09:18', '2020-01-22 20:22:40');
INSERT INTO `dictionaries` VALUES (429, 'en', 'product', 'd', 0, '2019-12-09 14:09:18', '2020-01-22 20:22:40');
INSERT INTO `dictionaries` VALUES (430, 'ru', 'product', 'd', 0, '2019-12-09 14:09:18', '2020-01-22 20:22:40');
INSERT INTO `dictionaries` VALUES (431, 'az', 'area', 'Ərazi', 0, '2019-12-09 14:09:26', '2020-01-22 20:22:59');
INSERT INTO `dictionaries` VALUES (432, 'en', 'area', 'd', 0, '2019-12-09 14:09:26', '2020-01-22 20:22:59');
INSERT INTO `dictionaries` VALUES (433, 'ru', 'area', 'd', 0, '2019-12-09 14:09:26', '2020-01-22 20:22:59');
INSERT INTO `dictionaries` VALUES (434, 'az', 'harvest', 'Yığım', 0, '2019-12-09 14:09:35', '2020-01-22 20:23:18');
INSERT INTO `dictionaries` VALUES (435, 'en', 'harvest', 'd', 0, '2019-12-09 14:09:35', '2020-01-22 20:23:18');
INSERT INTO `dictionaries` VALUES (436, 'ru', 'harvest', 'd', 0, '2019-12-09 14:09:35', '2020-01-22 20:23:18');
INSERT INTO `dictionaries` VALUES (437, 'az', 'cotton_harvesting_title1', 'Pambığın yığım sezonu başladı', 0, '2019-12-09 14:09:44', '2020-01-19 15:58:16');
INSERT INTO `dictionaries` VALUES (438, 'en', 'cotton_harvesting_title1', 'Pambığın yığım sezonu başladı', 0, '2019-12-09 14:09:44', '2020-01-19 15:58:16');
INSERT INTO `dictionaries` VALUES (439, 'ru', 'cotton_harvesting_title1', 'Pambığın yığım sezonu başladı', 0, '2019-12-09 14:09:44', '2020-01-19 15:58:16');
INSERT INTO `dictionaries` VALUES (440, 'az', 'cotton_harvesting_title2', 'Hər kəsə uğurlar', 0, '2019-12-09 14:09:57', '2020-01-19 15:58:31');
INSERT INTO `dictionaries` VALUES (441, 'en', 'cotton_harvesting_title2', 'Hər kəsə uğurlar', 0, '2019-12-09 14:09:57', '2020-01-19 15:58:31');
INSERT INTO `dictionaries` VALUES (442, 'ru', 'cotton_harvesting_title2', 'Hər kəsə uğurlar', 0, '2019-12-09 14:09:57', '2020-01-19 15:58:31');
INSERT INTO `dictionaries` VALUES (443, 'az', 'about_title', '“MKT İstehsalat Kommersiya” MMC', 0, '2020-01-18 11:15:40', '2020-01-18 11:15:40');
INSERT INTO `dictionaries` VALUES (444, 'en', 'about_title', '“MKT İstehsalat Kommersiya” MMC', 0, '2020-01-18 11:15:40', '2020-01-18 11:15:40');
INSERT INTO `dictionaries` VALUES (445, 'ru', 'about_title', '“MKT İstehsalat Kommersiya” MMC', 0, '2020-01-18 11:15:40', '2020-01-18 11:15:40');
INSERT INTO `dictionaries` VALUES (446, 'az', 'company_images', 'Şirkətdən görüntülər', 0, '2020-01-18 11:17:38', '2020-01-18 11:17:38');
INSERT INTO `dictionaries` VALUES (447, 'en', 'company_images', 'Şirkətdən görüntülər', 0, '2020-01-18 11:17:38', '2020-01-18 11:17:38');
INSERT INTO `dictionaries` VALUES (448, 'ru', 'company_images', 'Şirkətdən görüntülər', 0, '2020-01-18 11:17:38', '2020-01-18 11:17:38');
INSERT INTO `dictionaries` VALUES (449, 'az', 'company_logos', 'Şirkətin loqosu', 0, '2020-01-18 11:18:08', '2020-01-18 11:18:08');
INSERT INTO `dictionaries` VALUES (450, 'en', 'company_logos', 'Şirkətin loqosu', 0, '2020-01-18 11:18:08', '2020-01-18 11:18:08');
INSERT INTO `dictionaries` VALUES (451, 'ru', 'company_logos', 'Şirkətin loqosu', 0, '2020-01-18 11:18:08', '2020-01-18 11:18:08');
INSERT INTO `dictionaries` VALUES (452, 'az', 'company_videos', 'Reklam çarxları', 0, '2020-01-18 11:18:31', '2020-01-18 11:18:31');
INSERT INTO `dictionaries` VALUES (453, 'en', 'company_videos', 'Reklam çarxları', 0, '2020-01-18 11:18:31', '2020-01-18 11:18:31');
INSERT INTO `dictionaries` VALUES (454, 'ru', 'company_videos', 'Reklam çarxları', 0, '2020-01-18 11:18:31', '2020-01-18 11:18:31');
INSERT INTO `dictionaries` VALUES (455, 'az', 'download', 'Yüklə', 0, '2020-01-18 11:20:06', '2020-01-18 11:20:06');
INSERT INTO `dictionaries` VALUES (456, 'en', 'download', 'Yüklə', 0, '2020-01-18 11:20:06', '2020-01-18 11:20:06');
INSERT INTO `dictionaries` VALUES (457, 'ru', 'download', 'Yüklə', 0, '2020-01-18 11:20:06', '2020-01-18 11:20:06');
INSERT INTO `dictionaries` VALUES (458, 'az', 'company_leader', 'Rövşən Həsənov', 0, '2020-01-18 11:23:32', '2020-01-20 09:28:27');
INSERT INTO `dictionaries` VALUES (459, 'en', 'company_leader', 'Əhməd Bəxtiyarov', 0, '2020-01-18 11:23:32', '2020-01-20 09:28:27');
INSERT INTO `dictionaries` VALUES (460, 'ru', 'company_leader', 'Əhməd Bəxtiyarov', 0, '2020-01-18 11:23:32', '2020-01-20 09:28:27');
INSERT INTO `dictionaries` VALUES (461, 'az', 'head_of_company', 'Şirkət rəhbəri', 0, '2020-01-18 11:23:58', '2020-01-18 11:23:58');
INSERT INTO `dictionaries` VALUES (462, 'en', 'head_of_company', 'Şirkət rəhbəri', 0, '2020-01-18 11:23:58', '2020-01-18 11:23:58');
INSERT INTO `dictionaries` VALUES (463, 'ru', 'head_of_company', 'Şirkət rəhbəri', 0, '2020-01-18 11:23:58', '2020-01-18 11:23:58');
INSERT INTO `dictionaries` VALUES (464, 'az', 'branch_title', 'Filialın adı', 0, '2020-01-19 11:57:42', '2020-01-19 11:57:42');
INSERT INTO `dictionaries` VALUES (465, 'en', 'branch_title', 'Filialın adı', 0, '2020-01-19 11:57:42', '2020-01-19 11:57:42');
INSERT INTO `dictionaries` VALUES (466, 'ru', 'branch_title', 'Filialın adı', 0, '2020-01-19 11:57:42', '2020-01-19 11:57:42');
INSERT INTO `dictionaries` VALUES (467, 'az', 'farmers_count', 'Fermer sayı', 0, '2020-01-19 11:58:04', '2020-01-19 11:58:04');
INSERT INTO `dictionaries` VALUES (468, 'en', 'farmers_count', 'Fermer sayı', 0, '2020-01-19 11:58:04', '2020-01-19 11:58:04');
INSERT INTO `dictionaries` VALUES (469, 'ru', 'farmers_count', 'Fermer sayı', 0, '2020-01-19 11:58:04', '2020-01-19 11:58:04');
INSERT INTO `dictionaries` VALUES (470, 'az', 'sown_area', 'Əkin sahəsi, ha', 0, '2020-01-19 11:58:23', '2020-01-19 11:58:23');
INSERT INTO `dictionaries` VALUES (471, 'en', 'sown_area', 'Əkin sahəsi, ha', 0, '2020-01-19 11:58:23', '2020-01-19 11:58:23');
INSERT INTO `dictionaries` VALUES (472, 'ru', 'sown_area', 'Əkin sahəsi, ha', 0, '2020-01-19 11:58:23', '2020-01-19 11:58:23');
INSERT INTO `dictionaries` VALUES (473, 'az', 'published_at', 'Yaranma tarixi', 0, '2020-01-19 11:58:46', '2020-01-19 11:58:46');
INSERT INTO `dictionaries` VALUES (474, 'en', 'published_at', 'Yaranma tarixi', 0, '2020-01-19 11:58:46', '2020-01-19 11:58:46');
INSERT INTO `dictionaries` VALUES (475, 'ru', 'published_at', 'Yaranma tarixi', 0, '2020-01-19 11:58:46', '2020-01-19 11:58:46');
INSERT INTO `dictionaries` VALUES (476, 'az', 'about_file_download', 'Şirkətin təqdimatını yüklə', 0, '2020-01-19 13:18:04', '2020-01-19 13:18:04');
INSERT INTO `dictionaries` VALUES (477, 'en', 'about_file_download', 'Şirkətin təqdimatını yüklə', 0, '2020-01-19 13:18:04', '2020-01-19 13:18:04');
INSERT INTO `dictionaries` VALUES (478, 'ru', 'about_file_download', 'Şirkətin təqdimatını yüklə', 0, '2020-01-19 13:18:04', '2020-01-19 13:18:04');
INSERT INTO `dictionaries` VALUES (479, 'az', 'contact_information', 'Əlaqə Məlumatları', 0, '2020-01-19 13:19:33', '2020-01-19 13:19:33');
INSERT INTO `dictionaries` VALUES (480, 'en', 'contact_information', 'Əlaqə Məlumatları', 0, '2020-01-19 13:19:33', '2020-01-19 13:19:33');
INSERT INTO `dictionaries` VALUES (481, 'ru', 'contact_information', 'Əlaqə Məlumatları', 0, '2020-01-19 13:19:33', '2020-01-19 13:19:33');
INSERT INTO `dictionaries` VALUES (482, 'az', 'location', 'Ünvan', 0, '2020-01-19 13:19:56', '2020-01-19 13:19:56');
INSERT INTO `dictionaries` VALUES (483, 'en', 'location', 'Ünvan', 0, '2020-01-19 13:19:56', '2020-01-19 13:19:56');
INSERT INTO `dictionaries` VALUES (484, 'ru', 'location', 'Ünvan', 0, '2020-01-19 13:19:56', '2020-01-19 13:19:56');
INSERT INTO `dictionaries` VALUES (485, 'az', 'telephone_number', 'Telefon nömrəsi:', 0, '2020-01-19 13:20:10', '2020-01-27 14:12:58');
INSERT INTO `dictionaries` VALUES (486, 'en', 'telephone_number', 'Telefon nömrəsi:', 0, '2020-01-19 13:20:10', '2020-01-27 14:12:58');
INSERT INTO `dictionaries` VALUES (487, 'ru', 'telephone_number', 'Telefon nömrəsi:', 0, '2020-01-19 13:20:10', '2020-01-27 14:12:58');
INSERT INTO `dictionaries` VALUES (488, 'az', 'contact_email', 'E-mail:', 0, '2020-01-19 13:20:26', '2020-01-19 13:20:26');
INSERT INTO `dictionaries` VALUES (489, 'en', 'contact_email', 'E-mail:', 0, '2020-01-19 13:20:26', '2020-01-19 13:20:26');
INSERT INTO `dictionaries` VALUES (490, 'ru', 'contact_email', 'E-mail:', 0, '2020-01-19 13:20:26', '2020-01-19 13:20:26');
INSERT INTO `dictionaries` VALUES (491, 'az', 'succes_message', 'M&uuml;raciətiniz qeydə alındı.<br />\r\nSizinlə tezliklə əlaqə yaradılacaq', 1, '2020-01-19 13:21:25', '2020-01-19 13:22:02');
INSERT INTO `dictionaries` VALUES (492, 'en', 'succes_message', 'M&uuml;raciətiniz qeydə alındı.<br />\r\nSizinlə tezliklə əlaqə yaradılacaq', 1, '2020-01-19 13:21:25', '2020-01-19 13:22:02');
INSERT INTO `dictionaries` VALUES (493, 'ru', 'succes_message', 'Müraciətiniz qeydə alındı.<br>Sizinlə tezliklə əlaqə yaradılacaq', 1, '2020-01-19 13:21:25', '2020-01-19 13:22:02');
INSERT INTO `dictionaries` VALUES (494, 'az', 'import_export_chart_title', 'İxrac etdiyimiz ölkələr 2018/2019', 1, '2020-01-19 13:30:20', '2020-01-19 13:30:20');
INSERT INTO `dictionaries` VALUES (495, 'en', 'import_export_chart_title', 'İxrac etdiyimiz ölkələr 2018/2019', 1, '2020-01-19 13:30:20', '2020-01-19 13:30:20');
INSERT INTO `dictionaries` VALUES (496, 'ru', 'import_export_chart_title', 'İxrac etdiyimiz ölkələr 2018/2019', 1, '2020-01-19 13:30:20', '2020-01-19 13:30:20');
INSERT INTO `dictionaries` VALUES (497, 'az', 'dear_customer', 'Hörmətli müştəri,', 1, '2020-01-19 13:39:40', '2020-01-19 13:39:40');
INSERT INTO `dictionaries` VALUES (498, 'en', 'dear_customer', 'Hörmətli müştəri,', 1, '2020-01-19 13:39:40', '2020-01-19 13:39:40');
INSERT INTO `dictionaries` VALUES (499, 'ru', 'dear_customer', 'Hörmətli müştəri,', 1, '2020-01-19 13:39:40', '2020-01-19 13:39:40');
INSERT INTO `dictionaries` VALUES (500, 'az', 'comment', 'İrad', 1, '2020-01-19 13:40:28', '2020-01-19 13:42:44');
INSERT INTO `dictionaries` VALUES (501, 'en', 'comment', 'İrad', 1, '2020-01-19 13:40:28', '2020-01-19 13:42:44');
INSERT INTO `dictionaries` VALUES (502, 'ru', 'comment', 'İrad', 1, '2020-01-19 13:40:28', '2020-01-19 13:42:44');
INSERT INTO `dictionaries` VALUES (503, 'az', 'suggestion', 'Təklif', 1, '2020-01-19 13:42:15', '2020-01-19 13:42:15');
INSERT INTO `dictionaries` VALUES (504, 'en', 'suggestion', 'Təklif', 1, '2020-01-19 13:42:15', '2020-01-19 13:42:15');
INSERT INTO `dictionaries` VALUES (505, 'ru', 'suggestion', 'Təklif', 1, '2020-01-19 13:42:15', '2020-01-19 13:42:15');
INSERT INTO `dictionaries` VALUES (506, 'az', 'tenders_name', 'Tenderin adı:', 1, '2020-01-19 13:55:22', '2020-01-19 13:55:22');
INSERT INTO `dictionaries` VALUES (507, 'en', 'tenders_name', 'Tenderin adı:', 1, '2020-01-19 13:55:22', '2020-01-19 13:55:22');
INSERT INTO `dictionaries` VALUES (508, 'ru', 'tenders_name', 'Tenderin adı:', 1, '2020-01-19 13:55:22', '2020-01-19 13:55:22');
INSERT INTO `dictionaries` VALUES (509, 'az', 'tenders_published_at', 'Yerləşdirilmə tarixi', 1, '2020-01-19 13:55:37', '2020-01-19 13:55:37');
INSERT INTO `dictionaries` VALUES (510, 'en', 'tenders_published_at', 'Yerləşdirilmə tarixi', 1, '2020-01-19 13:55:37', '2020-01-19 13:55:37');
INSERT INTO `dictionaries` VALUES (511, 'ru', 'tenders_published_at', 'Yerləşdirilmə tarixi', 1, '2020-01-19 13:55:37', '2020-01-19 13:55:37');
INSERT INTO `dictionaries` VALUES (512, 'az', 'tenders_end_date', 'Bitmə tarixi', 1, '2020-01-19 13:55:59', '2020-01-19 13:55:59');
INSERT INTO `dictionaries` VALUES (513, 'en', 'tenders_end_date', 'Bitmə tarixi', 1, '2020-01-19 13:55:59', '2020-01-19 13:55:59');
INSERT INTO `dictionaries` VALUES (514, 'ru', 'tenders_end_date', 'Bitmə tarixi', 1, '2020-01-19 13:55:59', '2020-01-19 13:55:59');
INSERT INTO `dictionaries` VALUES (515, 'az', 'more', 'Ətraflı', 1, '2020-01-19 13:56:14', '2020-01-19 13:56:14');
INSERT INTO `dictionaries` VALUES (516, 'en', 'more', 'Ətraflı', 1, '2020-01-19 13:56:14', '2020-01-19 13:56:14');
INSERT INTO `dictionaries` VALUES (517, 'ru', 'more', 'Ətraflı', 1, '2020-01-19 13:56:14', '2020-01-19 13:56:14');
INSERT INTO `dictionaries` VALUES (518, 'az', 'details', 'Ətraflı məlumat', 1, '2020-01-19 13:57:09', '2020-01-19 13:57:09');
INSERT INTO `dictionaries` VALUES (519, 'en', 'details', 'Ətraflı məlumat', 1, '2020-01-19 13:57:09', '2020-01-19 13:57:09');
INSERT INTO `dictionaries` VALUES (520, 'ru', 'details', 'Ətraflı məlumat', 1, '2020-01-19 13:57:09', '2020-01-19 13:57:09');
INSERT INTO `dictionaries` VALUES (521, 'az', 'conditions', 'Şərtlər', 0, '2020-01-19 13:58:58', '2020-01-19 13:58:58');
INSERT INTO `dictionaries` VALUES (522, 'en', 'conditions', 'Şərtlər', 0, '2020-01-19 13:58:58', '2020-01-19 13:58:58');
INSERT INTO `dictionaries` VALUES (523, 'ru', 'conditions', 'Şərtlər', 0, '2020-01-19 13:58:58', '2020-01-19 13:58:58');
INSERT INTO `dictionaries` VALUES (524, 'az', 'contact_form', 'Müraciət formu', 0, '2020-01-19 13:59:14', '2020-01-19 13:59:14');
INSERT INTO `dictionaries` VALUES (525, 'en', 'contact_form', 'Müraciət formu', 0, '2020-01-19 13:59:14', '2020-01-19 13:59:14');
INSERT INTO `dictionaries` VALUES (526, 'ru', 'contact_form', 'Müraciət formu', 0, '2020-01-19 13:59:14', '2020-01-19 13:59:14');
INSERT INTO `dictionaries` VALUES (527, 'az', 'enterprise_catalog_download', 'Məhsul Kataloqunu yüklə', 0, '2020-01-19 14:15:28', '2020-01-19 14:15:28');
INSERT INTO `dictionaries` VALUES (528, 'en', 'enterprise_catalog_download', 'Məhsul Kataloqunu yüklə', 0, '2020-01-19 14:15:28', '2020-01-19 14:15:28');
INSERT INTO `dictionaries` VALUES (529, 'ru', 'enterprise_catalog_download', 'Məhsul Kataloqunu yüklə', 0, '2020-01-19 14:15:28', '2020-01-19 14:15:28');
INSERT INTO `dictionaries` VALUES (530, 'az', 'enterprise2_techinal_parametrs', 'Pambıq yağının texniki göstəriciləri', 0, '2020-01-19 14:50:27', '2020-01-19 14:50:27');
INSERT INTO `dictionaries` VALUES (531, 'en', 'enterprise2_techinal_parametrs', 'Pambıq yağının texniki göstəriciləri', 0, '2020-01-19 14:50:27', '2020-01-19 14:50:27');
INSERT INTO `dictionaries` VALUES (532, 'ru', 'enterprise2_techinal_parametrs', 'Pambıq yağının texniki göstəriciləri', 0, '2020-01-19 14:50:28', '2020-01-19 14:50:28');
INSERT INTO `dictionaries` VALUES (533, 'az', 'product2_technical_parameters', 'İplik &Ccedil;eşidləri', 1, '2020-01-19 15:05:21', '2020-01-30 21:53:23');
INSERT INTO `dictionaries` VALUES (534, 'en', 'product2_technical_parameters', 'Technical parameters of technical cotton seeds<br />\r\n(Texniki ciyidin texniki parametirləri)', 1, '2020-01-19 15:05:21', '2020-01-30 21:53:23');
INSERT INTO `dictionaries` VALUES (535, 'ru', 'product2_technical_parameters', 'Technical parameters of technical cotton seeds<br />\r\n(Texniki ciyidin texniki parametirləri)', 1, '2020-01-19 15:05:21', '2020-01-30 21:53:23');
INSERT INTO `dictionaries` VALUES (536, 'az', 'product2_chart_title', 'Çiyidin istehsal göstəricisi', 0, '2020-01-19 15:05:59', '2020-01-19 15:05:59');
INSERT INTO `dictionaries` VALUES (537, 'en', 'product2_chart_title', 'Çiyidin istehsal göstəricisi', 0, '2020-01-19 15:05:59', '2020-01-19 15:05:59');
INSERT INTO `dictionaries` VALUES (538, 'ru', 'product2_chart_title', 'Çiyidin istehsal göstəricisi', 0, '2020-01-19 15:05:59', '2020-01-19 15:05:59');
INSERT INTO `dictionaries` VALUES (539, 'az', 'other_products', 'Digər Məhsullar', 0, '2020-01-19 15:06:22', '2020-01-19 15:06:22');
INSERT INTO `dictionaries` VALUES (540, 'en', 'other_products', 'Digər Məhsullar', 0, '2020-01-19 15:06:22', '2020-01-19 15:06:22');
INSERT INTO `dictionaries` VALUES (541, 'ru', 'other_products', 'Digər Məhsullar', 0, '2020-01-19 15:06:22', '2020-01-19 15:06:22');
INSERT INTO `dictionaries` VALUES (542, 'az', 'mahlic_technial_parametrs', 'Texniki göstəriciləri', 0, '2020-01-19 15:11:50', '2020-01-19 15:11:50');
INSERT INTO `dictionaries` VALUES (543, 'en', 'mahlic_technial_parametrs', 'Texniki göstəriciləri', 0, '2020-01-19 15:11:50', '2020-01-19 15:11:50');
INSERT INTO `dictionaries` VALUES (544, 'ru', 'mahlic_technial_parametrs', 'Texniki göstəriciləri', 0, '2020-01-19 15:11:50', '2020-01-19 15:11:50');
INSERT INTO `dictionaries` VALUES (545, 'az', 'mahlic_chart1_title', 'İstehsal edilən mahlıcın keyfiyyət göstəricilərinə görə paylaşdırılma', 0, '2020-01-19 15:13:39', '2020-01-19 15:13:39');
INSERT INTO `dictionaries` VALUES (546, 'en', 'mahlic_chart1_title', 'İstehsal edilən mahlıcın keyfiyyət göstəricilərinə görə paylaşdırılma', 0, '2020-01-19 15:13:39', '2020-01-19 15:13:39');
INSERT INTO `dictionaries` VALUES (547, 'ru', 'mahlic_chart1_title', 'İstehsal edilən mahlıcın keyfiyyət göstəricilərinə görə paylaşdırılma', 0, '2020-01-19 15:13:39', '2020-01-19 15:13:39');
INSERT INTO `dictionaries` VALUES (548, 'az', 'mahlic_circle_chart1_title', 'Uzunluq (2018/2019)', 0, '2020-01-19 15:13:56', '2020-01-19 15:13:56');
INSERT INTO `dictionaries` VALUES (549, 'en', 'mahlic_circle_chart1_title', 'Uzunluq (2018/2019)', 0, '2020-01-19 15:13:56', '2020-01-19 15:13:56');
INSERT INTO `dictionaries` VALUES (550, 'ru', 'mahlic_circle_chart1_title', 'Uzunluq (2018/2019)', 0, '2020-01-19 15:13:56', '2020-01-19 15:13:56');
INSERT INTO `dictionaries` VALUES (551, 'az', 'mahlic_circle_chart2_title', 'Rəng (HVİ) (2018/2019)', 0, '2020-01-19 15:14:09', '2020-01-19 15:14:09');
INSERT INTO `dictionaries` VALUES (552, 'en', 'mahlic_circle_chart2_title', 'Rəng (HVİ) (2018/2019)', 0, '2020-01-19 15:14:09', '2020-01-19 15:14:09');
INSERT INTO `dictionaries` VALUES (553, 'ru', 'mahlic_circle_chart2_title', 'Rəng (HVİ) (2018/2019)', 0, '2020-01-19 15:14:09', '2020-01-19 15:14:09');
INSERT INTO `dictionaries` VALUES (554, 'az', 'mahlic_circle_chart3_title', 'Ştapel (2018/2019)', 0, '2020-01-19 15:15:15', '2020-01-19 15:15:15');
INSERT INTO `dictionaries` VALUES (555, 'en', 'mahlic_circle_chart3_title', 'Ştapel (2018/2019)', 0, '2020-01-19 15:15:15', '2020-01-19 15:15:15');
INSERT INTO `dictionaries` VALUES (556, 'ru', 'mahlic_circle_chart3_title', 'Ştapel (2018/2019)', 0, '2020-01-19 15:15:15', '2020-01-19 15:15:15');
INSERT INTO `dictionaries` VALUES (557, 'az', 'mahlic_circle_chart4_title', 'Micronaire (2018/2019)', 0, '2020-01-19 15:15:29', '2020-01-19 15:15:29');
INSERT INTO `dictionaries` VALUES (558, 'en', 'mahlic_circle_chart4_title', 'Micronaire (2018/2019)', 0, '2020-01-19 15:15:29', '2020-01-19 15:15:29');
INSERT INTO `dictionaries` VALUES (559, 'ru', 'mahlic_circle_chart4_title', 'Micronaire (2018/2019)', 0, '2020-01-19 15:15:29', '2020-01-19 15:15:29');
INSERT INTO `dictionaries` VALUES (560, 'az', 'mahlic_chart2_title', 'Mahlıcın növü 2018/2019 (rəng/zibillik)', 0, '2020-01-19 15:15:45', '2020-01-19 15:15:45');
INSERT INTO `dictionaries` VALUES (561, 'en', 'mahlic_chart2_title', 'Mahlıcın növü 2018/2019 (rəng/zibillik)', 0, '2020-01-19 15:15:45', '2020-01-19 15:15:45');
INSERT INTO `dictionaries` VALUES (562, 'ru', 'mahlic_chart2_title', 'Mahlıcın növü 2018/2019 (rəng/zibillik)', 0, '2020-01-19 15:15:45', '2020-01-19 15:15:45');
INSERT INTO `dictionaries` VALUES (563, 'az', 'product_name', 'Məhsulun adı:', 0, '2020-01-19 15:16:07', '2020-01-19 15:16:07');
INSERT INTO `dictionaries` VALUES (564, 'en', 'product_name', 'Məhsulun adı:', 0, '2020-01-19 15:16:07', '2020-01-19 15:16:07');
INSERT INTO `dictionaries` VALUES (565, 'ru', 'product_name', 'Məhsulun adı:', 0, '2020-01-19 15:16:07', '2020-01-19 15:16:07');
INSERT INTO `dictionaries` VALUES (566, 'az', 'product1_name_comp', NULL, 0, '2020-01-19 15:20:58', '2020-02-02 08:48:06');
INSERT INTO `dictionaries` VALUES (567, 'en', 'product1_name_comp', NULL, 0, '2020-01-19 15:20:58', '2020-02-02 08:48:06');
INSERT INTO `dictionaries` VALUES (568, 'ru', 'product1_name_comp', NULL, 0, '2020-01-19 15:20:58', '2020-02-02 08:48:06');
INSERT INTO `dictionaries` VALUES (569, 'az', 'product1_indicators', 'Göstəriçilər (indicators):', 0, '2020-01-19 15:21:19', '2020-01-19 15:21:19');
INSERT INTO `dictionaries` VALUES (570, 'en', 'product1_indicators', 'Göstəriçilər (indicators):', 0, '2020-01-19 15:21:19', '2020-01-19 15:21:19');
INSERT INTO `dictionaries` VALUES (571, 'ru', 'product1_indicators', 'Göstəriçilər (indicators):', 0, '2020-01-19 15:21:19', '2020-01-19 15:21:19');
INSERT INTO `dictionaries` VALUES (572, 'az', 'product1_test_result', 'Sınaq nəticəsi ( The result of test)', 0, '2020-01-19 15:21:35', '2020-01-19 15:21:35');
INSERT INTO `dictionaries` VALUES (573, 'en', 'product1_test_result', 'Sınaq nəticəsi ( The result of test)', 0, '2020-01-19 15:21:35', '2020-01-19 15:21:35');
INSERT INTO `dictionaries` VALUES (574, 'ru', 'product1_test_result', 'Sınaq nəticəsi ( The result of test)', 0, '2020-01-19 15:21:35', '2020-01-19 15:21:35');
INSERT INTO `dictionaries` VALUES (575, 'az', 'product1_chart_title', 'Pambıq Cecəsi istehsalı üzrə göstəricilər', 0, '2020-01-19 15:21:53', '2020-02-02 08:47:03');
INSERT INTO `dictionaries` VALUES (576, 'en', 'product1_chart_title', 'Jmıxın istehsal göstəricisi', 0, '2020-01-19 15:21:53', '2020-02-02 08:47:03');
INSERT INTO `dictionaries` VALUES (577, 'ru', 'product1_chart_title', 'Jmıxın istehsal göstəricisi', 0, '2020-01-19 15:21:53', '2020-02-02 08:47:03');
INSERT INTO `dictionaries` VALUES (578, 'az', 'product3_chart_title', 'Dənli bitkilərin hasilatı', 0, '2020-01-19 15:43:00', '2020-01-19 15:43:00');
INSERT INTO `dictionaries` VALUES (579, 'en', 'product3_chart_title', 'Dənli bitkilərin hasilatı', 0, '2020-01-19 15:43:00', '2020-01-19 15:43:00');
INSERT INTO `dictionaries` VALUES (580, 'ru', 'product3_chart_title', 'Dənli bitkilərin hasilatı', 0, '2020-01-19 15:43:00', '2020-01-19 15:43:00');
INSERT INTO `dictionaries` VALUES (581, 'az', 'products', 'Məhsullar', 0, '2020-01-19 15:49:47', '2020-01-19 15:49:47');
INSERT INTO `dictionaries` VALUES (582, 'en', 'products', 'Məhsullar', 0, '2020-01-19 15:49:47', '2020-01-19 15:49:47');
INSERT INTO `dictionaries` VALUES (583, 'ru', 'products', 'Məhsullar', 0, '2020-01-19 15:49:47', '2020-01-19 15:49:47');
INSERT INTO `dictionaries` VALUES (584, 'az', 'show_all', 'Hamısını göstər', 0, '2020-01-19 15:50:11', '2020-01-19 15:50:11');
INSERT INTO `dictionaries` VALUES (585, 'en', 'show_all', 'Hamısını göstər', 0, '2020-01-19 15:50:11', '2020-01-19 15:50:11');
INSERT INTO `dictionaries` VALUES (586, 'ru', 'show_all', 'Hamısını göstər', 0, '2020-01-19 15:50:11', '2020-01-19 15:50:11');
INSERT INTO `dictionaries` VALUES (587, 'az', 'enterprises', 'Müəssisələr', 0, '2020-01-19 15:50:27', '2020-01-19 15:50:27');
INSERT INTO `dictionaries` VALUES (588, 'en', 'enterprises', 'Müəssisələr', 0, '2020-01-19 15:50:27', '2020-01-19 15:50:27');
INSERT INTO `dictionaries` VALUES (589, 'ru', 'enterprises', 'Müəssisələr', 0, '2020-01-19 15:50:27', '2020-01-19 15:50:27');
INSERT INTO `dictionaries` VALUES (590, 'az', 'news', 'Xəbərlər', 0, '2020-01-19 15:50:44', '2020-01-19 15:50:44');
INSERT INTO `dictionaries` VALUES (591, 'en', 'news', 'Xəbərlər', 0, '2020-01-19 15:50:44', '2020-01-19 15:50:44');
INSERT INTO `dictionaries` VALUES (592, 'ru', 'news', 'Xəbərlər', 0, '2020-01-19 15:50:44', '2020-01-19 15:50:44');
INSERT INTO `dictionaries` VALUES (593, 'az', 'index_video_title', 'Tanıtım Çarxı', 0, '2020-01-19 16:17:20', '2020-01-19 16:17:20');
INSERT INTO `dictionaries` VALUES (594, 'en', 'index_video_title', 'Tanıtım Çarxı', 0, '2020-01-19 16:17:20', '2020-01-19 16:17:20');
INSERT INTO `dictionaries` VALUES (595, 'ru', 'index_video_title', 'Tanıtım Çarxı', 0, '2020-01-19 16:17:20', '2020-01-19 16:17:20');
INSERT INTO `dictionaries` VALUES (596, 'az', 'index_video_description', 'Şirkətin pambıq emalı zavodlarında xam pambığın emalından çiyid məhsulu alınır. Toxumluq çiyid şirkət tərəfindən səpin zamanı istifadə olunur. Texniki çiyid isə yağ emalı zavodunda təkrar yağ istehsalında istifadə olunur.', 0, '2020-01-19 16:17:52', '2020-01-22 20:19:01');
INSERT INTO `dictionaries` VALUES (597, 'en', 'index_video_description', 'Şirkətin pambıq emalı zavodlarinda xam pambığın emalından şiyid mıhsulu alınir. Toxumluq çiyid Şirkət tərəfindən səpin zamani istifadə olunur. Texniki çiyid isə yağ emalı zavodunda təkrar yağ istehsalında istifadə olunur.', 0, '2020-01-19 16:17:52', '2020-01-22 20:19:01');
INSERT INTO `dictionaries` VALUES (598, 'ru', 'index_video_description', 'Şirkətin pambıq emalı zavodlarinda xam pambığın emalından şiyid mıhsulu alınir. Toxumluq çiyid Şirkət tərəfindən səpin zamani istifadə olunur. Texniki çiyid isə yağ emalı zavodunda təkrar yağ istehsalında istifadə olunur.', 0, '2020-01-19 16:17:52', '2020-01-22 20:19:01');
INSERT INTO `dictionaries` VALUES (599, 'az', 'product5_technical_parameters', '<p>İplik &Ccedil;eşidləri</p>', 1, '2020-01-30 07:37:58', '2020-01-30 21:53:49');
INSERT INTO `dictionaries` VALUES (600, 'en', 'product5_technical_parameters', '<p>echnical parameters of technical cotton seeds</p>\r\n\r\n<p>(Texniki ipliyin texniki parametirləri)</p>', 0, '2020-01-30 07:37:58', '2020-01-30 21:53:49');
INSERT INTO `dictionaries` VALUES (601, 'ru', 'product5_technical_parameters', '<p>echnical parameters of technical cotton seeds</p>\r\n\r\n<p>(Texniki ipliyin texniki parametirləri)</p>', 0, '2020-01-30 07:37:58', '2020-01-30 21:53:49');

-- ----------------------------
-- Table structure for enterprise_blocks
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_blocks`;
CREATE TABLE `enterprise_blocks`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `enterprise_id` int(10) UNSIGNED NOT NULL,
  `title_az` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `title_ru` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `content_az` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `content_ru` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of enterprise_blocks
-- ----------------------------
INSERT INTO `enterprise_blocks` VALUES (1, 1, 'b1', 'GENCE-b1', 'GENCE-b1', '<p>Zavod Almaniyanın Rieter firmasının avadanlıqlarından olan Open-End avadanlığı ilə təchiz olunmuşdur. Fabrikdə şirkətin &ouml;z istehsal etdiyi xammaldan(pambıq mahlıcından) Ne6-Ne20 n&ouml;mrəli trikotaj və toxuma ipliyi istehsal olunur.</p>', '<p>Zavod Almaniyanın Rieter firmasının avadanlıqlarından olan Open-End avadanlığı ilə təchiz olunmuşdur. Fabrikdə şirkətin &ouml;z istehsal etdiyi xammaldan(pambıq mahlıcından) Ne6-Ne20 n&ouml;mrəli trikotaj və toxuma ipliyi istehsal olunur.</p>', '<p>Zavod Almaniyanın Rieter firmasının avadanlıqlarından olan Open-End avadanlığı ilə təchiz olunmuşdur. Fabrikdə şirkətin &ouml;z istehsal etdiyi xammaldan(pambıq mahlıcından) Ne6-Ne20 n&ouml;mrəli trikotaj və toxuma ipliyi istehsal olunur.</p>', '2020-01-19 14:21:30', '2020-01-19 14:21:30');
INSERT INTO `enterprise_blocks` VALUES (2, 2, 'bAKI -B1', 'bAKI -B1', 'bAKI -B1', '<ul>\r\n	<li>Zavod Almaniyanın Rieter firmasının avadanlıqlarından olan Open-End avadanlığı ilə təchiz olunmuşdur. Ən yeni texnoloji standartlara cavab verən fabrikdə şirkətin &ouml;z istehsal etdiyi xammaldan(pambıq mahlıcından) Ne6-Ne32 n&ouml;mrəli trikotaj və toxuma ipliyi istehsal olunur.Əyrici maşınların &uuml;st&uuml;ndə yerləşən x&uuml;susi proqramlaşdırılmış robotlar prosesin daha m&uuml;kəmməl və keyfiyyətli şəkildə getməsinə şərait yaradır.</li>\r\n	<li>Alınan məhsulun keyfiyyətinə nəzarət etmək &uuml;&ccedil;&uuml;n zavodda İsve&ccedil;rənin USTER şirkətinin avadanlıqları ilə təchiz edilmiş laboratoriyasıda fəaliyyət g&ouml;stərməkdədir.</li>\r\n	<li>İldə 5000-6000 ton pambıq mahlıcından iplik istehsal etmək g&uuml;c&uuml;nə malik fabrikin tikintisə 14 milyon 291 min manat investisiya qoyulmuşdur.Fabrik yerli m&uuml;təxəssislər tərəfindən idarə olunur və zavodda hal-hazırda &uuml;mumi olaraq 112 iş&ccedil;i fəaliyyət g&ouml;stərir.</li>\r\n	<li>Şirkət məhsullarının b&ouml;y&uuml;k bir həssəsi xarici &ouml;lkələrə ixrac olunur.Bu &ouml;lkələrə Rusiya Federasiyası, Belarusiya, Ukrayna, T&uuml;rkiyə, İran və s aiddir.</li>\r\n</ul>', '<ul>\r\n	<li>Zavod Almaniyanın Rieter firmasının avadanlıqlarından olan Open-End avadanlığı ilə təchiz olunmuşdur. Ən yeni texnoloji standartlara cavab verən fabrikdə şirkətin &ouml;z istehsal etdiyi xammaldan(pambıq mahlıcından) Ne6-Ne32 n&ouml;mrəli trikotaj və toxuma ipliyi istehsal olunur.Əyrici maşınların &uuml;st&uuml;ndə yerləşən x&uuml;susi proqramlaşdırılmış robotlar prosesin daha m&uuml;kəmməl və keyfiyyətli şəkildə getməsinə şərait yaradır.</li>\r\n	<li>Alınan məhsulun keyfiyyətinə nəzarət etmək &uuml;&ccedil;&uuml;n zavodda İsve&ccedil;rənin USTER şirkətinin avadanlıqları ilə təchiz edilmiş laboratoriyasıda fəaliyyət g&ouml;stərməkdədir.</li>\r\n	<li>İldə 5000-6000 ton pambıq mahlıcından iplik istehsal etmək g&uuml;c&uuml;nə malik fabrikin tikintisə 14 milyon 291 min manat investisiya qoyulmuşdur.Fabrik yerli m&uuml;təxəssislər tərəfindən idarə olunur və zavodda hal-hazırda &uuml;mumi olaraq 112 iş&ccedil;i fəaliyyət g&ouml;stərir.</li>\r\n	<li>Şirkət məhsullarının b&ouml;y&uuml;k bir həssəsi xarici &ouml;lkələrə ixrac olunur.Bu &ouml;lkələrə Rusiya Federasiyası, Belarusiya, Ukrayna, T&uuml;rkiyə, İran və s aiddir.</li>\r\n</ul>', '<ul>\r\n	<li>Zavod Almaniyanın Rieter firmasının avadanlıqlarından olan Open-End avadanlığı ilə təchiz olunmuşdur. Ən yeni texnoloji standartlara cavab verən fabrikdə şirkətin &ouml;z istehsal etdiyi xammaldan(pambıq mahlıcından) Ne6-Ne32 n&ouml;mrəli trikotaj və toxuma ipliyi istehsal olunur.Əyrici maşınların &uuml;st&uuml;ndə yerləşən x&uuml;susi proqramlaşdırılmış robotlar prosesin daha m&uuml;kəmməl və keyfiyyətli şəkildə getməsinə şərait yaradır.</li>\r\n	<li>Alınan məhsulun keyfiyyətinə nəzarət etmək &uuml;&ccedil;&uuml;n zavodda İsve&ccedil;rənin USTER şirkətinin avadanlıqları ilə təchiz edilmiş laboratoriyasıda fəaliyyət g&ouml;stərməkdədir.</li>\r\n	<li>İldə 5000-6000 ton pambıq mahlıcından iplik istehsal etmək g&uuml;c&uuml;nə malik fabrikin tikintisə 14 milyon 291 min manat investisiya qoyulmuşdur.Fabrik yerli m&uuml;təxəssislər tərəfindən idarə olunur və zavodda hal-hazırda &uuml;mumi olaraq 112 iş&ccedil;i fəaliyyət g&ouml;stərir.</li>\r\n	<li>Şirkət məhsullarının b&ouml;y&uuml;k bir həssəsi xarici &ouml;lkələrə ixrac olunur.Bu &ouml;lkələrə Rusiya Federasiyası, Belarusiya, Ukrayna, T&uuml;rkiyə, İran və s aiddir.</li>\r\n</ul>', '2020-01-19 14:36:52', '2020-01-19 14:36:52');

-- ----------------------------
-- Table structure for enterprise_translations
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_translations`;
CREATE TABLE `enterprise_translations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `enterprise_id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `order` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `table` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `enterprise_translations_enterprise_id_lang_unique`(`enterprise_id`, `lang`) USING BTREE,
  UNIQUE INDEX `enterprise_translations_slug_unique`(`slug`) USING BTREE,
  INDEX `enterprise_translations_page_id_foreign`(`page_id`) USING BTREE,
  CONSTRAINT `enterprise_translations_enterprise_id_foreign` FOREIGN KEY (`enterprise_id`) REFERENCES `enterprises` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `enterprise_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `page_translations` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of enterprise_translations
-- ----------------------------
INSERT INTO `enterprise_translations` VALUES (1, 1, 24, 0, 'MKT - Ağcabədi filialı', 'mkt-agcabedi-filiali', 'Şirkətin pambıq emalı zavodlarinda xam pambığın emalından şiyid mıhsulu alınir.', '<p><strong>1993-c&uuml; ildən</strong>&nbsp;MKT İstehsalat-Kommersiya MMC-nin Gəncə İplik Fabriki fəaliyyət g&ouml;stərməkdədir.</p>', NULL, 'az', '2020-01-19 14:04:14', '2020-01-28 12:18:53', NULL);
INSERT INTO `enterprise_translations` VALUES (2, 2, 24, 1, 'Bakı İplik Fabriki', 'baki-iplik-fabriki', 'Şirkətin pambıq emalı zavodlarinda / xam pambığın emalından şiyid mıhsulu alınir.', '<ul>\r\n	<li><strong>2008-ci il fevralın 8-də</strong>&nbsp;MKT İstehsalat-Kommersiya MMC-nin Bakı İplik Fabriki Azərbaycan Respublikasının Prezidenti İlham Əliyevin a&ccedil;ılışını etməsi ilə &ouml;z fəaliyyətə başlamışdır.</li>\r\n</ul>', NULL, 'az', '2020-01-19 14:26:33', '2020-01-20 16:59:55', NULL);
INSERT INTO `enterprise_translations` VALUES (3, 3, 24, 3, 'Yevlax İFT', 'yevlax-ift', 'Şirkətin pambıq emalı zavodlarinda xam pambığın emalından şiyid mıhsulu alınir./\r\nToxumluq çiyid səpin zamani istifadə olunur.', '<ul>\r\n	<li style=\"text-align: justify;\"><strong>1954-c&uuml; ildə</strong>&nbsp;Şirvan Yağ-Piy zavodunun təməli qoyulmuşdur. M&uuml;əssisə fəaliyyətə &ldquo;Yağ zavodu&rdquo; adı altında başlamışdır.</li>\r\n	<li style=\"text-align: justify;\"><strong>1966-cı ildən</strong>&nbsp;&ldquo;Yağ ekstraksiya&rdquo; zavodu adı ilə fəaliyyət g&ouml;stərmişdir</li>\r\n	<li style=\"text-align: justify;\"><strong>1976-cı ildən sonra</strong>&nbsp;m&uuml;əssisənin g&uuml;c&uuml; tədricən artırıldı. Nəticədə,g&uuml;ndəlik &ccedil;iyid emalı 270 tondan 700 tona &ccedil;atdırıldı.</li>\r\n	<li style=\"text-align: justify;\"><strong>1980-ci ildə</strong>&nbsp;yağın hidratlaşdırılması &uuml;zrə istehsalat sahəsi yaradıldı və m&uuml;əssisəyə &ldquo;Yağ-Piy kombinatı&rdquo; adı verildi.</li>\r\n	<li style=\"text-align: justify;\"><strong>1997-ci ildə</strong>&nbsp;Respublikada Pambıq istehsalı aşağı d&uuml;şd&uuml;kcə Yağ zavodununda da rentabellilik pozuldu və 1997-ci ildə m&uuml;əssisə a&ccedil;ıq səhmdar cəmiyyətinə &ccedil;evrildi.</li>\r\n	<li style=\"text-align: justify;\"><strong>2006-cı ildə</strong>&nbsp;zavodun bazasında yeni ki&ccedil;ik m&uuml;əssisənin tikilməsi qərara alındı.</li>\r\n	<li style=\"text-align: justify;\"><strong>2008-ci ildən</strong>&nbsp;&ldquo;Şirvan Yağ-Piy Zavodu&rdquo; adı ilə m&uuml;əssisə fəaliyyətə başladı. Bu d&ouml;vrdə zavodun g&uuml;ndəlik 120 ton &ccedil;iyid emal etmək imkanı vardı</li>\r\n	<li style=\"text-align: justify;\"><strong>2017-ci ildə</strong>&nbsp;&ouml;lkədə pambıq&ccedil;ılığın inkişafı nəticəsində &ccedil;iyid &ccedil;ıxımına uyğun olaraq zavodun imkanlarının genişləndirilməsinə qərar verilmişdir.Nəticə etibarilə zavodda yeni texnoloji avadanlıqlar quraşdırıldı və m&uuml;əssisədə g&uuml;ndəlik &ccedil;iyid emalı 350-400 tona &ccedil;atdırıldı.</li>\r\n	<li style=\"text-align: justify;\"><strong>2019-cu ildə</strong>&nbsp;M&uuml;əssisədə məhsulların standartlara uyğun olması &uuml;&ccedil;&uuml;n lazım olan b&uuml;t&uuml;n analizlər aparılır. Zavodda Amerkanın,Almaniyanın, İtalyanın və T&uuml;rkiyənin avadanlıqları ilə təchiz olunmuş mərkəzi laboratoriyası fəaliyyət g&ouml;stərməkdədir. Xammal da daxil olmaqla, b&uuml;t&uuml;n alınan məhsullar laboratoriyada analiz olunur. Burada Amerkanın BioTek şirkətinin 800 TS microplate reader, Almaniyanın Bruker şirkətinin Tango,İtaliyanın VELP Scientifica sirkətinin SRY-148,T&uuml;rkiyənin Magma Therm şirkətinin Sobaları və daha bir ne&ccedil;ə &ouml;l&ccedil;mə cihazları vasitəsi ilə texniki &ccedil;iyidin n&ouml;v&uuml;,yağlılığı və emaldan sonra alınan məhsulların yağlılığı, aflotoksini, k&uuml;l&uuml;, rəngi, şəffaflığı, nəmliyi,&ccedil;&ouml;k&uuml;nt&uuml;s&uuml; və s g&ouml;stəriciləri m&uuml;əyyənləşdirilir. Zavod məhsullarının b&ouml;y&uuml;k bir hissəsi Turkiyə,Qazaxıstan,Tacikistan və İrana ixrac olunur.</li>\r\n</ul>', NULL, 'az', '2020-01-19 14:39:14', '2020-01-28 12:19:31', NULL);
INSERT INTO `enterprise_translations` VALUES (4, 4, 24, 2, 'Şirvan Yağ Emalı Zavodu', 'sirvan-yag-emali-zavodu', 'Şirkətin pambıq emalı zavodlarinda xam pambığın emalından şiyid mıhsulu alınir./\r\nToxumluq çiyid səpin zamani istifadə olunur.', '<ul>\r\n	<li style=\"text-align: justify;\">&ldquo;MKT İstehsalat Kommersiya&rdquo; MMC-nin pambıq yağı istehsalını həyata ke&ccedil;irən &ldquo;Şirvan Yağ-Piy&rdquo; filialı fəaliyyət g&ouml;stərir.</li>\r\n	<li style=\"text-align: justify;\">Zavod 1962-ci ildə fəaliyyətə başlamışdır və pambıq &ccedil;iyidin presslənməsi metodu ilə yağ istehsalını həyata ke&ccedil;irməkdədir.</li>\r\n	<li style=\"text-align: justify;\">Zavod istehsal və rafinasiya &uuml;&ccedil;&uuml;n lazım olan ən son texnologiya ilə təchiz olunmuşdur. İstehsal olunan &ccedil;iyidin 12%-13%-i rafinasiya olunmuş yağ olaraq əldə edilməkdədir. Həm&ccedil;inin, zavod, pambıq &ccedil;iyidindən soapstock, pambıq jmıxı və linter istehsal edir.</li>\r\n</ul>', '<table>\r\n	<thead>\r\n		<tr>\r\n			<td>Characteristics:</td>\r\n			<td>Typical</td>\r\n			<td>Range</td>\r\n			<td>Method of Analysis</td>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Iodine Value</td>\r\n			<td>110</td>\r\n			<td>100 &ndash; 115</td>\r\n			<td>AOCS Cd-1-25 / ISO 3961</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Saponification Value (mg KOH/g)</td>\r\n			<td>196</td>\r\n			<td>189 &ndash; 198</td>\r\n			<td>AOCS Cd 3b-76 / ISO 3657</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Unsaponifiable Matter (%)</td>\r\n			<td>&nbsp;</td>\r\n			<td>1.5 max.</td>\r\n			<td>ISO 3596-1</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Refractive Index (nD50 1.4&hellip;)</td>\r\n			<td>612</td>\r\n			<td>608 &ndash; 617</td>\r\n			<td>AOCS Cc 7-25 / ISO 6320</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Fatty Acid Composition (%)\r\n			<ul>\r\n				<li>Myristic (C 14:0)</li>\r\n				<li>Myristic (C 14:0)</li>\r\n				<li>Myristic (C 14:0)</li>\r\n				<li>Myristic (C 14:0)</li>\r\n			</ul>\r\n			</td>\r\n			<td>&nbsp;\r\n			<ul>\r\n				<li>0.5</li>\r\n				<li>24.0</li>\r\n				<li>0.9</li>\r\n				<li>2.5</li>\r\n			</ul>\r\n			</td>\r\n			<td>&nbsp;\r\n			<ul>\r\n				<li>0.3 &ndash; 1.0</li>\r\n				<li>22.0 &ndash; 27.0</li>\r\n				<li>0.8 &ndash; 1.0</li>\r\n				<li>1.0 &ndash; 4.0</li>\r\n			</ul>\r\n			</td>\r\n			<td>AOCS Ch 1-91 / AOCS 2-91 /\r\n			<ul>\r\n				<li>ISO 5508 / ISO 5509</li>\r\n			</ul>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 'az', '2020-01-19 14:45:05', '2020-01-28 11:42:32', NULL);

-- ----------------------------
-- Table structure for enterprises
-- ----------------------------
DROP TABLE IF EXISTS `enterprises`;
CREATE TABLE `enterprises`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `template_id` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `youtube_link` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `location` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `linear` tinyint(1) NOT NULL DEFAULT 0,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of enterprises
-- ----------------------------
INSERT INTO `enterprises` VALUES (1, 3, 'https://www.youtube.com/watch?v=rd4-JyFnmQk', '40374104,49814730', 1, 1, '2020-01-19 14:04:14', '2020-01-19 14:22:50', NULL);
INSERT INTO `enterprises` VALUES (2, 4, 'https://www.youtube.com/watch?v=YVkUvmDQ3HY', '40374104,49814730', 0, 1, '2020-01-19 14:26:33', '2020-01-19 14:26:33', NULL);
INSERT INTO `enterprises` VALUES (3, 1, 'https://www.youtube.com/watch?v=OZafwTE1q4Q', '40374104,49814730', 1, 1, '2020-01-19 14:39:14', '2020-01-20 16:53:29', NULL);
INSERT INTO `enterprises` VALUES (4, 2, 'https://www.youtube.com/watch?v=YVkUvmDQ3HY', NULL, 0, 1, '2020-01-19 14:45:05', '2020-01-27 13:32:37', NULL);

-- ----------------------------
-- Table structure for faqs
-- ----------------------------
DROP TABLE IF EXISTS `faqs`;
CREATE TABLE `faqs`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title_az` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `title_ru` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `description_az` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `description_ru` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of faqs
-- ----------------------------
INSERT INTO `faqs` VALUES (1, 'Sual1', NULL, NULL, 'Cavab1', NULL, NULL, '2020-01-19 11:54:23', '2020-01-19 11:54:23');

-- ----------------------------
-- Table structure for kivs
-- ----------------------------
DROP TABLE IF EXISTS `kivs`;
CREATE TABLE `kivs`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title_az` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `title_en` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `title_ru` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `youtube_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `kiv_type` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kivs
-- ----------------------------
INSERT INTO `kivs` VALUES (1, NULL, NULL, NULL, NULL, '2020-01-19 13:46:30', '2020-01-19 13:46:30', 1);
INSERT INTO `kivs` VALUES (2, 'Təqdimat Çarxı', 'v1', 'v1', 'https://www.youtube.com/watch?v=OBk3pdtG3bg', '2020-01-19 13:47:28', '2020-01-29 13:51:53', 2);

-- ----------------------------
-- Table structure for mahlic_charts
-- ----------------------------
DROP TABLE IF EXISTS `mahlic_charts`;
CREATE TABLE `mahlic_charts`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `chart_type` int(10) UNSIGNED NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mahlic_charts
-- ----------------------------
INSERT INTO `mahlic_charts` VALUES (1, 1, '1 - 1/8\"', '#3492eb', 20, '2020-01-22 20:40:25', '2020-01-22 20:40:25');
INSERT INTO `mahlic_charts` VALUES (2, 1, '1 - 5/32\"', '#257a0b', 80, '2020-01-22 20:41:42', '2020-01-22 20:41:42');
INSERT INTO `mahlic_charts` VALUES (3, 2, '43 - 53', '#de9400', 17, '2020-01-22 20:43:35', '2020-01-22 20:43:35');
INSERT INTO `mahlic_charts` VALUES (4, 2, '11 - 21 - 31', '#007ede', 19, '2020-01-22 20:44:33', '2020-01-22 20:44:33');
INSERT INTO `mahlic_charts` VALUES (5, 2, '41 - 42', '#257a0b', 64, '2020-01-22 20:45:36', '2020-01-22 20:45:36');
INSERT INTO `mahlic_charts` VALUES (6, 4, '5 - 5.1', '#de9400', 30, '2020-01-22 20:47:00', '2020-01-22 20:47:00');
INSERT INTO `mahlic_charts` VALUES (7, 4, '4.1 - 5', '#3492eb', 70, '2020-01-22 20:47:48', '2020-01-22 20:47:48');
INSERT INTO `mahlic_charts` VALUES (8, 5, '1 / 1,2,3,4,5', '#00bd16', 15, '2020-01-23 11:34:34', '2020-01-23 11:34:34');
INSERT INTO `mahlic_charts` VALUES (9, 5, '2 / 2,3,4,5', '#ffc800', 37, '2020-01-23 11:35:44', '2020-01-23 11:35:44');
INSERT INTO `mahlic_charts` VALUES (10, 5, '3 / 2,3,4,5', '#a1a1a1', 28, '2020-01-23 11:37:24', '2020-01-23 11:37:24');
INSERT INTO `mahlic_charts` VALUES (11, 5, '4 / 3,4,5', '#ba3f3f', 17, '2020-01-23 11:39:14', '2020-01-23 11:39:14');
INSERT INTO `mahlic_charts` VALUES (12, 5, '5 / 4,5', '#4d81b3', 2, '2020-01-23 11:40:43', '2020-01-23 11:40:43');

-- ----------------------------
-- Table structure for media
-- ----------------------------
DROP TABLE IF EXISTS `media`;
CREATE TABLE `media`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `model_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `disk` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int(10) UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `media_model_type_model_id_index`(`model_type`, `model_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 114 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of media
-- ----------------------------
INSERT INTO `media` VALUES (1, 'App\\Models\\Slider', 1, 'default', 'slider_bg01', 'slider_bg01.jpg', 'image/jpeg', 'public', 300216, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"view\": true, \"thumb\": true}}', '[]', 1, '2020-01-14 12:16:25', '2020-01-14 12:16:25');
INSERT INTO `media` VALUES (2, 'App\\Models\\Page', 4, 'gallery', 'about_img01', 'about_img01.jpg', 'image/jpeg', 'public', 122353, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 2, '2020-01-18 11:19:20', '2020-01-18 11:19:20');
INSERT INTO `media` VALUES (3, 'App\\Models\\Page', 4, 'gallery', 'about_img02', 'about_img02.jpg', 'image/jpeg', 'public', 168532, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 3, '2020-01-18 11:19:21', '2020-01-18 11:19:21');
INSERT INTO `media` VALUES (10, 'App\\Models\\PageTranslation', 2, 'page_file', 'breadcrumb_bg (1)', 'breadcrumb_bg-(1).png', 'image/png', 'public', 37195, '[]', '{\"custom_headers\": []}', '[]', 9, '2020-01-19 13:17:37', '2020-01-19 13:17:37');
INSERT INTO `media` VALUES (13, 'App\\Models\\PageTranslation', 14, 'page_file', 'sxem', 'sxem.png', 'image/png', 'public', 19176, '[]', '{\"custom_headers\": []}', '[]', 11, '2020-01-19 13:24:58', '2020-01-19 13:24:58');
INSERT INTO `media` VALUES (16, 'App\\Models\\Kiv', 1, 'png_file', 'partner', 'partner.png', 'image/png', 'public', 10065, '[]', '{\"custom_headers\": []}', '[]', 14, '2020-01-19 13:46:30', '2020-01-19 13:46:30');
INSERT INTO `media` VALUES (17, 'App\\Models\\Kiv', 1, 'eps_file', 'partner', 'partner.png', 'image/png', 'public', 10065, '[]', '{\"custom_headers\": []}', '[]', 15, '2020-01-19 13:46:30', '2020-01-19 13:46:30');
INSERT INTO `media` VALUES (18, 'App\\Models\\Kiv', 2, 'default', 'product', 'product.png', 'image/png', 'public', 180787, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 16, '2020-01-19 13:47:28', '2020-01-19 13:47:28');
INSERT INTO `media` VALUES (20, 'App\\Models\\Page', 21, 'default', 'texnika', 'texnika.png', 'image/png', 'public', 208324, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 18, '2020-01-19 13:53:23', '2020-01-19 13:53:23');
INSERT INTO `media` VALUES (22, 'App\\Models\\Enterprise', 1, 'youtube_cover', 'news_gallery', 'news_gallery.png', 'image/png', 'public', 337935, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true}}', '[]', 20, '2020-01-19 14:04:14', '2020-01-19 14:04:14');
INSERT INTO `media` VALUES (23, 'App\\Models\\EnterpriseTranslation', 1, 'catalog', 'partner', 'partner.png', 'image/png', 'public', 10065, '[]', '{\"custom_headers\": []}', '[]', 21, '2020-01-19 14:04:14', '2020-01-19 14:04:14');
INSERT INTO `media` VALUES (24, 'App\\Models\\Enterprise', 1, 'gallery', 'enterprises1', 'enterprises1.png', 'image/png', 'public', 405265, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 22, '2020-01-19 14:20:26', '2020-01-19 14:20:27');
INSERT INTO `media` VALUES (26, 'App\\Models\\Enterprise', 2, 'youtube_cover', 'enterprises2', 'enterprises2.png', 'image/png', 'public', 860524, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true}}', '[]', 24, '2020-01-19 14:26:33', '2020-01-19 14:26:33');
INSERT INTO `media` VALUES (27, 'App\\Models\\EnterpriseTranslation', 2, 'catalog', 'enterprises1', 'enterprises1.png', 'image/png', 'public', 405265, '[]', '{\"custom_headers\": []}', '[]', 25, '2020-01-19 14:31:11', '2020-01-19 14:31:11');
INSERT INTO `media` VALUES (28, 'App\\Models\\Enterprise', 2, 'gallery', 'enterprises1', 'enterprises1.png', 'image/png', 'public', 405265, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 26, '2020-01-19 14:31:39', '2020-01-19 14:31:40');
INSERT INTO `media` VALUES (30, 'App\\Models\\Enterprise', 3, 'youtube_cover', 'enterprises2', 'enterprises2.png', 'image/png', 'public', 860524, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true}}', '[]', 28, '2020-01-19 14:41:11', '2020-01-19 14:41:11');
INSERT INTO `media` VALUES (31, 'App\\Models\\Enterprise', 3, 'gallery', 'enterprises2', 'enterprises2.png', 'image/png', 'public', 860524, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 29, '2020-01-19 14:43:06', '2020-01-19 14:43:06');
INSERT INTO `media` VALUES (33, 'App\\Models\\Enterprise', 4, 'youtube_cover', 'enterprises2', 'enterprises2.png', 'image/png', 'public', 860524, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true}}', '[]', 31, '2020-01-19 14:52:59', '2020-01-19 14:52:59');
INSERT INTO `media` VALUES (34, 'App\\Models\\Product', 1, 'default', 'product', 'product.png', 'image/png', 'public', 180787, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 32, '2020-01-19 15:04:05', '2020-01-19 15:04:05');
INSERT INTO `media` VALUES (35, 'App\\Models\\Product', 1, 'cover', 'product', 'product.png', 'image/png', 'public', 180787, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 33, '2020-01-19 15:04:05', '2020-01-19 15:04:05');
INSERT INTO `media` VALUES (50, 'App\\Models\\Page', 5, 'default', '2', '2.png', 'image/png', 'public', 503293, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 47, '2020-01-20 09:40:00', '2020-01-20 09:40:00');
INSERT INTO `media` VALUES (51, 'App\\Models\\Product', 2, 'default', 'Ciyid', 'Ciyid.svg', 'image/svg', 'public', 440962, '[]', '{\"custom_headers\": []}', '[]', 48, '2020-01-20 09:47:06', '2020-01-20 09:47:06');
INSERT INTO `media` VALUES (52, 'App\\Models\\Product', 3, 'default', 'Mahlıc', 'Mahlıc.svg', 'image/svg', 'public', 277230, '[]', '{\"custom_headers\": []}', '[]', 49, '2020-01-20 09:47:30', '2020-01-20 09:47:30');
INSERT INTO `media` VALUES (53, 'App\\Models\\Product', 4, 'default', 'Jmix', 'Jmix.svg', 'image/svg', 'public', 504420, '[]', '{\"custom_headers\": []}', '[]', 50, '2020-01-20 09:47:48', '2020-01-20 09:47:48');
INSERT INTO `media` VALUES (54, 'App\\Models\\Product', 5, 'default', 'Denli bitkiler', 'Denli-bitkiler.svg', 'image/svg', 'public', 2242902, '[]', '{\"custom_headers\": []}', '[]', 51, '2020-01-20 09:48:09', '2020-01-20 09:48:09');
INSERT INTO `media` VALUES (57, 'App\\Models\\Enterprise', 1, 'default', 'Agcabedi_filiali', 'Agcabedi_filiali.png', 'image/png', 'public', 458682, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 53, '2020-01-20 17:02:46', '2020-01-20 17:02:46');
INSERT INTO `media` VALUES (58, 'App\\Models\\Enterprise', 3, 'default', 'Yevlax_filiali', 'Yevlax_filiali.png', 'image/png', 'public', 531693, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 54, '2020-01-20 17:04:39', '2020-01-20 17:04:40');
INSERT INTO `media` VALUES (59, 'App\\Models\\Enterprise', 2, 'default', 'Bakı_iplik_zavodu', 'Bakı_iplik_zavodu.png', 'image/png', 'public', 262994, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 55, '2020-01-20 17:04:55', '2020-01-20 17:04:55');
INSERT INTO `media` VALUES (61, 'App\\Models\\Slider', 2, 'default', 'Mask Group 171', 'Mask-Group-171.png', 'image/png', 'public', 1898716, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"view\": true, \"thumb\": true}}', '[]', 57, '2020-01-21 10:43:05', '2020-01-21 10:43:05');
INSERT INTO `media` VALUES (63, 'App\\Models\\Page', 7, 'default', 'Sertifikat 1', 'Sertifikat-1.svg', 'image/svg', 'public', 1442188, '[]', '{\"custom_headers\": []}', '[]', 59, '2020-01-21 13:36:20', '2020-01-21 13:36:20');
INSERT INTO `media` VALUES (64, 'App\\Models\\Page', 26, 'default', 'Sertifikat 2', 'Sertifikat-2.svg', 'image/svg', 'public', 642442, '[]', '{\"custom_headers\": []}', '[]', 60, '2020-01-21 14:04:34', '2020-01-21 14:04:34');
INSERT INTO `media` VALUES (65, 'App\\Models\\Product', 6, 'default', 'Iplik', 'Iplik.svg', 'image/svg', 'public', 297669, '[]', '{\"custom_headers\": []}', '[]', 61, '2020-01-21 14:19:42', '2020-01-21 14:19:42');
INSERT INTO `media` VALUES (73, 'App\\Models\\Product', 2, 'cover', 'Ciyid', 'Ciyid.svg', 'image/svg', 'public', 441097, '[]', '{\"custom_headers\": []}', '[]', 66, '2020-01-24 15:10:54', '2020-01-24 15:10:54');
INSERT INTO `media` VALUES (74, 'App\\Models\\Product', 3, 'cover', 'Mahlıc', 'Mahlıc.svg', 'image/svg', 'public', 277658, '[]', '{\"custom_headers\": []}', '[]', 67, '2020-01-24 15:11:12', '2020-01-24 15:11:12');
INSERT INTO `media` VALUES (75, 'App\\Models\\Product', 4, 'cover', 'Jmix', 'Jmix.svg', 'image/svg', 'public', 504782, '[]', '{\"custom_headers\": []}', '[]', 68, '2020-01-24 15:11:26', '2020-01-24 15:11:26');
INSERT INTO `media` VALUES (76, 'App\\Models\\Product', 5, 'cover', 'Denli bitkiler', 'Denli-bitkiler.svg', 'image/svg', 'public', 621980, '[]', '{\"custom_headers\": []}', '[]', 69, '2020-01-24 15:11:44', '2020-01-24 15:11:44');
INSERT INTO `media` VALUES (77, 'App\\Models\\Product', 6, 'cover', 'Iplik', 'Iplik.svg', 'image/svg', 'public', 298062, '[]', '{\"custom_headers\": []}', '[]', 70, '2020-01-24 15:12:05', '2020-01-24 15:12:05');
INSERT INTO `media` VALUES (80, 'App\\Models\\Page', 28, 'default', 'Traktor MTZ 80-X', 'Traktor-MTZ-80-X.svg', 'image/svg', 'public', 862331, '[]', '{\"custom_headers\": []}', '[]', 73, '2020-01-27 11:31:59', '2020-01-27 11:31:59');
INSERT INTO `media` VALUES (81, 'App\\Models\\Page', 29, 'default', 'Traktor_Belarus_1221', 'Traktor_Belarus_1221.svg', 'image/svg', 'public', 246424, '[]', '{\"custom_headers\": []}', '[]', 74, '2020-01-27 11:33:04', '2020-01-27 11:33:04');
INSERT INTO `media` VALUES (82, 'App\\Models\\Page', 30, 'default', 'Traktor Belarus 2022', 'Traktor-Belarus-2022.svg', 'image/svg', 'public', 329520, '[]', '{\"custom_headers\": []}', '[]', 75, '2020-01-27 11:33:37', '2020-01-27 11:33:37');
INSERT INTO `media` VALUES (83, 'App\\Models\\Page', 31, 'default', 'Traktor_New_Holland_T7030', 'Traktor_New_Holland_T7030.svg', 'image/svg', 'public', 243337, '[]', '{\"custom_headers\": []}', '[]', 76, '2020-01-27 11:34:10', '2020-01-27 11:34:10');
INSERT INTO `media` VALUES (84, 'App\\Models\\Page', 32, 'default', 'Traktor New Holland T7060', 'Traktor-New-Holland-T7060.svg', 'image/svg', 'public', 2565778, '[]', '{\"custom_headers\": []}', '[]', 77, '2020-01-27 11:34:43', '2020-01-27 11:34:43');
INSERT INTO `media` VALUES (85, 'App\\Models\\Page', 33, 'default', 'Traktor New Holland T6090', 'Traktor-New-Holland-T6090.svg', 'image/svg', 'public', 286859, '[]', '{\"custom_headers\": []}', '[]', 78, '2020-01-27 11:35:23', '2020-01-27 11:35:23');
INSERT INTO `media` VALUES (86, 'App\\Models\\Page', 34, 'default', 'Traktor_New_Holland_TD_95', 'Traktor_New_Holland_TD_95.svg', 'image/svg', 'public', 289491, '[]', '{\"custom_headers\": []}', '[]', 79, '2020-01-27 11:35:57', '2020-01-27 11:35:57');
INSERT INTO `media` VALUES (87, 'App\\Models\\Page', 35, 'default', 'TRAK HARS', 'TRAK-HARS.svg', 'image/svg', 'public', 312800, '[]', '{\"custom_headers\": []}', '[]', 80, '2020-01-27 11:41:43', '2020-01-27 11:41:43');
INSERT INTO `media` VALUES (91, 'App\\Models\\Page', 36, 'default', 'TRAK J Deere 6175 M - 1', 'TRAK-J-Deere-6175-M---1.svg', 'image/svg', 'public', 1193128, '[]', '{\"custom_headers\": []}', '[]', 81, '2020-01-27 11:44:05', '2020-01-27 11:44:05');
INSERT INTO `media` VALUES (93, 'App\\Models\\Page', 37, 'default', 'Belarus 89-2 - 1', 'Belarus-89-2---1.svg', 'image/svg', 'public', 43927, '[]', '{\"custom_headers\": []}', '[]', 82, '2020-01-27 11:44:54', '2020-01-27 11:44:54');
INSERT INTO `media` VALUES (94, 'App\\Models\\Page', 38, 'default', 'Trak Landini -110 hc', 'Trak-Landini--110-hc.svg', 'image/svg', 'public', 242736, '[]', '{\"custom_headers\": []}', '[]', 83, '2020-01-27 11:45:28', '2020-01-27 11:45:28');
INSERT INTO `media` VALUES (95, 'App\\Models\\Page', 39, 'default', 'Kombayn - John-deere-9970', 'Kombayn---John-deere-9970.svg', 'image/svg', 'public', 379723, '[]', '{\"custom_headers\": []}', '[]', 84, '2020-01-27 11:50:55', '2020-01-27 11:50:55');
INSERT INTO `media` VALUES (96, 'App\\Models\\Page', 40, 'default', 'John Deere CP 690', 'John-Deere-CP-690.svg', 'image/svg', 'public', 875967, '[]', '{\"custom_headers\": []}', '[]', 85, '2020-01-27 11:51:52', '2020-01-27 11:51:52');
INSERT INTO `media` VALUES (97, 'App\\Models\\Page', 41, 'default', 'Case CPX-420', 'Case-CPX-420.svg', 'image/svg', 'public', 2933818, '[]', '{\"custom_headers\": []}', '[]', 86, '2020-01-27 11:52:34', '2020-01-27 11:52:34');
INSERT INTO `media` VALUES (100, 'App\\Models\\Article', 1, 'gallery', 'marja15749388389', 'marja15749388389.jpg', 'image/jpeg', 'public', 168669, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 88, '2020-01-27 13:38:49', '2020-01-27 13:38:49');
INSERT INTO `media` VALUES (101, 'App\\Models\\Article', 1, 'gallery', 'marja15749388388', 'marja15749388388.jpg', 'image/jpeg', 'public', 181746, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 89, '2020-01-27 13:38:49', '2020-01-27 13:38:49');
INSERT INTO `media` VALUES (102, 'App\\Models\\Article', 1, 'default', 'esas_ixracatci_sirketler_siyahisinda_mkt_istehsalat_kommersiya_mmc_nin_de_adi_var', 'esas_ixracatci_sirketler_siyahisinda_mkt_istehsalat_kommersiya_mmc_nin_de_adi_var.png', 'image/png', 'public', 223766, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 90, '2020-01-27 13:43:31', '2020-01-27 13:43:31');
INSERT INTO `media` VALUES (103, 'App\\Models\\Article', 3, 'default', 'Pambıq_yigiminda_rekord_mehsuldarliq', 'Pambıq_yigiminda_rekord_mehsuldarliq.jpg', 'image/jpeg', 'public', 473797, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 91, '2020-01-27 14:00:45', '2020-01-27 14:00:45');
INSERT INTO `media` VALUES (104, 'App\\Models\\Article', 4, 'default', 'Pambıq_Satishi', 'Pambıq_Satishi.jpg', 'image/jpeg', 'public', 49692, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 92, '2020-01-27 14:09:19', '2020-01-27 14:09:19');
INSERT INTO `media` VALUES (106, 'App\\Models\\Article', 5, 'default', 'AGCAB_DI FILIALI', 'AGCAB_DI-FILIALI.jpg', 'image/jpeg', 'public', 3845415, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 93, '2020-01-27 14:27:14', '2020-01-27 14:27:17');
INSERT INTO `media` VALUES (108, 'App\\Models\\Article', 2, 'default', 'ca5fc96f-b1cc-4694-85df-d464c2e5a06d_1569318548', 'ca5fc96f-b1cc-4694-85df-d464c2e5a06d_1569318548.jpg', 'image/jpeg', 'public', 208543, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 94, '2020-01-28 05:21:51', '2020-01-28 05:21:51');
INSERT INTO `media` VALUES (109, 'App\\Models\\Enterprise', 4, 'default', 'Sirvan yag piy zavodu', 'Sirvan-yag-piy-zavodu.png', 'image/png', 'public', 285274, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 95, '2020-01-28 11:15:17', '2020-01-28 11:15:17');
INSERT INTO `media` VALUES (110, 'App\\Models\\Page', 15, 'cover', 'Innovasiyalar', 'Innovasiyalar.png', 'image/png', 'public', 532199, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 96, '2020-01-28 11:29:15', '2020-01-28 11:29:15');
INSERT INTO `media` VALUES (111, 'App\\Models\\Page', 19, 'default', 'Texnikalar', 'Texnikalar.png', 'image/png', 'public', 214815, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 97, '2020-01-28 11:29:55', '2020-01-28 11:29:55');
INSERT INTO `media` VALUES (112, 'App\\Models\\Kiv', 1, 'logo_image', 'Logo', 'Logo.png', 'image/png', 'public', 7526, '[]', '{\"custom_headers\": []}', '[]', 98, '2020-01-28 11:30:40', '2020-01-28 11:30:40');
INSERT INTO `media` VALUES (113, 'App\\Models\\Page', 14, 'cover', 'Istehsalat mehsullari', 'Istehsalat-mehsullari.png', 'image/png', 'public', 91044, '[]', '{\"custom_headers\": [], \"generated_conversions\": {\"blade\": true, \"thumb\": true}}', '[]', 99, '2020-01-28 11:31:14', '2020-01-28 11:31:14');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2017_04_17_111544_create_sliders_table', 1);
INSERT INTO `migrations` VALUES (4, '2017_04_29_001453_create_dictionaries_table', 1);
INSERT INTO `migrations` VALUES (5, '2017_12_07_171010_create_partners_table', 1);
INSERT INTO `migrations` VALUES (6, '2018_04_19_100226_create_settings_table', 1);
INSERT INTO `migrations` VALUES (7, '2018_04_19_155536_create_admins_table', 1);
INSERT INTO `migrations` VALUES (8, '2018_05_09_165719_create_subscribers_table', 1);
INSERT INTO `migrations` VALUES (9, '2018_07_31_194021_create_pages_table', 1);
INSERT INTO `migrations` VALUES (10, '2018_07_31_194022_create_articles_table', 1);
INSERT INTO `migrations` VALUES (11, '2018_08_01_120000_create_page_translations_table', 1);
INSERT INTO `migrations` VALUES (12, '2018_08_01_122925_create_article_translations_table', 1);
INSERT INTO `migrations` VALUES (13, '2018_08_11_194018_create_media_table', 1);
INSERT INTO `migrations` VALUES (14, '2019_05_19_181450_add_summary_to_page_translations_table', 1);
INSERT INTO `migrations` VALUES (15, '2019_05_19_182658_add_show_index_to_pages_table', 1);
INSERT INTO `migrations` VALUES (16, '2019_05_25_193553_add_show_index_to_articles_table', 1);
INSERT INTO `migrations` VALUES (17, '2019_05_28_202555_create_vacancies_table', 1);
INSERT INTO `migrations` VALUES (18, '2019_05_28_202614_create_vacancy_translations_table', 1);
INSERT INTO `migrations` VALUES (19, '2019_06_17_181019_add_column_to_vacancy_translations', 1);
INSERT INTO `migrations` VALUES (20, '2019_07_05_111050_create_certificates_table', 1);
INSERT INTO `migrations` VALUES (21, '2019_07_10_114645_create_faqs_table', 1);
INSERT INTO `migrations` VALUES (22, '2019_07_11_104129_create_kivs_table', 1);
INSERT INTO `migrations` VALUES (23, '2019_07_12_133443_add_column_to_kivs_table', 1);
INSERT INTO `migrations` VALUES (24, '2019_07_15_183638_create_products_table', 1);
INSERT INTO `migrations` VALUES (25, '2019_07_15_185518_create_product_translations_table', 1);
INSERT INTO `migrations` VALUES (26, '2019_10_12_074249_add_column_to_pages_table', 1);
INSERT INTO `migrations` VALUES (27, '2019_10_12_110333_create_branch_elements_table', 1);
INSERT INTO `migrations` VALUES (28, '2019_10_12_122155_create_branches_table', 1);
INSERT INTO `migrations` VALUES (29, '2019_10_22_131530_create_tenders_table', 1);
INSERT INTO `migrations` VALUES (30, '2019_10_22_131545_create_tender_translations_table', 1);
INSERT INTO `migrations` VALUES (31, '2019_10_25_082947_add_column_to_products_table', 1);
INSERT INTO `migrations` VALUES (32, '2019_10_25_083008_add_column_to_product_translations_table', 1);
INSERT INTO `migrations` VALUES (33, '2019_10_28_104320_create_product_blocks_table', 1);
INSERT INTO `migrations` VALUES (34, '2019_10_28_114103_add_color_to_products_table', 1);
INSERT INTO `migrations` VALUES (35, '2019_10_29_084243_create_enterprises_table', 1);
INSERT INTO `migrations` VALUES (36, '2019_10_29_084515_create_enterprise_translations_table', 1);
INSERT INTO `migrations` VALUES (37, '2019_10_30_100845_add_column_to_enterprises_table', 1);
INSERT INTO `migrations` VALUES (38, '2019_10_31_123049_create_enterprise_blocks_table', 1);
INSERT INTO `migrations` VALUES (39, '2019_11_07_104112_create_chart_titles_table', 1);
INSERT INTO `migrations` VALUES (40, '2019_11_08_071209_create_chart_statistics_table', 1);
INSERT INTO `migrations` VALUES (41, '2019_11_10_094750_add_type_to_chart_titles_table', 1);
INSERT INTO `migrations` VALUES (42, '2019_11_12_115216_create_mahlic_charts_table', 1);
INSERT INTO `migrations` VALUES (43, '2019_11_16_111439_create_branch_maps_table', 1);
INSERT INTO `migrations` VALUES (44, '2019_11_20_185505_add_order_to_chart_titles_table', 1);

-- ----------------------------
-- Table structure for page_translations
-- ----------------------------
DROP TABLE IF EXISTS `page_translations`;
CREATE TABLE `page_translations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `page_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `lang` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `summary` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `forward_url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `page_translations_page_id_lang_unique`(`page_id`, `lang`) USING BTREE,
  UNIQUE INDEX `page_translations_lang_slug_unique`(`lang`, `slug`) USING BTREE,
  INDEX `page_translations_parent_id_foreign`(`parent_id`) USING BTREE,
  CONSTRAINT `page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `page_translations_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `page_translations` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of page_translations
-- ----------------------------
INSERT INTO `page_translations` VALUES (1, 1, NULL, 'az', 'Haqqımızda', 'haqqimizda', 0, NULL, NULL, NULL, NULL, NULL, '2020-01-18 11:10:58', '2020-01-20 09:09:54', NULL);
INSERT INTO `page_translations` VALUES (2, 2, 1, 'az', 'MKT haqqında', 'mkt-haqqinda', 2, '<p style=\"text-align: justify;\">&ldquo;MKT İstehsalat Kommersiya&rdquo; MMC Azərbaycan iqtisadiyyatının qeyri-neft sektorunda 25 illik iş təcr&uuml;bəsinə malikdir. Şirkətin fəaliyyətinin əsas istiqaməti &ouml;lkənin 20 pambıq&ccedil;ılıq rayonundan 15-də pambığın yetişdirilməsi, tədar&uuml;k&uuml; və daha sonra səhm nəzarət paketinə malik olduğu pambıqtəmizləmə m&uuml;əssisələrində emalından ibarətdir.</p>\r\n\r\n<p style=\"text-align: justify;\">İstehsal olunan pambıq mahlıcı ortaliflidir və &ouml;z&uuml;nəməxsus rəngə malikdir, bununla da o, məsələn, Amerika pambığından &ouml;z sarımtıl &ccedil;alarları ilə fərqlənir.</p>\r\n\r\n<p style=\"text-align: justify;\">1998-ci ildən Azərbaycanda Liverpul Pambıq Assosiasiyası tərəfindən təsdiq olunmuş yeni pambıq mahlıcı etalonları tətbiq olunub. &ldquo;MKT İstehsalat Kommersiya&rdquo; MMC 2003-c&uuml; ildən bu təşkilatın assosiativ &uuml;zv&uuml;d&uuml;r.</p>\r\n\r\n<hr />\r\n<p style=\"text-align: justify;\">Hal-hazırda şirkət Azərbaycanda tekstil sənayesinin inkişafı ilə məşğuldur. Bakı şəhərində İsve&ccedil;rənin RİETER şirkətinin m&uuml;asir texnologiyası ilə təchiz olunmuş, ildə 7 min ton iplik istehsal etmək g&uuml;c&uuml;nə malik olan yeni pambıq-əyirici fabriki tikilmiş və istismara verilmişdir.</p>\r\n\r\n<p style=\"text-align: justify;\">Şirkət, həm&ccedil;inin, Gəncə şəhərində yerləşən ildə 3 min ton iplik istehsal edən &ldquo;Gəncə Tekstil&rdquo; filialında iplik&nbsp;istehsalı sahəsində &ccedil;oxillik təcr&uuml;bəyə malikdir. İstehsal olunan pambıq ipliyi &ldquo;open end&rdquo; texnologiyası ilə hazırlanır, Azərbaycan və Rusiya bazarlaında b&ouml;y&uuml;k tələbata malikdir.</p>\r\n\r\n<p style=\"text-align: justify;\">2006 cı ildə Şirvan şəhərində &ldquo;MKT İstehsalat Kommersiya&rdquo; MMC T&uuml;rkiyə istehsalı olan yeni avadanlıqla təchiz edilmişdir, yeni yağ-piy kombinatı tikilmişdir. Bu zavod texniki- &ccedil;iyidindən pambıq yağı və jmıx istehsal edir, m&uuml;əssisədə linter xətti və təmizlənmiş yağ istehsal edən xətt fəaliyyət g&ouml;stərir.</p>\r\n\r\n<hr />\r\n<p style=\"text-align: justify;\">Bu g&uuml;n &ldquo;MKT İstehsalat Kommersiya&rdquo; MMC &ouml;z beynəlxalq standartlara cavab verən məhsullarını -&nbsp;pambıq-mahlıcı, tullantılar (uyluk, lint, tiftik), m&uuml;xtəlif n&ouml;mrəli pambıq ipliklərini, habelə linter, pambıq yağı və jmıxı T&uuml;rkiyə, İran, Rusiya və digər MDB &ouml;lkələrinə ixrac edir.</p>\r\n\r\n<p style=\"text-align: justify;\">&ldquo;MKT İstehsalat Kommersiya&rdquo; MMC &ouml;z tərəfdaşlarına və alıcılara &ouml;z mallarının y&uuml;ksək keyfiyyətinə və vaxtında &ccedil;atdırılmasına zəmanət verir. Cəmiyyət inkişaf dinamıkasını qorumaq &uuml;&ccedil;&uuml;n strateji əhəmiyyətli regional filiallarda əsaslı təmirlər, yenidənqurma işləri və modernizasiyalar həyata ke&ccedil;irir. Zavod və m&uuml;əssisələrə yeni və daha m&uuml;asir T&uuml;rkiyə, ABŞ, &Ouml;zbəkistan Respublikası, İsve&ccedil;rə və Ukrayna Respublikasında istehsal olunmuş avadanlıqlar, texniki vasitələr və m&uuml;təxəssislər cəlb edilir.</p>\r\n\r\n<p style=\"text-align: justify;\">&ldquo;MKT İstehsalat Kommersiya&rdquo; MMC hazır məhsulları &uuml;zrə Standartlaşdırma, Metrologiya və Patent &uuml;zrə D&ouml;vlət Komitəsindən Uyğunluq Sertifikatı almışdır. Regional və Bakı filiallarda istehsal olunan mallar: pambıq mahlıcı, ulyuk, lint, &ccedil;iyid, yağ, jmıx, m&uuml;xtəlif n&ouml;v iplik Rusiya Federasiyası, T&uuml;rkiyə, Malta, Ukrayna və s. &ouml;lkələrə ixrac olunur.</p>\r\n\r\n<p style=\"text-align: justify;\">2018-ci ildə cəmiyyət 45,000 Ha sahədən rekord g&ouml;stərici olan 27.3 sentner/Ha xam pambıq istehsal etmişdir. Cari ilin yanvar-fevral ayları &uuml;zrə hazırlanan qeyri-d&ouml;vlət ixracat&ccedil;ı subyektlərin reytinqində &ldquo;MKT İstehsalat Kommersiya&rdquo; MMC birinci yerdə qərarlaşmışdır. Hal-Hazırda m&uuml;əssısəmizdə ış&ccedil;ı sayısı 3200 nəfərdir və regionlarda 25 filialımız fəaliyyət g&ouml;stərməkdədir.</p>', NULL, NULL, NULL, NULL, '2020-01-18 11:11:56', '2020-01-20 09:27:37', NULL);
INSERT INTO `page_translations` VALUES (3, 3, NULL, 'az', 'Media Mərkəzi', 'media-merkezi', 6, NULL, NULL, NULL, NULL, NULL, '2020-01-18 11:16:38', '2020-01-21 14:25:01', NULL);
INSERT INTO `page_translations` VALUES (4, 4, 3, 'az', 'KİV üçün', 'kiv-ucun-1', 4, NULL, NULL, NULL, NULL, NULL, '2020-01-18 11:16:59', '2020-01-20 09:20:00', NULL);
INSERT INTO `page_translations` VALUES (5, 5, 1, 'az', 'Şirkət Rəhbərinin Müraciəti', 'sirket-rehberinin-muracieti', 5, '<p style=\"text-align: justify;\">Azərbaycanın ən b&ouml;y&uuml;k pambıq&ccedil;ılıq şirkəti adını qazanmış MKT İstehsalat Kommersiya MMC-nin fəaliyyəti və xidmətləri haqqında ətraflı məlumatı &ouml;z&uuml;ndə cəmləşdirən veb-səhifəmizi diqqətinizə təqdim etməkdən məmnunluq hissi duyuruq! Səhifəmizin Azərbaycandan və b&uuml;t&uuml;n d&uuml;nyadan olan dəyərli tərəfdaş və m&uuml;ştərilərimizlə əməkdaşlıq imkanları yaradacağına &uuml;mid edirik.</p>\r\n\r\n<p style=\"text-align: justify;\">D&ouml;vlət baş&ccedil;ımız cənab Prezident İlham Əliyevin birbaşa dəstəyi və qayğısının nəticəsidir ki, &ouml;lkədə aqrar sektorun m&uuml;h&uuml;m sahəsi olan pambıq&ccedil;ılıq son illər &ouml;z&uuml;n&uuml;n inkişaf d&ouml;vr&uuml;nə qədəm qoymuş və bu sahədə y&uuml;ksək nailiyyətlər əldə edilməkdədir. MKT İstehsalat Kommersiya MMC artıq 25 ildir ki, bu sahədə əsas rol almış, əldə etdiyi nailiyyətlərdən ilhamlanaraq,&nbsp;uğurlarını davam etdirməyə &ccedil;alışmış, həyata ke&ccedil;irilən hər bir layihədə ən son beynəlxalq&nbsp;standartlara cavab verən avadanlıq və həll &uuml;sulları tətbiq etməklə m&uuml;ştəri və tərəfdaşlarının&nbsp;keyfiyyət g&ouml;zləntilərini qarşılamağa nail olmuşdur. Yerinə yetirdiyimiz işlərin həcmindən asılı&nbsp;olmayaraq, hər bir layihəyə x&uuml;susi diqqət və qayğı g&ouml;stərmiş və&nbsp;&nbsp;m&uuml;vəffəqiyyətli nəticə əldə&nbsp;etməyi hədəf kimi m&uuml;əyyən etmişik. MKT İstehsalat Kommersiya MMC-nin respublikamızın 18&nbsp;rayonunda 12 pambığın ilkin emalı zavodu, 11 pambıq qəbulu məntəqəsi, 6 innovasiya&nbsp;təsərr&uuml;fatı filialı, 2 tekstil fabriki və 1 yağ emalı zavodu fəaliyyət g&ouml;stərir. Hər il daha y&uuml;ksək&nbsp;məhsuldarlıq əldə etmək &uuml;&ccedil;&uuml;n pambıq&ccedil;ılığın inkişafı istiqamətində qurduğumuz beynəlxalq&nbsp;əməkdaşlıqlar sayəsində şirkətimizdə d&uuml;nyanın ən m&uuml;asir texniki avadanlıqları tətbiq olunmaqda&nbsp;davam edir. Minlərlə zəhmətkeş insanı &ouml;z&uuml;ndə cəmləşdirən kollektivimizin uğurlarının&nbsp;b&uuml;n&ouml;vrəsində vətənimizə sevgi və cəmiyyətimizə sonsuz qayğı hissi durur.</p>\r\n\r\n<p style=\"text-align: justify;\">Təqdim etdiyimiz veb-səhifə vasitəsi ilə &ouml;z fəaliyyətimizi və xidmətlərimizi daha geniş ictimaiyyətə təqdim etmək niyyətindəyik.&nbsp;Veb-səhifəmizi ziyarət etdiyiniz &uuml;&ccedil;&uuml;n hər birinizə &ouml;z təşəkk&uuml;r&uuml;m&uuml;z&uuml; bildirir, b&ouml;y&uuml;k məmnunluq hissi ilə rəy və təkliflərinizi bizimlə b&ouml;l&uuml;şmənizi g&ouml;zləyirik.</p>\r\n\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n\r\n<p style=\"text-align: right;\"><strong>MKT-İK MMC-nin İdarə Heyətinin sədri<br />\r\nR&ouml;vşən Həsənov</strong></p>', NULL, NULL, NULL, NULL, '2020-01-18 11:22:17', '2020-01-20 09:41:22', NULL);
INSERT INTO `page_translations` VALUES (6, 6, 1, 'az', 'Uğurlarımız / Sertifikatlar', 'ugurlarimiz-sertifikatlar', 6, NULL, NULL, NULL, NULL, NULL, '2020-01-18 11:25:07', '2020-01-18 11:25:07', NULL);
INSERT INTO `page_translations` VALUES (7, 7, 6, 'az', 'Food Safety Management System', 'food-safety-management-system', 7, NULL, NULL, NULL, NULL, NULL, '2020-01-18 11:25:37', '2020-01-18 11:25:37', NULL);
INSERT INTO `page_translations` VALUES (8, 8, NULL, 'az', 'Müştərilərə', 'musterilere', 5, NULL, NULL, NULL, NULL, NULL, '2020-01-19 11:53:36', '2020-01-21 14:25:01', NULL);
INSERT INTO `page_translations` VALUES (9, 9, 8, 'az', 'Ən çox verilən suallar', 'en-cox-verilen-suallar', 9, NULL, NULL, NULL, NULL, NULL, '2020-01-19 11:53:54', '2020-01-19 11:53:54', NULL);
INSERT INTO `page_translations` VALUES (10, 10, NULL, 'az', 'İstehsalat', 'istehsalat', 2, NULL, NULL, NULL, NULL, NULL, '2020-01-19 11:55:15', '2020-01-22 20:26:59', NULL);
INSERT INTO `page_translations` VALUES (11, 11, 10, 'az', 'Filiallar', 'filiallar', 11, NULL, NULL, NULL, NULL, NULL, '2020-01-19 11:55:31', '2020-01-28 11:23:13', NULL);
INSERT INTO `page_translations` VALUES (12, 12, 3, 'az', 'Xəbərlər', 'xeberler', 12, NULL, NULL, NULL, NULL, NULL, '2020-01-19 12:13:35', '2020-01-19 12:13:35', NULL);
INSERT INTO `page_translations` VALUES (13, 13, NULL, 'az', 'Əlaqə', 'elaqe', 7, NULL, NULL, NULL, NULL, NULL, '2020-01-19 13:18:44', '2020-01-20 09:08:54', NULL);
INSERT INTO `page_translations` VALUES (14, 14, 10, 'az', 'İstehsalat məhsulları', 'istehsalat-mehsullari', 14, '<p style=\"text-align: justify;\">&ldquo;MKT İstehsalat Kommersiya&rdquo; MMC Azəbaycanın ən b&ouml;y&uuml;k xam pambıq istehsal&ccedil;ısıdır və Azərbaycan ərazisində 11 pambıq istehsal zavodu var. Şirkət hər m&ouml;vs&uuml;m Azərbaycan ərazisində əkilən pambığın 50%-dən &ccedil;oxunu toplayır və xam pambığın 40%-dən mahlıc, 52%-dən &ccedil;iyid, geri qalan 8%-dən isə pambıq tullantıları əldə edilir.</p>\r\n\r\n<p style=\"text-align: justify;\">Hər pambıq emal zavodu m&ouml;vs&uuml;m ili ərzində 5000-10000 ton mahlıc istehsal etməyə qadirdir. Bundan əlavə zavodlarda &ccedil;iyid, uluk, lint və tifitik istehsal edilir.</p>\r\n\r\n<p style=\"text-align: justify;\">MKT İK MMC-nin əlavə olaraq 2 open end iplik istehsal zavodu və 1 yağ, piy istehsal edən zavodu vardır. Yağ və piy zavodu rafinə olunmuş pambıq yağı və yan məhsullar istehsal edir.</p>', NULL, NULL, NULL, NULL, '2020-01-19 13:24:16', '2020-01-23 11:29:48', NULL);
INSERT INTO `page_translations` VALUES (15, 15, 10, 'az', 'İnnovasiyalar', 'innovasiyalar', 15, '<p style=\"text-align: justify;\"><span style=\"font-size:14px;\">İnnovativ fəaliyyət baxımından x&uuml;susi n&uuml;mayiş və m&uuml;şahidə sahələri yaradılmışdır ki, həmin sahələrdə yeni pambıq sortları və dərman preparatlarının təsir imkanları yoxlanılır.</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:14px;\">Pambıq sovkasına qarşı vaxtında və d&uuml;zg&uuml;n m&uuml;barizə apara bilmək &uuml;&ccedil;&uuml;n 200 ədəd feromen alınmış və rayonlar &uuml;zrə fermerlərin sahələrində yerləşdirilmişdir. </span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:14px;\">Sovkanın nəsillər &uuml;zrə həyat mərhələlərini m&uuml;şahidə etmək, dərman preparatlarından vaxtında və d&uuml;zg&uuml;n istifadə edə bilmək &uuml;&ccedil;&uuml;n feromenlərin əhəmiyyəti b&ouml;y&uuml;kd&uuml;r. Artıq bu m&uuml;şahidələr sovka ilə m&uuml;barizədə &uuml;st&uuml;n cəhətlərini g&ouml;stərməkdədir.</span></p>', NULL, NULL, NULL, NULL, '2020-01-19 13:27:58', '2020-01-24 15:03:32', NULL);
INSERT INTO `page_translations` VALUES (16, 16, NULL, 'az', 'Korporativ Əməkdaşlıq', 'korporativ-emekdasliq', 4, NULL, NULL, NULL, NULL, NULL, '2020-01-19 13:29:22', '2020-01-21 14:24:59', NULL);
INSERT INTO `page_translations` VALUES (17, 17, 16, 'az', 'İdxal - ixrac', 'idxal-ixrac', 17, '<p style=\"text-align: justify;\">&ldquo;MKT IK&rdquo; MMC olaraq respublika &uuml;zrə ixrac potensialı y&uuml;ksək firmaların arasında olmağın haqlı q&uuml;rurunu yaşayırıq. Belə ki, 2018/2019 m&ouml;vs&uuml;m ili &uuml;zrə 30000 ton mahlıc, 5000 ton yağ, 30000 ton pambıq jmıxı ixrac edilmişdir.</p>\r\n\r\n<p style=\"text-align: justify;\">Bu m&uuml;vəffəqiyyət rəhbərliyin dəstəyi və peşəkar satış komandasının hər bir m&uuml;ştəriyə x&uuml;susi yanaşması nəticəsində əldə edilmişdir.</p>\r\n\r\n<p style=\"text-align: justify;\">Əldə edilən g&ouml;stəricilərin daha y&uuml;ksək hədəflərə &ccedil;atdırlması &uuml;&ccedil;&uuml;n yeni bazarların yaradılması sahəsində addımlar atılmaqdadır.</p>', NULL, NULL, NULL, NULL, '2020-01-19 13:29:52', '2020-01-24 15:04:06', NULL);
INSERT INTO `page_translations` VALUES (18, 18, 8, 'az', 'İrad və təkliflər', 'irad-ve-teklifler', 18, '<p>Fəaliyyətimizlə bağlı har hansı irad və ya təklifiniz varsa, elektron formanı doldurmağınızı rica edirik.</p>\r\n\r\n<p><strong>Sizin sorğunuz 5 iş g&uuml;n&uuml; ərzində cavablandırılacaqdır.</strong></p>\r\n\r\n<p><strong>Telefon:</strong>&nbsp;+994 55 515&nbsp;01 01</p>', NULL, NULL, NULL, NULL, '2020-01-19 13:38:56', '2020-01-30 22:00:27', NULL);
INSERT INTO `page_translations` VALUES (19, 19, 10, 'az', 'Texnikalar', 'texnikalar', 19, '<p>&ldquo;MKT İK MMC&rdquo; open-end iplik isehsaltında Azərbaycanda yeganə firma olub.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>D&uuml;nya standartlarına (ISO 9001) uyğun ipliyin keyfiyyətinə nəzaret etmək &uuml;&ccedil;&uuml;n USTER markalı avadanlıqdan istifadə olunmaqdadır.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>İstehsal etdiyimiz məhsullar toxuma və trikotaj sənayesində xammal kimi istifadə olunmaqdadır.</p>', NULL, NULL, NULL, NULL, '2020-01-19 13:51:46', '2020-01-19 13:51:46', NULL);
INSERT INTO `page_translations` VALUES (20, 20, 19, 'az', 'Traktor', 'traktor', 20, NULL, NULL, NULL, NULL, NULL, '2020-01-19 13:52:39', '2020-01-19 13:52:39', NULL);
INSERT INTO `page_translations` VALUES (21, 21, 20, 'az', 'Traktor JOHN DEERE', 'traktor-john-deere', 21, NULL, '7930/ 8335 R/ 6130 D', NULL, NULL, NULL, '2020-01-19 13:53:23', '2020-01-27 11:51:10', '2020-01-27 11:51:10');
INSERT INTO `page_translations` VALUES (22, 22, 16, 'az', 'Tenderlər', 'tenderler', 22, NULL, NULL, NULL, NULL, NULL, '2020-01-19 13:54:39', '2020-01-19 13:54:39', NULL);
INSERT INTO `page_translations` VALUES (23, 23, 16, 'az', 'Topdan satış', 'topdan-satis', 23, '<ul>\r\n	<li style=\"text-align: justify;\"><span style=\"font-size:16px;\">Xahiş edirik, bizim topdan satıcı olmadığımızı və m&uuml;ştərilərimizə birbaşa pərakəndə satdığımızı nəzərə alın.</span></li>\r\n	<li style=\"text-align: justify;\"><span style=\"font-size:16px;\">Tək bir məhsuldan b&ouml;y&uuml;k miqdarlarda satmırıq.</span></li>\r\n	<li style=\"text-align: justify;\"><span style=\"font-size:16px;\">Bizim m&uuml;ştərinin sifarişində bəzi məhsulların sayına məhdudiyyət qoymaq h&uuml;ququmuz vardır və sizin sifarişinizdə d&uuml;zəliş olunmağa ehtiyac olduqda M&uuml;ştəri Xidmətləri komandası sizinlə əlaqə saxlayacaqdır. Qeyd edək ki, biz bəzi fərdi hallarda hər sifarişi nəzərdən ke&ccedil;iririk.</span></li>\r\n	<li style=\"text-align: justify;\"><span style=\"font-size:16px;\">Əgər ticarət edirsinizsə və ya potensial tərəfdaşsınızsa, bir məhsuldan b&ouml;y&uuml;k miqdarda almamaq və məhsul &ccedil;eşidlərimizə daxil olan bir &ccedil;ox məhsuldan az miqdarda almaqla topdan satış imkanı nəzərə alına bilər.</span></li>\r\n	<li style=\"text-align: justify;\"><span style=\"font-size:16px;\">Xahiş edirik, tələblərinizi Əlaqə səhifəsini istifadə edərək bizə &ccedil;atdırın. Sizlə qiymət m&ouml;vzusunda lazımlı əlaqə yaradılacaq və &uuml;midlərimiz doğrulduğu halda, siz məhsulla təmin ediləcəksiniz.</span></li>\r\n</ul>', NULL, NULL, NULL, NULL, '2020-01-19 13:58:37', '2020-01-27 13:02:58', NULL);
INSERT INTO `page_translations` VALUES (24, 24, NULL, 'az', 'Müəssisələr', 'muessiseler', 1, NULL, NULL, NULL, NULL, NULL, '2020-01-19 14:00:17', '2020-01-22 20:26:56', NULL);
INSERT INTO `page_translations` VALUES (25, 25, NULL, 'az', 'Məhsullar', 'mehsullar', 3, NULL, NULL, NULL, NULL, NULL, '2020-01-19 14:57:18', '2020-01-22 20:26:59', NULL);
INSERT INTO `page_translations` VALUES (26, 26, 6, 'az', 'Uyğunluq Sertifikatı', 'uygunluq-sertifikati', 24, NULL, NULL, NULL, NULL, NULL, '2020-01-21 14:04:34', '2020-01-21 14:04:34', NULL);
INSERT INTO `page_translations` VALUES (27, 27, 19, 'az', 'Kombayn', 'kombayn', 25, NULL, NULL, NULL, NULL, NULL, '2020-01-27 11:30:45', '2020-01-27 11:30:45', NULL);
INSERT INTO `page_translations` VALUES (28, 28, 20, 'az', 'Traktor MTZ', 'traktor-mtz-80x', 26, NULL, '80 X', NULL, NULL, NULL, '2020-01-27 11:31:59', '2020-01-27 11:32:24', NULL);
INSERT INTO `page_translations` VALUES (29, 29, 20, 'az', 'Traktor Belarus', 'traktor-belarus', 27, NULL, '1221', NULL, NULL, NULL, '2020-01-27 11:33:04', '2020-01-27 11:33:04', NULL);
INSERT INTO `page_translations` VALUES (30, 30, 20, 'az', 'Traktor Belarus', 'traktor-belarus-1', 28, NULL, '2022', NULL, NULL, NULL, '2020-01-27 11:33:37', '2020-01-27 11:33:37', NULL);
INSERT INTO `page_translations` VALUES (31, 31, 20, 'az', 'Traktor New Holland', 'traktor-new-holland', 29, NULL, 'T 7030', NULL, NULL, NULL, '2020-01-27 11:34:10', '2020-01-27 11:34:10', NULL);
INSERT INTO `page_translations` VALUES (32, 32, 20, 'az', 'Traktor New Holland', 'traktor-new-holland-1', 30, NULL, 'T 7060', NULL, NULL, NULL, '2020-01-27 11:34:43', '2020-01-27 11:34:43', NULL);
INSERT INTO `page_translations` VALUES (33, 33, 20, 'az', 'Traktor  New Holland', 'traktor-new-holland-2', 31, NULL, 'T 6090', NULL, NULL, NULL, '2020-01-27 11:35:23', '2020-01-27 11:35:23', NULL);
INSERT INTO `page_translations` VALUES (34, 34, 20, 'az', 'Traktor New Holland', 'traktor-new-holland-3', 32, NULL, 'TD 95', NULL, NULL, NULL, '2020-01-27 11:35:57', '2020-01-27 11:35:57', NULL);
INSERT INTO `page_translations` VALUES (35, 35, 20, 'az', 'Traktor Hars', 'trak-hars', 33, NULL, 'Hars', NULL, NULL, NULL, '2020-01-27 11:41:43', '2020-01-27 11:43:20', NULL);
INSERT INTO `page_translations` VALUES (36, 36, 20, 'az', 'Trak John Deere', 'trak-john-deere', 34, NULL, '6175 M', NULL, NULL, NULL, '2020-01-27 11:42:58', '2020-01-27 11:42:58', NULL);
INSERT INTO `page_translations` VALUES (37, 37, 20, 'az', 'Traktor Belarus', 'traktor-belarus-2', 35, NULL, '89 / 2', NULL, NULL, NULL, '2020-01-27 11:44:43', '2020-01-27 11:44:43', NULL);
INSERT INTO `page_translations` VALUES (38, 38, 20, 'az', 'Trak Landini', 'trak-landini', 36, NULL, '110 hc', NULL, NULL, NULL, '2020-01-27 11:45:28', '2020-01-27 11:45:28', NULL);
INSERT INTO `page_translations` VALUES (39, 39, 27, 'az', 'Kombayn John Deere', 'kombayn-john-deere', 37, NULL, '9970', NULL, NULL, NULL, '2020-01-27 11:50:55', '2020-01-27 11:50:55', NULL);
INSERT INTO `page_translations` VALUES (40, 40, 27, 'az', 'Kombayn John Deere', 'kombayn-john-deere-1', 38, NULL, 'CP 690', NULL, NULL, NULL, '2020-01-27 11:51:52', '2020-01-27 11:51:52', NULL);
INSERT INTO `page_translations` VALUES (41, 41, 27, 'az', 'Kombayn Case', 'kombayn-case', 39, NULL, 'CPX 420', NULL, NULL, NULL, '2020-01-27 11:52:34', '2020-01-27 11:52:34', NULL);
INSERT INTO `page_translations` VALUES (42, 42, 19, 'az', 'Taxılbiçən', 'taxilbicen', 40, NULL, NULL, NULL, NULL, NULL, '2020-01-27 11:53:24', '2020-01-27 11:53:24', NULL);
INSERT INTO `page_translations` VALUES (43, 43, 42, 'az', 'Taxılbiçən New Holland', 'taxilbicen-new-holland', 41, NULL, 'J-DW 230', NULL, NULL, NULL, '2020-01-27 12:10:32', '2020-01-27 12:10:32', NULL);

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `template_id` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT 0,
  `target` tinyint(1) NOT NULL DEFAULT 1,
  `show_index` tinyint(1) NOT NULL DEFAULT 0,
  `icon` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `no` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES (1, 1, NULL, 1, 1, 0, 0, NULL, '2020-01-18 11:10:58', '2020-01-18 11:10:58', NULL);
INSERT INTO `pages` VALUES (2, 6, NULL, 1, 1, 0, 0, NULL, '2020-01-18 11:11:56', '2020-01-18 11:11:56', NULL);
INSERT INTO `pages` VALUES (3, 1, NULL, 1, 1, 0, 0, NULL, '2020-01-18 11:16:38', '2020-01-18 11:16:38', NULL);
INSERT INTO `pages` VALUES (4, 20, NULL, 1, 1, 0, 0, NULL, '2020-01-18 11:16:59', '2020-01-18 11:16:59', NULL);
INSERT INTO `pages` VALUES (5, 13, NULL, 1, 1, 0, 0, NULL, '2020-01-18 11:22:17', '2020-01-20 09:40:00', NULL);
INSERT INTO `pages` VALUES (6, 19, NULL, 1, 1, 0, 0, NULL, '2020-01-18 11:25:07', '2020-01-18 11:25:07', NULL);
INSERT INTO `pages` VALUES (7, 5, NULL, 1, 1, 0, 0, NULL, '2020-01-18 11:25:37', '2020-01-21 13:36:20', NULL);
INSERT INTO `pages` VALUES (8, 1, NULL, 1, 1, 0, 0, NULL, '2020-01-19 11:53:36', '2020-01-19 11:53:36', NULL);
INSERT INTO `pages` VALUES (9, 18, NULL, 0, 1, 0, 0, NULL, '2020-01-19 11:53:54', '2020-01-23 12:21:32', NULL);
INSERT INTO `pages` VALUES (10, 1, NULL, 1, 1, 0, 0, NULL, '2020-01-19 11:55:15', '2020-01-19 11:55:15', NULL);
INSERT INTO `pages` VALUES (11, 17, NULL, 1, 1, 0, 0, NULL, '2020-01-19 11:55:31', '2020-01-28 10:23:13', NULL);
INSERT INTO `pages` VALUES (12, 3, NULL, 1, 1, 0, 0, NULL, '2020-01-19 12:13:35', '2020-01-19 12:13:35', NULL);
INSERT INTO `pages` VALUES (13, 4, NULL, 1, 1, 0, 0, NULL, '2020-01-19 13:18:44', '2020-01-19 13:18:56', NULL);
INSERT INTO `pages` VALUES (14, 7, NULL, 1, 1, 0, 0, NULL, '2020-01-19 13:24:16', '2020-01-28 11:31:14', NULL);
INSERT INTO `pages` VALUES (15, 8, NULL, 1, 1, 0, 0, NULL, '2020-01-19 13:27:58', '2020-01-28 11:29:15', NULL);
INSERT INTO `pages` VALUES (16, 1, NULL, 1, 1, 0, 0, NULL, '2020-01-19 13:29:22', '2020-01-19 13:29:22', NULL);
INSERT INTO `pages` VALUES (17, 10, NULL, 1, 1, 0, 0, NULL, '2020-01-19 13:29:52', '2020-01-19 13:29:52', NULL);
INSERT INTO `pages` VALUES (18, 9, NULL, 1, 1, 0, 0, NULL, '2020-01-19 13:38:56', '2020-01-19 13:38:56', NULL);
INSERT INTO `pages` VALUES (19, 22, NULL, 1, 1, 0, 0, NULL, '2020-01-19 13:51:46', '2020-01-28 11:29:55', NULL);
INSERT INTO `pages` VALUES (20, 23, NULL, 1, 1, 0, 0, NULL, '2020-01-19 13:52:39', '2020-01-19 13:52:39', NULL);
INSERT INTO `pages` VALUES (21, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-19 13:53:23', '2020-01-19 13:53:23', NULL);
INSERT INTO `pages` VALUES (22, 11, NULL, 0, 1, 0, 0, NULL, '2020-01-19 13:54:39', '2020-01-21 14:25:36', NULL);
INSERT INTO `pages` VALUES (23, 16, NULL, 0, 1, 0, 0, NULL, '2020-01-19 13:58:37', '2020-01-28 11:16:40', NULL);
INSERT INTO `pages` VALUES (24, 12, NULL, 1, 1, 0, 0, NULL, '2020-01-19 14:00:17', '2020-01-28 11:01:32', NULL);
INSERT INTO `pages` VALUES (25, 21, NULL, 1, 1, 0, 0, NULL, '2020-01-19 14:57:18', '2020-01-19 14:57:18', NULL);
INSERT INTO `pages` VALUES (26, 5, NULL, 1, 1, 0, 0, 'Uyğunluq Sertifikatı №1', '2020-01-21 14:04:34', '2020-01-21 14:04:34', NULL);
INSERT INTO `pages` VALUES (27, 23, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:30:45', '2020-01-27 11:30:45', NULL);
INSERT INTO `pages` VALUES (28, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:31:59', '2020-01-27 11:31:59', NULL);
INSERT INTO `pages` VALUES (29, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:33:04', '2020-01-27 11:33:04', NULL);
INSERT INTO `pages` VALUES (30, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:33:37', '2020-01-27 11:33:37', NULL);
INSERT INTO `pages` VALUES (31, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:34:10', '2020-01-27 11:34:10', NULL);
INSERT INTO `pages` VALUES (32, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:34:43', '2020-01-27 11:34:43', NULL);
INSERT INTO `pages` VALUES (33, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:35:23', '2020-01-27 11:35:23', NULL);
INSERT INTO `pages` VALUES (34, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:35:57', '2020-01-27 11:35:57', NULL);
INSERT INTO `pages` VALUES (35, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:41:43', '2020-01-27 11:41:43', NULL);
INSERT INTO `pages` VALUES (36, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:42:58', '2020-01-27 11:44:05', NULL);
INSERT INTO `pages` VALUES (37, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:44:43', '2020-01-27 11:44:54', NULL);
INSERT INTO `pages` VALUES (38, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:45:28', '2020-01-27 11:45:28', NULL);
INSERT INTO `pages` VALUES (39, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:50:55', '2020-01-27 11:50:55', NULL);
INSERT INTO `pages` VALUES (40, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:51:52', '2020-01-27 11:51:52', NULL);
INSERT INTO `pages` VALUES (41, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:52:34', '2020-01-27 11:52:34', NULL);
INSERT INTO `pages` VALUES (42, 23, NULL, 1, 1, 0, 0, NULL, '2020-01-27 11:53:24', '2020-01-27 11:53:24', NULL);
INSERT INTO `pages` VALUES (43, 24, NULL, 1, 1, 0, 0, NULL, '2020-01-27 12:10:32', '2020-01-27 12:10:32', NULL);

-- ----------------------------
-- Table structure for partners
-- ----------------------------
DROP TABLE IF EXISTS `partners`;
CREATE TABLE `partners`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `site_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product_blocks
-- ----------------------------
DROP TABLE IF EXISTS `product_blocks`;
CREATE TABLE `product_blocks`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(10) UNSIGNED NOT NULL,
  `title_az` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `title_ru` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `summary_az` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `summary_ru` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_blocks
-- ----------------------------
INSERT INTO `product_blocks` VALUES (1, 2, 'Növ', NULL, NULL, '1-4', '1-4', '1-4', '2020-01-19 15:07:46', '2020-01-19 15:07:46');
INSERT INTO `product_blocks` VALUES (2, 3, 'Staple (MM):', 'Staple (MM):', 'Staple (MM):', '27,5-32,00', '27,5-32,00', '27,5-32,00', '2020-01-19 15:17:07', '2020-01-19 15:17:07');
INSERT INTO `product_blocks` VALUES (3, 4, 'Rəng (Colour)', 'Rəng (Colour)', 'Rəng (Colour)', 'Qəhveyi (Brown)', 'Qəhveyi (Brown)', 'Qəhveyi (Brown)', '2020-01-19 15:22:31', '2020-01-19 15:22:31');
INSERT INTO `product_blocks` VALUES (4, 2, 'Nəmlik', NULL, NULL, '10,0%', NULL, NULL, '2020-01-21 14:40:22', '2020-01-21 14:41:17');
INSERT INTO `product_blocks` VALUES (5, 2, 'Yağlılıq', NULL, NULL, '19,16%', NULL, NULL, '2020-01-21 14:40:41', '2020-01-21 14:41:12');
INSERT INTO `product_blocks` VALUES (6, 2, 'Zibillilik', NULL, NULL, '1,50%', NULL, NULL, '2020-01-21 14:41:03', '2020-01-21 14:41:03');
INSERT INTO `product_blocks` VALUES (7, 2, 'Quru Maddə', NULL, NULL, '90%', NULL, NULL, '2020-01-21 14:41:35', '2020-01-21 14:41:35');
INSERT INTO `product_blocks` VALUES (8, 2, 'Xam Protein', NULL, NULL, '23%', NULL, NULL, '2020-01-21 14:42:00', '2020-01-21 14:42:00');
INSERT INTO `product_blocks` VALUES (9, 2, 'Xam Seliloza', NULL, NULL, '25%', NULL, NULL, '2020-01-21 14:42:16', '2020-01-21 14:42:16');
INSERT INTO `product_blocks` VALUES (10, 4, 'Növ (Sort)', NULL, NULL, '1-4', NULL, NULL, '2020-01-21 14:54:32', '2020-01-21 14:54:32');
INSERT INTO `product_blocks` VALUES (11, 4, 'Nəmlik (Mouisture)', NULL, NULL, '5% - 7%', NULL, NULL, '2020-01-21 14:54:56', '2020-01-21 14:54:56');
INSERT INTO `product_blocks` VALUES (12, 4, 'Yağlılıq (Fats)', NULL, NULL, '4% - 8%', NULL, NULL, '2020-01-21 14:55:39', '2020-01-21 14:55:39');
INSERT INTO `product_blocks` VALUES (13, 4, 'Protein (Protein)', NULL, NULL, '22% - 26%', NULL, NULL, '2020-01-21 14:56:41', '2020-01-21 14:56:41');
INSERT INTO `product_blocks` VALUES (14, 4, 'Metal qırıntılarl (Metal crubs)', NULL, NULL, '0,001%', NULL, NULL, '2020-01-21 14:57:04', '2020-01-21 14:57:04');
INSERT INTO `product_blocks` VALUES (15, 4, 'Kül (Ash)', NULL, NULL, '2,17%', NULL, NULL, '2020-01-21 14:57:23', '2020-01-21 14:57:23');
INSERT INTO `product_blocks` VALUES (16, 4, 'Qossipol (Gossypol)', NULL, NULL, '0,002%', NULL, NULL, '2020-01-21 14:57:40', '2020-01-21 14:57:40');
INSERT INTO `product_blocks` VALUES (17, 4, 'Selüloz (Cellulose)', NULL, NULL, '22% - 26%', NULL, NULL, '2020-01-21 14:58:00', '2020-01-21 14:58:00');
INSERT INTO `product_blocks` VALUES (18, 4, 'Aflatosin (Aphlatoxin-B1)', NULL, NULL, '7 - 20 ppb', NULL, NULL, '2020-01-21 14:58:21', '2020-01-21 14:58:21');
INSERT INTO `product_blocks` VALUES (19, 3, 'Uzunluq (İNC):', NULL, NULL, '1-1/8\" - 1-5/32\"', NULL, NULL, '2020-01-22 20:37:06', '2020-01-22 20:37:06');
INSERT INTO `product_blocks` VALUES (20, 3, 'Micronaire dəyəri (NCL):', NULL, NULL, '4.1 - 5.1 mic', NULL, NULL, '2020-01-22 20:37:32', '2020-01-22 20:37:32');
INSERT INTO `product_blocks` VALUES (21, 3, 'Rəng (HVI)', NULL, NULL, '11.1 – 53.1', NULL, NULL, '2020-01-22 20:37:56', '2020-01-22 20:37:56');
INSERT INTO `product_blocks` VALUES (22, 6, 'Bakı', NULL, NULL, 'Gəncə', NULL, NULL, '2020-01-30 21:48:41', '2020-01-30 21:48:56');
INSERT INTO `product_blocks` VALUES (23, 6, 'İplik NE 16/1', NULL, NULL, 'İplik NE 20/1', NULL, NULL, '2020-01-30 21:51:14', '2020-01-30 21:51:14');
INSERT INTO `product_blocks` VALUES (24, 6, 'İplik NE 20/1', NULL, NULL, 'İplik NE 16/1', NULL, NULL, '2020-01-30 21:51:50', '2020-01-30 21:51:50');
INSERT INTO `product_blocks` VALUES (25, 6, 'İplik NE 24/1', NULL, NULL, 'İplik NE 12/1', NULL, NULL, '2020-01-30 21:52:05', '2020-01-30 21:52:05');
INSERT INTO `product_blocks` VALUES (26, 6, 'İplik NE 30/1', NULL, NULL, 'İplik NE 6/1', NULL, NULL, '2020-01-30 21:52:29', '2020-01-30 21:52:29');

-- ----------------------------
-- Table structure for product_translations
-- ----------------------------
DROP TABLE IF EXISTS `product_translations`;
CREATE TABLE `product_translations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `technical_indicators` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `product_translations_product_id_lang_unique`(`product_id`, `lang`) USING BTREE,
  UNIQUE INDEX `product_translations_slug_unique`(`slug`) USING BTREE,
  INDEX `product_translations_page_id_foreign`(`page_id`) USING BTREE,
  CONSTRAINT `product_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `page_translations` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `product_translations_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_translations
-- ----------------------------
INSERT INTO `product_translations` VALUES (1, 1, 25, 'Çiyid', 'ciyid', 'Toxumluq çiyid Şirkətə məxsus yağ emalı zavodunda təkrar yağ istehsalında istifadə olunur.', '<p>Xam pambıq əsasən iki hissədən ibarətdir 40% pambıq mahlıcı və 60% pambıq &ccedil;iyidi. Pamıq &ccedil;iyidi təxminən 60% pambıq yağının istehsalı &uuml;&ccedil;&uuml;n istifadə olunur. Geri qalan pambıq &ccedil;iyidi isə yem xammadəsi kimi yerli və xarici bazara satılır.</p>\r\n\r\n<p><strong>Pambıq tullantıları</strong></p>\r\n\r\n<p>Pambıq istehsalı zamanı əldə edilən tullantılardan tiftik, lint və uluk məhsullarımız əsasən xarici bazara satış &uuml;&ccedil;&uuml;n nəzərdə tutulmuşdur.</p>', NULL, 'az', '2020-01-19 15:04:05', '2020-01-19 15:04:16', '2020-01-19 15:04:16');
INSERT INTO `product_translations` VALUES (2, 2, 25, 'Çiyid', 'ciyid-1', 'Toxumluq çiyid Şirkətə məxsus yağ emalı zavodunda təkrar yağ istehsalında istifadə olunur.', '<p style=\"text-align: justify;\">Xam pambıq əsasən iki hissədən ibarətdir. 40% pambıq mahlıcı və 60% pambıq &ccedil;iyidi. Pambıq &ccedil;iyidi təxminən 60% pambıq yağının istehsalı &uuml;&ccedil;&uuml;n istifadə olunur. Geri qalan pambıq &ccedil;iyidi isə yem xammadəsi kimi yerli və xarici bazara satılır.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Pambıq tullantıları</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Pambıq istehsalı zamanı əldə edilən tullantılardan tiftik, lint və uluk məhsullarımız əsasən xarici bazara satış &uuml;&ccedil;&uuml;n nəzərdə tutulmuşdur.</p>', NULL, 'az', '2020-01-19 15:04:13', '2020-01-21 14:44:24', NULL);
INSERT INTO `product_translations` VALUES (3, 3, 25, 'Mahlıc', 'mahlic', 'Toxumluq çiyid Şirkətə məxsus yağ emalı zavodunda təkrar yağ istehsalında istifadə olunur.', '<p style=\"text-align: justify;\">Azərbaycan ərazisində yerləşən 11 pambıq emalı zavodu roller gin və saw gin avadanlıqları ilə təchiz olunmuşdur. Mahlıcın texniki g&ouml;stərcilərini dəqiq və ətraflı &ouml;yrənmək &uuml;c&uuml;n Bərdə şəhərində yerləşən zavodda yeni avadanlıqlarla təchiz&nbsp;olunmuş mərkəzi laboratoriya qurulmuşdur. Bu laboratoriyada mahlıcın rəngi, zibillik dərəcəsi m&uuml;təxəssislər tərəfindən &ouml;l&ccedil;&uuml;l&uuml;r və HVİ &ouml;l&ccedil;mə cihazı ilə mikronier və lif uzunluğu nəticələri əldə olunur.</p>', '<p><strong>Rəng:</strong> Hal-hazırda təmizlənmiş mahlıcın rəngi mütəxəssis tərəfindən dəyərləndirilir.</p>\r\n\r\n<p><strong>Zibillik:</strong> Mahlıcın zibilik dəyəri pampıq nümunəsinin içindəki yarpaqların və yığım zamanı xam pambıqa qarışan materialların sayına əsasən ölçülür. Təmizlənmə zamanı xam pambığa qarışan yarpaqlar və digər materiallar xam pambıqdan ayrılır və mütəxəssislər tərəfdən zibillik dəyəri qiymətləndirilir. Zibillik dərəcəsi 1-dən (ən az zibillikli) 5-dək (ən çox zibillikli) qiymətləndirilir. Lif uzunluğu: Mahlıcın lif uzunluğu HVİ ölçmə cihazı vaistəsi ilə ölçülür.</p>\r\n\r\n<p><strong>Micronaire:</strong> Micronaire is measured by placing lint in a chamber, compressing it to a set volume and subjecting it to a set pressure. The micronaire result measured with HVI reports.</p>', 'az', '2020-01-19 15:10:35', '2020-01-22 20:35:00', NULL);
INSERT INTO `product_translations` VALUES (4, 4, 25, 'Pambıq cecəsi', 'pambiq-cecesi', 'Toxumluq çiyid Şirkətə məxsus yağ emalı zavodunda təkrar yağ istehsalında istifadə olunur.', '<p style=\"text-align: justify;\">Pambıq cecəsi&nbsp;pambıq yağı istehsalından sonra qalan &ccedil;iyid qabıqlarıdır. Bu qalıqlar heyvan yemi kimi daxili və xarici bazarda istifadə olunur. Bu məhsulun tərkibində y&uuml;ksək dərəcədə protein olduğu &uuml;c&uuml;n xarici bazarda bu məhsula tələbat b&ouml;y&uuml;kd&uuml;r. Məhsul xarici bazara rahat ixrac oluna bilməsi &uuml;&ccedil;&uuml;n &ldquo;MKT İK&rdquo; MMC Norve&ccedil;&nbsp;istehsalı olan Orkel markalı paketləmə avadanlığı gətirmişdir. Bu avadanlıq vasitəsi ilə pambıq cecəsi&nbsp;kiplərə yığılaraq d&uuml;nyanın istənilən &ouml;lkəsinə ixrac oluna bilər. 2018-2019 m&ouml;vs&uuml;m ilində pambıq cecəsi T&uuml;rkiyə, İran və Qazaxıstana ixrac olunmuşdur.</p>', NULL, 'az', '2020-01-19 15:19:54', '2020-01-28 11:18:51', NULL);
INSERT INTO `product_translations` VALUES (5, 5, 25, 'Dənli bitkilər', 'denli-bitkiler', 'Toxumluq çiyid Şirkətə məxsus yağ emalı zavodunda təkrar yağ istehsalında istifadə olunur.', '<p style=\"text-align: justify;\">Əsas fəaliyyət sahəsi pambıq&ccedil;ılıq olan &ldquo;MKT İK&ldquo; MMC n&ouml;vbəli əkin sisteminə uyğun olaraq dənli bitkilərin əkilib becərilməsi ilə də məşğul olmaqdadır.</p>\r\n\r\n<p style=\"text-align: justify;\">İllik dənli bitki hasilatı 10.000-15.000 tondur, əsasən yerli iqlim şəraitinə uyğun yemlik arpa və buğda &ccedil;eşidləri əkilməkdədir. İstehsal etdiyimiz dənli bitkilər yerli yem və un zavodlarının ehtiyacı &uuml;&ccedil;&uuml;n nəzərdə tutulmuşdur.</p>', NULL, 'az', '2020-01-19 15:42:20', '2020-01-22 20:30:11', NULL);
INSERT INTO `product_translations` VALUES (6, 6, 25, 'İplik', 'iplik', 'Toxumluq çiyid Şirkətə məxsus yağ emalı zavodunda təkrar yağ istehsalında istifadə olunur.', '<p style=\"text-align: justify;\"><span style=\"font-size:14px;\">&ldquo;MKT İK MMC&rdquo; open-end iplik istehsalatında Azərbaycanda yeganə firma olub.</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:14px;\">Azərbaycanda 2 iplik istehsal zavodu var, bu zavodlar Gəncə və Bakıda yerləşir. </span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:14px;\">Zavodların illik istehsal həcmləri 4000 - 6000 ton-dur. Hər iki zavod&nbsp;Ritter markalı avadanlıqlarla təchiz edilmişdir.</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:14px;\">D&uuml;nya standartlarına (ISO 9001) uyğun ipliyin keyfiyyətinə nəzarət etmək &uuml;&ccedil;&uuml;n USTER markalı avadanlıqdan istifadə olunmaqdadır.</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:14px;\">İstehsal etdiyimiz məhsullar toxuma və trikotaj sənayesində xammal kimi istifadə olunmaqdadır</span></p>\r\n\r\n<p style=\"text-align: justify;\"><span style=\"font-size:14px;\">İstehsal edilən iplik markaları;&nbsp;NE 12/1, NE 16/1, NE 20/1,NE 24/1, NE 30/1.</span></p>', NULL, 'az', '2020-01-19 15:48:17', '2020-01-28 11:51:10', NULL);

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `text_color_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `color_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `template_id` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, NULL, NULL, 2, 1, '2020-01-19 15:04:05', '2020-01-19 15:04:05', NULL);
INSERT INTO `products` VALUES (2, '#3E9F17', '#DDF1D5', 2, 1, '2020-01-19 15:04:13', '2020-01-21 14:20:37', NULL);
INSERT INTO `products` VALUES (3, '#F5780D', '#FEE7D5', 4, 1, '2020-01-19 15:10:35', '2020-01-21 14:21:21', NULL);
INSERT INTO `products` VALUES (4, '#FE5852', '#FCE0DF', 1, 1, '2020-01-19 15:19:54', '2020-01-21 14:21:58', NULL);
INSERT INTO `products` VALUES (5, '#B465F1', '#F2DEFF', 3, 1, '2020-01-19 15:42:20', '2020-01-21 14:23:20', NULL);
INSERT INTO `products` VALUES (6, NULL, NULL, 5, 1, '2020-01-19 15:48:17', '2020-01-19 15:48:17', NULL);

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `config_path` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `settings_key_unique`(`key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES (1, 'name', NULL, 'app.name', '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (2, 'url', NULL, 'app.url', '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (3, 'lang', 'az', NULL, '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (4, 'host', NULL, 'mail.host', '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (5, 'port', NULL, 'mail.port', '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (6, 'encryption', NULL, 'mail.encryption', '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (7, 'username', NULL, 'mail.username', '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (8, 'password', NULL, 'mail.password', '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (9, 'smtp_address', NULL, 'mail.from.address', '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (10, 'smtp_title', NULL, 'mail.from.name', '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (11, 'google_api_key', NULL, NULL, '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (12, 'google_analytic_key', NULL, NULL, '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (13, 'google_analytic_view_id', NULL, 'analytics.view_id', '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (14, 'email', NULL, NULL, '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (15, 'contact_email', 'info@mktcotton.com', NULL, '2020-01-14 12:14:11', '2020-01-27 14:14:35');
INSERT INTO `settings` VALUES (16, 'contact_phone', '(+994 12) 497 13 04', NULL, '2020-01-14 12:14:11', '2020-01-27 14:13:52');
INSERT INTO `settings` VALUES (17, 'contact_phone2', '(+994 55) 515 01 01', NULL, '2020-01-14 12:14:11', '2020-01-28 11:24:41');
INSERT INTO `settings` VALUES (18, 'location', NULL, NULL, '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (19, 'facebook', NULL, NULL, '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (20, 'instagram', NULL, NULL, '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (21, 'youtube', NULL, NULL, '2020-01-14 12:14:11', '2020-01-14 12:14:11');
INSERT INTO `settings` VALUES (22, 'count1', '25', NULL, '2020-01-14 12:14:11', '2020-01-28 12:14:28');
INSERT INTO `settings` VALUES (23, 'count2', '5572', NULL, '2020-01-14 12:14:11', '2020-01-28 12:14:44');
INSERT INTO `settings` VALUES (24, 'count3', '17', NULL, '2020-01-14 12:14:11', '2020-01-22 20:24:29');
INSERT INTO `settings` VALUES (25, 'count4', '8', NULL, '2020-01-14 12:14:11', '2020-01-28 12:16:32');
INSERT INTO `settings` VALUES (26, 'count5', '75', NULL, '2020-01-14 12:14:11', '2020-01-22 20:24:50');
INSERT INTO `settings` VALUES (27, 'count6', '81', NULL, '2020-01-14 12:14:11', '2020-01-22 20:24:59');
INSERT INTO `settings` VALUES (28, 'video_link', 'https://www.youtube.com/watch?v=OBk3pdtG3bg', NULL, '2020-01-14 12:14:11', '2020-01-20 09:26:26');

-- ----------------------------
-- Table structure for sliders
-- ----------------------------
DROP TABLE IF EXISTS `sliders`;
CREATE TABLE `sliders`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `title2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sliders
-- ----------------------------
INSERT INTO `sliders` VALUES (1, NULL, NULL, NULL, 'az', 1, NULL, '2020-01-14 12:16:25', '2020-01-14 12:16:43', '2020-01-14 12:16:43');
INSERT INTO `sliders` VALUES (2, NULL, 'MKT İstehsalat Kommersiya MMC', 'Metrologiya və Patent üzrə Dövlət Komitəsindən Uyğunluq Sertifikatı almışdır.', 'az', 1, '243', '2020-01-19 16:16:29', '2020-01-24 13:15:44', NULL);

-- ----------------------------
-- Table structure for subscribers
-- ----------------------------
DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE `subscribers`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `full_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `subscribers_email_unique`(`email`) USING BTREE,
  INDEX `subscribers_created_at_index`(`created_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tender_translations
-- ----------------------------
DROP TABLE IF EXISTS `tender_translations`;
CREATE TABLE `tender_translations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tender_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tender_translations_lang_slug_unique`(`lang`, `slug`) USING BTREE,
  INDEX `tender_translations_tender_id_foreign`(`tender_id`) USING BTREE,
  CONSTRAINT `tender_translations_tender_id_foreign` FOREIGN KEY (`tender_id`) REFERENCES `tenders` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tender_translations
-- ----------------------------
INSERT INTO `tender_translations` VALUES (1, 1, 'Əkin materiallarının satın alınması', '<p>Lot 1 &quot;Yaşıllıq sahələri &uuml;&ccedil;&uuml;n tələb olunan əkin materiallarının&quot; satın alınması Tender iştirak&ccedil;ılarına təklif edilir ki, https://www.etender.gov.az/ - d&ouml;vlət satınalmalarının vahid internet Portalına (Portal) elektron imzaları vasitəsilə daxil olsunlar və tender haqqında ətraflı məlumatı əldə etsinlər. M&uuml;qaviləni yerinə yetirmək &uuml;&ccedil;&uuml;n tender iştirak&ccedil;ıları lazımi maliyyə və texniki imkanlara malik olmalıdırlar. Tenderdə iştirak etmək istəyən təşkilatlar lot &uuml;zrə m&uuml;əyyən edilən məbləğdə iştirak haqqını g&ouml;stərilən hesaba k&ouml;&ccedil;&uuml;rd&uuml;kdən sonra m&uuml;sabiqə &uuml;zrə təkliflərini Portal vasitəsi ilə təqdim edə bilərlər.</p>\r\n\r\n<p>Lot 1 &quot;Yaşıllıq sahələri &uuml;&ccedil;&uuml;n tələb olunan əkin materiallarının&quot; satın alınması Tender iştirak&ccedil;ılarına təklif edilir ki, https://www.etender.gov.az/ - d&ouml;vlət satınalmalarının vahid internet Portalına (Portal) elektron imzaları vasitəsilə daxil olsunlar və tender haqqında ətraflı məlumatı əldə etsinlər. M&uuml;qaviləni yerinə yetirmək &uuml;&ccedil;&uuml;n tender iştirak&ccedil;ıları lazımi maliyyə və texniki imkanlara malik olmalıdırlar. Tenderdə iştirak etmək istəyən təşkilatlar lot &uuml;zrə m&uuml;əyyən edilən məbləğdə iştirak haqqını g&ouml;stərilən hesaba k&ouml;&ccedil;&uuml;rd&uuml;kdən sonra m&uuml;sabiqə &uuml;zrə təkliflərini Portal vasitəsi ilə təqdim edə bilərlər.</p>\r\n\r\n<p>Lot 1 &quot;Yaşıllıq sahələri &uuml;&ccedil;&uuml;n tələb olunan əkin materiallarının&quot; satın alınması Tender iştirak&ccedil;ılarına təklif edilir ki, https://www.etender.gov.az/ - d&ouml;vlət satınalmalarının vahid internet Portalına (Portal) elektron imzaları vasitəsilə daxil olsunlar və tender haqqında ətraflı məlumatı əldə etsinlər. M&uuml;qaviləni yerinə yetirmək &uuml;&ccedil;&uuml;n tender iştirak&ccedil;ıları lazımi maliyyə və texniki imkanlara malik olmalıdırlar. Tenderdə iştirak etmək istəyən təşkilatlar lot &uuml;zrə m&uuml;əyyən edilən məbləğdə iştirak haqqını g&ouml;stərilən hesaba k&ouml;&ccedil;&uuml;rd&uuml;kdən sonra m&uuml;sabiqə &uuml;zrə təkliflərini Portal vasitəsi ilə təqdim edə bilərlər.</p>\r\n\r\n<p>Lot 1 &quot;Yaşıllıq sahələri &uuml;&ccedil;&uuml;n tələb olunan əkin materiallarının&quot; satın alınması Tender iştirak&ccedil;ılarına təklif edilir ki, https://www.etender.gov.az/ - d&ouml;vlət satınalmalarının vahid internet Portalına (Portal) elektron imzaları vasitəsilə daxil olsunlar və tender haqqında ətraflı məlumatı əldə etsinlər. M&uuml;qaviləni yerinə yetirmək &uuml;&ccedil;&uuml;n tender iştirak&ccedil;ıları lazımi maliyyə və texniki imkanlara malik olmalıdırlar. Tenderdə iştirak etmək istəyən təşkilatlar lot &uuml;zrə m&uuml;əyyən edilən məbləğdə iştirak haqqını g&ouml;stərilən hesaba k&ouml;&ccedil;&uuml;rd&uuml;kdən sonra m&uuml;sabiqə &uuml;zrə təkliflərini Portal vasitəsi ilə təqdim edə bilərlər.</p>', 'az', 'ekin-materiallarinin-satin-alinmasi', '2020-01-19 13:56:48', '2020-01-19 13:57:37', NULL);

-- ----------------------------
-- Table structure for tenders
-- ----------------------------
DROP TABLE IF EXISTS `tenders`;
CREATE TABLE `tenders`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `published_by` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tenders
-- ----------------------------
INSERT INTO `tenders` VALUES (1, '2020-01-14', '2020-02-01', '2020-01-19 13:56:48', '2020-01-19 13:56:48', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for vacancies
-- ----------------------------
DROP TABLE IF EXISTS `vacancies`;
CREATE TABLE `vacancies`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `published_by` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for vacancy_translations
-- ----------------------------
DROP TABLE IF EXISTS `vacancy_translations`;
CREATE TABLE `vacancy_translations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vacancy_id` int(10) UNSIGNED NOT NULL,
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_experience` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `salary` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `requirements` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `vacancy_translations_vacancy_id_foreign`(`vacancy_id`) USING BTREE,
  CONSTRAINT `vacancy_translations_vacancy_id_foreign` FOREIGN KEY (`vacancy_id`) REFERENCES `vacancies` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
