<?php

namespace App\Crud;

use App\Models\ChartTitle;

class ChartStatisticCrud extends RenderCrud
{
    private $type;

    public function __construct($type)
    {
        $this->type = $type;
    }


    private function category()
    {

        $query = ChartTitle::where('type','title')->pluck('title_az as title', 'chart_titles.id');;

        $query->prepend('---', '');

        return $query;
    }

    private function LineX()
    {

        $query = ChartTitle::where('type','line_x')->pluck('title_az as title', 'chart_titles.id');;

        $query->prepend('---', '');

        return $query;
    }




    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Template",
                "db" => "template_id",
                "type" => "select",
                "data" => config('config.chart_template'),
                "selected" => 0,
                "attr" => ['class'=>'form-control','required'],
            ],
            [
                "label" => "Kateqoriya",
                "db" => "category_id",
                "type" => "select",
                "data" => $this->category(),
                "selected" => null,
                "hide" => [1],
                "attr" => ['class'=>'select-search form-control']
            ],
            [
                "label" => 'LineX',
                "db" => "product_id",
                "type" => 'select',
                "data" => $this->LineX(),
                "selected" => null,
                "hide" =>[1,2,3],
                "attr" => ['class'=>'select-search form-control']
            ],
            [
                "label" => 'Dəyər',
                "db" => "value",
                "type" => 'number',
                "value" =>0,
                "hide" =>[10],
                "attr" => ['class'=>'form-control', 'min' => 1],
            ],
            [
                "label" => 'İl',
                "db" => "year",
                "type" => 'number',
                "value" =>0,
                "hide" =>[10],
                "attr" => ['class'=>'form-control', 'min' => 1],
            ],
            [
                "label" => 'Faiz',
                "db" => "percent",
                "type" => 'number',
                "value" =>0,
                "hide" =>[1,2,3],
                "attr" => ['class'=>'form-control', 'min' => 1],
            ]
        ];
        return $this->render($fields, $action, $data);
    }


}


