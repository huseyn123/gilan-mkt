<?php

namespace App\Crud;

class ProductParameterCrud extends RenderCrud
{
    public function fields($action, $data = false)
    {
        $fields = [
            [
                "label" => "Template",
                "db" => "template_id",
                "type" => "select",
                "data" => config('config.product_template'),
                "selected" => 0,
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "Arxa Fon Rəngi",
                "db" => "color_code",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'title'=>"Məs: #DDF1D5"],
            ],
            [
                "label" => "Baslıq Rəngi",
                "db" => "text_color_code",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'title'=>"Məs: #DDF1D5"],
            ],
            [
                "label" => "Şəkil",
                "db" => "image",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    if($data != false && $data->getFirstMedia()){
                        if(substr($data->getFirstMedia()->getFullUrl(), -3) == 'svg'){
                            $url = $data->getFirstMedia()->getFullUrl();
                            $style = 'width:100px;height:100px';
                        }else{
                            $url = $data->getFirstMedia()->getUrl('blade');
                            $style="max-width:100%";
                        }

                        $img = '<div class="input-group"><img src="'.asset($url).'" style="'.$style.'"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="" required>
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                },
            ],
            [
                "label" => "Kover Şəkil",
                "db" => "cover",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    if($data != false && $data->getFirstMedia('cover')->getUrl('thumb')){
                        if(substr($data->getFirstMedia('cover')->getFullUrl(), -3) == 'svg'){
                            $url = $data->getFirstMedia('cover')->getFullUrl();
                            $style = 'width:100px;height:100px';
                        }else{
                            $url = $data->getFirstMedia('cover')->getUrl('thumb');
                            $style="max-width:100%";
                        }

                        $img = '<div class="input-group"><img src="'.asset($url).'" style="'.$style.'"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                            <span class="btn btn-primary">
                                <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                            </span>
                        </label>
                        <input type="text" class="form-control" readonly="" required>
                        <div class="divImage" style="display:none">
                            <img class="showImage" src="#">
                        </div>';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                },
            ],
            [
                "label" => "Ana Səhifədə Görünsün",
                "db" => "featured",
                "type" => "checkbox",
                "value" => 1,
                "checked" => false,
                "attr" => []
            ]
        ];


        return $this->render($fields, $action, $data);
    }
}


