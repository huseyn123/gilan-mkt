<?php

namespace App\Crud;

use App\Models\Product;

class ProductBlockCrud extends RenderCrud
{
    private function category()
    {

        $query = Product::join('product_translations as pt', 'pt.product_id', '=', 'products.id')
            ->select('products.id', 'pt.name', 'pt.lang')
            ->where('pt.lang','az')
            ->whereNull('pt.deleted_at')
            ->orderBy('pt.name', 'asc')
            ->pluck('pt.name', 'products.id');

        $query->prepend('---', '');


        return $query;
    }


    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Kateqoriya",
                "db" => "product_id",
                "type" => "select",
                "data" => $this->category(),
                "selected" => null,
                "hide" => [],
                "attr" => ['class'=>'select-search form-control']
            ],
        ];
        return $this->render($fields, $action, $data);
    }


}


