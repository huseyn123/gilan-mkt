<?php

namespace App\Crud;

class ChartTitleLineXCrud extends RenderCrud
{

    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => " ",
                "db" => "category_id",
                "type" => "hidden",
                "value" => 10,
            ],
            [
                "label" => " ",
                "db" => "type",
                "type" => "hidden",
                "value" => 'line_x',
            ]
        ];
        return $this->render($fields, $action, $data);
    }


}


