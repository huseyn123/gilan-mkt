<?php

namespace App\Crud;

class BranchMapCrud extends RenderCrud
{

    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Location",
                "db" => "location",
                "type" => "select",
                "data" => config('config.branch_location'),
                "selected" => null,
                "hide" => [],
                "attr" => ['class'=>'select-search form-control']
            ],
            [
                "label" => 'Fermer sayı',
                "db" => "farmers_count",
                "type" => 'number',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Əkin sahəsi',
                "db" => "sown_area",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Yaranma tarixi",
                "db" => "published_at",
                "type" => "text",
                "attr" => ['class'=>'form-control datepicker', 'autocomplete' => 'off', 'required']
            ]
        ];
        return $this->render($fields, $action, $data);
    }
}