<?php

namespace App\Crud;

use App\Models\Page;

class KivLogoCrud extends RenderCrud
{


    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Loqo Şəkili",
                "db" => "logo_image",
                "type" => "file",
                "show" => true,
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false && $data->getFirstMedia('logo_image')){
                        $title='Yenilə';
                        $img = '<div class="input-group"><img  style="max-width:250px;max-height:250px;" src="'.asset($data->getFirstMedia('logo_image')->getFullUrl()).'"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
            [
                "label" => "Png File",
                "db" => "png_file",
                "type" => "file",
                "show" => true,
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false && $data->getFirstMedia('png_file')){
                        $title = 'Yenilə';
                        $img = '<div class="input-group"><a href="'.asset($data->getFirstMedia('png_file')->getFullUrl()).'" download="donload">Faylı Yüklə</a></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
            [
                "label" => "Eps File",
                "db" => "eps_file",
                "type" => "file",
                "show" => true,
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false && $data->getFirstMedia('eps_file')){
                        $title = 'Yenilə';
                        $img = '<div class="input-group"><a href="'.asset($data->getFirstMedia('eps_file')->getFullUrl()).'" download="donload">Faylı Yüklə</a></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
            [
                "label" => "Image  Title",
                "db" => "image_title",
                "type" => "text",
                "data" => [],
                "selected" => null,
                "attr" => ['class'=>'form-control'],
            ],
        ];
        return $this->render($fields, $action, $data);
    }


}


