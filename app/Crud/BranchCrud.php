<?php

namespace App\Crud;


use App\Models\BranchElement;
use App\Models\Page;

class BranchCrud extends RenderCrud
{
    private function select($type)
    {

        $query = BranchElement::where('type',$type)->pluck('title_az as title', 'id');;

        $query->prepend('---', '');

        return $query;
    }

    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => 'Əlaqə',
                "db" => "phone",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Region",
                "db" => "region_id",
                "type" => "select",
                "data" => $this->select('region'),
                "selected" => null,
                "hide" => [],
                "attr" => ['class'=>'select-search form-control']
            ],
            [
                "label" => "Topdançı",
                "db" => "network_id",
                "type" => "select",
                "data" => $this->select('network'),
                "selected" => null,
                "hide" => [],
                "attr" => ['class'=>'select-search form-control']
            ],
        ];
        return $this->render($fields, $action, $data);
    }
}