<?php

namespace App\Crud;

use App\Models\Enterprise;

class EnterpriseBlockCrud extends RenderCrud
{
    private function category()
    {

        $query = Enterprise::join('enterprise_translations as et', 'et.enterprise_id', '=', 'enterprises.id')
            ->select('enterprises.id', 'et.name', 'et.lang')
            ->where('et.lang','az')
            ->whereNull('et.deleted_at')
            ->orderBy('et.name', 'asc')
            ->pluck('et.name', 'enterprises.id');

        $query->prepend('---', '');


        return $query;
    }


    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Kateqoriya",
                "db" => "enterprise_id",
                "type" => "select",
                "data" => $this->category(),
                "selected" => null,
                "hide" => [],
                "attr" => ['class'=>'select-search form-control']
            ],
        ];
        return $this->render($fields, $action, $data);
    }


}


