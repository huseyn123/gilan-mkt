<?php

namespace App\Crud;

class MahlicChartCrud extends RenderCrud
{


    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Chart Növü",
                "db" => "chart_type",
                "type" => "select",
                "data" => config('config.mahlic_chart'),
                "selected" => 0,
                "attr" => ['class'=>'form-control','required'],
            ],
            [
                "label" => 'Ad',
                "db" => "title",
                "type" => 'text',
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => 'Rəng',
                "db" => "color",
                "type" => 'text',
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => 'Dəyər',
                "db" => "value",
                "type" => 'number',
                "value" =>0,
                "hide" =>[10],
                "attr" => ['class'=>'form-control', 'min' => 1],
            ],
        ];
        return $this->render($fields, $action, $data);
    }


}


