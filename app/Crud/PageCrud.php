<?php

namespace App\Crud;
use App\Logic\MultiLanguageSelect;
use App\Models\Page;
use phpDocumentor\Reflection\Types\Null_;

class PageCrud extends RenderCrud
{
    private function category($lang)
    {
        if(is_null($lang)){
            $select = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
                ->whereNull('page_translations.deleted_at')
                ->whereNotIn('pages.template_id',[2,5,24])
                ->select('page_translations.id','page_translations.name', 'page_translations.lang','page_translations.deleted_at')
                ->orderBy('page_translations.name', 'asc')
                ->get();

            $query = MultiLanguageSelect::multiLang($select, false, true);
        }
        else{
            $query = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
                ->select('page_translations.id', 'page_translations.name', 'page_translations.lang')
                ->where('page_translations.lang', $lang)
                ->whereNotIn('pages.template_id',[2,5,24])
                ->whereNull('page_translations.deleted_at')
                ->orderBy('page_translations.name', 'asc')
                ->pluck('page_translations.name', 'page_translations.id');

            $query->prepend('---', '');
        }
        return $query;
    }

    public function fields($lang, $action, $data = false)
    {
        $fields = [
            [
                "label" => 'Ad',
                "db" => "name",
                "type" => 'text',
                "action" => 'edit',"attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => 'Ad(Seo)',
                "db" => "seo_title",
                "type" => 'text',
                "hide" => [1,2,4,6,11,14,15,16,18,20,23,24],
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Kateqoriya",
                "db" => "parent_id",
                "type" => "select",
                "data" => $this->category($lang),
                "selected" => null,
                "hide" => [],
                "attr" => ['class'=>'select-search form-control']
            ],
            [
                "label" => "Dil",
                "db" => "lang",
                "type" => "select",
                "data" => config('app.locales'),
                "selected" => array_first(config('app.locales')),
                "attr" => ['class'=>'form-control'],
                "edit" => false,
                "divClass" => "language-form"
            ],
            [
                "label" => "Slug (URL)",
                "db" => "slug",
                "type" => "text",
                "hide" => [2,24],
                "attr" => ['class'=>'form-control', 'autocomplete' => 'off', 'title'=>"Boş saxladığınız təqdirdə sluq avtomatik yaradılacaq."],
            ],
            [
                "label" => "Qısa məzmun",
                "db" => "summary",
                "type" => "textarea",
                "hide" => [1,3,2,4,6,7,8,9,10,11,12,13,16,17,18,19,20,21,22,23],
                "attr" => ['class'=>'form-control', 'rows' => 5],
            ],
            [
                "label" => "Məzmun",
                "db" => "content",
                "type" => "textarea",
                "hide" => [1,3,4,11,12,17,18,19,20,21,23,24],
                "attr" => ['class'=>'form-control ckeditor', 'id' => 'editor'.$lang]
            ],
            [
                "label" => "Link",
                "db" => "forward_url",
                "type" => "text",
                "hide" => [1,2,23,24],
                "attr" => ['class'=>'form-control', 'title'=>"Səhifəni başqa ünvana yönləndirmək üçün nəzərdə tutulub.", 'placeholder' => 'Məs: http://google.az'],
            ],
            [
                "label" => "Meta description",
                "db" => "meta_description",
                "type" => "textarea",
                "hide" => [2,23,24],
                "attr" => ['class'=>'form-control', 'maxlength' => 160, 'rows' => 3, 'title' => 'Maksimum simvol sayı 160 olmalıdır.Sosial şəbəkələrdə paylaşılan zaman məhz bu yazı linkin altında göstəriləcək']
            ],
            [
                "label" => "Meta keywords",
                "db" => "meta_keywords[]",
                "type" => "select",
                "data" => [],
                "selected" => null,
                "hide" => [2,23,24],
                "attr" => ['class'=>'form-control meta_keywords', 'id'  => 'meta_keywords'.$lang, 'multiple' => 'multiple', 'autocomplete' => 'off', 'title' => 'Keyword-ləri bir birindən ayırmaq üçün Enter düyməsini sıxın.'],
            ],
            [
                "label" => "Image  Title",
                "db" => "image_title",
                "type" => "text",
                "data" => [],
                "hide" => [1,2,23],
                "selected" => null,
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "Fayl",
                "db" => "page_file",
                "type" => "file",
                "hide" => [1,3,2,4,10,8,9,11,12,13,16,17,18,19,20,21,22,23,24],
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false && $data->getFirstMedia('page_file')){
                        $title = 'Yenilə';
                        $file = '<a href="'.$data->getFirstMedia('page_file')->getFullUrl().'" target="_blank">Fayla bax </a>';
                    }
                    else{
                        $file = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="" required>
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$file;
                },
            ],
        ];

        if(is_null($lang)){
            $paramFields = (new PageParameterCrud())->fields('get');
            return $this->render(array_merge($fields, $paramFields), $action, $data);
        }
        else{
            return $this->render($fields, $action, $data);
        }
    }
}


