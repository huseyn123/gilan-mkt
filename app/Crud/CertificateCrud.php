<?php

namespace App\Crud;

use App\Logic\MultiLanguageSelect;
use App\Models\Page;

class CertificateCrud extends RenderCrud
{


    private function category($lang)
    {
        if(is_null($lang)){
            $select = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
                ->whereNull('page_translations.deleted_at')
                ->whereIn('pages.template_id',[19])
                ->select('page_translations.id','page_translations.name', 'page_translations.lang','page_translations.deleted_at')
                ->orderBy('page_translations.name', 'asc')
                ->get();

            $query = MultiLanguageSelect::multiLang($select, false, true);
        }
        else{
            $query = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
                ->select('page_translations.id', 'page_translations.name', 'page_translations.lang')
                ->where('page_translations.lang', $lang)
                ->whereIn('pages.template_id',[19])
                ->whereNull('page_translations.deleted_at')
                ->orderBy('page_translations.name', 'asc')
                ->pluck('page_translations.name', 'page_translations.id');

            $query->prepend('---', '');
        }
        return $query;
    }


    public function fields($lang,$action, $data = false)
    {
        $fields = [
            [
                "label" => 'Ad',
                "db" => "name",
                "type" => 'text',
                "action" => 'edit',"attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => "Dil",
                "db" => "lang",
                "type" => "select",
                "data" => config('app.locales'),
                "selected" => array_first(config('app.locales')),
                "attr" => ['class'=>'form-control'],
                "edit" => false,
                "divClass" => "language-form"
            ],
            [
                "label" => "Kateqoriya",
                "db" => "parent_id",
                "type" => "select",
                "data" => $this->category($lang),
                "selected" => null,
                "hide" => [2],
                "attr" => ['class'=>'select-search form-control']
            ],
            [
                "label" => "Image  Title",
                "db" => "image_title",
                "type" => "text",
                "data" => [],
                "selected" => null,
                "attr" => ['class'=>'form-control'],
            ],
        ];

        if(is_null($lang)){
            $paramFields = (new CertificateParameterCrud())->fields('get');
            return $this->render(array_merge($fields, $paramFields), $action, $data);
        }
        else{
            return $this->render($fields, $action, $data);
        }
    }


}


