<?php

namespace App\Crud;

use App\Models\Page;
use App\Models\Product;

class ChartTitleCrud extends RenderCrud
{

    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Kateqoriya",
                "db" => "category_id",
                "type" => "select",
                "data" => config('config.chart_template'),
                "selected" => null,
                "hide" => [],
                "attr" => ['class'=>'select-search form-control','required']
            ],
            [
                "label" => 'Rəng',
                "db" => "color",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'min' => 1, 'required' => 'required'],
            ],
        ];
        return $this->render($fields, $action, $data);
    }


}


