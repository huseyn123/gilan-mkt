<?php

namespace App\Crud;

class EnterpriseParameterCrud extends RenderCrud
{
    public function fields($action, $data = false)
    {
        $fields = [
            [
                "label" => "Template",
                "db" => "template_id",
                "type" => "select",
                "data" => config('config.enterprise_template'),
                "selected" => 0,
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "Kover Şəkil",
                "db" => "image",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    if($data != false && $data->getFirstMedia()){
                        if(substr($data->getFirstMedia()->getFullUrl(), -3) == 'svg'){
                            $url = $data->getFirstMedia()->getFullUrl();
                            $style = 'width:100px;height:100px';
                        }else{
                            $url = $data->getFirstMedia()->getUrl('thumb');
                            $style="max-width:100%";
                        }

                        $img = '<div class="input-group"><img src="'.asset($url).'" style="'.$style.'"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="" required>
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                },
            ],
            [
                "label" => "Youtube Kover",
                "db" => "youtube_cover",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    if($data != false && $data->getFirstMedia('youtube_cover')){
                        if(substr($data->getFirstMedia('youtube_cover')->getFullUrl(), -3) == 'svg'){
                            $url = $data->getFirstMedia('youtube_cover')->getFullUrl();
                            $style = 'width:100px;height:100px';
                        }else{
                            $url = $data->getFirstMedia('youtube_cover')->getUrl('blade');
                            $style="max-width:100%";
                        }

                        $img = '<div class="input-group"><img src="'.asset($url).'" style="'.$style.'"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                            <span class="btn btn-primary">
                                <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                            </span>
                        </label>
                        <input type="text" class="form-control" readonly="" required>
                        <div class="divImage" style="display:none">
                            <img class="showImage" src="#">
                        </div>';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                },
            ],
            [
                "label" => "Youtube Link",
                "db" => "youtube_link",
                "type" => "text",
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Location",
                "db" => "location",
                "type" => "text",
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Ana Səhifədə Görünsün",
                "db" => "featured",
                "type" => "checkbox",
                "value" => 1,
                "checked" => false,
                "attr" => []
            ]
        ];


        return $this->render($fields, $action, $data);
    }
}


