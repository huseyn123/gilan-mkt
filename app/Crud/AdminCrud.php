<?php

namespace App\Crud;

class AdminCrud extends RenderCrud
{
    public function fields($action, $data = false, $disabled = false)
    {
        $fields = [
            [
                "label" => 'Ad, soyad',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Email',
                "db" => "email",
                "type" => 'email',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Şifrə',
                "db" => "password",
                "type" => 'password',
                "attr" => ['class'=>'form-control'],
                'edit' => false,
                'profile' => false
            ],
        ];

        return $this->render($fields, $action, $data, $disabled);
    }


}