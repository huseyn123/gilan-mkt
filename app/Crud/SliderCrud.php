<?php

namespace App\Crud;


use App\Logic\MultiLanguageSelect;
use App\Models\Page;

class SliderCrud extends RenderCrud
{
    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Dil",
                "db" => "lang",
                "type" => "select",
                "data" => config('app.locales'),
                "selected" => array_first(config('app.locales')),
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => 'Başlıq',
                "db" => "title",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Başlıq2',
                "db" => "title2",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Şəkil",
                "db" => "image",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    if($data != false){
                        if(substr($data->getFirstMedia()->getFullUrl(), -3) == 'svg'){
                            $url = $data->getFirstMedia()->getFullUrl();
                            $style = 'width:100px;height:100px';
                        }else{
                            $url = $data->getFirstMedia()->getUrl('thumb');
                            $style="max-width:100%";
                        }
                        $img = '<div class="input-group"><img src="'.asset($url).'" style="'.$style.'"></div>';

                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
            [
                "label" => 'Qısa məzmun',
                "db" => "summary",
                "type" => 'textarea',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Link",
                "db" => "link",
                "type" => "text",
                "attr" => ['class'=>'form-control'],
            ],
        ];
        return $this->render($fields, $action, $data);
    }
}