<?php

namespace App\DataTables;

use App\Models\ProductBlock;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use DB;

class ProductBlockDataTable extends DataTable
{
    protected $route;

    public function route($route) {
        $this->route = $route;
        return $this;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->rawColumns(['action'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', [
                    'route' => $this->route,
                    'row' => $row,
                    'forceDelete' => true,
                    'largeModal'  => true
                ])->render();
            });
    }



    public function query(ProductBlock $model)
    {
        $query = $model->newQuery()
            ->leftJoin('products', 'products.id', '=', 'product_blocks.product_id')
            ->leftJoin('product_translations as pt', 'pt.product_id', '=', 'products.id')
            ->whereNull('pt.deleted_at')
            ->where('pt.lang','az')
            ->select('product_blocks.*','pt.name as product_name');
        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '90px', 'orderable' => false, 'searchable' => false, 'title' => ' '])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'product_blocks.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'title_az', 'name' => 'product_blocks.title_az', 'title' => 'Ad-az'],
            ['data' => 'summary_az', 'name' => 'product_blocks.summary_az', 'title' => 'Dəyər-az'],
            ['data' => 'product_name', 'name' => 'pt.name', 'title' => 'Kateqoriya'],
            ['data' => 'created_at','name' => 'product_blocks.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false],
            ['data' => 'updated_at','name' => 'product_blocks.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [5,10],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'productBlocksdatatable_' . time();
    }
}
