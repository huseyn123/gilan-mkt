<?php

namespace App\DataTables;

use App\Models\Tender;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class TenderDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'tenders','forceDelete' =>true,'translation' =>true,'largeModal' => true])->render();
            });
    }

    public function query(Tender $model)
    {
        $query = $model->newQuery()
            ->join('tender_translations as tt', 'tt.tender_id', '=', 'tenders.id')
            ->select(
                'tt.*',
                'tt.id as tid',
                'tenders.id',
                'tenders.published_by',
                'tenders.end_date'
            );

        if ($this->request()->has('lang') && $this->request()->get('lang') != 'all') {
            $query->where('tt.lang', $this->request()->get('lang'));
        }

        if($this->request()->get('type') == 1){
            $query->where('tenders.end_date', '>=', date('Y-m-d'));
        }
        elseif($this->request()->get('type') == 2){
            $query->where('tenders.end_date', '<', date('Y-m-d'));
        }
        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '140px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'tt.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'name', 'name' => 'tt.name', 'title' => 'Ad','searchable' => true],
            ['data' => 'slug', 'name' => 'tt.slug', 'title' => 'Slug','searchable' => true],
            ['data' => 'published_by', 'name' => 'tenders.published_by', 'title' => 'Elanın başlanma tarixi', 'searchable' => false],
            ['data' => 'end_date', 'name' => 'tenders.published_by', 'title' => 'Elanın bitmə tarixi', 'searchable' => false],
            ['data' => 'lang', 'name' => 'tt.lang', 'title' => 'Dil', 'searchable' => false,'orderable' => false],
            ['data' => 'created_at', 'name' => 'tt.created_at', 'title' => 'Yaradıldı','orderable' => false, 'class' => 'none', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'tt.updated_at', 'title' => 'Yenilənib','orderable' => false, 'class' => 'none', 'searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [1,'desc'] ],
            'lengthMenu' => [10,25,50],
            'language' => [
                'url' => url('lang.json'),
            ],
            'drawCallback' => 'function() {
                $("#tenders_length").prependTo($("#dataTables_length_box"));
            }',
        ];
    }
}
