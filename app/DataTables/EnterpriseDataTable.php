<?php

namespace App\DataTables;

use App\Models\Enterprise;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class EnterpriseDataTable extends DataTable
{

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable

            ->editColumn('title', function($row) {
                return str_limit($row->title, $limit = 50, $end = '...');
            })
            ->editColumn('page_name', function($row) {
                if(is_null($row->page_id)){
                    return '<span class="text text-danger">Silinib və ya mövcud deyil!</span>';
                }
                elseif(is_null($row->deleted_page)){
                    return $row->page_name;
                }
                else{
                    return '<span class="text text-danger" style="text-decoration: line-through">'.$row->page_name.'</span>';
                }
            })
            ->editColumn('image', function($row) {
                if(!is_null($row->getFirstMedia())){
                    if(substr($row->getFirstMedia()->getFullUrl(), -3) == 'svg'){
                        $url = $row->getFirstMedia()->getFullUrl();
                        $style = 'width:100px;height:100px';
                    }else{
                        $url = $row->getFirstMedia()->getUrl('thumb');
                        $style="max-width:100%";
                    }

                    $img = '<div class="input-group"><img src="'.asset($url).'" style="'.$style.'"></div>';

                    return '<img src="'.asset($url).'" style="'.$style.'">';
                }
            })
            ->rawColumns(['image', 'page_name', 'action'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-page', [
                    'album' => true,
                    'editRoute' => 'enterprises',
                    'route' => 'enterprises',
                    'row' => $row,
                    'softDelete' => true,
                    'forceDelete' => true,
                    'largeModal' => true,
                ])->render();
            });
    }


    public function query(Enterprise $model)
    {
        $query = $model->newQuery()
            ->join('enterprise_translations as et', 'et.enterprise_id', '=', 'enterprises.id')
            ->leftJoin('page_translations as pt', 'pt.id', '=', 'et.page_id')
            ->leftJoin('pages', 'pages.id', '=', 'pt.page_id')
            ->where('pages.template_id',12)
            ->with(['media'])
            ->select('et.*', 'et.id as tid',
                'enterprises.id',
                'pt.name as page_name',
                'pt.lang','pt.deleted_at as deleted_page','pages.template_id');

        if ($this->request()->has('lang') && $this->request()->get('lang') != 'all') {
            $query->where('et.lang', $this->request()->get('lang'));
        }

        if($this->request()->get('type') == 1){
            $query->whereNull('et.deleted_at');
        }
        elseif($this->request()->get('type') == 2){
            $query->whereNotNull('et.deleted_at');
        }

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '140px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'et.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'media.image', 'title' => 'Şəkil', 'orderable' => false,'searchable' => false],
            ['data' => 'name', 'name' => 'et.name', 'title' => 'Ad', 'orderable' => false],
            ['data' => 'page_name', 'name' => 'et.name', 'title' => 'Kateqoriya'],
            ['data' => 'created_at', 'name' => 'et.created_at', 'title' => 'Yaradıldı','orderable' => false, 'class' => 'none', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'et.updated_at', 'title' => 'Yenilənib','orderable' => false, 'class' => 'none', 'searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25,50],
            'language' => [
                'url' => url('lang.json'),
            ],
            'drawCallback' => 'function() {
                $("#enterprises_length").prependTo($("#dataTables_length_box"));
            }',
        ];
    }
}
