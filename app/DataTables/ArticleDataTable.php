<?php

namespace App\DataTables;

use App\Models\Article;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ArticleDataTable extends DataTable
{

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('published_at', function($row) {
                return filterDate($row->published_at, true, 'eDay');
            })

            ->editColumn('title', function($row) {
                return str_limit($row->title, $limit = 50, $end = '...');
            })
            ->editColumn('page_name', function($row) {
                if(is_null($row->page_id)){
                    return '<span class="text text-danger">Silinib və ya mövcud deyil!</span>';
                }
                elseif(is_null($row->deleted_page)){
                    return $row->page_name;
                }
                else{
                    return '<span class="text text-danger" style="text-decoration: line-through">'.$row->page_name.'</span>';
                }
            })
            ->editColumn('image', function($row) {
                if(!is_null($row->getFirstMedia())){
                    return '<img src="'.asset($row->getFirstMedia()->getUrl('thumb')).'">';
                }
            })
            ->editColumn('status', function($row) {
                return '<span class="text-'.config("config.alert.$row->status").' text-bold">'.config("config.status.$row->status").'</span>';
            })

            ->rawColumns(['status', 'image', 'page_name', 'action'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-page', [
                    'editRoute' => 'articles',
                    'route' => 'articles',
                    'row' => $row,
                    'softDelete' => true,
                    'forceDelete' => true,
                    'show' => url("/$row->page_slug/$row->slug"),
                    'largeModal' => true,
                    'album' => true
                ])->render();
            });
    }


    public function query(Article $model)
    {
        $query = $model->newQuery()
            ->join('article_translations as at', 'at.article_id', '=', 'articles.id')
            ->leftJoin('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->leftJoin('pages', 'pages.id', '=', 'pt.page_id')

            ->where('pages.template_id',3)
            ->with(['media'])
            ->select('at.*', 'at.id as tid',
                'articles.id', 'articles.published_at', 'articles.published_by','articles.status',
                'pt.name as page_name',
                'pt.lang','pt.slug as page_slug',
                'pt.deleted_at as deleted_page','pages.template_id');

        if ($this->request()->has('lang') && $this->request()->get('lang') != 'all') {
            $query->where('at.lang', $this->request()->get('lang'));
        }

        if($this->request()->get('type') == 1){
            $query->whereNull('at.deleted_at');
        }
        elseif($this->request()->get('type') == 2){
            $query->whereNotNull('at.deleted_at');
        }

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '140px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'at.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'name', 'name' => 'at.name', 'title' => 'Başlıq', 'orderable' => false],
            ['data' => 'page_name', 'name' => 'pt.name', 'title' => 'Kateqoriya'],
            ['data' => 'published_at', 'name' => 'articles.published_at', 'title' => 'Tarix'],
            ['data' => 'status', 'name' => 'articles.status', 'title' => 'Status', 'searchable' => false],
            ['data' => 'lang', 'name' => 'at.lang', 'title' => 'Dil', 'searchable' => false],
            ['data' => 'slug', 'name' => 'at.slug', 'title' => 'Slug', 'orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'published_by', 'name' => 'articles.published_by', 'title' => 'Müəllif','orderable' => false, 'class' => 'none', 'searchable' => false],
            ['data' => 'image', 'name' => 'media.image', 'title' => 'Şəkil', 'orderable' => false, 'class' => 'none', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'at.created_at', 'title' => 'Yaradıldı','orderable' => false, 'class' => 'none', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'at.updated_at', 'title' => 'Yenilənib','orderable' => false, 'class' => 'none', 'searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25,50],
            'language' => [
                'url' => url('lang.json'),
            ],
            'drawCallback' => 'function() {
                $("#articles_length").prependTo($("#dataTables_length_box"));
            }',
        ];
    }
}
