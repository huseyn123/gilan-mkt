<?php

namespace App\DataTables;

use App\Models\Branch;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use DB;

class BranchDataTable extends DataTable
{
    protected $route;

    public function route($route) {
        $this->route = $route;
        return $this;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->rawColumns(['action'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', [
                    'route' => $this->route,
                    'row' => $row,
                    'forceDelete' => true,
                    'largeModal'  => true
                ])->render();
            });
    }



    public function query(Branch $model)
    {
        $query = $model->newQuery()
           ->join('branch_elements as region', function($data)
               {
                   $data->on('region.id', '=', 'branches.region_id')
                       ->where('region.type', '=', "region");
               })
            ->join('branch_elements as network', function($data)
            {
                $data->on('network.id', '=', 'branches.network_id')
                    ->where('network.type', '=', "network");
            })
            ->select('branches.*','region.title_az as region','network.title_az as network')
        ;

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '90px', 'orderable' => false, 'searchable' => false, 'title' => ' '])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'branches.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'location_az', 'name' => 'branches.location_az', 'title' => 'Ünvan-az'],
            ['data' => 'region', 'name' => 'region.region', 'title' => 'Region'],
            ['data' => 'network', 'name' => 'network.network', 'title' => 'Topdançı'],
            ['data' => 'created_at','name' => 'branches.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false],
            ['data' => 'updated_at','name' => 'branches.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [5,10],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'branchdatatable_' . time();
    }
}
