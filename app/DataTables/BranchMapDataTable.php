<?php

namespace App\DataTables;

use App\Models\BranchMap;
use Yajra\DataTables\Services\DataTable;
use App\Logic\userAction;
use Yajra\DataTables\EloquentDataTable;

class BranchMapDataTable extends DataTable
{
    protected $route;

    public function route($route) {
        $this->route = $route;
        return $this;
    }


    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('location', function($post) {
                return config("config.branch_location.$post->location");
            })
            ->editColumn('published_at', function($row) {
                return blogDate($row->published_at);
            })
            ->rawColumns(['action','published_at','location'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', [
                    'route' => $this->route,
                    'row' => $row,
                    'forceDelete' => true,
                    'largeModal' => true,
                ])->render();
            });
    }

    public function query(BranchMap $model)
    {
        $query = $model->newQuery();

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'branch_maps.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'location', 'name' => 'branch_maps.location', 'title' => 'Location'],
            ['data' => 'region_az', 'name' => 'branch_maps.region_az', 'title' => 'Region az'],
            ['data' => 'branch_title_az', 'name' => 'branch_maps.branch_title_az', 'title' => 'Filial az'],
            ['data' => 'farmers_count', 'name' => 'branch_maps.farmers_count', 'title' => 'Fermer sayı'],
            ['data' => 'sown_area', 'name' => 'branch_maps.sown_area', 'title' => 'Əkin sahəsi'],
            ['data' => 'published_at', 'name' => 'branch_maps.published_at', 'title' => 'Yaranma tarixi'],
            ['data' => 'created_at', 'name' => 'branch_maps.created_at','title' => 'Yaradıldı', 'class' => 'none', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'branch_maps.updated_at', 'title' => 'Yenilənib', 'class' => 'none', 'searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }



    protected function filename()
    {
        return 'branchMapsdatatable_' . time();
    }
}
