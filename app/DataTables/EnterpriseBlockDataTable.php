<?php

namespace App\DataTables;

use App\Models\EnterpriseBlock;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use DB;

class EnterpriseBlockDataTable extends DataTable
{
    protected $route;

    public function route($route) {
        $this->route = $route;
        return $this;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->rawColumns(['action'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', [
                    'route' => $this->route,
                    'row' => $row,
                    'forceDelete' => true,
                    'largeModal'  => true
                ])->render();
            });
    }



    public function query(EnterpriseBlock $model)
    {
        $query = $model->newQuery()
            ->leftJoin('enterprises', 'enterprises.id', '=', 'enterprise_blocks.enterprise_id')
            ->leftJoin('enterprise_translations as et', 'et.enterprise_id', '=', 'enterprises.id')
            ->whereNull('et.deleted_at')
            ->where('et.lang','az')
            ->select('enterprise_blocks.*','et.name as enterprise_name');
        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '90px', 'orderable' => false, 'searchable' => false, 'title' => ' '])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'enterprise_blocks.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'title_az', 'name' => 'enterprise_blocks.title_az', 'title' => 'Ad-az'],
            ['data' => 'enterprise_name', 'name' => 'pt.name', 'title' => 'Kateqoriya'],
            ['data' => 'created_at','name' => 'enterprise_blocks.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false],
            ['data' => 'updated_at','name' => 'enterprise_blocks.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [5,10],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'enterpriseBlocksdatatable_' . time();
    }
}
