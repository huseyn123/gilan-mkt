<?php

namespace App\DataTables;

use App\Models\MahlicChart;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use DB;

class MahlicChartDataTable extends DataTable
{
    protected $route;

    public function route($route) {
        $this->route = $route;
        return $this;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('chart_type', function($row) {
               return config('config.mahlic_chart.'.$row->chart_type);
            })
            ->rawColumns(['action'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', [
                    'route' => $this->route,
                    'row' => $row,
                    'forceDelete' => true,
                    'largeModal'  => true
                ])->render();
            });
    }



    public function query(MahlicChart $model)
    {
       $query = $model->newQuery();

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '90px', 'orderable' => false, 'searchable' => false, 'title' => ' '])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'mahlic_charts.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'chart_type', 'name' => 'mahlic_charts.chart_type', 'title' => 'Kateqoriya','orderable' => false],
            ['data' => 'title', 'name' => 'mahlic_charts.title', 'title' => 'Ad','orderable' => false],
            ['data' => 'color', 'name' => 'mahlic_charts.color', 'title' => 'Rəng','orderable' => false],
            ['data' => 'value', 'name' => 'mahlic_charts.value', 'title' => 'Dəyər','orderable' => false],
            ['data' => 'created_at','name' => 'mahlic_charts.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false],
            ['data' => 'updated_at','name' => 'mahlic_charts.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [5,10],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'mahlicChartsdatatable_' . time();
    }
}
