<?php

namespace App\DataTables;

use App\Models\ChartTitle;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use DB;

class ChartTitleDataTable extends DataTable
{
    protected $route;

    public function route($route) {
        $this->route = $route;
        return $this;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('category_id', function($row) {
                return config("config.chart_template.$row->category_id");
            })
            ->rawColumns(['action'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', [
                    'route' => $this->route,
                    'row' => $row,
                    'forceDelete' => true,
                    'largeModal'  => true
                ])->render();
            });
    }



    public function query(ChartTitle $model)
    {
        $query = $model->newQuery()
            ->where('chart_titles.type','title')
            ->select('chart_titles.*');

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '90px', 'orderable' => false, 'searchable' => false, 'title' => ' '])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'chart_titles.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'title_az', 'name' => 'chart_titles.title_az', 'title' => 'Ad-az'],
            ['data' => 'category_id', 'name' => 'chart_titles.category_id', 'title' => 'Kateqoriya'],
            ['data' => 'color', 'name' => 'chart_titles.color', 'title' => 'Rəng'],
            ['data' => 'created_at','name' => 'chart_titles.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false],
            ['data' => 'updated_at','name' => 'chart_titles.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [5,10],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'chartTitelsdatatable_' . time();
    }
}
