<?php

namespace App\DataTables;

use App\Models\ChartStatistic;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use DB;

class ChartStatisticDataTable extends DataTable
{
    protected $route;

    public function route($route) {
        $this->route = $route;
        return $this;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('template_id', function($row) {
                if($row->template_id){
                    return config('config.chart_template.'.$row->template_id);
                }
            })
            ->editColumn('year', function($row) {
                if($row->template_id == 10 && $row->line_x_title){
                    return $row->line_x_title;
                }else{
                    return $row->year;
                }
            })
            ->editColumn('value', function($row) {
                if($row->template_id == 10 && $row->percent){
                    return $row->percent.'%';
                }else{
                    return $row->value;
                }
            })
            ->editColumn('title_az', function($row) {
                if(!$row->title_az){
                    return '----------';
                }else{
                    return $row->title_az;
                }
            })

            ->rawColumns(['action'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', [
                    'route' => $this->route,
                    'row' => $row,
                    'forceDelete' => true,
                    'largeModal'  => true
                ])->render();
            });
    }



    public function query(ChartStatistic $model)
    {
            $query = $model->newQuery()
                ->leftJoin('chart_titles', 'chart_statistics.category_id', '=', 'chart_titles.id')
                ->leftJoin('chart_titles as line_x', 'chart_statistics.product_id', '=', 'line_x.id')
                ->select('chart_statistics.*','chart_titles.title_az','line_x.title_az as line_x_title');

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '90px', 'orderable' => false, 'searchable' => false, 'title' => ' '])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'chart_statistics.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'title_az', 'name' => 'chart_titles.title_az', 'title' => 'Kateqoriya','orderable' => false],
            ['data' => 'template_id', 'name' => 'chart_statistics.template_id', 'title' => 'Template','orderable' => false],
            ['data' => 'year', 'name' => 'chart_statistics.year', 'title' => 'İl || LineX'],
            ['data' => 'value', 'name' => 'chart_statistics.value', 'title' => 'Dəyər || faiz'],
            ['data' => 'created_at','name' => 'chart_statistics.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false],
            ['data' => 'updated_at','name' => 'chart_statistics.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [5,10],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'chartTitelsdatatable_' . time();
    }
}
