<?php

namespace App\Logic;
use App\Models\Article;
use App\Models\Banner;
use App\Models\Branch;
use App\Models\BranchElement;
use App\Models\BranchMap;
use App\Models\Certificates;
use App\Models\ChartStatistic;
use App\Models\ChartTitle;
use App\Models\Enterprise;
use App\Models\EnterpriseBlock;
use App\Models\Faq;
use App\Models\Kiv;
use App\Models\MahlicChart;
use App\Models\Page;
use App\Models\ArticleTranslation;
use App\Models\PageTranslation;
use App\Models\Product;
use App\Models\ProductBlock;
use App\Models\Slider;
use App\Models\Tender;
use App\Models\Vacancy;
use App\Models\VacancyTranslation;
use DB;
use function GuzzleHttp\Promise\queue;

trait Pages
{
    protected function static_block($getPage, $relatedPages,$view,$template_id =[2])
    {

        $blocks = Page::join('page_translations as pt','pt.page_id','pages.id')
            ->whereNull('pt.deleted_at')
            ->where('pt.parent_id',$getPage->tid)
            ->where('pt.lang',$this->lang)
            ->whereIn('pages.template_id',$template_id)
            ->select('pt.name','pt.content','pages.id')
            ->with('media')
            ->get();


        return view("web.".$view, ['page' => $getPage,'blocks' =>$blocks,'relatedPages' => $relatedPages]);
    }


    protected function productSingle($checkPages, $slug)
    {
        $chart_titles =[];
        $years =[];
        $mahlic_charts=[];

        $category = $checkPages->first();
        $getProduct = Product::join('product_translations as pt', 'products.id', '=', 'pt.product_id')
            ->where('pt.slug', $slug)
            ->whereNull('pt.deleted_at')
            ->with('media')
            ->select('pt.*', 'pt.id as tid', 'products.id','products.template_id')
            ->firstOrFail();

        if($getProduct->template_id != 4){
            $chart_titles = ChartTitle::where('category_id',$getProduct->template_id)
                ->select('id','title_'.$this->lang.' as title','color')
                ->orderBy('order','ASC')
                ->where('type','title')
                ->get();

            $years= $this->statistic($chart_titles,$getProduct)
                ->select('year','category_id','value')
                ->groupBy('year')
                ->orderBy('year','asc')
                ->with([
                    'statistics' => function($query) use($getProduct){
                        $query
                            ->where('template_id',$getProduct->template_id);
                    }
                ])
                ->get();
        }else{
            $mahlic_charts = MahlicChart::get();
        }

        $canonical_url = route('showPage',[$category->slug, $getProduct->slug]);
        $blocks = ProductBlock::where('product_id',$getProduct->id)->select('title_'.$getProduct->lang.' as title','summary_'.$getProduct->lang.' as summary')->get();
        $relatedPages = $this->menu->relatedPages($checkPages->last(), $getProduct);

        $similarProducts = $this->similarProducts($getProduct->id,21,4)->get();
        return view('web.product-single',['product' => $getProduct,'similarProducts' => $similarProducts,'page' => $category,'relatedPages' => $relatedPages,'blocks' =>$blocks,'chart_titles' => $chart_titles,'years' =>$years,'mahlic_charts' =>$mahlic_charts,'canonical_url' => $canonical_url]);
    }
    protected function enterpriseSingle($checkPages, $slug)
    {
        $category = $checkPages->first();
        $getEnterprise = Enterprise::join('enterprise_translations as et', 'enterprises.id', '=', 'et.enterprise_id')
            ->where('et.slug', $slug)
            ->whereNull('et.deleted_at')
            ->with(['lang_media'])
            ->select('et.*', 'et.id as tid', 'enterprises.id','enterprises.template_id','enterprises.location','enterprises.youtube_link')
            ->firstOrFail();

        $relatedPages = $this->menu->relatedPages($checkPages->last(), $getEnterprise);
        $blocks = EnterpriseBlock::where('enterprise_id',$getEnterprise->id)->select('title_'.$getEnterprise->lang.' as title','content_'.$getEnterprise->lang.' as content')->get();
        $canonical_url = route('showPage',[$category->slug, $getEnterprise->slug]);

        return view('web.enterprise-single',['enterprise' => $getEnterprise,'page' => $category,'relatedPages' => $relatedPages,'blocks' =>$blocks,'canonical_url' => $canonical_url]);
    }
    protected function tenderSingle($checkPages, $slug)
    {
        $category = $checkPages->first();

        $tender = Tender::join('tender_translations as tt', 'tenders.id', '=', 'tt.tender_id')
            ->where('tt.slug', $slug)
            ->where('tt.lang', $this->lang)
            ->select('tt.name','tt.slug','tt.lang','tt.content','tt.id as tid', 'tenders.id')
            ->firstOrFail();


        $relatedPages = $this->menu->relatedPages($checkPages->last(), $tender);
        $canonical_url = route('showPage',[$category->slug, $tender->slug]);

        return view('web.tender-single', ['tender' => $tender,'page' => $category,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url]);
    }
    protected function articleSingle($checkPages, $slug)
    {
        $category = $checkPages->first();
        $getArticle = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->where('at.slug', $slug)
            ->whereNull('at.deleted_at')
            ->with('media')
            ->select('at.*', 'at.id as tid', 'articles.*',DB::raw('YEAR(articles.published_at) as year'))
            ->firstOrFail();

        $relatedPages = $this->menu->relatedPages($checkPages->last(), $getArticle);
        $similarArticles = $this->similarArticles($getArticle->id,3,5,$getArticle->year)->get();
        $canonical_url = route('showPage',[$category->slug, $getArticle->slug]);

        return view('web.article-single', ['article' => $getArticle,'similarArticles' => $similarArticles,'page' => $category,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url]);
    }


    protected function static($getPage, $relatedPages,$view,$canonical_url)
    {
        return view("web.".$view, ['page' => $getPage,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url]);
    }


    protected function branch($getPage, $relatedPages,$canonical_url)
    {
        $maps = BranchMap::groupBy('location')->with('data')->orderBy('created_at')->select('location','region_'.$this->lang.' as region')->get();

        return view("web.branch", ['page' => $getPage,'relatedPages' => $relatedPages,'maps' => $maps,'canonical_url' => $canonical_url]);
    }

    protected function kiv($getPage, $relatedPages,$canonical_url)
    {

        if(request()->type && array_key_exists(request()->type,config('config.kiv_types'))){
            $type =request()->type;
        }else{
            $type =key(config('config.kiv_types'));
        }

        $kivs = Kiv::where('kiv_type',config('config.kiv_type_database.'.$type))->select('id','kiv_type','title_'.$this->lang.' as title','youtube_id','image_title')
            ->with('media')
            ->get();
        $canonical_url =$canonical_url.'?type='.$type;
        return view("web.kiv", ['page' => $getPage,'relatedPages' => $relatedPages,'kivs' => $kivs,'type' =>$type,'canonical_url' => $canonical_url]);
    }

    protected function import_export($getPage, $relatedPages,$canonical_url)
    {
        $chart_titles = ChartTitle::where('category_id',$getPage->template_id)
            ->select('id','title_'.$this->lang.' as title','color')
            ->orderBy('order','ASC')
            ->where('type','title')
            ->get();

        $line_x = ChartStatistic::where('template_id',$getPage->template_id)
            ->join('chart_titles','chart_titles.id','chart_statistics.product_id')
            ->whereIn('chart_statistics.category_id',$chart_titles->pluck('id'))
            ->groupBy('chart_statistics.product_id')
            ->orderBy('chart_statistics.product_id','asc')
            ->with(['statistics_line_x' => function($query) use($getPage){
                    $query
                        ->where('template_id',$getPage->template_id)
                        ->where('type','line_x');
                }
            ])
            ->select('chart_statistics.percent','chart_statistics.product_id','chart_titles.title_'.$this->lang.' as title')
            ->get();

        return view("web.import_export", ['page' => $getPage,'relatedPages' => $relatedPages,'chart_titles' =>$chart_titles,'line_x' =>$line_x,'canonical_url' => $canonical_url]);
    }

    protected function products($getPage, $relatedPages,$canonical_url)
    {

        $products = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
            ->where('ptr.page_id', $getPage->tid)
            ->whereIn('pages.template_id',[21])
            ->select('ptr.*','pt.slug as page_slug','products.id','products.color_code','products.text_color_code')
            ->with('media')
            ->get();

        return view("web.products", ['page' => $getPage,'relatedPages' => $relatedPages,'products' =>$products,'canonical_url' => $canonical_url]);
    }

    protected function enterprise($getPage, $relatedPages,$canonical_url)
    {

        $enterprises = Enterprise::join('enterprise_translations as et','et.enterprise_id','enterprises.id')
            ->join('page_translations as pt', 'pt.id', '=', 'et.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->whereNull('et.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('et.lang',$this->lang)
            ->where('et.page_id', $getPage->tid)
            ->whereIn('pages.template_id',[12])
            ->select('et.*','pt.slug as page_slug','enterprises.id','enterprises.linear')
            ->with('media')
            ->orderBy('et.order','asc')
            ->paginate(4);

        return view("web.enterprise", ['page' => $getPage,'relatedPages' => $relatedPages,'enterprises' =>$enterprises,'canonical_url' => $canonical_url]);
    }

    protected function faq($getPage, $relatedPages,$canonical_url)
    {
        $faqs = Faq::select('title_'.$this->lang.' as question','description_'.$this->lang.' as answer')->paginate(8);

        return view("web.faq", ['page' => $getPage,'relatedPages' => $relatedPages,'faqs' => $faqs,'canonical_url' => $canonical_url]);
    }


    protected function news($getPage, $relatedPages,$canonical_url)
    {
        $years = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->where('articles.status', 1)
            ->where('at.page_id', $getPage->tid)
            ->whereNull('pt.deleted_at')
            ->whereNull('at.deleted_at')
            ->select(DB::raw('YEAR(articles.published_at) as year'))->groupBy(DB::raw('YEAR(articles.published_at)'))->orderBy('articles.published_at', 'desc')->pluck('year', 'year');


        if($years->count())
        {
            if(request()->get('year') != "undefined" && request()->get('year') > 0){
                $selectedYear = request()->get('year');
            }
            else{
                $selectedYear = $years->first();
            }
        }
        else{
            $selectedYear = null;
        }

        $articles = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->select('articles.id','at.article_id','at.name','at.image_title', 'at.slug', 'at.summary', 'articles.published_at', 'pt.name as page_name', 'pt.slug as page_slug')
            ->where('articles.status', 1)
            ->where('at.page_id', $getPage->tid)
            ->where('at.lang', $getPage->lang)
            ->whereNull('pt.deleted_at')
            ->whereNull('at.deleted_at')
            ->selectByYear($selectedYear)
            ->orderBy('articles.created_at', 'desc')
            ->orderBy('articles.id', 'desc')
            ->with('media')
            ->paginate(9);


        return view("web.news", ['page' => $getPage,'articles' =>$articles,'relatedPages' => $relatedPages,'years' => $years,'selectedYear' => $selectedYear,'canonical_url' => $canonical_url]);
    }

    protected function techniques($getPage, $relatedPages,$canonical_url)
    {

        $main= Page::join('page_translations as pt','pt.page_id','pages.id')
            ->whereNull('pt.deleted_at')
            ->where('pt.parent_id',$getPage->tid)
            ->where('pt.lang',$this->lang)
            ->whereIn('pages.template_id',[23])
            ->select('pt.name','pt.slug','pages.id','pt.id as tid');

        $categories = $main->get();


        if($categories->count()){
            if(request()->get('category') && request()->get('category') != "undefined"){
                $selectedcat = request()->get('category');
                $selected_page = $main->where('slug',$selectedcat)->first();
                if(!$selected_page){
                    $selectedcat = $categories->first()->slug;
                    $selected_page = $categories->first();
                }
            }else{
                $selectedcat = $categories->first()->slug;
                $selected_page = $categories->first();
            }

        }else{
            $selectedcat = null;
            $selected_page = null;
        }


        $techniques= Page::join('page_translations as pt','pt.page_id','pages.id')
            ->whereNull('pt.deleted_at')
            ->where('pt.lang',$this->lang)
            ->Parent($selected_page)
            ->whereIn('pages.template_id',[24])
            ->select('pt.name','pt.image_title','pt.slug','pages.id','pt.id as tid','pt.parent_id','pt.summary')
            ->with('media')
            ->get();
        $canonical_url =$canonical_url.'?category='.$selectedcat;
        return view("web.techniques", ['page' => $getPage,'relatedPages' => $relatedPages,'categories' => $categories,'techniques'=>$techniques,'selectedcat' =>$selectedcat,'canonical_url' => $canonical_url]);
    }

    protected function certificates($getPage, $relatedPages,$canonical_url)
    {
        $certificates = Page::join('page_translations as pt','pt.page_id','pages.id')
            ->whereNull('pt.deleted_at')
            ->where('pt.lang',$this->lang)
            ->where('pages.template_id',5)
            ->where('pt.parent_id',$getPage->tid)
            ->select('pt.name','pt.image_title','pt.content','pages.id','pages.no')
            ->with('media')
            ->paginate(8);

        return view("web.certificates", ['page' => $getPage,'relatedPages' => $relatedPages,'certificates' => $certificates,'canonical_url' => $canonical_url]);
    }

    protected function vacancies($getPage, $relatedPages,$canonical_url)
    {
        $vacancies = Vacancy::join('vacancy_translations as vt','vt.vacancy_id','vacancies.id')
            ->where('vt.lang',$this->lang)
            ->where('vacancies.end_date', '>=', date('Y-m-d'))
            ->select('vt.*','vacancies.published_by as start_date','vt.id as tid','vacancies.id')
            ->get();

        return view("web.vacancies", ['page' => $getPage,'relatedPages' => $relatedPages,'vacancies' => $vacancies,'canonical_url' => $canonical_url]);
    }


    protected function tenders($getPage, $relatedPages,$canonical_url)
    {
        $tenders = Tender::join('tender_translations as tt','tt.tender_id','tenders.id')
            ->where('tt.lang',$this->lang)
            ->where('tenders.end_date', '>=', date('Y-m-d'))
            ->select('tt.name','tt.slug','tt.lang','tt.id as tid','tenders.published_by as start_date','tenders.end_date','tenders.id')
            ->get();

        return view("web.tenders", ['page' => $getPage,'relatedPages' => $relatedPages,'tenders' => $tenders,'canonical_url' => $canonical_url]);
    }

    protected function getPageByTemplateId($templateId)
    {
        $page = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->where('pt.lang', $this->lang)
            ->where('pages.template_id', $templateId)
            ->select('pt.slug', 'pt.name', 'pt.id')
            ->first();

        return $page;
    }

    private function getChildrenPage($page)
    {
        $children = $page->children;

        if($children->count() && $page->id != $children->first()->id){

            return $children->first();
        }
        else{
            return $page;
        }
    }

    protected function similarArticles($id = 0,$template_id,$limit,$year)
    {
        $articles = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->select('at.name', 'at.slug as article_slug','pt.slug','articles.published_at','articles.id')
            ->where('articles.id', '<>', $id)
            ->where('pages.template_id',$template_id)
            ->where('at.lang', $this->lang)
            ->where('articles.status', 1)
            ->whereNull('pt.deleted_at')
            ->whereNull('at.deleted_at')
            ->selectByYear($year)
            ->orderBy('articles.published_at', 'desc')
            ->orderBy('articles.id', 'desc')
            ->limit($limit);

        return $articles;
    }

    protected function similarProducts($id = 0,$template_id,$limit)
    {
        $products = Product::join('product_translations as ptr', 'products.id', '=', 'ptr.product_id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->where('products.id', '<>', $id)
            ->whereNull('pt.deleted_at')
            ->whereNull('ptr.deleted_at')
            ->where('pt.lang', $this->lang)
            ->where('ptr.lang', $this->lang)
            ->where('pages.template_id',$template_id)
            ->with('media')
            ->select('ptr.*', 'ptr.id as tid', 'products.id','products.template_id','products.color_code','products.text_color_code','pt.slug as page_slug')
            ->inRandomOrder()
            ->limit($limit);

        return $products;
    }

    protected function dropdown($getPage,$relatedPages)
    {
        if ($getPage->MenuChildrenDropDown('tid')->count()) {
            return redirect()->route('showPage', $getPage->MenuChildrenDropDown('tid')->first()->slug);
        } else {
            return redirect()->route('home');
        }
    }

    protected function statistic($chart_titles,$getProduct)
    {
        if($getProduct->template_id == 1){
            $statistic_query = ChartStatistic::where('template_id',$getProduct->template_id)
                ->whereNotNull('year');
        }
        else{
            $statistic_query = ChartStatistic::where('template_id',$getProduct->template_id)
                ->whereIn('category_id',$chart_titles->pluck('id'))
                ->whereNotNull('year');
        }


        return $statistic_query;

    }
}
