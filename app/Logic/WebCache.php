<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

use App\Models\ApartmentElement;
use App\Models\Config;
use App\Models\Dictionary;
use App\Models\Enterprise;
use App\Models\Product;
use App\Models\Slider;
use App\Models\Partner;
use App\Models\Page;
use DB;
use Cache;

class WebCache
{
    public function getDictionary($lang)
    {
        $collect = [];

        $dictionary = Cache::rememberForever("dictionary_$lang", function () use($collect, $lang)
        {
            return Dictionary::where('lang_id', $lang)->select('keyword', 'content')->orderBy('keyword', 'asc')->pluck('content', 'keyword')->toArray();
        });

        return $dictionary;
    }

    public function getSlider($lang)
    {
        $slider = Cache::rememberForever("slider_$lang", function () use($lang){
            return Slider::with('media')->where('lang',$lang)->orderBy('order', 'asc')->limit(10)->get();
        });

        return $slider;
    }

    public function getEnterprise($lang)
    {
        $enterprises = Cache::rememberForever("enterprise_$lang", function () use ($lang){
            return  Enterprise::join('enterprise_translations as et','et.enterprise_id','enterprises.id')
                ->join('page_translations as pt', 'pt.id', '=', 'et.page_id')
                ->join('pages', 'pages.id', '=', 'pt.page_id')
                ->whereNull('et.deleted_at')
                ->whereNull('pt.deleted_at')
                ->where('et.lang',$lang)
                ->whereIn('pages.template_id',[12])
                ->select('et.name','et.slug','et.id as tid','et.page_id','pt.slug as page_slug','enterprises.id')
                ->orderBy('et.order','asc')
                ->get();
        });

        return $enterprises;
    }

    public function getProducts($lang)
    {
        $products = Cache::rememberForever("product_$lang", function () use ($lang){
            return  Product::join('product_translations as ptr','ptr.product_id','products.id')
                ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
                ->join('pages', 'pages.id', '=', 'pt.page_id')
                ->whereNull('ptr.deleted_at')
                ->whereNull('pt.deleted_at')
                ->where('ptr.lang',$lang)
                ->whereIn('pages.template_id',[21])
                ->select('ptr.name','ptr.slug','ptr.page_id','ptr.id as tid','pt.slug as page_slug','products.id')
                ->get();
        });

        return $products;
    }

    public function getPartner()
    {
        $partners = Cache::rememberForever("partners", function () {
            return Partner::with('media')->orderBy('order', 'desc')->limit(40)->get();
        });

        return $partners;
    }

    public static function config()
    {
        $config = Cache::rememberForever('config', function () {
               return Config::all();
        });

        return $config;
    }
}