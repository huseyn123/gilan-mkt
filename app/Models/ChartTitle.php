<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Db;

class ChartTitle extends Model
{

    protected $guarded = ['id'];
    protected $hidden = ['_token'];

    public static  $rules  = [
            'title_az' => "required",
            'title_en' => "required",
            'title_ru' => "required",
            'category_id' => "required",
            'color' => "required",
        ];

    public static  $line_x_rules  = [
        'title_az' => "required",
        'title_en' => "required",
        'title_ru' => "required",
    ];

    public static $messages = [
        'title_az.required' => "Ad az doldurulmayb",
        'title_en.required' => "Ad en doldurulmayb",
        'title_ru.required' => "Ad ru doldurulmayb",
        'page_id.required' => "Kateqoriya seçilməyib",
        'color.required' => "Rəng doldurulmayb",
    ];


    public function statistic()
    {
        return $this->hasMany('App\Models\ChartStatistic', 'category_id', 'id')->whereNotNull('year')->orderBy('year','asc')->select('category_id','value','year');

    }

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

}

