<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Tender extends Model
{
    use SoftDeletes;

    protected $table='tenders';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates =   ['created_at', 'updated_at', 'deleted_at'];



    public static function rules($id,$lang)
    {
        return [
            'name' => 'required',
            'published_by' => 'required',
            'end_date' => 'required',
            'slug' => 'unique:tender_translations,slug,'.$id.',id,lang,'.$lang
        ];
    }

    public static $parameterRules = [
        'published_by' => 'required',
        'end_date' => 'required',
    ];


    public static $messages = [
        'name.required' =>'Ad daxil edilməyib',
        'published_by.required' =>'Yerləşdirilmə tarixi	 seçilməyib',
        'end_date.required' =>'Bitmə tarixi seçilməyib',
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function relatedTenders()
    {
        return $this->hasMany('App\Models\TenderTranslation', 'tender_id');
    }

    public function relatedPages()
    {
        return $this->hasMany('App\Models\TenderTranslation', 'tender_id', 'id');
    }



    public function trans()
    {
        return $this->hasOne(TenderTranslation::class, 'tender_id');
    }
}

