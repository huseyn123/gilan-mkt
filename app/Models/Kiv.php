<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Db;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Kiv  extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $guarded = ['id'];
    protected $hidden = ['_token'];

    public static function rules($id,$type){
        is_null($id) ? $img = 'required' : $img = 'some times';

        if($type == 1){
            return [
                'logo_image' => $img.'|mimes:jpeg,jpg,png|max:10000',
            ];
        }else{
            return [
                'image' => $img.'|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=263,min_height=145',
            ];
        }

    }


    public static $messages = [
        'image.required' => "Kover şəkil seçilməyib",
        'logo_image.required' => "Loqo şəkil seçilməyib",
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit('stretch',263,145)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',263,145)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();
    }


}

