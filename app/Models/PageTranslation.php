<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;


class PageTranslation extends Model implements HasMedia
{

    public static $template_id =0;

    public static $width =270;
    public static $height =600;

    public static $thumb_width =68;
    public static $thumb_height =150;

    use HasMediaTrait,SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden =  ['_token'];

    public static function rules($lang, $id,$template = null){
        self::$width =  config('config.page_size.'.$template.'.width') ?? '10000';
        self::$height =  config('config.page_size.'.$template.'.height') ?? '10000';
        return [
            'name' => 'required|max:255',
            'parent_id' => 'nullable|exists:page_translations,id',
            'slug' => 'unique:page_translations,slug,'.$id.',id,lang,'.$lang,
            'tr_image' => 'max:10000|dimensions:min_width='.self::$width.',min_height='.self::$height,
            'page_file' => 'max:10000',
            'content' => 'nullable',
            'forward_url' => 'nullable',
            'meta_description' => 'nullable|max:160',
        ];
    }

    public static $messages = [
        'name.required' => "Ad qeyd olunmayıb.",
    ];


    public function setMetaKeywordsAttribute($value) {

        $keywords = null;

        if(!is_null($value) && is_array($value))
        {
            $keywords = implode(",", $value);
        }

        $this->attributes['meta_keywords'] = $keywords;
    }


    public function relatedPages()
    {
        return $this->hasMany('App\Models\PageTranslation', 'page_id', 'page_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\PageTranslation', 'parent_id', 'id');
    }


    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit('stretch',self::$thumb_width,self::$thumb_height)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$width,self::$height)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

    }


}
