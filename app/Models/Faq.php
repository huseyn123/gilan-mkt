<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Db;

class Faq extends Model
{

    protected $guarded = ['id'];
    protected $hidden = ['_token'];

    public static function rules(){
        return [
            'title_az' => "required",
            'description_az' => "required",
        ];
    }


    public static $messages = [
        'title_az.required' => "Sual az doldurulmayb",
        'description_az.required' => "Cavab az doldurulmayb",
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

}

