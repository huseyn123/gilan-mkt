<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TenderTranslation extends Model
{

    use SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden =  ['_token'];

    public static function rules($lang, $id)
    {
        return [
            'name' => 'required',
            'slug' => 'unique:tender_translations,slug,'.$id.',id,lang,'.$lang,
        ];
    }

    public static $messages = [
        'name.required' =>'Ad daxil edilməyib',

    ];

}
