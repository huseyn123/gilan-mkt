<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VacancyTranslation extends Model
{

    use SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden =  ['_token'];

    public static function rules($required = 'required|')
    {
        return [
//            'name' => 'nullable',
//            'site_url' => 'nullable',
        ];
    }

    public static $messages = [
    ];

}
