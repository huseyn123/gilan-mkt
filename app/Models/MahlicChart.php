<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Db;

class MahlicChart extends Model
{

    protected $guarded = ['id'];
    protected $hidden = ['_token'];

    public static function  rules()
    {
        return [
            'chart_type' => "required",
            'title' => "required",
            'color' => "required",
            'value' => "required|numeric"
        ];
    }

    public static $messages = [
        'chart_type.required' => "ChartType seçilməyib",
        'title.required' => "Ad doldurulmayıb",
        'color.required' => "Rəng doldurulmayıb",
        'value.required' => "Dəyər doldurulmayıb",
        'value.numeric' => 'Dəyər rəqəm olmalıdır',
    ];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

}

