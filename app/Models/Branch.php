<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Db;

class Branch extends Model
{

    protected $guarded = ['id'];
    protected $hidden = ['_token'];

    public static function rules(){
        return [
            'location_az' => "required",
            'region_id' => "required",
            'network_id' => "required",
        ];
    }


    public static $messages = [
        'location_az.required' => "Ünvan az doldurulmayb",
        'region_id.required' => "Region Seçılməyib",
        'network_id.required' => "Topdançı Seçılməyib",
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

}

