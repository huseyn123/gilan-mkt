<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Auth;
use Db;

class Product extends Model implements HasMedia
{
    use HasMediaTrait, SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    public static $type ='image';
    public static $width =266;
    public static $height =146;

    public static $cover_width =1920;
    public static $cover_height =300;
    public static $thumb_cover_width =1920;
    public static $thumb_cover_height =300;


    public static function rules($id){

        is_null($id) ? $img = 'required' : $img = 'sometimes';
        return [
            'name' => "required",
            'slug' => 'unique:product_translations,slug,'.$id,
            'image' => $img.'|dimensions:min_width='.self::$width.',min_height='.self::$height.'|max:10000',
            'cover' => $img.'|dimensions:min_width='.self::$cover_width.',min_height='.self::$cover_height.'|max:10000',
            'page_id' => 'required|exists:page_translations,id',
        ];
    }

    public static function parameterRules()
    {
        return [
            'image' => 'max:10000|dimensions:min_width='.self::$width.',min_height='.self::$height.'|max:10000',
            'cover' => '|dimensions:min_width='.self::$cover_width.',min_height='.self::$cover_height.'|max:10000',
        ];
    }

    public static $messages = [
        'image.required' => "Şəkil əlavə olunmayıb",
        'page_id.required' => 'Kateqoriya seçilməyib',
        'cover.required' => 'Kover şəkil əlavə olunmayıb',
        'cover.dimensions' => 'Kover şəkilin ölçüsü :min_width x :min_height nisbətdə olmalıdır.',
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function setMetaKeywordsAttribute($value) {

        $keywords = null;

        if(!is_null($value) && is_array($value))
        {
            $keywords = implode(",", $value);
        }

        $this->attributes['meta_keywords'] = $keywords;
    }

    public function relatedPages()
    {
        return $this->hasMany('App\Models\ProductTranslation', 'product_id', 'id');
    }


    public function trans()
    {
        return $this->hasOne(ProductTranslation::class, 'product_id')->orderBy('product_translations.id', 'asc');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit('stretch',183,100)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$width,self::$height)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();


        $this->addMediaConversion('thumb')
            ->fit('stretch',self::$thumb_cover_width,self::$thumb_cover_height)
            ->performOnCollections('cover')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$cover_width,self::$cover_height)
            ->performOnCollections('cover')
            ->keepOriginalImageFormat();

    }

}

