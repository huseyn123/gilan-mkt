<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Db;

class ChartStatistic extends Model
{

    protected $guarded = ['id'];
    protected $hidden = ['_token'];

    public static  $rules  = [
//        'year' => "numeric",
//        'value' => "numeric",
//        'present' => "numeric"
    ];

    public static $messages = [
        'value.numeric' => 'Dəyər rəqəm olmalıdır',
        'year.required' => "İl qeyd olunmayıb",
        'present.numeric' => 'Faiz rəqəm olmalıdır',
    ];

    public function statistics()
    {
        return $this->hasMany('App\Models\ChartStatistic', 'year', 'year')
            ->whereNotNull('year')
            ->select('category_id','value','year');

    }

    public function statistics_line_x()
    {
        return $this->hasMany('App\Models\ChartStatistic', 'product_id', 'product_id')
            ->join('chart_titles','chart_titles.id','chart_statistics.product_id')
            ->select('chart_statistics.product_id','chart_statistics.template_id','chart_statistics.percent','chart_titles.type','chart_statistics.category_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

}

