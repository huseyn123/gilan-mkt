<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Auth;
use Db;

class Enterprise extends Model implements HasMedia
{
    use HasMediaTrait, SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    public static $type ='image';

    public static $width =360;
    public static $height =360;
    public static $gallery_width =540;
    public static $gallery_height =420;

    public static function rules($id){

        is_null($id) ? $img = 'required' : $img = 'sometimes';
        return [
            'name' => "required",
            'slug' => 'unique:enterprise_translations,slug,'.$id,
            'page_id' => 'required|exists:page_translations,id',
            'image' => $img.'|dimensions:min_width='.self::$width.',min_height='.self::$height.'|max:10000',
            'youtube_cover' => '|dimensions:min_width=270,min_height=210|max:10000',
        ];
    }

    public static function parameterRules()
    {
        return [
            'image' => 'max:10000|dimensions:min_width='.self::$width.',min_height='.self::$height.'|max:10000',
            'youtube_cover' => '|dimensions:min_width=270,min_height=210|max:10000',
        ];
    }

    public static $messages = [
        'page_id.required' => 'Kateqoriya seçilməyib',
        'youtube_cover.dimensions' => 'Youtube Koverin ölçüsü :min_width x :min_height nisbətdə olmalıdır.',
        'image.required' => "Kover Şəkil əlavə olunmayıb",
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function lang_media()
    {
        return $this->hasOne('Spatie\MediaLibrary\Models\Media', 'model_id', 'tid')->where('model_type','App\Models\EnterpriseTranslation');
    }


    public function relatedPages()
    {
        return $this->hasMany('App\Models\EnterpriseTranslation', 'enterprise_id', 'id');
    }

    public function setMetaKeywordsAttribute($value) {

        $keywords = null;

        if(!is_null($value) && is_array($value))
        {
            $keywords = implode(",", $value);
        }

        $this->attributes['meta_keywords'] = $keywords;
    }

    public function trans()
    {
        return $this->hasOne(EnterpriseTranslation::class, 'enterprise_id')->orderBy('enterprise_translations.id', 'asc');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('blade')
            ->fit('stretch',self::$width,self::$height)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('thumb')
            ->fit('fill',160,160)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',270,210)
            ->performOnCollections('youtube_cover')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('thumb')
            ->fit('stretch',270,210)
            ->performOnCollections('gallery')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$gallery_width,self::$gallery_height)
            ->performOnCollections('gallery')
            ->keepOriginalImageFormat();
    }

}

