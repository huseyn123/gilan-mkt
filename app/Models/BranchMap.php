<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Db;

class BranchMap extends Model
{

    protected $guarded = ['id'];
    protected $hidden = ['_token'];

    public static  $rules  = [
        'location' => "required",
        'region_az' => "required",
        'region_en' => "required",
        'region_ru' => "required",
        'branch_title_az' => "required",
        'branch_title_en' => "required",
        'branch_title_ru' => "required",
        'farmers_count' => "required",
        'sown_area' => "required",
        'published_at' => "required",
    ];

    public static $messages = [
        'location.required' => "Location seçilməyib",
        'region_az.required' => "Region az doldurulmayb",
        'region_en.required' => "Region en doldurulmayb",
        'region_ru.required' => "Region ru doldurulmayb",
        'branch_title_az.required' => "Filial az doldurulmayb",
        'branch_title_en.required' => "Filial en doldurulmayb",
        'branch_title_ru.required' => "Filial ru doldurulmayb",
        'farmers_count.required' => "Fermer sayı doldurulmayb",
        'sown_area.required' => "Əkin sahəsi doldurulmayb",
        'published_at.required' => "Yaranma tarixi doldurulmayb",
    ];

    public function data()
    {
        return $this->hasMany('App\Models\BranchMap', 'location', 'location')
            ->select('location','region_'.app()->getLocale().' as region','branch_title_'.app()->getLocale().' as branch_title','farmers_count','sown_area','published_at');

    }



    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

}

