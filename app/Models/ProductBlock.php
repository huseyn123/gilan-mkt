<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Db;

class ProductBlock  extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];


    public static $rules = [
        'product_id' =>'required',
        'title_az' =>'required',
        'summary_az' =>'required',
    ];


    public static $messages = [
        'product_id.required' => "Məhsul seçilməyib",
        'title_az.required' => "Ad az doldurulmayıb",
        'summary_az.required' => "Dəyər az doldurulmayıb",
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

}

