<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Db;

class EnterpriseBlock  extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];


    public static $rules = [
        'enterprise_id' =>'required',
        'title_az' =>'required',
        'content_az' =>'required',
    ];


    public static $messages = [
        'enterprise_id.required' => "Müəssisə seçilməyib",
        'title_az.required' => "Ad az doldurulmayıb",
        'content_az.required' => "Dəyər az doldurulmayıb",
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

}

