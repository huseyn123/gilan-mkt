<?php

namespace App\Http\Middleware;

use Closure;

class xss
{
    public function handle($request, Closure $next)
    {
        if (!in_array(strtolower($request->method()), ['put', 'post', 'patch'])) {
            return $next($request);
        }

        $input = $request->except('content', 'summary', 'content_az', 'content_en', 'content_ru','work_information','requirements','technical_indicators','table');

        array_walk_recursive($input, function(&$input) {
            $input = strip_tags($input);
        });

        $request->merge($input);

        return $next($request);
    }
}
