<?php

namespace App\Http\Controllers;
use App\Logic\Slug;
use App\Models\Tender;
use App\Models\TenderTranslation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;

class TenderTranslationController extends Controller
{
    private $requests;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax');
        $this->requests = $request->except('_token', '_method');

    }


    public function store(Request $request)
    {
        $response = $this->validation($request->lang,null);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'tenderTranslation', $request->lang );


        DB::beginTransaction();

        $tender = TenderTranslation::create($this->requests);

        DB::commit();

        return $this->responseJson($response);
    }


    public function update(Request $request, $id)
    {
        $tender = TenderTranslation::findOrFail($id);

        $response = $this->validation($tender->lang,$id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'tenderTranslation', $request->lang );

        DB::beginTransaction();


        foreach($this->requests as $key => $put){
            $tender->$key = $put;
        }

        $tender->save();


        DB::commit();

        return $this->responseJson($response);
    }


    public function trash($id)
    {
        $tender = TenderTranslation::findOrFail($id);
        $tender->delete();

        $response = $this->responseDataTable(0,"", "#tenders", "#modal-confirm");
        return $this->responseJson($response);
    }


    public function restore($id)
    {
        $tender = TenderTranslation::onlyTrashed()->findOrFail($id);
        $tender->restore();

        $response = $this->responseDataTable(0,"", "#tenders", "#modal-confirm");
        return $this->responseJson($response);
    }


    public function destroy($id)
    {
        $tender = TenderTranslation::findOrFail($id);

        $relatedTenders= TenderTranslation::where('tender_id', $tender->tender_id)->withTrashed()->count();

        if($relatedTenders == 1){
            Tender::where('id', $tender->tender_id)->forceDelete();
        }
        else{
            $tender->forceDelete();
        }

        $response = $this->responseDataTable(0,"", "#tenders", "#modal-confirm");
        return $this->responseJson($response);
    }


    private function validation($lang,$id = null)
    {
        $validation = Validator::make($this->requests, TenderTranslation::rules($lang,$id), TenderTranslation::$messages);

        $response = $this->responseDataTable(0,"", "#tenders", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }


    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }
}
