<?php

namespace App\Http\Controllers;
use App\Crud\ProductCrud;
use App\Crud\ProductParameterCrud;
use App\DataTables\ProductDataTable;
use App\Logic\Slug;
use App\Models\Page;
use App\Models\PageTranslation;
use App\Models\Product;
use App\Models\ProductTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    private $crud, $requests, $title,$route,$create_name;

    const TRANSLATES = ['name','seo_title','page_id','product_id','lang', 'slug', 'summary', 'content','technical_indicators', 'meta_description', 'meta_keywords','image_title'];

    public function __construct(ProductCrud $crud, Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->requests = $request->except('_token', '_method', 'image','cover');


        $this->route = 'products';
        $this->title = "Məhsullar";
        $this->create_name = "Məhsul";
        $this->crud = new ProductCrud();
        $this->paramCrud = new ProductParameterCrud();

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('product');
        }
    }


    public function index(ProductDataTable $dataTable)
    {
        return $dataTable->render('admin.products', ['title' => $this->title, 'route' => $this->route]);
    }

    public function create()
    {

        $fields = $this->crud->fields(null, 'create');

        return view('admin.page.create', ['route' => "$this->route.store", 'fields' => $fields, 'title' => 'Yeni','editor2' =>true]);
    }

    public function store(Request $request)
    {
        if (count(config('app.locales')) == 1) {
            $lang = app()->getLocale();
        } else {
            $lang = $request->lang;
        }

        $translates = self::TRANSLATES;


        // check Validation
        Product::$cover_width = config('config.product_cower_size.'.$request->template_id.'.width');
        Product::$cover_height = config('config.product_cower_size.'.$request->template_id.'.height');
        Product::$thumb_cover_width = config('config.product_thumb_cower_size.'.$request->template_id.'.width');
        Product::$thumb_cover_height = config('config.product_thumb_cower_size.'.$request->template_id.'.height');


        $validation = Validator::make($request->all(), Product::rules(null), Product::$messages);
        $response = $this->validation($validation, $lang);

        if ($response['code'] == 1) {
            return $this->responseJson($response);
        }

        $parent = PageTranslation::findOrFail($request->page_id);
        $lang = $parent->lang;


        // apply
        $this->requests['lang'] = $lang;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'productTranslation', $lang);

        //inputs for models
        $productInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $productInputs);

        //store
        DB::beginTransaction();


        try {
            $product = Product::create($productInputs);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try {
            $translationInputs['product_id'] = $product->id;
            $product_translation = ProductTranslation::create($translationInputs);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }


        if ($request->has('image')){
            try {
                $product
                    ->addMedia($request->image)
                    ->toMediaCollection();
            } catch (\Exception $e) {
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        if ($request->has('cover')){
            try {
                $product
                    ->addMedia($request->cover)
                    ->toMediaCollection('cover');
            } catch (\Exception $e) {
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        DB::commit();

        return $this->responseJson($response);
    }

    public function edit($id)
    {
        $parameterCrud = $this->paramCrud;//

        $fields = [];
        $langs = [];
        $keys = [];

        $product = Product::join('product_translations as prt', 'prt.product_id', '=', 'products.id')
            ->where('prt.id', $id)
            ->whereNull('prt.deleted_at')
            ->select(
                'prt.*',
                'prt.id as tid',
                'products.id',
                'products.template_id',
                'products.featured',
                'products.color_code',
                'products.text_color_code'
            )
            ->firstOrFail();

        if(count(config('app.locales')) == 1){
            return $this->singleEdit($product);
        }

        $relatedPage = $product->relatedPages;
        $parameters = $parameterCrud->fields('edit', $product);

        foreach ($relatedPage as $rel){
            $fields[$rel->lang] = $this->crud->fields($rel->lang, 'edit', $rel);
            $langs[$rel->lang] = $rel->lang;
            $keys[$rel->lang] = array_filter(explode(",", $rel->meta_keywords));
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields( $key, 'edit', null);
            $keys[$key] = [];
        }

        return view('admin.page.edit-withlocale', ['info' => $product, 'langs' => $langs, 'parameters' => $parameters,'fields' => $fields, 'relatedPage' => $relatedPage,'model' => 'products', 'route' => 'productTranslation','editor2' =>true, 'keys' => $keys, ]);
    }

    private function singleEdit($page)
    {
        $fields = $this->crud->fields(null,'edit', $page);

        return view('admin.page.edit', ['info' => $page, 'fields' => $fields, 'model' => 'product']);
    }

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        // check Validation
        Product::$cover_width = config('config.product_cower_size.'.$request->template_id.'.width');
        Product::$cover_height = config('config.product_cower_size.'.$request->template_id.'.height');
        Product::$thumb_cover_width = config('config.product_thumb_cower_size.'.$request->template_id.'.width');
        Product::$thumb_cover_height = config('config.product_thumb_cower_size.'.$request->template_id.'.height');

        $validation = Validator::make($request->all(), Product::parameterRules(), Product::$messages);
        $response = $this->validation($validation, $id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['featured'] = $request->featured;


        foreach($this->requests as $key => $put){
            $product->$key = $put;
        }

        $product->save();



        if($request->hasFile('image')){
            try{

                $product->clearMediaCollection();  //delete old file
                $product->addMedia($request->image)
                    ->toMediaCollection();
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        if($request->hasFile('cover')){
            try{

                $product->clearMediaCollection('cover');  //delete old file
                $product->addMedia($request->cover)
                    ->toMediaCollection('cover');
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }
        return $this->responseJson($response);
    }

    public function updateSingle(Request $request, $id)
    {
        $productTranslation = PageTranslation::findOrFail($id);
        $product = Product::findOrFail($productTranslation->product_id);

        $translates = self::TRANSLATES;

        // check Validation
        $validation = Validator::make($this->requests, Product::rules($id), Product::$messages);
        $response = $this->validation($validation, $productTranslation->lang);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        // apply
        $this->requests['lang'] = $productTranslation->lang;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'productTranslation', $productTranslation->lang );

        //inputs for models
        $productInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $productInputs);

        //store
        DB::beginTransaction();


        try{
            foreach($productInputs as $key => $put){
                $product->$key = $put;
            }
            $product->save();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try{
            foreach($translationInputs as $key => $put){
                $productTranslation->$key = $put;
            }
            $productTranslation->save();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }


        DB::commit();

        return $this->responseJson($response);
    }

    private function validation($validation, $lang, $id = null)
    {
        $response = $this->responseDataTable(0,"", "#products", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }

    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }


}
