<?php

namespace App\Http\Controllers;

use App\Crud\ChartTitleCrud;
use App\DataTables\ChartTitleDataTable;
use App\Logic\Simple;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\ChartTitle;
use DB;
use App\Logic\Order;


class ChartTitleController extends Controller
{
    use Simple;

    private  $order,$crud,$requests,$title,$route,$createName,$model,$view,$create_title,$lang_tab;

    public function __construct(Request $request, Order $order)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index', 'order']);

        $this->requests = $request->except('_token', '_method');

        $this->order = $order;
        $this->view = 'chartTitle';
        $this->create_title = 'Yeni';
        $this->route = $request->segment(2);
        $this->title = "ChartTitle";
        $this->createName = "Yeni ChartTitle";
        $this->model = 'App\Models\ChartTitle';
        $this->crud = new ChartTitleCrud();


    }


    public function index(ChartTitleDataTable $dataTable)
    {
        return $dataTable->route($this->route)->render('admin.main', ['title' => $this->title,'route' =>$this->route]);
    }

    public function store(Request $request)
    {
        $response = $this->validation();

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['order'] = ChartTitle::max('order') + 1;

        $data = $this->model::create($this->requests);

        return $this->responseJson($response);
    }


    public function order()
    {
        return $this->order->get('chartTitle', "sex", 'title_az', false, 5,true);
    }

    public function postOrder(Request $request)
    {
        return $this->order->post($request, 'chartTitle', false);

    }


    private function validation($id = null)
    {
        $validation = Validator::make(request()->all(), $this->model::$rules, $this->model::$messages);

        $response = $this->responseDataTable(0,"", "#".$this->route, '#myModal');

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
        }

        return $response;
    }



}
