<?php

namespace App\Http\Controllers;

use App\Crud\ChartTitleCrud;
use App\Crud\ChartTitleLineXCrud;
use App\DataTables\ChartTitleDataTable;
use App\DataTables\ChartTitleXDataTable;
use App\Logic\Simple;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\ChartTitle;
use DB;

class ChartTitleLineXController extends Controller
{
    use Simple;

    private $crud,$requests,$title,$route,$createName,$model,$view,$create_title,$lang_tab;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);
        $this->requests = $request->except('_token', '_method');


        $this->view = 'chartTitle';
        $this->create_title = 'Yeni';
        $this->route = $request->segment(2);
        $this->title = "ChartTitleX";
        $this->createName = "Yeni";
        $this->model = 'App\Models\ChartTitle';
        $this->crud = new ChartTitleLineXCrud();


    }


    public function index(ChartTitleXDataTable $dataTable)
    {
        return $dataTable->route($this->route)->render('admin.main', ['title' => $this->title,'route' =>$this->route]);
    }

    private function validation($id = null)
    {
        $validation = Validator::make(request()->all(), $this->model::$line_x_rules, $this->model::$messages);

        $response = $this->responseDataTable(0,"", "#".$this->route, '#myModal');

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
        }

        return $response;
    }



}
