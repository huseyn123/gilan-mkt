<?php

namespace App\Http\Controllers;
use App\Models\EnterpriseTranslation;
use Illuminate\Support\Facades\Validator;
use App\Models\Enterprise;
use Illuminate\Http\Request;
use App\Logic\Slug;
use App\Logic\Order;
use DB;

class EnterpriseTranslationController extends Controller
{
    private $order, $requests;

    public function __construct(Request $request, Order $order)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['order']);

        $this->requests = $request->except('_token', '_method','catalog');
        $this->order = $order;

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete','postOrder'])) {
            clearCache('enterprise');
        }
    }


    public function store(Request $request)
    {
        $response = $this->validation($request->lang);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'enterpriseTranslation', $request->lang );
        $this->requests['order'] = EnterpriseTranslation::max('order') + 1;


       $enterprise = EnterpriseTranslation::create($this->requests);

        if ($request->has('catalog')){
            try {
                $enterprise
                    ->addMedia($request->catalog)
                    ->toMediaCollection('catalog');
            } catch (\Exception $e) {
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        return $this->responseJson($response);
    }


    public function update(Request $request, $id)
    {
        $enterprise = EnterpriseTranslation::findOrFail($id);

        $response = $this->validation($enterprise->lang, $id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'enterpriseTranslation', $request->lang );


        foreach($this->requests as $key => $put){
            $enterprise->$key = $put;
        }
        $enterprise->save();

        if($request->hasFile('catalog')){
            try{

                $enterprise->clearMediaCollection('catalog');  //delete old file
                $enterprise->addMedia($request->catalog)
                    ->toMediaCollection('catalog');
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }


        return $this->responseJson($response);
    }


    public function trash($id)
    {
        $enterprise = EnterpriseTranslation::findOrFail($id);
        $enterprise->delete();

        $response = $this->responseDataTable(0,"", "#enterprises", "#modal-confirm");
        return $this->responseJson($response);
    }

    public function restore($id)
    {
        $enterprise = EnterpriseTranslation::onlyTrashed()->findOrFail($id);
        $enterprise->restore();

        $response = $this->responseDataTable(0,"", "#enterprises", "#modal-confirm");
        return $this->responseJson($response);
    }


    public function destroy($id)
    {
        $enterprise =  EnterpriseTranslation::onlyTrashed()->findOrFail($id);

        $relatedPages = EnterpriseTranslation::where('enterprise_id', $enterprise->enterprise_id)->withTrashed()->count();

        if($relatedPages == 1){
            Enterprise::where('id', $enterprise->enterprise_id)->forceDelete();
        }
        else{
            $enterprise->forceDelete();
        }

        $response = $this->responseDataTable(0,"", "#enterprises", "#modal-confirm");
        return $this->responseJson($response);
    }

    public function order()
    {
        $lang = request()->get('lang', 'az');

        return $this->order->get('enterpriseTranslation', "sex", 'name', $lang, 5,true);
    }


    public function postOrder(Request $request)
    {
        return $this->order->post($request, 'enterpriseTranslation', false);

    }


    private function validation($lang, $id = null)
    {
        $validation = Validator::make(request()->all(), EnterpriseTranslation::rules($id), EnterpriseTranslation::$messages);

        $response = $this->responseDataTable(0,"", "#enterprises", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }


    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }
}
