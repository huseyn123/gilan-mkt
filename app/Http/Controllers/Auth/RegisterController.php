<?php

namespace App\Http\Controllers\Auth;

use App\Models\Config;
use App\Logic\Share;
use App\Rules\Cellphone;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewUser;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
        /*
        |--------------------------------------------------------------------------
        | Register Controller
        |--------------------------------------------------------------------------
        |
        | This controller handles the registration of new users as well as their
        | validation and creation. By default this controller uses a trait to
        | provide this functionality without requiring any additional code.
        |
        */
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest'); //QEYD: middlewareni auth:api etmekde meqsedim qeydiyyati disable etmekdir
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required','string','max:40'],
            'email' => ['required','email','unique:users'],
            'phone' => ['required', 'numeric'],
            'sex' => ['required','boolean'],
            'password' => ['required','string','min:6','confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //$contactEmail = Config::where('key', 'contact_email')->firstOrFail();

        $newUser =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' =>$data['phone'],
            'sex' => $data['sex'],
            'password' => Hash::make($data['password']),
        ]);

        //Notification::route('mail', $contactEmail->value)->notify(new NewUser($newUser));

        return $newUser;
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $request->session()->flash('auth_error', true);
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }


    public function showRegistrationForm()  //this will be deleted
    {
        return view('web.404');
    }
}
