<?php

namespace App\Http\Controllers;

use App\Crud\BranchElementCrud;
use App\DataTables\BranchElementDataTable;
use App\Logic\Simple;
use Illuminate\Http\Request;
use App\Models\BranchElement;
use Illuminate\Support\Facades\Validator;
use DB;

class BranchRegionController extends Controller
{
    use Simple;

    private $crud,$requests,$title,$route,$createName,$model,$view,$create_title,$lang_tab;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);
        $this->requests = $request->except('_token', '_method');

        $this->view = 'branch_element';
        $this->create_title = 'Yeni Region';
        $this->route = 'region';
        $this->title = "Region";
        $this->createName = "Yeni Region";
        $this->model = 'App\Models\BranchElement';
        $this->crud = new BranchElementCrud($this->route);


    }


    public function index(BranchElementDataTable $dataTable)
    {
        return $dataTable->route($this->route)->render('admin.main', ['title' => $this->title,'route' =>$this->route]);
    }



}
