<?php

namespace App\Http\Controllers;
use App\Crud\TenderCrud;
use App\Crud\TenderParameterCrud;
use App\DataTables\TenderDataTable;
use App\Logic\Slug;
use App\Models\Tender;
use App\Models\TenderTranslation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;

class TenderController extends Controller
{
    private $crud, $requests, $title;

    public function __construct(TenderCrud $crud,Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->requests = $request->except('_token', '_method');
        $this->crud = $crud;
        $this->title = "Tenderlər";
    }


    public function index(TenderDataTable $dataTable)
    {
        return $dataTable->render('admin.tenders', ['title' => $this->title]);
    }

    public function create()
    {
        $fields = $this->crud->fields(null, 'create');

        return view('admin.dt.create', ['title' => 'Yeni Tender', 'fields' => $fields, 'route' => 'tenders.store','editor'=>true,'translation' => true]);
    }


    public function store(Request $request)
    {
        if(count(config('app.locales')) == 1){
            $lang = app()->getLocale();
        }
        else{
            $lang = $request->lang;
        }

        $translates = ['tender_id','name','slug','content','lang'];

        // check Validation
        $validation = Validator::make($this->requests, Tender::rules(null, $lang), Tender::$messages);
        $response = $this->validation($validation, $lang);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }


        $this->requests['lang'] = $lang;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'tenderTranslation', $lang);


        //inputs for models
        $tenderInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $tenderInputs);

        //store
        DB::beginTransaction();


        try{
            $tender = Tender::create($tenderInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try{
            $translationInputs['tender_id'] = $tender->id;
            TenderTranslation::create($translationInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        DB::commit();

        return $this->responseJson($response);
    }


    public function edit($id)
    {
        $parameterCrud = new TenderParameterCrud();
        $fields = [];
        $langs = [];


        $tender = Tender::join('tender_translations as tt', 'tt.tender_id', '=', 'tenders.id')
            ->where('tt.id', $id)
            ->select(
                'tt.*',
                'tt.id as tid',
                'tenders.id',
                'tenders.published_by',
                'tenders.end_date'

            )
            ->firstOrFail();


        if(count(config('app.locales')) == 1){
            return $this->singleEdit($tender);
        }

        $relatedTender = $tender->relatedTenders;
        $parameters = $parameterCrud->fields('edit', $tender);

        foreach ($relatedTender as $rel){
            $fields[$rel->lang] = $this->crud->fields($rel->lang, 'edit', $rel);
            $langs[$rel->lang] = $rel->lang;
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields( $key, 'edit', null);
        }

        return view('admin.page.edit-withlocale', ['info' => $tender, 'langs' => $langs, 'parameters' => $parameters, 'pageId' => $tender->tender_id, 'fields' => $fields, 'relatedPage' => $relatedTender,'model' => 'tenders', 'route' => 'tenderTranslation']);
    }


    private function singleEdit($page)
    {
        $fields = $this->crud->fields(null,'edit', $page);


        return view('admin.page.edit', ['info' => $page, 'fields' => $fields, 'model' => 'tenders']);
    }


    public function update(Request $request, $id)
    {
        $tender = Tender::findOrFail($id);

        $validation = Validator::make($this->requests, Tender::$parameterRules, Tender::$messages);
        $response = $this->validation($validation, $id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        foreach($this->requests as $key => $put){
            $tender->$key = $put;
        }

        $tender->save();

        return $this->responseJson($response);
    }


    public function updateSingle(Request $request, $id)
    {
        $tenderTranslation = TenderTranslation::findOrFail($id);

        $translates = ['name', 'parent_id', 'lang', 'slug', 'order', 'content', 'forward_url', 'meta_description', 'meta_keywords'];

        // check Validation
        $validation = Validator::make($this->requests, Tender::rules($tenderTranslation->lang, $id), Tender::$messages);
        $response = $this->validation($validation, $tenderTranslation->lang);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }


        //inputs for models
        $tenderInput = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $tenderInput);

        //store
        DB::beginTransaction();


        try{
            Tender::where('id', $tenderTranslation->tender_id)->update($tenderInput);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try{
            foreach($translationInputs as $key => $put){
                $tenderInput->$key = $put;
            }
            $tenderTranslation->save();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        DB::commit();

        return $this->responseJson($response);
    }


    private function validation($validation, $lang, $id = null)
    {
        $response = $this->responseDataTable(0,"", "#tenders", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }

    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }
}
