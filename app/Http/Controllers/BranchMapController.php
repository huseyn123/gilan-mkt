<?php

namespace App\Http\Controllers;

use App\Crud\BranchMapCrud;
use App\DataTables\BranchMapDataTable;
use App\Logic\Simple;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\BranchMap;
use DB;

class BranchMapController extends Controller
{
    use Simple;

    private $crud,$requests,$title,$route,$createName,$model,$view,$create_title,$lang_tab;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);
        $this->requests = $request->except('_token', '_method');


        $this->view = 'branchMap';
        $this->create_title = 'Yeni';
        $this->route = $request->segment(2);
        $this->title = "Filiallar";
        $this->createName = "Yeni Filiallar";
        $this->model = 'App\Models\BranchMap';
        $this->crud = new BranchMapCrud();


    }


    public function index(BranchMapDataTable $dataTable)
    {
        return $dataTable->route($this->route)->render('admin.main', ['title' => $this->title,'route' =>$this->route]);
    }

    private function validation($id = null)
    {
        $validation = Validator::make(request()->all(), $this->model::$rules, $this->model::$messages);

        $response = $this->responseDataTable(0,"", "#".$this->route, '#myModal');

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
        }

        return $response;
    }



}
