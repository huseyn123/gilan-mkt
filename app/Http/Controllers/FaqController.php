<?php

namespace App\Http\Controllers;

use App\DataTables\FaqDataTable;
use App\Logic\NoCrud;
use Illuminate\Http\Request;
use App\Models\Faq;
use Illuminate\Support\Facades\Validator;
use DB;

class FaqController extends Controller
{
    use NoCrud;

    private $crud,$requests,$title,$route;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->title = "Faq";
        $this->route = "faq";
        $this->view = "faq";
        $this->model = 'App\Models\Faq';
        $this->requests = $request->except('_token', '_method');

    }


    public function index(FaqDataTable $dataTable)
    {
        return $dataTable->render('admin.main', ['title' => $this->title,'route' =>$this->route]);
    }



}
