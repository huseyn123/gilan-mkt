<?php

namespace App\Http\Controllers;
use App\Crud\EnterpriseCrud;
use App\Crud\EnterpriseParameterCrud;
use App\DataTables\EnterpriseDataTable;
use App\Logic\Slug;
use App\Logic\Order;
use App\Models\Page;
use App\Models\PageTranslation;
use App\Models\Enterprise;
use App\Models\EnterpriseTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class EnterpriseController extends Controller
{
    private $crud, $requests, $title,$route, $order,$create_name;

    const TRANSLATES = ['name','seo_title','page_id','enterprise_id','lang', 'order', 'slug', 'summary', 'content','table', 'meta_description', 'meta_keywords','image_title'];

    public function __construct(EnterpriseCrud $crud, Request $request, Order $order)
    {
        $this->order = $order;

        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->requests = $request->except('_token', '_method', 'image','youtube_cover','catalog', 'order');

        $this->route = 'enterprises';
        $this->title = "Müəssisələr";
        $this->create_name = "Müəssisə";
        $this->crud = new EnterpriseCrud();
        $this->paramCrud = new EnterpriseParameterCrud();

          if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
              clearCache('enterprise');
          }
    }


    public function index(EnterpriseDataTable $dataTable)
    {
        return $dataTable->render('admin.enterprises', ['title' => $this->title, 'route' => $this->route]);
    }

    public function create()
    {

        $fields = $this->crud->fields(null, 'create');

        return view('admin.page.create', ['route' => "$this->route.store", 'fields' => $fields, 'title' => 'Yeni','editor2' =>true]);
    }

    public function store(Request $request)
    {
        if (count(config('app.locales')) == 1) {
            $lang = app()->getLocale();
        } else {
            $lang = $request->lang;
        }

        $translates = self::TRANSLATES;


        // check Validation
        if($request->image){
            $imagedetails = getimagesize($request->image);
            $width = $imagedetails[0];
            $height = $imagedetails[1];
            if ($width > $height){
                $this->requests['linear'] = 1;
                Enterprise::$width = 750;
                Enterprise::$height = 360;
            }

        }

        $validation = Validator::make($request->all(), Enterprise::rules(null), Enterprise::$messages);
        $response = $this->validation($validation, $lang);



        if ($response['code'] == 1) {
            return $this->responseJson($response);
        }

        $parent = PageTranslation::findOrFail($request->page_id);
        $lang = $parent->lang;


        // apply
        $this->requests['lang'] = $lang;
        $this->requests['order'] = EnterpriseTranslation::max('order') + 1;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'enterpriseTranslation', $lang);

        //inputs for models
        $enterpriseInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $enterpriseInputs);

        //store
        DB::beginTransaction();


        try {
            $enterprise = Enterprise::create($enterpriseInputs);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try {
            $translationInputs['enterprise_id'] = $enterprise->id;
            $enterprise_translation = EnterpriseTranslation::create($translationInputs);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }


        if ($request->has('image')){
            try {
                $enterprise
                    ->addMedia($request->image)
                    ->toMediaCollection();
            } catch (\Exception $e) {
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        if ($request->has('youtube_cover')){
            try {
                $enterprise
                    ->addMedia($request->youtube_cover)
                    ->toMediaCollection('youtube_cover');
            } catch (\Exception $e) {
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }


        if ($request->has('catalog')){
            try {
                $enterprise_translation
                    ->addMedia($request->catalog)
                    ->toMediaCollection('catalog');
            } catch (\Exception $e) {
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        DB::commit();

        return $this->responseJson($response);
    }

    public function edit($id)
    {
        $parameterCrud = $this->paramCrud;//

        $fields = [];
        $langs = [];
        $keys = [];

        $enterprise = Enterprise::join('enterprise_translations as et', 'et.enterprise_id', '=', 'enterprises.id')
            ->where('et.id', $id)
            ->whereNull('et.deleted_at')
            ->select(
                'et.*',
                'et.id as tid',
                'enterprises.id',
                'enterprises.template_id',
                'enterprises.featured',
                'enterprises.location',
                'enterprises.location',
                'enterprises.youtube_link'
            )
            ->firstOrFail();

        if(count(config('app.locales')) == 1){
            return $this->singleEdit($enterprise);
        }

        $relatedPage = $enterprise->relatedPages;
        $parameters = $parameterCrud->fields('edit', $enterprise);

        foreach ($relatedPage as $rel){
            $fields[$rel->lang] = $this->crud->fields($rel->lang, 'edit', $rel);
            $langs[$rel->lang] = $rel->lang;
            $keys[$rel->lang] = array_filter(explode(",", $rel->meta_keywords));
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields( $key, 'edit', null);
            $keys[$key] = [];
        }

        return view('admin.page.edit-withlocale', ['info' => $enterprise, 'langs' => $langs, 'parameters' => $parameters,'fields' => $fields, 'relatedPage' => $relatedPage,'model' => 'enterprises', 'route' => 'enterpriseTranslation','editor2' =>true,'keys' => $keys]);
    }

    private function singleEdit($page)
    {
        $fields = $this->crud->fields(null,'edit', $page);

        return view('admin.page.edit', ['info' => $page, 'fields' => $fields, 'model' => 'enterprises']);
    }

    public function update(Request $request, $id)
    {
        $enterprise = Enterprise::findOrFail($id);

        // check Validation

        if($request->image){
            $imagedetails = getimagesize($request->image);

            $width = $imagedetails[0];
            $height = $imagedetails[1];

            if ($width > $height){
                $this->requests['linear'] = 1;
                Enterprise::$width = 750;
                Enterprise::$height = 360;
            }else{
                $this->requests['linear'] = 0;
            }
        }

        $validation = Validator::make($request->all(), Enterprise::parameterRules(), Enterprise::$messages);
        $response = $this->validation($validation, $id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['featured'] = $request->featured;


        foreach($this->requests as $key => $put){
            $enterprise->$key = $put;
        }

        $enterprise->save();



        if($request->hasFile('image')){
            try{

                $enterprise->clearMediaCollection();  //delete old file
                $enterprise->addMedia($request->image)
                    ->toMediaCollection();
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        if($request->hasFile('youtube_cover')){
            try{

                $enterprise->clearMediaCollection('youtube_cover');  //delete old file
                $enterprise->addMedia($request->youtube_cover)
                    ->toMediaCollection('youtube_cover');
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }
        return $this->responseJson($response);
    }

    public function updateSingle(Request $request, $id)
    {
        $enterpriseTranslation = EnterpriseTranslation::findOrFail($id);
        $enterprise = Enterprise::findOrFail($enterpriseTranslation->enterprise_id);

        $translates = self::TRANSLATES;

        // check Validation
        $validation = Validator::make($this->requests, Enterprise::rules($id), Enterprise::$messages);
        $response = $this->validation($validation, $enterpriseTranslation->lang);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        // apply
        $this->requests['lang'] = $enterpriseTranslation->lang;
        $this->requests['order'] = EnterpriseTranslation::max('order') + 1;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'enterpriseTranslation', $enterpriseTranslation->lang );

        //inputs for models
        $enterpriseInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $enterpriseInputs);

        //store
        DB::beginTransaction();


        try{
            foreach($enterpriseInputs as $key => $put){
                $enterprise->$key = $put;
            }
            $enterprise->save();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try{
            foreach($translationInputs as $key => $put){
                $enterpriseTranslation->$key = $put;
            }
            $enterpriseTranslation->save();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }


        DB::commit();

        return $this->responseJson($response);
    }

    private function validation($validation, $lang, $id = null)
    {
        $response = $this->responseDataTable(0,"", "#enterprises", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }

    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }


}
