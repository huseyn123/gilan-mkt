<?php

namespace App\Http\Controllers;

use App\Crud\MahlicChartCrud;
use App\DataTables\MahlicChartDataTable;
use App\Logic\Simple;
use Illuminate\Http\Request;
use App\Models\MahlicChart;
use Illuminate\Support\Facades\Validator;
use DB;

class MahlicChartController extends Controller
{
    use Simple;

    private $crud,$requests,$title,$route,$createName,$model,$view,$create_title,$lang_tab;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);
        $this->requests = $request->except('_token', '_method');


        $this->view = 'mahlic_chart';
        $this->create_title = 'Yeni';
        $this->route = 'mahlic_chart';
        $this->title = "Mahlic Chart";
        $this->createName = "Yeni Mahlic Chart";
        $this->model = 'App\Models\MahlicChart';
        $this->crud = new MahlicChartCrud();


    }


    public function index(MahlicChartDataTable $dataTable)
    {
        return $dataTable->route($this->route)->render('admin.main', ['title' => $this->title,'route' =>$this->route]);
    }




}
