<?php

namespace App\Http\Controllers;

use App\Crud\ProductBlockCrud;
use App\DataTables\ProductBlockDataTable;
use App\Logic\Simple;
use Illuminate\Http\Request;
use App\Models\ProductBlock;
use Illuminate\Support\Facades\Validator;
use DB;

class ProductBlockController extends Controller
{
    use Simple;

    private $crud,$requests,$title,$route,$createName,$model,$view,$create_title,$lang_tab;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);
        $this->requests = $request->except('_token', '_method');


        $this->view = 'productBlocks';
        $this->create_title = 'Yeni Blok';
        $this->route = 'productBlocks';
        $this->title = "Blok";
        $this->createName = "Yeni Blok";
        $this->model = 'App\Models\ProductBlock';
        $this->crud = new ProductBlockCrud();


    }


    public function index(ProductBlockDataTable $dataTable)
    {
        return $dataTable->route($this->route)->render('admin.main', ['title' => $this->title,'route' =>$this->route]);
    }

    private function validation($id = null)
    {
        $validation = Validator::make(request()->all(), $this->model::$rules, $this->model::$messages);

        $response = $this->responseDataTable(0,"", "#".$this->route, '#myModal');

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
        }

        return $response;
    }



}
