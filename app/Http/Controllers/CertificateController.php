<?php

namespace App\Http\Controllers;
use App\Crud\CertificateCrud;
use App\Crud\CertificateParameterCrud;
use App\Crud\PageParameterCrud;
use App\DataTables\CertificateDataTable;
use App\Logic\AdminPages;
use App\Logic\ImageRepo;
use App\Logic\Order;
use App\Models\PageTranslation;
use Illuminate\Support\Facades\Validator;
use App\Crud\PageCrud;
use App\Models\Page;
use Illuminate\Http\Request;
use App\DataTables\PageDataTable;
use App\Logic\Slug;
use DB;

class CertificateController extends Controller
{

    use AdminPages;

    use AdminPages {
        AdminPages::__construct as private __TrConstruct;
    }

    const TRANSLATES = ['name', 'parent_id','lang', 'slug', 'order', 'summary', 'content', 'forward_url', 'meta_description', 'meta_keywords','image_title'];

    private $crud,$paramCrud, $image, $requests, $title, $order, $route;

    public function __construct(Request $request, Order $order)
    {
        $this->__TrConstruct($request,$order);
        $this->order = $order;


        $this->crud = new CertificateCrud();
        $this->title = "Sertifikatlar";
        $this->paramCrud = new CertificateParameterCrud();

    }


    public function index(CertificateDataTable $dataTable)
    {
        return $dataTable->render('admin.pages', ['title' => $this->title, 'route' => $this->route]);
    }

}
