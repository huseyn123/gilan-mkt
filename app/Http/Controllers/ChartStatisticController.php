<?php

namespace App\Http\Controllers;

use App\Crud\ChartStatisticCrud;
use App\DataTables\ChartStatisticDataTable;
use App\DataTables\ChartTitleDataTable;
use App\Logic\Simple;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\ChartStatistic;
use DB;

class ChartStatisticController extends Controller
{
    use Simple;

    private $crud,$requests,$title,$route,$createName,$model,$view,$create_title,$lang_tab;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);
        $this->requests = $request->except('_token', '_method');


        $this->view = 'chartStatistic';
        $this->create_title = 'Yeni';
        $this->route = $request->segment(2);
        $this->title = "chartStatistic";
        $this->createName = "Yeni Statistika";
        $this->model = 'App\Models\ChartStatistic';
        $this->crud = new ChartStatisticCrud($this->route);


    }


    public function index(ChartStatisticDataTable $dataTable)
    {
        return $dataTable->route($this->route)->render('admin.main', ['title' => $this->title,'route' =>$this->route]);
    }

    private function validation($id = null)
    {
        $validation = Validator::make(request()->all(), $this->model::$rules, $this->model::$messages);

        $response = $this->responseDataTable(0,"", "#".$this->route, '#myModal');

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
        }

        return $response;
    }



}
