<?php

namespace App\Http\Controllers\Web;

use App\Logic\Share;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Page;
use App\Models\Article;
use App\Models\Product;
use DB;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    use Share;

    public function __construct()
    {
        $this->middleware('web');
        $this->middleware('ajax')->only(['findBranch']);
        $this->loadData();
    }

    public function search()
    {
       $keyword = trim(strip_tags(request()->get('keyword')));

       $pages = Page::join('page_translations as pt', 'pages.id', '=', 'pt.page_id')
           ->where('pt.lang',  $this->lang)
           ->whereNull('pt.deleted_at')
           ->where('pt.name', 'like', "%$keyword%")
           ->whereNotIn('template_id',[2,5,23,24])
           ->whereNotIn('pages.visible', [0])
           ->orderBy('pt.order', 'asc')
           ->select('pages.visible','pages.id', 'pt.id as tid','pt.name','pt.slug','pt.content','pt.summary')
           ->paginate(10);

        if(!$keyword){
            return redirect()->route('home');
        }

        if (request()->ajax()) {
            $view = view('web.elements.search_result', ['pages' => $pages] )->render();
            return response()->json(['html'=>$view,'pages' => $pages]);
        }

        return view("web.search", ['keyword' => $keyword,'pages' => $pages]);
    }

    public function productSearch(Request $request)
    {

        $keyword = trim(strip_tags(request()->get('keyword')));
        $shop_id =(int) request()->get('shop_id');
        $branch_id =(int) request()->get('branch_id');
        $category =(int) request()->get('category');

        $page = Page::join('page_translations as pt','pt.page_id','pages.id')
            ->whereNull('pt.deleted_at')
            ->where('pt.lang',$this->lang)
            ->whereIn('pages.template_id',[21])
            ->select('pages.template_id', 'pages.visible', 'pt.*', 'pt.id as tid', 'pages.id')
            ->first();

        $products = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->where('ptr.name', 'like', "%$keyword%")
            ->where('ptr.lang',$this->lang)
            ->Filter($category,$branch_id,$shop_id)
            ->whereNull('pt.deleted_at')
            ->whereNull('ptr.deleted_at')
            ->select('ptr.name','ptr.shop_id','ptr.branch_id','pt.name as category','products.price','products.discount_price','products.id','pt.id as tid')
            ->with('media')
            ->paginate(12);

        $category = Page::join('page_translations as pt','pt.page_id','pages.id')
            ->whereNull('pt.deleted_at')
            ->where('pt.lang',$this->lang)
            ->whereIn('pages.template_id',[20])
            ->pluck('pt.name','pt.id');

        $brands = Page::join('page_translations as pt','pt.page_id','pages.id')
            ->whereNull('pt.deleted_at')
            ->where('pt.lang',$this->lang)
            ->whereIn('pages.template_id',[19])
            ->select('pt.name','pt.id')
            ->get();

        $banners = Banner::with('media')->limit(2)->orderBy('created_at','desc')->get();


        return view("web.products", ['keyword' => $keyword,'products' => $products,'page' => $page,'brands' => $brands,'category'=>$category,'banners' => $banners]);
    }

    public function findBranch(Request $request)
    {
        $brands = Page::join('page_translations as pt','pt.page_id','pages.id')
            ->whereNull('pt.deleted_at')
            ->where('pt.lang',$this->lang)
            ->whereIn('pages.template_id',[19])
            ->ShopId($request->id)
            ->select('pt.name','pt.id')
            ->get();

        $view = view('web.elements.brand_select', ['brands' => $brands] )->render();
        return response()->json(['html'=>$view]);
    }
}
