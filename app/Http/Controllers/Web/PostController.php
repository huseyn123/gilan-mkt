<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Logic\Share;
use App\Mail\ContactForm;
use App\Models\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Mail\ApplicationForm;
use Mail;
 use DB;

class PostController extends Controller
{

    use Share;

    public function __construct(Request $request)
    {
        $this->loadData();
//        $this->middleware('ajax');
    }

    public function store(Request $request)
    {
        $method = $request->input('method');

        if($method == 'contact'){
            $rules = [
                'full_name' => 'required',
                'phone' => 'required',
                'email' => 'required|email',
            ];
        }else{
            $rules = [
                'full_name' => 'required',
                'phone' => 'required',
                'email' => 'required|email',
                'subject' => 'required',
            ];
        }



        return $this->$method($request,$method,$rules);

    }


    public function contact($request,$post_page,$mainRules)
    {

        $rules = $mainRules;

        return $this->sendMail($request->all(),$rules,[], $post_page);
    }

    public function comment_suggestion($request,$post_page,$mainRules)
    {
        $rules = [
            'comment_suggestion' => 'required',
        ];

        $rules = array_merge($mainRules,$rules);

        return $this->sendMail($request->all(),$rules,[], $post_page);
    }

    public function product($request,$post_page,$mainRules)
    {
        $rules = $mainRules;

        return $this->sendMail($request->all(),$rules,[], $post_page);
    }

    public function enterprise($request,$post_page,$mainRules)
    {
        $rules = $mainRules;

        return $this->sendMail($request->all(),$rules,[], $post_page);
    }


    public function wholesale($request,$post_page,$mainRules)
    {
        $rules = $mainRules;

        return $this->sendMail($request->all(),$rules,[], $post_page);
    }

    public function product_offer_survey($request,$post_page,$mainRules)
    {
        $rules = [
            'company_name' => 'required',
            'brend_name' => 'required',
            'product_name' => 'required',
            'message' => 'required|min:10,max:500',
            'image' =>'required|mimes:png,zip,pdf|max:1000',
        ];

        $rules = array_merge($mainRules,$rules);

        return $this->sendMail($request->all(),$rules,[$request->file('image')], $post_page);
    }

    public function satisfaction_survey($request,$post_page,$mainRules)
    {
        $rules = [
            'shop' => 'required',
            's_shop_price' => 'required',
            's_product_type' => 'required',
            's_product_quality' => 'required',
            's_product_find' => 'required',
            's_product_followed' => 'required',
            's_shop_employee' => 'required',
            's_shop_till' => 'required',
        ];

        $rules = array_merge($mainRules,$rules);

        return $this->sendMail($request->all(),$rules,[$request->file('image')], $post_page);
    }

    public function feedback($request,$post_page,$mainRules)
    {

        $rules = [
            'shop' => 'required',
            'f_type' => 'required',
            'message' => 'required|min:10,max:500',
        ];

        $rules = array_merge($mainRules,$rules);

        return $this->sendMail($request->all(),$rules,[], $post_page);
    }

    public function offer_ads($request,$post_page,$mainRules)
    {

        $rules = [
            'shop' => 'required',
            'ads_type' => 'required',
            'message' => 'required|min:10,max:500',
        ];

        $rules = array_merge($mainRules,$rules);

        return $this->sendMail($request->all(),$rules,[], $post_page);
    }

    public function vacany_apply($request,$post_page,$mainRules)
    {
        $rules = [
            'cv' =>'required|mimes:png,jpeg,pdf,doc,docx|max:1000',
        ];

        $rules = array_merge($mainRules,$rules);

        return $this->sendMail($request->all(),$rules,[$request->file('cv')], $post_page,'cv@rahat.az');
    }

    private function sendMail($form,$rules,$files, $title,$send_mail = null)
    {
        $email = Config::where('key', 'contact_email')->firstOrFail();

        $subject = $this->dictionary[$title.'_subject'] ?? 'AAAAAAA';

        $validation = Validator::make($form , $rules);

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
            return $this->responseJson($response);
        }

        try {
            if($send_mail){
                Mail::to($send_mail)->send(new ApplicationForm($form,'contact',array_filter($files),$subject));
            }else{
                Mail::to($email->value)->send(new ApplicationForm($form,'contact',array_filter($files),$subject));
            }

            $response = $this->responseDataTable(0, 'Succes');
            return $this->responseJson($response);

        } catch (\Exception $e) {
            $response = $this->responseDataTable(1, $email);
            return $this->responseJson($response);
        }


    }

}
