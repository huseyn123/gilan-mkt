<?php

namespace App\Http\Controllers;

use App\Models\Config;
use App\Logic\Pages;

use App\Models\Page;
use App\Models\Article;
use App\Models\Product;
use App\Models\Enterprise;
use App\Logic\Menu;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use DB;

class SitemapController extends Controller
{
    public $menu;


    public function __construct()
    {
        $menu = new Menu;
        $this->menu = $menu;

    }

    public function index()
    {
        $date = Config::where('key', 'sitemap')->first();
        return view('admin.sitemap', ['title' => 'Sitemap', 'date' => $date]);
    }


    public function store()
    {
        $config = Config::where('key', 'sitemap')->first();

        // create new sitemap object
        $sitemap = App::make('sitemap');

        // set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
        // by default cache is disabled
        $sitemap->setCache('laravel.sitemap', 60);
        $lang = 'az';

        // check if there is cached sitemap and build new only if is not
        if (!$sitemap->isCached()) {

            //home page
            $translations = [];

            foreach(config("app.locales") as $locale){
                if($locale != $lang){
                    $translations[] = ['language' => $locale, 'url' => url('/'.$locale)];
                }
            }

            $sitemap->add(url()->to('/'), Carbon::now(), '1', 'monthly', [], null, $translations);


            //pages
            $translations = [];


           $pages = Page::join('page_translations as pt', 'pages.id', '=', 'pt.page_id')
                ->whereNull('pt.deleted_at')
                ->whereNotIn('template_id',[2,5,22,24])
                ->whereNotIn('pages.visible', [0])
                ->orderBy('pt.order', 'asc')
                ->select('pt.slug', 'pt.name', 'pt.updated_at', 'pt.lang', 'pages.id', 'pt.page_id','pages.visible','pages.template_id','pt.parent_id','pages.template_id')
                ->get();


            foreach ($pages as $page) {


                $images = [
                ];


                if($page->lang == $lang){

                    foreach($pages as $transPages){
                        if($page->id == $transPages->page_id && $transPages->lang != $lang){
                            if($page->template_id ==20){
                                foreach(config('config.kiv_types') as $key => $value) {
                                    $kiv[config('config.kiv_types_sitemap')[$key]][] = ['language' => $transPages->lang,'url' => ($this->menu->fullNestedSlug($transPages->parent_id,$transPages->slug,$transPages->lang)).'?type='.$key];
                                }
                            }elseif ($page->template_id ==23){
                                $translations[$page->id][] = ['language' => $transPages->lang,'url' => $this->menu->fullNestedSlug($transPages->parent_id,$transPages->slug,$transPages->lang)];
                            }else{
                                   $translations[$page->id][] = ['language' => $transPages->lang,'url' => $this->menu->fullNestedSlug($transPages->parent_id,$transPages->slug,$transPages->lang)];
                            }
                        }
                    }

                    if(in_array($page->template_id,[10])){
                        $priority = 1;
                    }else{
                        $priority = 0.5;
                    }

                    if($page->template_id ==20){
                        foreach(config('config.kiv_types') as $key => $value){
                            $sitemap->add(url()->to(($this->menu->fullNestedSlug($page->parent_id,$page->slug)).'?type='.$key), $page->updated_at, $priority, 'monthly',$images, $page->name,$kiv[config('config.kiv_types_sitemap')[$key]] ?? []);
                        }
                    }elseif ($page->template_id ==23){
                        $sitemap->add(url()->to(($this->menu->fullNestedSlug($page->parent_id,null)).'?category='.$page->slug), $page->updated_at, $priority, 'monthly',$images, $page->name,$translations[$page->id] ?? []);
                    }
                    else{
                        $sitemap->add(url()->to($this->menu->fullNestedSlug($page->parent_id,$page->slug)), $page->updated_at, $priority, 'monthly',$images, $page->name,$translations[$page->id] ?? []);
                    }
                }
            }
//            //articles
            $translations = [];


            $articles = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
                ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
                ->where('articles.status', 1)
                ->whereNull('pt.deleted_at')
                ->whereNull('at.deleted_at')
                ->orderBy('articles.published_at', 'desc')
                ->orderBy('articles.id', 'desc')
                ->with('media')
                ->select('articles.id','articles.id','at.article_id','at.name',
                    'at.slug','at.lang', 'at.summary', 'articles.published_at',
                    'pt.name as page_name', 'pt.slug as page_slug')
                ->get();

            foreach ($articles as $article){
                if ($article->getFirstMedia()){
                    $url = asset($article->getFirstMedia()->getUrl('thumb'));
                }else{
                    $url = null;
                }

                $images = [
                    ['url' =>$url, 'title' => $article->name, 'caption' => $article->summary],
                ];

                if($article->lang == $lang){

                    foreach($articles as $transArticles){
                        if($article->id == $transArticles->article_id && $transArticles->lang != $lang){
                            $translations[$article->id][] = ['language' => $transArticles->lang, 'url' => url()->to($transArticles->lang.'/'.$transArticles->page_slug.'/'.$transArticles->slug)];
                        }
                    }
                    $sitemap->add(url()->to($article->page_slug.'/'.$article->slug), $article->updated_at, '1', 'monthly', $images,$article->name, $translations[$article->id] ?? []);
                }
            }


           //products
            $translations = [];


            $products = Product::join('product_translations as ptr','ptr.product_id','products.id')
                ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
                ->join('pages', 'pages.id', '=', 'pt.page_id')
                ->whereNull('ptr.deleted_at')
                ->whereNull('pt.deleted_at')
                ->whereIn('pages.template_id',[21])
                ->select('ptr.*','pt.slug as page_slug','products.id')
                ->with('media')
                ->get();


            foreach ($products as $product){
                if ($product->getFirstMedia()){
                    $url = asset($product->getFirstMedia()->getUrl('thumb'));
                }else{
                    $url = null;
                }

                $images = [
                    ['url' =>$url, 'title' => $product->name, 'caption' => $product->summary],
                ];

                if($product->lang == $lang){

                    foreach($products as $transProducts){
                        if($product->id == $transProducts->product_id && $transProducts->lang != $lang){
                            $translations[$product->id][] = ['language' => $transProducts->lang, 'url' => url()->to($transProducts->lang.'/'.$transProducts->page_slug.'/'.$transProducts->slug)];
                        }
                    }
                    $sitemap->add(url()->to($product->page_slug.'/'.$product->slug), $product->updated_at, '1', 'monthly', $images,$product->name, $translations[$product->id] ?? []);
                }
            }


            //enterprice
            $translations = [];


            $enterprices = Enterprise::join('enterprise_translations as et','et.enterprise_id','enterprises.id')
                    ->join('page_translations as pt', 'pt.id', '=', 'et.page_id')
                    ->join('pages', 'pages.id', '=', 'pt.page_id')
                    ->whereNull('et.deleted_at')
                    ->whereNull('pt.deleted_at')
                    ->whereIn('pages.template_id',[12])
                    ->select('et.*','pt.slug as page_slug','enterprises.id','enterprises.linear')
                    ->get();


            foreach ($enterprices as $enterprice){
                if ($enterprice->getFirstMedia()){
                    $url = asset($enterprice->getFirstMedia()->getUrl('thumb'));
                }else{
                    $url = null;
                }

                $images = [
                    ['url' =>$url, 'title' => $enterprice->name, 'caption' => $enterprice->summary],
                ];

                if($enterprice->lang == $lang){

                    foreach($enterprices as $transEnterprices){
                        if($enterprice->id == $transEnterprices->enterprise_id && $transEnterprices->lang != $lang){
                            $translations[$enterprice->id][] = ['language' => $transEnterprices->lang, 'url' => url()->to($transEnterprices->lang.'/'.$transEnterprices->page_slug.'/'.$transEnterprices->slug)];
                        }
                    }
                    $sitemap->add(url()->to($enterprice->page_slug.'/'.$enterprice->slug), $enterprice->updated_at, '1', 'monthly', $images,$enterprice->name, $translations[$enterprice->id] ?? []);
                }
            }




        }




        $sitemap->store('xml', 'storage/sitemap');

        $config->value = Carbon::now();
        $config->save();

        return redirect()->back();
    }
}
