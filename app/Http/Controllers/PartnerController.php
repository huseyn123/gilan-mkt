<?php

namespace App\Http\Controllers;

use App\Logic\Order;
use Illuminate\Http\Request;
use App\Models\Partner;
use App\DataTables\PartnerDataTable;
use App\Crud\PartnerCrud;
use Illuminate\Support\Facades\Validator;
use DB;

class PartnerController extends Controller
{
    private $crud, $image, $requests, $order, $title;

    public function __construct(Request $request,PartnerCrud $crud, Order $order)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->title = "Tərəfdaşlar";
        $this->order = $order;
        $this->requests = $request->except('_token', '_method','image');

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('partners', false);
        }
    }


    public function index(PartnerDataTable $dataTable)
    {
        return $dataTable->render('admin.partners', ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');
        return view('admin.dt.create', ['title' => 'Yeni tərəfdaş', 'fields' => $fields, 'route' => 'partners.store']);
    }



    public function store(Request $request)
    {
        $response = $this->validation();

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['order'] = Partner::max('order')+1;

        $partner = Partner::create($this->requests);

        $partner
            ->addMedia($request->image)
            ->withResponsiveImages()
            ->toMediaCollection();

        return $this->responseJson($response);
    }


    public function edit($id)
    {
        $data = Partner::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.dt.edit', ['title' => 'Düzəliş et', 'fields' => $fields, 'data' => $data, 'route' => ['partners.update', $id]]);
    }


    public function update(Request $request, $id)
    {
        $data = Partner::findOrFail($id);

        $response = $this->validation(null);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        if($request->hasFile('image')){

            $data->clearMediaCollection();  //delete old file
            $data->addMedia($request->image)
                  ->withResponsiveImages()
                  ->toMediaCollection();
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseJson($response);
    }


    public function order()
    {
        return $this->order->get('partners', $this->title, 'name', false, 1);
    }


    public function postOrder(Request $request)
    {
        return $this->order->post($request, 'partners', false);
    }


    public function destroy($id)
    {
        $data = Partner::findOrFail($id);
        $data->forceDelete();

        $response = $this->responseDataTable(0,"", "#partners", "#modal-confirm");
        return $this->responseJson($response);
    }


    protected function resize()
    {
        $resizeImage = ['resize' => ['fit' => false, 'size' => [150, null]], 'thumb' => null ];

        return $resizeImage;
    }


    private function validation($required = 'required|')
    {
        $validation = Validator::make(request()->all(), Partner::rules($required), Partner::$messages);

        $response = $this->responseDataTable(0,"", "#partners", '#myModal');

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
        }

        return $response;
    }
}
